# gbd

A work in progress GameBoy Debugger written in rust powered by [SameBoy](https://github.com/LIJI32/SameBoy/).

## Features

- CPU Usage / per scanline screen overlays
- VRAM, Background and Sprite Viewers
- Palette and Display Register information
- Support halting on uninitialized or inaccessible memory reads / writes
- Screen overlays for vram and display register writes
- Fully configurable Watchpoints for halting and logging
- Frame and line advance as well as stepping
- Memory view with support for displaying changes between frames and editing individual bytes
- Code view with full disassembly and statistic for calls
- Supports the [Debug Adapter Protocol](https://microsoft.github.io/debug-adapter-protocol/) so you can directly step and debug from within your editor (requires a source map)

## Emulation Backend

All emulation is performed via a rust wrapper of the incredible [SameBoy](https://github.com/LIJI32/SameBoy/) Emulator core.

Minor patches have been applied to expose more of the internals to the debugger.

## Extended Symbol File Format

gbd support/requires an extended symbol file format with additional information on labels inside the ROM:

### Format

Based on the format used by BGB, but with additional information on the data type / size in bytes:

```
bank:address symbol code_size:data_size:ram_size
```

### Example

```
00:0001 core_loop 29:0:0
01:491e instrument_sfx_confirm 0:3:0
00:ffa9 lineError 0:0:1
```

## Source Map File Format

For the Debug Adapter Protocol implementation a custom source map file format is required.

### Format

1. Memory addresses are mapped onto relative source file paths along with a line and column number.
2. Memory ranges are also mapped to the symbols the memory addresses and symbols they reference.

```
a bank:address "path",line,column
s bank:address-bank:address address,adress.. symbols,symbol,..
```

### Example

```
a 00:0000 "lib/core.gbc",39,5
a 00:0001 "lib/core.gbc",46,5
a 00:0002 "lib/core.gbc",47,5
a 00:0003 "lib/core.gbc",50,5
a 00:0005 "lib/core.gbc",51,5
a 00:0006 "lib/core.gbc",53,5
a 00:0008 "lib/core.gbc",54,5

s 00:0001-00:001E FF44,FF55 core_input,core_loop,game_update
s 00:001E-00:0036 FF44 
s 00:0037-00:003E  core_dma_transfer
```

## Showcase (outdated)

![Showcase Video](https://gitlab.com/BonsaiDen/gbd-rs/raw/master/media/demo.webm)

## Usage

```
gbd 0.1.0
GameBoy Debugger

USAGE:
    gbd [OPTIONS] [ROM_FILE]

ARGS:
    <ROM_FILE>    ROM file to emulate

OPTIONS:
    -d, --debug                      Enable debugger UI
        --dap                        Enable debug adapter protocol
    -h, --halted                     Start with debugger halted
        --help                       Print help information
    -m, --model <MODEL>              GameBoy model to emulate [possible values: agb, cgb, dmg, mgb,
                                     sgb, sgb2]
        --skip-boot-rom              Start with debugger halted
        --source-dir <SOURCE_DIR>    Source base directory for source map file resolution
    -V, --version                    Print version information

```

## License

Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
at your option.

