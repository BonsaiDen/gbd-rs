use std::env;
use std::path::PathBuf;

fn main() {
    // Tell cargo to tell rustc to link the system bzip2
    // shared library.
    // println!("cargo:rustc-link-lib=bz2");

    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=wrapper.h");
    println!("cargo:rerun-if-changed=deps/Core/gb.h");

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .clang_arg("-DGB_INTERNAL")
        .clang_arg("-DBINDGEN")
        .header("wrapper.h")
        // Tell cargo to invalidate the built crate whenever any of the
        // included header files changed.
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");

    cc::Build::new()
        .flag("-std=gnu11")
        .flag("-w")
        .flag("-D_GNU_SOURCE")
        .flag("-DGB_VERSION=\"0.14.7\"")
        .flag("-DGB_INTERNAL")
        .flag("-DGB_DISABLE_CHEATS")
        .flag("-DGB_DISABLE_REWIND")
        .flag("-DGB_DISABLE_DEBUGGER")
        .flag("-DGB_DISABLE_TIMEKEEPING")
        .flag("-D_USE_MATH_DEFINES")
        .flag("-O3")

        .file("deps/Core/apu.c")
        .file("deps/Core/camera.c")
        .file("deps/Core/cheats.c")
        .file("deps/Core/display.c")
        .file("deps/Core/gb.c")
        .file("deps/Core/joypad.c")
        .file("deps/Core/mbc.c")
        .file("deps/Core/memory.c")
        .file("deps/Core/printer.c")
        .file("deps/Core/random.c")
        .file("deps/Core/rewind.c")
        .file("deps/Core/rumble.c")
        .file("deps/Core/sgb.c")
        .file("deps/Core/sm83_cpu.c")
        .file("deps/Core/timing.c")
        .file("deps/Core/workboy.c")
        .compile("sameboy");
}
