# sameboy-rs

A wrapper around the [Sameboy](https://github.com/LIJI32/SameBoy/) emulation 
core that abstracts ways the unsafe parts of the underlying sys crate and exposes 
the callback hooks used by the debugger / analyzer.
