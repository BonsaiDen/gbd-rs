// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::pin::Pin;


// Bindgen Dependencies -------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs_sys::*;


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod callback;
mod core;
mod model;
mod key;
mod registers;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use self::callback::CallbackHandle;


// Exports --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub use self::core::*;
pub use self::model::Model;
pub use self::key::Key;
pub use self::callback::{CallbackCore, ReadCallbackCore, VBlankType, WriteCallbackCore};
pub use self::registers::{ByteRegisters, WordRegisters};


// Runnable GameBoy Core ------------------------------------------------------
// ----------------------------------------------------------------------------
/// A runnable GameBoy Core
pub struct RunnableCore {
    gb: GB_gameboy_t,
    rom_size: usize,
    boot_rom_size: usize,
    palette: Option<GB_palette_t>,
    callbacks: CallbackHandle
}

impl CallbackCore for RunnableCore {
    fn gb(&mut self) -> &mut GB_gameboy_t {
        &mut self.gb
    }

    fn handle(&mut self) -> &mut CallbackHandle {
        &mut self.callbacks
    }
}

impl ReadableCore for RunnableCore {
    fn gb(&self) -> &GB_gameboy_t {
        &self.gb
    }
}

impl WritableCore for RunnableCore {
    fn gb(&mut self) -> &mut GB_gameboy_t {
        &mut self.gb
    }
}

impl RunnableCore {
    pub fn boot_rom_size(&self) -> usize {
        self.boot_rom_size
    }

    /// Create a new core for the given model.
    pub fn from_model(model: Model) -> Pin<Box<Self>> {
        let core = Self {
            gb: unsafe {
                let mut gb: GB_gameboy_t = std::mem::zeroed();
                GB_init(&mut gb, model.into());
                gb
            },
            boot_rom_size: 0,
            rom_size: 0,
            palette: None,
            callbacks: CallbackHandle::default()
        };
        let mut core = Box::pin(core);
        {
            let p = &mut *core.as_mut();
            let p = p as *mut _ as *mut libc::c_void;
            unsafe { GB_set_user_data(&mut core.gb, p); }
        }
        core
    }

    /// Sets the palette used for DMG
    pub fn set_dmg_palette(&mut self, palette: [[u8; 3]; 5]) {
        self.palette = Some(GB_palette_t {
            colors: [
                GB_palette_t_GB_color_s { r: palette[0][0], g: palette[0][1], b: palette[0][2]},
                GB_palette_t_GB_color_s { r: palette[1][0], g: palette[1][1], b: palette[1][2]},
                GB_palette_t_GB_color_s { r: palette[2][0], g: palette[2][1], b: palette[2][2]},
                GB_palette_t_GB_color_s { r: palette[3][0], g: palette[3][1], b: palette[3][2]},
                GB_palette_t_GB_color_s { r: palette[4][0], g: palette[4][1], b: palette[4][2]},
            ]
        });
        unsafe { GB_set_palette(&mut self.gb, self.palette.as_ref().unwrap()) };
    }

    /// Sets the boot rom contents from the given slice.
    ///
    /// If more than 256 bytes are passed the additional ones will be ignored.
    pub fn set_boot_rom_from_slice(&mut self, rom: &[u8]) {
        self.boot_rom_size = rom.len();
        unsafe { GB_load_boot_rom_from_buffer(&mut self.gb, rom.as_ptr(), rom.len() as u64) }
    }

    /// Sets boot rom contents from the given slice.
    pub fn set_rom_from_slice(&mut self, rom: &[u8]) {
        self.rom_size = rom.len();
        unsafe { GB_load_rom_from_buffer(&mut self.gb, rom.as_ptr(), rom.len() as u64) }
    }

    /// Reads from the specified address.
    ///
    /// This might trigger side-effects based on the address being read from.
    pub fn read_memory(&mut self, addr: u16) -> u8 {
        unsafe { GB_read_memory(&mut self.gb, addr) }
    }

    /// Writes to the specified address.
    ///
    /// This might trigger side-effects based on the address being written to.
    pub fn write_memory(&mut self, addr: u16, value: u8) {
        match addr {
            0x0000..=0x7FFF => {
                self.write_rom_memory(addr, value);
            },
            _ => {
                unsafe { GB_write_memory(&mut self.gb, addr, value) }
            }
        }
    }

    pub fn write_rom_memory(&mut self, addr: u16, value: u8) {
        unsafe {
            let rom = std::slice::from_raw_parts_mut(self.unsaved_state().rom, self.rom_size);
            let bank = self.bank_from_address(addr) as usize;
            let addr = addr as usize % 0x4000;
            let addr = addr+ bank * 0x4000;
            if addr < self.rom_size {
                rom[addr] = value;
            }
        }
    }

    /// Returns the number of executed cycles.
    pub fn run(&mut self, pixels_output: Option<&mut [u32]>) -> u32 {
        if let Some(buffer) = pixels_output {
            unsafe { GB_set_pixels_output(&mut self.gb, buffer.as_ptr() as *mut u32) }

        } else {
            unsafe { GB_set_pixels_output(&mut self.gb, libc::PT_NULL as *mut u32) }
        }
        unsafe { GB_run(&mut self.gb) }
    }

    /// Returns the number of executed cycles.
    pub fn run_frame(&mut self, pixels_output: &mut [u32]) -> u64 {
        unsafe { GB_set_pixels_output(&mut self.gb, pixels_output.as_ptr() as *mut u32) }
        unsafe { GB_run_frame(&mut self.gb) }
    }

    /// Runs until the end of the bootom
    pub fn skip_bootrom(&mut self) {
        unsafe { GB_set_rendering_disabled(&mut self.gb, true) }
        while !self.is_boot_rom_finished() {
            unsafe { GB_run(&mut self.gb) };
        }
        unsafe { GB_set_rendering_disabled(&mut self.gb, false) }
    }

    /// Rewinds by one frame from the rewind buffer.
    pub fn rewind_frame(&mut self) -> bool {
        unsafe { GB_rewind_pop(&mut self.gb) }
    }

    pub fn set_rewind_buffer_seconds(&mut self, seconds: f32) {
        unsafe { GB_set_rewind_length(&mut self.gb, seconds as f64) }
    }

    /// Resets the core with the given model.
    pub fn reset_with_model(&mut self, model: Model) {
        unsafe {
            GB_switch_model_and_reset(&mut self.gb, model.into())
        };
    }

    /// Resets the core.
    pub fn reset(&mut self) {
        unsafe { GB_reset(&mut self.gb) };
    }
}

impl Drop for RunnableCore {
    fn drop(&mut self) {
        unsafe { GB_free(&mut self.gb) };
    }
}

