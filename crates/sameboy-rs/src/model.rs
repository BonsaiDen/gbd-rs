#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]


// Bindgen Dependencies -------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs_sys::*;


// GameBoy Models -------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Model {
    DMG,
    MGB,
    CGB,
    AGB,
    SGB,
    SGB2
}


impl From<Model> for usize {
    fn from(m: Model) -> usize {
        match m {
            Model::DMG => 0,
            Model::MGB => 1,
            Model::CGB => 2,
            Model::AGB => 3,
            Model::SGB => 4,
            Model::SGB2 => 5
        }
    }
}

impl From<usize> for Model {
    fn from(id: usize) -> Self {
        match id {
            0 => Self::DMG,
            1 => Self::MGB,
            2 => Self::CGB,
            3 => Self::AGB,
            4 => Self::SGB,
            5 => Self::SGB2,
            _ => Self::DMG
        }
    }
}

impl std::str::FromStr for Model {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "DMG" | "dmg" => Ok(Self::DMG),
            "MGB" | "mgb" => Ok(Self::MGB),
            "CGB" | "cgb"=> Ok(Self::CGB),
            "AGB" | "agb"=> Ok(Self::AGB),
            "SGB" | "sgb"=> Ok(Self::SGB),
            "SGB2" | "sgb2"=> Ok(Self::SGB2),
            m => Err(format!("\"{}\" is not on of: DMG, MGB, CGB, AGB, SGB, SGB2", m))
        }
    }
}

impl From<GB_model_t> for Model {
    fn from(m: GB_model_t) -> Self {
        match m {
            GB_model_t_GB_MODEL_SGB |
            GB_model_t_GB_MODEL_SGB_PAL |
            GB_model_t_GB_MODEL_SGB_NTSC_NO_SFC |
            GB_model_t_GB_MODEL_SGB_PAL_NO_SFC => Self::SGB,
            GB_model_t_GB_MODEL_MGB => Self::MGB,
            GB_model_t_GB_MODEL_SGB2 |
            GB_model_t_GB_MODEL_SGB2_NO_SFC => Self::SGB2,
            GB_model_t_GB_MODEL_CGB_0 |
            GB_model_t_GB_MODEL_CGB_B |
            GB_model_t_GB_MODEL_CGB_C |
            GB_model_t_GB_MODEL_CGB_D |
            GB_model_t_GB_MODEL_CGB_E => Self::CGB,
            GB_model_t_GB_MODEL_AGB => Self::AGB,
            _ => Self::DMG
        }
    }
}

impl From<Model> for GB_model_t {
    fn from(m: Model) -> GB_model_t {
        match m {
            Model::DMG => GB_model_t_GB_MODEL_DMG_B,
            Model::MGB => GB_model_t_GB_MODEL_MGB,
            Model::CGB => GB_model_t_GB_MODEL_CGB_E,
            Model::AGB => GB_model_t_GB_MODEL_AGB,
            Model::SGB => GB_model_t_GB_MODEL_SGB,
            Model::SGB2 => GB_model_t_GB_MODEL_SGB2,
        }
    }
}

