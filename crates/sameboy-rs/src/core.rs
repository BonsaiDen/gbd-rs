// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::ops::Range;
use std::{fmt, hash::Hash};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs_sys::*;
use serde::{Serialize, Deserialize};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{ByteRegisters, WordRegisters, Model, Key};


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Copy, Clone)]
pub struct BankedAddressRange(BankedAddress, u16);

impl BankedAddressRange {
    pub fn start(&self) -> BankedAddress {
        self.0
    }

    pub fn end(&self) -> Option<BankedAddress> {
        if self.1 > 1 {
            Some(self.0.add(self.1).sub(1))

        } else {
            None
        }
    }

    pub fn size(&self) -> u16 {
        self.1
    }

    pub fn contains(&self, addr: BankedAddress) -> bool {
        addr.within_range(self.0, self.0.add(self.1).sub(1))
    }
}

impl fmt::Display for BankedAddressRange {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.1 > 1 {
            write!(f, "{}[{}]", self.0, self.1)

        } else {
            write!(f, "{}", self.0)
        }
    }
}

impl From<BankedAddress> for BankedAddressRange {
    fn from(addr: BankedAddress) -> Self {
        Self(addr, 1)
    }
}

impl std::ops::Deref for BankedAddressRange {
    type Target = BankedAddress;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}


#[derive(Default, Debug, Serialize, Deserialize, Copy, Clone, Eq, PartialOrd, Ord)]
pub struct BankedAddress(u16, u16);

impl PartialEq for BankedAddress {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0 && self.1 == other.1
    }
}

impl Hash for BankedAddress {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        state.write_u32(((self.1 as u32) << 16) | self.0 as u32);
    }
}

impl BankedAddress {
    pub fn zero() -> Self {
        Self(0, 0)
    }

    pub fn offset(&self) -> usize {
        self.0 as usize
    }

    pub fn address(&self) -> u16 {
        self.0
    }

    pub fn bank(&self) -> u16 {
        self.1
    }

    pub fn with_size(&self, size: u16) -> BankedAddressRange {
        BankedAddressRange(*self, size)
    }

    pub fn within_range(&self, start: BankedAddress, end: BankedAddress) -> bool {
        self.0 >= start.0 && self.0 <= end.0 && self.1 >= start.1 && self.1 <= end.1
    }

    pub fn new(addr: u16, bank: u16) -> Self {
        Self(addr, bank)
    }

    pub fn sub(&self, i: u16) -> Self {
        Self(self.0.saturating_sub(i), self.1)
    }

    pub fn add(&self, i: u16) -> Self {
        Self(self.0.saturating_add(i), self.1)
    }

    pub fn to_label_string(&self) -> String {
        format!("{:0>4X}:{:0>2X}", self.0, self.1)
    }

    pub fn to_symbol_string(&self) -> String {
        format!("{:0>2X}:{:0>4X}", self.1, self.0)
    }
}

impl fmt::Display for BankedAddress {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.1 > 0 {
            write!(f, "${:0>4X}:{:0>2X}", self.0, self.1)

        } else {
            write!(f, "${:0>4X}", self.0)
        }
    }
}


// Readable GameBoy Core ------------------------------------------------------
// ----------------------------------------------------------------------------
type CoreState = GB_gameboy_s__bindgen_ty_2__bindgen_ty_1;
// type DMAState = GB_gameboy_s__bindgen_ty_3__bindgen_ty_1;
type MBCState = GB_gameboy_s__bindgen_ty_4__bindgen_ty_1;
// type HRAMState = GB_gameboy_s__bindgen_ty_5__bindgen_ty_1;
// type TimingState = GB_gameboy_s__bindgen_ty_6__bindgen_ty_1;
type APUState = GB_gameboy_s__bindgen_ty_7__bindgen_ty_1;
// type RTCState = GB_gameboy_s__bindgen_ty_8__bindgen_ty_1;
type VRAMState = GB_gameboy_s__bindgen_ty_9__bindgen_ty_1;
type UnsavedState = GB_gameboy_s__bindgen_ty_11__bindgen_ty_1;

type Reg16 = GB_gameboy_s__bindgen_ty_2__bindgen_ty_1__bindgen_ty_1__bindgen_ty_1;
type Reg8 = GB_gameboy_s__bindgen_ty_2__bindgen_ty_1__bindgen_ty_1__bindgen_ty_2;

pub trait ReadableCore {
    #[doc(hidden)]
    fn gb(&self) -> &GB_gameboy_t;

    fn core_state(&self) -> &CoreState {
        unsafe { &self.gb().__bindgen_anon_2.__bindgen_anon_1 }
    }

    fn vram_state(&self) -> &VRAMState {
        unsafe { &self.gb().__bindgen_anon_9.__bindgen_anon_1 }
    }

    fn mbc_state(&self) -> &MBCState {
        unsafe { &self.gb().__bindgen_anon_4.__bindgen_anon_1 }
    }

    fn unsaved_state(&self) -> &UnsavedState {
        unsafe { &self.gb().__bindgen_anon_11.__bindgen_anon_1 }
    }

    fn r16(&self) -> &Reg16 {
        unsafe { &self.core_state().__bindgen_anon_1.__bindgen_anon_1 }
    }

    fn r8(&self) -> &Reg8 {
        unsafe { &self.core_state().__bindgen_anon_1.__bindgen_anon_2 }
    }

    fn mbc_name(&self) -> Option<&str> {
        match unsafe { (*self.unsaved_state().cartridge_type).mbc_type } {
            1 => Some("MBC1"),
            2 => Some("MBC2"),
            3 => Some("MBC3"),
            4 => Some("MBC5"),
            5 => Some("MBC7"),
            6 => Some("MMM01"),
            7 => Some("HUC1"),
            8 => Some("HUC2"),
            9 => Some("TPP1"),
            _ => None
        }
    }

    fn previous_pc(&self) -> BankedAddress {
        let addr = self.r16().pc.saturating_sub(1);
        let bank = self.bank_from_address(addr);
        BankedAddress(addr, bank)
    }

    fn pc(&self) -> BankedAddress {
        let addr = self.r16().pc;
        let bank = self.bank_from_address(addr);
        BankedAddress(addr, bank)
    }

    fn sp(&self) -> u16 {
        self.r16().sp
    }

    /// Returns the current value of all registers as bytes.
    fn byte_registers(&self) -> ByteRegisters {
        let r8 = self.r8();
        ByteRegisters {
            a: r8.a,
            f: r8.f,
            b: r8.b,
            c: r8.c,
            d: r8.d,
            e: r8.e,
            h: r8.h,
            l: r8.l,
        }
    }

    /// Returns the current value of all registers as words.
    fn word_registers(&self) -> WordRegisters {
        let r16 = self.r16();
        WordRegisters {
            af: r16.af,
            bc: r16.bc,
            de: r16.de,
            hl: r16.hl,
            pc: r16.pc,
            sp: r16.sp
        }
    }

    /// Returns the Interrupt Master Enable
    fn ime(&self) -> bool {
        self.core_state().ime != 0
    }

    fn ie(&self) -> u8 {
        self.core_state().interrupt_enable
    }

    /// Reads from the specified address without causing side effects.
    fn read_memory_safe(&self, addr: u16) -> u8 {
        let gb = self.gb() as *const GB_gameboy_s;
        unsafe { GB_safe_read_memory(gb as *mut GB_gameboy_s, addr)}
    }

    /// Reads from the specified IO register offset without causing side effects.
    fn read_io_register(&self, addr: u8) -> u8 {
        let gb = self.gb() as *const GB_gameboy_s;
        unsafe { GB_safe_read_memory(gb as *mut GB_gameboy_s, 0xFF00 + ((addr & 0x7F) as u16))}
    }

    fn lcdc(&self) -> u8 {
        self.read_io_register(0x40)
    }

    fn is_sprite_size_16(&self) -> bool {
        (self.lcdc() & 4) != 0
    }

    fn is_window_enabled(&self) -> bool {
        (self.lcdc() & 0x20) != 0
    }

    fn bg_map_range(&self) -> Range<u16> {
        let lcdc = self.lcdc();
        if (lcdc & 8) != 0 { 0x9C00..0xA000 } else { 0x9800..0x9C00 }
    }

    fn bg_map_base(&self) -> u16 {
        let lcdc = self.lcdc();
        if (lcdc & 8) != 0 { 0x9C00 } else { 0x9800 }
    }

    fn vram_tile_range(&self) -> Range<u16> {
        0x8000..0x9800
    }

    fn oam_range(&self) -> Range<u16> {
        0xFE00..0xFEA0
    }

    fn oam_base(&self) -> u16 {
        0xFE00
    }

    fn vram(&self) -> &[u8] {
        unsafe { std::slice::from_raw_parts_mut(self.unsaved_state().vram, self.vram_state().vram_size as usize) }
    }

    fn rom(&self) -> &[u8] {
        unsafe { std::slice::from_raw_parts_mut(self.unsaved_state().rom, self.unsaved_state().rom_size as usize) }
    }

    fn gbc_vram_bank(&self) -> u16 {
        if self.vram_state().cgb_vram_bank {
            1

        } else {
            0
        }
    }

    fn mbc_rom_bank(&self) -> u16 {
        self.mbc_state().mbc_rom_bank
    }

    fn mbc_ram_bank(&self) -> u8 {
        self.mbc_state().mbc_ram_bank
    }

    fn mbc_ram_enable(&self) -> bool {
        self.mbc_state().mbc_ram_enable
    }

    fn mbc_ram_size(&self) -> u32 {
        self.mbc_state().mbc_ram_size
    }

    fn gbc_ram_bank(&self) -> u16 {
        self.core_state().cgb_ram_bank as u16
    }

    /// Returns a slice of the raw OAM contents.
    fn oam(&self) -> &[u8] {
        &self.vram_state().oam
    }

    /// Returns the current model used by the core.
    fn model(&self) -> Model {
        let gb = self.gb() as *const GB_gameboy_s;
        Model::from(unsafe { GB_get_model(gb as *mut GB_gameboy_s) })
    }

    fn is_cgb(&self) -> bool {
        let gb = self.gb() as *const GB_gameboy_s;
        unsafe { GB_is_cgb(gb as *mut GB_gameboy_s) }
    }

    fn is_cgb_in_cgb_mode(&self) -> bool {
        let gb = self.gb() as *const GB_gameboy_s;
        unsafe { GB_is_cgb_in_cgb_mode(gb as *mut GB_gameboy_s) }
    }

    fn is_sgb(&self) -> bool {
        let gb = self.gb() as *const GB_gameboy_s;
        unsafe { GB_is_sgb(gb as *mut GB_gameboy_s) }
    }

    fn is_hle_sgb(&self) -> bool {
        let gb = self.gb() as *const GB_gameboy_s;
        unsafe { GB_is_hle_sgb(gb as *mut GB_gameboy_s) }
    }

    /// Returns current LCD line
    fn current_line(&self) -> u8 {
        self.vram_state().current_line
    }

    /// Returns current LCD pixel
    fn current_pixel(&self) -> u8 {
        self.vram_state().lcd_x
    }

    fn is_vram_write_blocked(&self) -> bool {
        self.vram_state().vram_write_blocked
    }

    fn is_dma_in_progress(&self) -> bool {
        self.unsaved_state().in_dma_read
    }

    fn is_hdma_in_progress(&self) -> bool {
        self.unsaved_state().hdma_in_progress
    }

    fn is_debug_stopped(&self) -> bool {
        self.unsaved_state().debug_stopped
    }

    fn is_halted(&self) -> bool {
        self.core_state().halted
    }

    fn is_boot_rom_finished(&self) -> bool {
        self.core_state().boot_rom_finished
    }

    fn vblank_just_occured(&self) -> bool {
        self.unsaved_state().vblank_just_occured
    }

    fn banked_address(&self, addr: u16) -> BankedAddress {
        let bank = self.bank_from_address(addr);
        BankedAddress(addr, bank)
    }

    fn bank_from_address(&self, addr: u16) -> u16 {
        let is_gbc = self.is_cgb();
        match addr {
            0x0000..=0x3FFF => self.mbc_state().mbc_rom0_bank,
            0x4000..=0x7FFF => self.mbc_state().mbc_rom_bank,
            0x8000..=0x9FFF if is_gbc => if self.vram_state().cgb_vram_bank { 1 } else { 0 },
            0xA000..=0xBFFF => self.mbc_ram_bank() as u16,
            0xC000..=0xCFFF => 0,
            0xD000..=0xDFFF if is_gbc => self.core_state().cgb_ram_bank as u16,
            _ => 0
        }
    }

    fn cgb_bg_palettes(&self) -> &[u8] {
        &self.vram_state().background_palettes_data
    }

    fn cgb_obj_palettes(&self) -> &[u8] {
        &self.vram_state().object_palettes_data
    }
}


// Writable GameBoy Core ------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait WritableCore {
    #[doc(hidden)]
    fn gb(&mut self) -> &mut GB_gameboy_t;

    fn core_state_mut(&mut self) -> &mut CoreState {
        unsafe { &mut self.gb().__bindgen_anon_2.__bindgen_anon_1 }
    }

    fn apu_state_mut(&mut self) -> &mut APUState {
        unsafe { &mut self.gb().__bindgen_anon_7.__bindgen_anon_1 }
    }

    fn unsaved_state_mut(&mut self) -> &mut UnsavedState {
        unsafe { &mut self.gb().__bindgen_anon_11.__bindgen_anon_1 }
    }

    fn r16_mut(&mut self) -> &mut Reg16 {
        unsafe { &mut self.core_state_mut().__bindgen_anon_1.__bindgen_anon_1 }
    }

    fn r8_mut(&mut self) -> &mut Reg8 {
        unsafe { &mut self.core_state_mut().__bindgen_anon_1.__bindgen_anon_2 }
    }

    fn set_pc(&mut self, pc: u16) {
        self.r16_mut().pc = pc;
    }

    fn set_byte_registers(&mut self, r: ByteRegisters) {
        let r8 = self.r8_mut();
        r8.a = r.a;
        r8.f = r.f;
        r8.b = r.b;
        r8.c = r.c;
        r8.d = r.d;
        r8.e = r.e;
        r8.h = r.h;
        r8.l = r.l;
    }

    fn set_word_registers(&mut self, r: WordRegisters) {
        let r16 = self.r16_mut();
        r16.af = r.af;
        r16.bc = r.bc;
        r16.de = r.de;
        r16.hl = r.hl;
        r16.pc = r.pc;
        r16.sp = r.sp;
    }

    fn set_audio_channel_enable(&mut self, index: usize, enable: bool) {
        if index < 4 {
            self.apu_state_mut().apu.channel_disabled[index] = !enable;
        }
    }

    fn set_display_background_enable(&mut self, enable: bool) {
        unsafe { GB_set_background_rendering_disabled(self.gb(), !enable)}
    }

    fn set_display_oam_enable(&mut self, enable: bool) {
        unsafe { GB_set_object_rendering_disabled(self.gb(), !enable)}
    }

    fn set_debug_stopped(&mut self, stopped: bool) {
        self.unsaved_state_mut().debug_stopped = stopped;
    }

    fn set_key_state(&mut self, key: Key, pressed: bool) {
        unsafe { GB_set_key_state(self.gb(), key.into(), pressed) }
    }

    fn set_emulate_joypad_bouncing(&mut self, emulate: bool) {
        unsafe { GB_set_emulate_joypad_bouncing(self.gb(), emulate) }
    }

    /// Sets the audio sample rate from the given slice.
    fn set_sample_rate(&mut self, rate: u32) {
        unsafe { GB_set_interference_volume(self.gb(), 0.0) }
        unsafe { GB_set_highpass_filter_mode(self.gb(), GB_highpass_mode_t_GB_HIGHPASS_ACCURATE) }
        unsafe { GB_set_sample_rate(self.gb(), rate) }
    }
}

