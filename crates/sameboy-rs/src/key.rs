#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

// Bindgen Dependencies -------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs_sys::*;


// GameBoy KEys ---------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Key {
    Right,
    Left,
    Up,
    Down,
    A,
    B,
    Select,
    Start
}

impl From<GB_key_t> for Key {
    fn from(k: GB_model_t) -> Self {
        match k {
            GB_key_t_GB_KEY_RIGHT => Self::Right,
            GB_key_t_GB_KEY_LEFT => Self::Left,
            GB_key_t_GB_KEY_UP => Self::Up,
            GB_key_t_GB_KEY_DOWN => Self::Down,
            GB_key_t_GB_KEY_A => Self::A,
            GB_key_t_GB_KEY_B => Self::B,
            GB_key_t_GB_KEY_SELECT => Self::Select,
            GB_key_t_GB_KEY_START => Self::Start,
            _ => Self::Start
        }
    }
}

impl From<Key> for GB_key_t {
    fn from(k: Key) -> GB_key_t {
        match k {
            Key::Right => GB_key_t_GB_KEY_RIGHT,
            Key::Left => GB_key_t_GB_KEY_LEFT,
            Key::Up => GB_key_t_GB_KEY_UP,
            Key::Down => GB_key_t_GB_KEY_DOWN,
            Key::A => GB_key_t_GB_KEY_A,
            Key::B => GB_key_t_GB_KEY_B,
            Key::Select => GB_key_t_GB_KEY_SELECT,
            Key::Start => GB_key_t_GB_KEY_START
        }
    }
}

