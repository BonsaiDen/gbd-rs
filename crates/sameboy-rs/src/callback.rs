// Bindgen Dependencies -------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs_sys::*;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{ReadableCore, RunnableCore, WritableCore};


// Enums ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, PartialEq, Eq)]
pub enum VBlankType {
    NormalFrame,
    LcdOff,
    Artificial
}

impl VBlankType {
    #[allow(non_upper_case_globals)]
    fn from_typ(typ: GB_vblank_type_t) -> Self {
        match typ {
            GB_vblank_type_t_GB_VBLANK_TYPE_NORMAL_FRAME => Self::NormalFrame,
            GB_vblank_type_t_GB_VBLANK_TYPE_LCD_OFF => Self::LcdOff,
            GB_vblank_type_t_GB_VBLANK_TYPE_ARTIFICIAL => Self::Artificial,
            _  => unreachable!("unmapped vblank typ")
        }
    }
}


// Callback Types -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub type LCDLineCallback = Box<dyn Fn(WriteCallbackCore, u8)>;
pub type RGBEncodeCallback = Box<dyn Fn(ReadCallbackCore, u8, u8, u8) -> u32>;
pub type VBlankCallback = Box<dyn Fn(ReadCallbackCore, VBlankType)>;
pub type APUSampleCallback = Box<dyn FnMut(ReadCallbackCore, i16, i16)>;
pub type LogCallback = Box<dyn Fn(ReadCallbackCore, String)>;
pub type DebuggerCallback = Box<dyn Fn(WriteCallbackCore)>;
pub type DebuggerReadCallback = Box<dyn Fn(WriteCallbackCore, u16, u8)>;
pub type DebuggerWriteCallback = Box<dyn Fn(WriteCallbackCore, u16, u8)>;
pub type DebuggerMBCWriteCallback = Box<dyn Fn(WriteCallbackCore, u16, u8)>;
pub type DebuggerCycleCallback = Box<dyn Fn(WriteCallbackCore, u8)>;
pub type DebuggerCallCallback = Box<dyn Fn(WriteCallbackCore, u16, bool)>;
pub type DebuggerCondCallback = Box<dyn Fn(WriteCallbackCore, u16, bool)>;
pub type DebuggerJPHLCallback = Box<dyn Fn(WriteCallbackCore, u16)>;
pub type DebuggerBrkCallback = Box<dyn Fn(WriteCallbackCore)>;
pub type DebuggerMsgCallback = Box<dyn Fn(WriteCallbackCore, u16, u8)>;
pub type DebuggerRetCallback = Box<dyn Fn(WriteCallbackCore)>;


#[derive(Default)]
pub struct CallbackHandle {
    pub lcd_line: Option<LCDLineCallback>,
    pub rgb_encode: Option<RGBEncodeCallback>,
    pub vblank: Option<VBlankCallback>,
    pub apu_sample: Option<APUSampleCallback>,
    pub log: Option<LogCallback>,
    pub debugger: Option<DebuggerCallback>,
    pub debugger_read: Option<DebuggerReadCallback>,
    pub debugger_write: Option<DebuggerWriteCallback>,
    pub debugger_mbc_write: Option<DebuggerMBCWriteCallback>,
    pub debugger_cycle: Option<DebuggerCycleCallback>,
    pub debugger_call: Option<DebuggerCallCallback>,
    pub debugger_cond: Option<DebuggerCondCallback>,
    pub debugger_jphl: Option<DebuggerJPHLCallback>,
    pub debugger_brk: Option<DebuggerBrkCallback>,
    pub debugger_msg: Option<DebuggerMsgCallback>,
    pub debugger_ret: Option<DebuggerRetCallback>,
}

pub struct ReadCallbackCore<'a> {
    gb: &'a GB_gameboy_t,
}

impl<'a> ReadableCore for ReadCallbackCore<'a> {
    fn gb(&self) -> &GB_gameboy_t {
        self.gb
    }
}

pub struct WriteCallbackCore<'a> {
    gb: &'a mut GB_gameboy_t,
}

impl<'a> ReadableCore for WriteCallbackCore<'a> {
    fn gb(&self) -> &GB_gameboy_t {
        self.gb
    }
}

impl<'a> WritableCore for WriteCallbackCore<'a> {
    fn gb(&mut self) -> &mut GB_gameboy_t {
        self.gb
    }
}

macro_rules! callback_method {
    ($set:ident, $typ:ty, $cname:ident, $attr:ident, $rname:ident) => {
        fn $set(&mut self, cb: Option<$typ>) {
            if cb.is_some() {
                self.handle().$attr = cb;
                unsafe { $cname(self.gb(), Some($rname)); }

            } else {
                self.handle().$attr = None;
                unsafe { $cname(self.gb(), None); }
            }
        }
    }
}

pub trait CallbackCore {
    fn gb(&mut self) -> &mut GB_gameboy_t;
    fn handle(&mut self) -> &mut CallbackHandle;
    callback_method!(set_apu_sample_callback, APUSampleCallback, GB_apu_set_sample_callback, apu_sample, apu_sample_callback);
    callback_method!(set_lcd_line_callback, LCDLineCallback, GB_set_lcd_line_callback, lcd_line, lcd_line_callback_handler);
    callback_method!(set_rgb_encode_callback, RGBEncodeCallback, GB_set_rgb_encode_callback, rgb_encode, rgb_encode_callback_handler);
    callback_method!(set_vblank_callback, VBlankCallback, GB_set_vblank_callback, vblank, vblank_callback_handler);
    callback_method!(set_log_callback, LogCallback, GB_set_log_callback, log, log_callback_handler);
    callback_method!(set_debugger_callback, DebuggerCallback, GB_set_debugger_callback, debugger, debugger_callback_handler);
    callback_method!(set_debugger_cycle_callback, DebuggerCycleCallback, GB_set_debugger_cycle_callback, debugger_cycle, debugger_cycle_callback_handler);
    callback_method!(set_debugger_call_callback, DebuggerCallCallback, GB_set_debugger_call_callback, debugger_call, debugger_call_callback_handler);
    callback_method!(set_debugger_cond_callback, DebuggerCondCallback, GB_set_debugger_cond_callback, debugger_cond, debugger_cond_callback_handler);
    callback_method!(set_debugger_jphl_callback, DebuggerJPHLCallback, GB_set_debugger_jphl_callback, debugger_jphl, debugger_jphl_callback_handler);
    callback_method!(set_debugger_brk_callback, DebuggerBrkCallback, GB_set_debugger_brk_callback, debugger_brk, debugger_brk_callback_handler);
    callback_method!(set_debugger_msg_callback, DebuggerMsgCallback, GB_set_debugger_msg_callback, debugger_msg, debugger_msg_callback_handler);
    callback_method!(set_debugger_ret_callback, DebuggerRetCallback, GB_set_debugger_ret_callback, debugger_ret, debugger_ret_callback_handler);
    callback_method!(set_debugger_read_callback, DebuggerReadCallback, GB_set_debugger_read_callback, debugger_read, debugger_read_callback_handler);
    callback_method!(set_debugger_write_callback, DebuggerWriteCallback, GB_set_debugger_write_callback, debugger_write, debugger_write_callback_handler);
    callback_method!(set_debugger_mbc_write_callback, DebuggerMBCWriteCallback, GB_set_debugger_mbc_write_callback, debugger_mbc_write, debugger_mbc_write_callback_handler);
}

fn get_core<'a>(gb: *mut GB_gameboy_t) -> &'a mut RunnableCore {
    unsafe { &mut *((*gb).__bindgen_anon_11.__bindgen_anon_1.user_data as *mut RunnableCore) }
}

pub extern "C" fn apu_sample_callback(gb: *mut GB_gameboy_t, sample: *mut GB_sample_t) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().apu_sample {
        let gb = ReadCallbackCore {
            gb: unsafe { &(*gb) }
        };
        callback(gb, unsafe { (*sample).left }, unsafe { (*sample).right });
    }
}

pub extern "C" fn lcd_line_callback_handler(gb: *mut GB_gameboy_t, line: u8) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().lcd_line {
        let gb = WriteCallbackCore {
            gb: unsafe { &mut (*gb) }
        };
        callback(gb, line);
    }
}

pub extern "C" fn rgb_encode_callback_handler(gb: *mut GB_gameboy_t, r: u8, g: u8, b: u8) -> u32 {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().rgb_encode {
        let gb = ReadCallbackCore {
            gb: unsafe { &(*gb) }
        };
        callback(gb, r, g, b)

    } else {
        0
    }
}

pub extern "C" fn vblank_callback_handler(gb: *mut GB_gameboy_t, typ: GB_vblank_type_t) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().vblank {
        let gb = ReadCallbackCore {
            gb: unsafe { &(*gb) }
        };
        callback(gb, VBlankType::from_typ(typ))
    }
}

pub extern "C" fn log_callback_handler(gb: *mut GB_gameboy_t, msg: *const i8, _attr: GB_log_attributes) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().log {
        let gb = ReadCallbackCore {
            gb: unsafe { &(*gb) }
        };
        let s = unsafe { std::ffi::CStr::from_ptr(msg) };
        callback(gb, s.to_string_lossy().to_string());
    }
}

pub extern "C" fn debugger_callback_handler(gb: *mut GB_gameboy_t) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().debugger {
        let gb = WriteCallbackCore {
            gb: unsafe { &mut (*gb) }
        };
        callback(gb);
    }
}

pub extern "C" fn debugger_cycle_callback_handler(gb: *mut GB_gameboy_t, cycles: u8) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().debugger_cycle {
        let gb = WriteCallbackCore {
            gb: unsafe { &mut (*gb) }
        };
        callback(gb, cycles);
    }
}

pub extern "C" fn debugger_call_callback_handler(gb: *mut GB_gameboy_t, call_addr: u16, interrupt: bool) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().debugger_call {
        let gb = WriteCallbackCore {
            gb: unsafe { &mut (*gb) }
        };
        callback(gb, call_addr, interrupt);
    }
}

pub extern "C" fn debugger_cond_callback_handler(gb: *mut GB_gameboy_t, addr: u16, truthy: bool) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().debugger_cond {
        let gb = WriteCallbackCore {
            gb: unsafe { &mut (*gb) }
        };
        callback(gb, addr, truthy);
    }
}

pub extern "C" fn debugger_jphl_callback_handler(gb: *mut GB_gameboy_t, addr: u16) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().debugger_jphl {
        let gb = WriteCallbackCore {
            gb: unsafe { &mut (*gb) }
        };
        callback(gb, addr);
    }
}

pub extern "C" fn debugger_brk_callback_handler(gb: *mut GB_gameboy_t) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().debugger_brk {
        let gb = WriteCallbackCore {
            gb: unsafe { &mut (*gb) }
        };
        callback(gb);
    }
}

pub extern "C" fn debugger_msg_callback_handler(gb: *mut GB_gameboy_t, msg_addr: u16, msg_len: u8) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().debugger_msg {
        let gb = WriteCallbackCore {
            gb: unsafe { &mut (*gb) }
        };
        callback(gb, msg_addr, msg_len);
    }
}

pub extern "C" fn debugger_ret_callback_handler(gb: *mut GB_gameboy_t) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().debugger_ret {
        let gb = WriteCallbackCore {
            gb: unsafe { &mut (*gb) }
        };
        callback(gb);
    }
}

pub extern "C" fn debugger_read_callback_handler(gb: *mut GB_gameboy_t, addr: u16, value: u8) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().debugger_read {
        let gb = WriteCallbackCore {
            gb: unsafe { &mut (*gb) }
        };
        callback(gb, addr, value);
    }
}

pub extern "C" fn debugger_write_callback_handler(gb: *mut GB_gameboy_t, addr: u16, value: u8) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().debugger_write {
        let gb = WriteCallbackCore {
            gb: unsafe { &mut (*gb) }
        };
        callback(gb, addr, value);
    }
}

pub extern "C" fn debugger_mbc_write_callback_handler(gb: *mut GB_gameboy_t, addr: u16, value: u8) {
    let core = get_core(gb);
    if let Some(callback) = &mut core.handle().debugger_mbc_write {
        let gb = WriteCallbackCore {
            gb: unsafe { &mut (*gb) }
        };
        callback(gb, addr, value);
    }
}

