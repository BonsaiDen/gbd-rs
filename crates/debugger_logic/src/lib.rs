// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::rc::Rc;
use std::cell::RefCell;
use std::path::PathBuf;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use dap::{Debuggee, DapHandler, DapProtocol};
use analyzer::{Analyzer, Symbol, SymbolCache, SymbolProvider};
use sameboy_rs::{BankedAddress, Model, ReadableCore, RunnableCore};


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod breakpoint;
mod debuggee;
mod watchpoint;


// Re-Exports -----------------------------------------------------------------
// ----------------------------------------------------------------------------
pub use breakpoint::DebuggerBreakpoint;
pub use watchpoint::DebuggerWatchpoint;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use debuggee::DapState;


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct DebuggerScope {
    pub start: BankedAddress,
    pub end: BankedAddress,
    pub io_references: Vec<u16>,
    pub symbol_references: Vec<String>
}

#[derive(Debug)]
pub struct DebuggerLocation {
    pub addr: BankedAddress,
    pub path: PathBuf,
    pub line: usize,
    pub column: usize
}


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct DebuggerLogic {
    pub analyzer: Rc<RefCell<Analyzer<DebuggerBreakpoint, DebuggerWatchpoint>>>,

    symbols: SymbolCache,
    user_symbol_path: Option<PathBuf>,
    locations: Vec<DebuggerLocation>,
    scopes: Vec<DebuggerScope>,
    dap: DapState,

    base_frame: u32,
    did_step: bool,
    ready: bool,

    should_switch_model: Option<Model>,
    should_reset: Option<()>,
    should_terminate: Option<()>,
    should_halt: bool,
    should_reload: Option<()>,
}

impl SymbolProvider for DebuggerLogic {
    fn gen(&self) -> usize {
        self.symbols.gen()
    }

    fn set_symbol_flags(&mut self, ie: u8, lcdc: u8) {
        self.symbols.set_symbol_flags(ie, lcdc);
    }

    fn symbols(&self) -> &[Symbol] {
        self.symbols.symbols()
    }

    fn dynamic_symbols(&self) -> Vec<&Symbol> {
        self.symbols.dynamic_symbols()
    }

    fn resolve_symbol_by_name(&self, name: &str) -> Option<&Symbol> {
        self.symbols.resolve_symbol_by_name(name)
    }

    fn resolve_symbol_absolute(&self, addr: BankedAddress) -> Option<&Symbol> {
        self.symbols.resolve_symbol_absolute(addr)
    }

    fn resolve_symbol_name_relative(&self, addr: BankedAddress) -> String {
        self.symbols.resolve_symbol_name_relative(addr)
    }
}

impl DebuggerLogic {
    pub fn new(source_dir: PathBuf) -> Self {
        Self {
            analyzer: Analyzer::new(),

            symbols: SymbolCache::default(),
            user_symbol_path: None,
            locations: Vec::new(),
            scopes: Vec::new(),
            dap: DapState::new(source_dir),

            base_frame: 0,
            did_step: false,
            ready: false,

            should_switch_model: None,
            should_reset: None,
            should_terminate: None,
            should_halt: false,
            should_reload: None
        }
    }

    pub fn parse_address(s: &str) -> Option<BankedAddress> {
        if let Some((addr, bank)) = s.split_once(':') {
            let addr = u16::from_str_radix(addr, 16).ok()?;
            let bank = u16::from_str_radix(bank, 16).ok()?;
            Some(BankedAddress::new(addr, bank))

        } else {
            let addr = u16::from_str_radix(s, 16).ok()?;
            if (0x4000..0x8000).contains(&addr) {
                Some(BankedAddress::new(addr, 1))

            } else {
                Some(BankedAddress::new(addr, 0))
            }
        }
    }

    pub fn is_ready(&self) -> bool {
        self.ready
    }

    pub fn is_halted(&self) -> bool {
        self.analyzer.borrow().is_halted()
    }

    pub fn did_step(&self) -> bool {
        self.is_halted() && self.did_step
    }

    pub fn in_boot_rom(&self) -> bool {
        self.analyzer.borrow().in_boot_rom()
    }

    pub fn base_frame(&self) -> u32 {
        self.base_frame
    }

    pub fn should_switch_model(&mut self) -> Option<Model> {
        self.should_switch_model.take()
    }

    pub fn should_reset(&mut self) -> bool {
        self.should_reset.take().is_some()
    }

    pub fn should_reload(&mut self) -> bool {
        self.should_reload.take().is_some()
    }

    pub fn should_terminate(&mut self) -> bool {
        self.should_terminate.take().is_some()
    }
}

impl DebuggerLogic {
    pub fn halt(&mut self) {
        self.should_halt = true;
    }

    pub fn reset(&mut self, core: &mut RunnableCore) {
        self.analyzer = Analyzer::new();
        self.symbols.reset();
        Analyzer::set_enabled(&self.analyzer, core, true);
        self.ready = false;
    }

    pub fn request_reset(&mut self) {
        self.should_reset = Some(());
    }

    pub fn request_reload(&mut self) {
        self.should_reload = Some(());
        self.should_reset = Some(());
    }

    pub fn request_model_switch(&mut self, model: Model) {
        self.should_switch_model = Some(model);
    }

    pub fn toggle_pause(&mut self) {
        if self.is_halted() {
            self.resume(1);

        } else {
            self.pause(1);
        }
    }

    pub fn advance_frames(&mut self, frames: usize, sync_vblank: bool) {
        self.analyzer.borrow_mut().advance_frames(frames, sync_vblank);
    }

    pub fn advance_lines(&mut self, lines: usize) {
        self.analyzer.borrow_mut().advance_lines(lines);
    }

    pub fn insert_user_symbol(&mut self, symbol: Symbol) {
        self.symbols.insert(symbol.into_user_symbol());
        self.symbols.build();
        self.store_user_symbol_map();
    }

    pub fn remove_user_symbol(&mut self, symbol: Symbol) {
        self.symbols.remove(symbol.into_user_symbol());
        self.symbols.build();
        self.store_user_symbol_map();
    }
}

impl DebuggerLogic {
    pub fn load_maps(&mut self, rom_path: Option<PathBuf>) {
        self.load_symbol_and_source_maps(rom_path);
        self.analyzer.borrow_mut().set_symbols(&self.symbols);
    }

    pub fn show(&mut self, core: &mut RunnableCore, halt: bool) {
        Analyzer::set_enabled(&self.analyzer, core, true);
        if halt {
            self.halt();
        }
    }

    pub fn hide(&mut self, core: &mut RunnableCore) {
        Analyzer::set_enabled(&self.analyzer, core, false);
    }

    pub fn run_frame<C: FnMut(bool)>(
        &mut self,
        core: &mut RunnableCore,
        pixels_output: &mut [u32],
        dap: &mut Option<DapProtocol>,
        skip_boot_rom: bool,
        mut silence_audio: C
    ) {
        // Receive DAP Commands
        if let Some(dap) = dap {
            DapHandler::before_frame(dap, self, core);
            // Wait for DAP if required
            if !dap.is_connected() {
                // Automatically resume emulation after connection
                self.should_halt = false;
                return;
            }
        }

        // Skip boot rom if equested
        if self.should_halt && (!skip_boot_rom || core.is_boot_rom_finished()) {
            self.analyzer.borrow_mut().halt();
            self.should_halt = false;
        }

        // Run Emulation through the Analyzer
        if self.analyzer.borrow_mut().begin_frame(core, false) {
            if skip_boot_rom && !core.is_boot_rom_finished() {
                Analyzer::set_enabled(&self.analyzer, core, false);
                silence_audio(true);
                core.skip_bootrom();
                silence_audio(false);
                Analyzer::set_enabled(&self.analyzer, core, true);

            } else {
                core.run_frame(pixels_output);
            }
            self.did_step = true;

        } else {
            self.did_step = false;
        }
        self.ready = true;

        // Send DAP Responses
        if let Some(dap) = dap {
            DapHandler::after_frame(dap, self);
        }
    }
}

impl DebuggerLogic {
    fn load_symbol_and_source_maps(&mut self, rom_path: Option<PathBuf>) {
        fn parse_symbol_line(line: &str, user_provided: bool) -> Option<Symbol> {
            let mut parts = line.split(' ');
            let addr = parts.next()?;
            let name = parts.next()?;
            let (bank, addr) = addr.split_once(':')?;
            let bank = u16::from_str_radix(bank, 16).ok()?;
            let addr = u16::from_str_radix(addr, 16).ok()?;
            if let Some(sizes) = parts.next() {
                // Extract sizes from symbol file
                let mut size_parts = sizes.split(':');
                let code_size = size_parts.next()?.parse().ok()?;
                let data_size = size_parts.next()?.parse().ok()?;
                let variable_size = size_parts.next()?.parse().ok()?;
                Some(Symbol::new(
                    name.to_string(),
                    BankedAddress::new(addr, bank),
                    code_size,
                    data_size,
                    variable_size,
                    0,
                    user_provided
                ))

            } else {
                // Make assumptions if no sizes are provided by the symbol file
                let (code_size, data_size, variable_size, register_size) = match addr {
                    0x0000..=0x7FFF => (0, 1, 0, 0),
                    0x8000..=0xDFFF => (0, 0, 1, 0),
                    0xFF00..=0xFF7F => (0, 0, 0, 1),
                    0xFF80..=0xFFFF => (0, 0, 1, 0),
                    _ => (0, 1, 0, 0)
                };
                Some(Symbol::new(
                    name.to_string(),
                    BankedAddress::new(addr, bank),
                    code_size,
                    data_size,
                    variable_size,
                    register_size,
                    user_provided
                ))
            }
        }

        enum Line {
            Location(DebuggerLocation),
            Scope(DebuggerScope)
        }

        fn parse_location_line(line: &str) -> Option<Line> {
            let (typ, data) = line.split_once(' ')?;
            match typ {
                "a" => {
                    let (addr, loc) = data.split_once(' ')?;
                    let (bank, addr) = addr.split_once(':')?;
                    let bank = u16::from_str_radix(bank, 16).ok()?;
                    let addr = u16::from_str_radix(addr, 16).ok()?;
                    let (path, line) = loc.split_once(',')?;
                    let (line, column) = line.split_once(',')?;
                    let line = line.parse::<usize>().ok()?;
                    let column = column.parse::<usize>().ok()?;
                    Some(Line::Location(DebuggerLocation {
                        addr: BankedAddress::new(addr, bank),
                        path: PathBuf::from(path.trim_matches('"')),
                        line,
                        column
                    }))
                },
                "s" => {
                    let (addr, scope) = data.split_once(' ')?;
                    let (start, end) = addr.split_once('-')?;
                    let (start_bank, start_addr) = start.split_once(':')?;
                    let (end_bank, end_addr) = end.split_once(':')?;
                    let start_addr = u16::from_str_radix(start_addr, 16).ok()?;
                    let start_bank = u16::from_str_radix(start_bank, 16).ok()?;
                    let end_addr = u16::from_str_radix(end_addr, 16).ok()?;
                    let end_bank = u16::from_str_radix(end_bank, 16).ok()?;
                    let (ios, symbols) = scope.split_once(' ')?;
                    let io_references: Vec<u16> = ios.split(',').flat_map(|s| u16::from_str_radix(s, 16).ok()).collect();
                    let symbol_references: Vec<String> = symbols.split(',').map(|s| s.to_string()).filter(|s| !s.is_empty()).collect();
                    Some(Line::Scope(DebuggerScope {
                        start: BankedAddress::new(start_addr, start_bank),
                        end: BankedAddress::new(end_addr, end_bank),
                        io_references,
                        symbol_references
                    }))
                },
                _ => None
            }
        }

        self.symbols.clear();
        self.locations.clear();
        self.scopes.clear();

        if let Some(mut p) = rom_path {
            p.set_extension("map");
            if let Ok(data) = std::fs::read_to_string(&p) {
                for line in data.lines().flat_map(parse_location_line) {
                    match line {
                        Line::Location(l) => self.locations.push(l),
                        Line::Scope(s) => self.scopes.push(s)
                    }
                }
            }

            p.set_extension("sym");
            if let Ok(data) = std::fs::read_to_string(&p) {
                for s in data.lines().flat_map(|l| parse_symbol_line(l, false)) {
                    self.symbols.insert(s);
                }
            }

            p.set_extension("user.sym");
            if let Ok(data) = std::fs::read_to_string(&p) {
                for s in data.lines().flat_map(|l| parse_symbol_line(l, true)) {
                    self.symbols.insert(s);
                }
            }
            self.user_symbol_path = Some(p);
        }
        self.symbols.build();
    }

    fn store_user_symbol_map(&mut self) {
        if let Some(p) = &self.user_symbol_path {
            let lines: Vec<String> = self.symbols().iter().filter(|s| s.is_user_provided()).map(|s| {
                s.to_string()

            }).collect();
            std::fs::write(p, lines.join("\n")).ok();
        }
    }
}

