// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::path::PathBuf;
use std::collections::{HashMap, HashSet};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use analyzer::{Breakpoint, SymbolProvider};
use sameboy_rs::{BankedAddress, ReadableCore, RunnableCore, WritableCore};
use evalexpr::{build_operator_tree, Value};
use dap::{
    Debuggee,
    DapBreakpoint, DapThread, DapScope, DapSource, DapStackFrame, DapVariable, DapSourceBreakpoint,
    DapEvent, DapCompletionItem
};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{DebuggerLogic, DebuggerBreakpoint, DebuggerScope};
use crate::breakpoint::EvaluatorContext;


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct DapState {
    line_offset: usize,
    column_offset: usize,
    running: bool,
    was_started: bool,
    started: bool,
    stop_reason: Option<(String, Option<usize>)>,
    breakpoint_id: usize,
    breakpoints: HashMap<String, HashSet<BankedAddress>>,
    events: Vec<DapEvent>,
    source_dir: PathBuf
}

impl DapState {
    const TOP_FRAME_ID: usize = 0;
    const REGISTERS_VAR_ID: usize = 1;
    const FLAGS_VAR_ID: usize = 2;
    const LOCALS_VAR_ID: usize = 3;

    pub fn new(source_dir: PathBuf) -> Self {
        Self {
            line_offset: 0,
            column_offset: 0,
            running: true,
            was_started: true,
            started: false,
            stop_reason: None,
            breakpoint_id: 0,
            breakpoints: HashMap::new(),
            events: Vec::new(),
            source_dir
        }
    }
}


// DAP Support ----------------------------------------------------------------
// ----------------------------------------------------------------------------
impl DebuggerLogic {
    pub fn forward_message_to_dap(&mut self, addr: BankedAddress, output: String) {
        if self.dap.started {
            self.dap.events.push(if let Some((source, line, column, _)) = self.resolve_source_location(addr) {
                DapEvent::Output {
                    category: "console".to_string(),
                    output,
                    source: Some(source),
                    line: Some(line),
                    column: Some(column)
                }

            } else {
                DapEvent::Output {
                    category: "console".to_string(),
                    output,
                    source: None,
                    line: None,
                    column: None
                }
            });
        }
    }

    fn resolve_stack_frame(&self, addr: BankedAddress) -> Option<DapStackFrame> {
        if let Some((source, line, column, symbol)) = self.resolve_source_location(addr) {
            Some(DapStackFrame {
                id: DapState::TOP_FRAME_ID,
                name: format!("{} ({})", addr, symbol),
                source,
                line,
                column
            })

        } else {
            None
        }
    }

    fn resolve_source_location(&self, addr: BankedAddress) -> Option<(DapSource, usize, usize, String)> {
        for loc in &self.locations {
            if loc.addr == addr {
                let mut full_path = self.dap.source_dir.clone();
                full_path.push(&loc.path);
                let symbol = self.resolve_symbol_name_relative(addr);
                let source = DapSource {
                    name: full_path.file_name().expect("Missing filename").to_string_lossy().to_string(),
                    path: full_path.display().to_string()
                };
                return Some((
                    source,
                    loc.line.saturating_sub(self.dap.line_offset),
                    loc.column.saturating_sub(self.dap.column_offset),
                    symbol
                ))
            }
        }
        None
    }

    fn resolve_source_address(&self, source_path: &str, source_line: usize) -> Option<(BankedAddress, usize, usize)> {
        let source_path = PathBuf::from(source_path);
        if let Ok(relative_source_path) = source_path.strip_prefix(&self.dap.source_dir) {
            let source_line = source_line.saturating_add(self.dap.line_offset);
            for loc in &self.locations {
                if loc.line >= source_line && loc.path == relative_source_path {
                    return Some((
                        loc.addr,
                        loc.line.saturating_sub(self.dap.line_offset),
                        loc.column.saturating_sub(self.dap.column_offset)
                    ));
                }
            }
        }
        None
    }

    fn resolve_scope(&self, addr: BankedAddress) -> Option<&DebuggerScope> {
        self.scopes.iter().find(|&scope| addr.within_range(scope.start, scope.end))
    }

    fn get_scope_variables(&self, core: &mut RunnableCore) -> Vec<(String, String, BankedAddress, u16)> {
        let pc = core.pc();
        if let Some(scope) = self.resolve_scope(pc) {
            let mut variables: Vec<(BankedAddress, String, u16)> = Vec::new();
            for addr in &scope.io_references {
                if let Some(symbol) = self.resolve_symbol_absolute(BankedAddress::new(*addr, 0)) {
                    if symbol.is_register() {
                        variables.push((symbol.address(), symbol.name().to_string(), symbol.size()));
                    }
                }
            }
            for name in &scope.symbol_references {
                if let Some(symbol) = self.resolve_symbol_by_name(name) {
                    if symbol.is_variable() {
                        variables.push((symbol.address(), symbol.name().to_string(), symbol.size()));
                    }
                }
            }
            variables.into_iter().map(|(addr, name, size)| {
                let (name, value) = match size {
                    1 => {
                        let v = core.read_memory_safe(addr.address());
                        (name, format!("0x{:0>2X}", v))
                    },
                    2 => {
                        let v = core.read_memory_safe(addr.address()) as u16 | ((core.read_memory_safe(addr.address().saturating_add(1)) as u16) << 8);
                        (name, format!("0x{:0>4X}", v))
                    },
                    _ => (format!("{}[{}]", name, size), "?".to_string())
                };
                (name, value, addr, size)

            }).collect()

        } else {
            Vec::new()
        }
    }
}


// DAP Protocol ---------------------------------------------------------------
// ----------------------------------------------------------------------------
impl Debuggee for DebuggerLogic {
    fn set_line_offset(&mut self, offset: usize) {
        self.dap.line_offset = offset;
    }

    fn set_column_offset(&mut self, offset: usize) {
        self.dap.column_offset = offset;
    }

    fn was_started(&mut self) -> bool {
        if !self.dap.started {
            false

        } else {
            let s = self.dap.was_started;
            self.dap.was_started = false;
            s
        }
    }

    fn was_paused(&mut self) -> bool {
        let analyzer = self.analyzer.borrow();
        if analyzer.is_halted() && self.dap.running {
            self.dap.stop_reason = if let Some(i) = analyzer.halt_information() {
                Some((i.reason.to_string(), None))

            } else {
                Some(("Halted".to_string(), None))
            };
            self.dap.running = false;
            true

        } else {
            false
        }
    }

    fn was_resumed(&mut self) -> bool {
        if !self.dap.started {
            false

        } else if !self.analyzer.borrow_mut().is_halted() && !self.dap.running {
            self.dap.running = true;
            true

        } else {
            false
        }
    }

    fn has_stopped(&mut self) -> Option<(String, Option<usize>)> {
        self.dap.stop_reason.take()
    }

    fn start(&mut self) {
        self.dap.started = true;
    }

    fn restart(&mut self) {
        self.should_reload = Some(());
        self.should_reset = Some(());
        self.dap.running = true;
    }

    fn step_in(&mut self, _thread_id: u8) {
        self.analyzer.borrow_mut().step_into(1);
        self.dap.running = true;
    }

    fn step_out(&mut self, _thread_id: u8) {
        self.analyzer.borrow_mut().return_from_calls(1);
        self.dap.running = true;
    }

    fn next(&mut self, _thread_id: u8) {
        self.analyzer.borrow_mut().step_over(1);
        self.dap.running = true;
    }

    fn pause(&mut self, _thread_id: u8) {
        self.analyzer.borrow_mut().halt();
        self.base_frame = self.analyzer.borrow_mut().frame();
    }

    fn resume(&mut self, _thread_id: u8) {
        self.analyzer.borrow_mut().resume();
    }

    fn terminate(&mut self) {
        self.should_terminate = Some(());
    }

    fn events(&mut self) -> Vec<DapEvent> {
        self.dap.events.drain(0..).collect()
    }

    fn completions(&self, text: String, column: usize) -> Vec<DapCompletionItem> {
        eprintln!("Complete: {} @ {}", text, column);
        vec![DapCompletionItem {
            label: "Hello".to_string()
        }]
    }

    fn evaluate(&mut self, core: &mut RunnableCore, expression: String) -> String {
        match build_operator_tree(&expression) {
            Ok(node) => {
                let mut ctx = EvaluatorContext::default();
                ctx.prefetch(core, self, &node);
                let result = match node.eval_with_context_mut(&mut ctx) {
                    Ok(v) => match v {
                        Value::Int(i) => format!("0x{:0>2X} / 0x{:0>4X} / {}", i as u8, i as u16, i),
                        _ => v.to_string()
                    }
                    Err(e) => e.to_string()
                };
                ctx.postset(core, self, &node);
                result
            },
            Err(e) => e.to_string()
        }
    }

    fn set_breakpoints(&mut self, source: DapSource, breakpoints: Vec<DapSourceBreakpoint>) -> Vec<DapBreakpoint> {

        // Remove existing breakpoints from analyzer
        let mut analyzer = self.analyzer.borrow_mut();
        let source_breakpoints = self.dap.breakpoints.entry(source.path.clone()).or_insert_with(HashSet::new);
        for index in source_breakpoints.drain() {
            analyzer.remove_breakpoint(index);
        }

        // Generate breakpoint locations
        let mut response = Vec::new();
        let mut points = Vec::new();
        for b in breakpoints {
            if let Some((addr, line, column)) = self.resolve_source_address(&source.path, b.line) {
                response.push(DapBreakpoint {
                    id: Some(self.dap.breakpoint_id),
                    verified: true,
                    message: None,
                    source: source.clone(),
                    line,
                    column
                });
                points.push(DebuggerBreakpoint::new(addr, b.condition, b.hit_condition));
                self.dap.breakpoint_id += 1;

            } else {
                response.push(DapBreakpoint {
                    id: None,
                    verified: false,
                    message: Some("Unknown location".to_string()),
                    source: source.clone(),
                    line: b.line,
                    column: 0
                });
            }
        }

        // Insert generated breakpoints into analyzer
        if let Some(source_breakpoints) = self.dap.breakpoints.get_mut(&source.path) {
            for b in points {
                source_breakpoints.insert(b.address());
                analyzer.add_breakpoint(b);
            }
        }
        response
    }

    fn threads(&self) -> Vec<DapThread> {
        vec![DapThread {
            id: 1,
            name: "ROM".to_string()
        }]
    }

    fn stack_frames(&self, core: &mut RunnableCore, _thread_id: u8) -> Vec<DapStackFrame> {
        let mut frames = Vec::new();
        if let Some(current) = self.resolve_stack_frame(core.pc()) {
            frames.push(current)
        }
        for b in self.analyzer.borrow().backtraces().iter().rev() {
            if let Some(next) = self.resolve_stack_frame(b.addr) {
                frames.push(next)
            }
        }
        frames
    }

    fn scopes(&self, frame_id: usize) -> Vec<DapScope> {
        match frame_id {
            DapState::TOP_FRAME_ID => vec![DapScope {
                name: "Registers".to_string(),
                expensive: false,
                presentation_hint: "registers".to_string(),
                variables_reference: DapState::REGISTERS_VAR_ID

            }, DapScope {
                name: "Flags".to_string(),
                expensive: false,
                presentation_hint: "registers".to_string(),
                variables_reference: DapState::FLAGS_VAR_ID

            }, DapScope {
                name: "Locals".to_string(),
                expensive: false,
                presentation_hint: "registers".to_string(),
                variables_reference: DapState::LOCALS_VAR_ID
            }],
            _ => Vec::new()
        }
    }

    fn variables(&self, core: &mut RunnableCore, var_id: usize) -> Vec<DapVariable> {
        match var_id {
            DapState::REGISTERS_VAR_ID => {
                let words = core.word_registers();
                vec![DapVariable {
                    name: "AF".to_string(),
                    value: format!("0x{:>04X}", words.af),
                    variables_reference: 0

                }, DapVariable {
                    name: "BC".to_string(),
                    value: format!("0x{:>04X}", words.bc),
                    variables_reference: 0

                }, DapVariable {
                    name: "DE".to_string(),
                    value: format!("0x{:>04X}", words.de),
                    variables_reference: 0

                }, DapVariable {
                    name: "HL".to_string(),
                    value: format!("0x{:>04X}", words.hl),
                    variables_reference: 0

                }, DapVariable {
                    name: "SP".to_string(),
                    value: format!("0x{:>04X}", words.sp),
                    variables_reference: 0

                }, DapVariable {
                    name: "PC".to_string(),
                    value: format!("0x{:>04X}", words.pc),
                    variables_reference: 0
                }]
            },
            DapState::FLAGS_VAR_ID => {
                let bytes = core.byte_registers();
                vec![DapVariable {
                    name: "C".to_string(),
                    value: if (bytes.f & 16) == 16 { "1" } else { "0" }.to_string(),
                    variables_reference: 0

                }, DapVariable {
                    name: "H".to_string(),
                    value: if (bytes.f & 32) == 32 { "1" } else { "0" }.to_string(),
                    variables_reference: 0

                }, DapVariable {
                    name: "N".to_string(),
                    value: if (bytes.f & 64) == 64 { "1" } else { "0" }.to_string(),
                    variables_reference: 0

                }, DapVariable {
                    name: "Z".to_string(),
                    value: if (bytes.f & 128) == 128 { "1" } else { "0" }.to_string(),
                    variables_reference: 0
                }]
            },
            DapState::LOCALS_VAR_ID => {
                let variables = self.get_scope_variables(core);
                variables.into_iter().map(|(name, value, _, _)| {
                    DapVariable {
                        name,
                        value,
                        variables_reference: 0
                    }
                }).collect()
            },
            _ => Vec::new()
        }
    }

    fn set_variable(&mut self, core: &mut RunnableCore, var_id: usize, name: String, mut value: String) -> String {
        match var_id {
            DapState::REGISTERS_VAR_ID => {
                let v = u16::from_str_radix(value.trim_start_matches("0x"), 16).unwrap_or(0);
                let mut words = core.word_registers();
                let value = match name.as_str() {
                    "A" => {
                        words.af = (words.af & 0x00FF) | (v << 8);
                        format!("0x{:>02X}", words.af >> 8)
                    },
                    "BC" => {
                        words.bc = v;
                        format!("0x{:>04X}", words.bc)
                    },
                    "DE" => {
                        words.de = v;
                        format!("0x{:>04X}", words.de)
                    },
                    "HL" => {
                        words.hl = v;
                        format!("0x{:>04X}", words.hl)
                    },
                    "SP" => {
                        words.sp = v;
                        format!("0x{:>04X}", words.sp)
                    },
                    "PC" => {
                        words.pc = v;
                        format!("0x{:>04X}", words.pc)
                    },
                    _ => value
                };
                core.set_word_registers(words);
                value
            },
            DapState::LOCALS_VAR_ID => {
                let variables = self.get_scope_variables(core);
                let v = u16::from_str_radix(value.trim_start_matches("0x"), 16).unwrap_or(0);
                for (n, _, addr, size) in variables {
                    if n == name {
                        if size ==1 {
                            core.write_memory(addr.address(), v as u8);
                            value = format!("0x{:>02X}", v);

                        } else if size == 2 {
                            core.write_memory(addr.address(), v as u8);
                            core.write_memory(addr.address().saturating_add(1), (v >> 8) as u8);
                            value = format!("0x{:>04X}", v);
                        }
                        break;
                    }
                }
                value
            },
            _ => value
        }
    }
}

