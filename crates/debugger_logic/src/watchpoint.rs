// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use analyzer::Watchpoint;
use sameboy_rs::BankedAddress;
use serde::{Serialize, Deserialize};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::DebuggerLogic;


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct DebuggerWatchpoint {
    #[serde(default, skip_serializing)]
    pub index: usize,
    pub start_address: BankedAddress,
    pub end_address: Option<BankedAddress>,
    pub start_value: Option<u8>,
    pub end_value: Option<u8>,
    pub active: bool,
    pub write: bool,
    pub read: bool,
    pub log: bool,
    pub halt: bool,
}

impl Watchpoint for DebuggerWatchpoint {
    fn start_address(&self) -> BankedAddress {
        self.start_address
    }

    fn end_address(&self) -> Option<BankedAddress> {
        self.end_address
    }

    fn start_value(&self) -> Option<u8> {
        self.start_value
    }

    fn end_value(&self) -> Option<u8> {
        self.end_value
    }

    fn active(&self) -> bool {
        self.active
    }

    fn write(&self) -> bool {
        self.write
    }

    fn read(&self) -> bool {
        self.read
    }

    fn log(&self) -> bool {
        self.log
    }

    fn halt(&self) -> bool {
        self.halt
    }
}

impl DebuggerWatchpoint {
    pub fn new(index: usize, start_address: BankedAddress, end_address: Option<BankedAddress>) -> Self {
        Self {
            index,
            start_address,
            end_address: if end_address != Some(start_address) {
                end_address

            } else {
                None
            },
            start_value: None,
            end_value: None,
            active: true,
            write: true,
            read: true,
            log: true,
            halt: true,
        }
    }

    pub fn to_address_label(&self) -> String {
        if let Some(end) = &self.end_address {
            if self.start_address.bank() == end.bank() {
                let size = end.address().saturating_sub(self.start_address.address()).saturating_add(1);
                format!("{}[{}]", self.start_address.to_label_string(), size)

            } else {
                format!("{}-{}", self.start_address.to_label_string(), end.to_label_string())
            }

        } else {
            self.start_address.to_label_string()
        }
    }

    pub fn to_address_string(&self) -> String {
        if let Some(end) = &self.end_address {
            format!("{}-{}", self.start_address.to_label_string(), end.to_label_string())

        } else {
            self.start_address.to_label_string()
        }
    }

    pub fn update_address(&mut self, addr: &str) {
        if let Some((start, end)) = addr.split_once('-') {
            if let (Some(start), Some(end)) = (DebuggerLogic::parse_address(start), DebuggerLogic::parse_address(end)) {
                self.start_address = start;
                if end.address() > start.address() || end.bank() > start.bank() {
                    self.end_address = Some(end);

                } else {
                    self.end_address = None;
                }
            }

        } else {
            self.end_address = None;
            self.start_address = DebuggerLogic::parse_address(addr).unwrap_or(self.start_address);
        }

        if self.end_address == Some(self.start_address) {
            self.end_address = None;
        }
    }

    pub fn to_value_label(&self) -> String {
        match (&self.start_value, &self.end_value) {
            (Some(start), Some(end)) => format!("{:0>2X}-{:0>2X}", start, end),
            (Some(start), None) => format!("{:0>2X}", start),
            (_, _) => "-".to_string()
        }
    }

    pub fn update_value(&mut self, value: &str) {
        fn parse(s: &str) -> Option<u8> {
            u8::from_str_radix(s, 16).ok()
        }
        if let Some((start, end)) = value.split_once('-') {
            if let (Some(start), Some(end)) = (parse(start), parse(end)) {
                self.start_value = Some(start);
                if end > start {
                    self.end_value = Some(end);

                } else {
                    self.end_value = None;
                }
            }

        } else {
            self.end_value = None;
            self.start_value = parse(value);
        }

        if self.end_value == self.start_value {
            self.end_value = None;
        }
    }
}

