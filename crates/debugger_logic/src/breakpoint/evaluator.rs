// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::collections::HashMap;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::ReadableCore;
use analyzer::SymbolProvider;
use evalexpr::{Node, Value, EvalexprError, EvalexprResult, Context, ContextWithMutableVariables};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct EvaluatorContext {
    values: HashMap<String, Value>,
    set_values: HashMap<String, Value>
}

impl EvaluatorContext {
    pub fn prefetch<T: SymbolProvider>(&mut self, core: &dyn ReadableCore, provider: &T, node: &Node) {
        let bytes = core.byte_registers();
        let words = core.word_registers();
        for s in node.iter_variable_identifiers() {
            if let Some(value) = match s {
                "a" | "A" => Some(Value::Int(bytes.a.into())),
                "b" | "B" => Some(Value::Int(bytes.b.into())),
                "c" | "C" => Some(Value::Int(bytes.c.into())),
                "d" | "D" => Some(Value::Int(bytes.d.into())),
                "e" | "E" => Some(Value::Int(bytes.e.into())),
                "h" | "H" => Some(Value::Int(bytes.h.into())),
                "l" | "L" => Some(Value::Int(bytes.l.into())),
                "af" | "AF" => Some(Value::Int(words.af.into())),
                "bc" | "BC" => Some(Value::Int(words.bc.into())),
                "de" | "DE" => Some(Value::Int(words.de.into())),
                "hl" | "HL" => Some(Value::Int(words.hl.into())),
                "sp" | "SP" => Some(Value::Int(words.sp.into())),
                "pc" | "PC" => Some(Value::Int(words.pc.into())),
                name => if let Some(symbol) = provider.resolve_symbol_by_name(name) {
                    let addr = symbol.address().address();
                    if symbol.is_variable() {
                        match symbol.size() {
                            0 => None,
                            1 => {
                                Some(Value::Int(core.read_memory_safe(addr).into()))
                            },
                            2 => {
                                Some(Value::Int((
                                    core.read_memory_safe(addr) as u16 |
                                    ((core.read_memory_safe(addr.saturating_add(1)) as u16) << 8)

                                ).into()))
                            },
                            n => {
                                let mut bytes = Vec::with_capacity(n as usize);
                                for i in 0..n {
                                    bytes.push(core.read_memory_safe(addr.saturating_add(i)));
                                }
                                Some(Value::String(format!("{:?}", bytes)))
                            }
                        }

                    } else {
                        None
                    }

                } else {
                    None
                }
            } {
                self.values.insert(s.to_string(), value);
            }
        }
    }

    pub fn postset<T: SymbolProvider>(&mut self, _core: &dyn ReadableCore, _provider: &T, _node: &Node) {
        for (_k, _value) in self.set_values.drain() {
            // TODO apply values recorded inside set_value()
        }
    }
}

impl Context for EvaluatorContext {
    fn get_value(&self, s: &str) -> Option<&Value> {
        self.values.get(s)
    }

    fn call_function(&self, _: &str, _: &Value) -> EvalexprResult<Value> {
        Err(EvalexprError::CustomMessage("functions not supported".to_string()))
    }
}

impl ContextWithMutableVariables for EvaluatorContext {
    fn set_value(&mut self, key: String, value: Value) -> EvalexprResult<()> {
        self.set_values.insert(key, value);
        Ok(())
    }
}

