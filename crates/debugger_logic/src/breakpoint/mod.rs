// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use serde::{Serialize, Deserialize};
use analyzer::{Breakpoint, SymbolProvider};
use sameboy_rs::{BankedAddress, ReadableCore};


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod condition;
mod evaluator;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::DebuggerLogic;
use condition::Condition;
pub use evaluator::EvaluatorContext;


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct DebuggerBreakpoint {
    #[serde(default, skip_serializing)]
    pub changed: Option<BankedAddress>,
    pub address: BankedAddress,
    pub active: bool,
    #[serde(default, skip_serializing)]
    pub software: bool,
    #[serde(default)]
    condition: Condition,
    #[serde(default)]
    hit_condition: Condition,
    #[serde(default, skip_serializing)]
    hit_count: i64
}

impl Breakpoint for DebuggerBreakpoint {
    fn from_software(address: BankedAddress) -> Self {
        Self {
            changed: None,
            address,
            active: true,
            software: true,
            condition: Condition::None,
            hit_condition: Condition::None,
            hit_count: 0
        }
    }

    fn active(&self) -> bool {
        self.active
    }

    fn software(&self) -> bool {
        self.software
    }

    fn address(&self) -> BankedAddress {
        self.address
    }

    fn evaluate<T: SymbolProvider>(
        &mut self,
        core: &dyn ReadableCore,
        software_break_enabled: bool,
        provider: &T

    ) -> bool {
        // Skip non-active breakpoints
        if !self.active {
            return false;
        }

        // Skip if software breakpoint and those are not enabled
        if self.software && !software_break_enabled {
            return false;
        }

        // Check if there is a condition that must be met for the breakpoint to become active
        let active = if let Condition::Some((condition, _)) = &self.condition {
            let mut ctx = EvaluatorContext::default();
            ctx.prefetch(core, provider, condition);
            condition.eval_boolean_with_context(&ctx).unwrap_or(false)

        } else {
            // No condition, breakpoint is always active
            true
        };

        // If the breakpoint is active and there is also a condition of the numbre of hits
        // also check that
        if let (true, Condition::Some((condition, _))) = (active, &self.hit_condition) {
            // Increase hit counter
            self.hit_count += 1;

            // Check if we reached the required number of hits and break
            if Ok(self.hit_count) == condition.eval_int() {
                self.hit_count = 0;
                true

            } else {
                false
            }

        } else {
            // No hit condition
            active
        }
    }
}

impl DebuggerBreakpoint {
    pub fn new(address: BankedAddress, condition: Option<String>, hit_condition: Option<String>) -> Self {
        Self {
            changed: Some(address),
            address,
            active: true,
            software: false,
            condition: condition.map(|s| Condition::from_str(&s)).unwrap_or(Condition::None),
            hit_condition: hit_condition.map(|s| Condition::from_str(&s)).unwrap_or(Condition::None),
            hit_count: 0
        }
    }

    pub fn as_str(&self) -> &str {
        self.condition.as_str()
    }

    pub fn address_string(&self) -> String {
        self.address.to_label_string()
    }

    pub fn update_address(&mut self, addr: &str) {
        self.address = DebuggerLogic::parse_address(addr).unwrap_or(self.address);
    }

    pub fn update_condition(&mut self, condition: &str) {
        self.condition = Condition::from_str(condition);
    }
}

