// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::fmt;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use evalexpr::{Node, build_operator_tree};
use serde::{Serialize, Serializer, Deserialize, Deserializer};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, PartialEq)]
pub enum Condition {
    Some((Node, String)),
    None
}

impl Eq for Condition {}

impl Default for Condition {
    fn default() -> Self {
        Self::None
    }
}

impl Condition {
    pub fn as_str(&self) -> &str {
        match self {
            Self::Some((_, s)) => s.as_str(),
            Self::None => "-"
        }
    }

    pub fn from_str(s: &str) -> Self {
        match build_operator_tree(s).ok() {
            Some(node) => Condition::Some((node, s.to_string())),
            None => Condition::None
        }
    }
}

impl Serialize for Condition {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self {
            Self::Some((_, s)) => {
                serializer.serialize_str(s)
            },
            Self::None => {
                serializer.serialize_none()
            }
        }
    }
}

impl<'de> Deserialize<'de> for Condition {
    fn deserialize<D>(deserializer: D) -> Result<Condition, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct StringVisitor;
        impl<'de> serde::de::Visitor<'de> for StringVisitor {
            type Value = Condition;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("a string containing an expression")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E> where E: serde::de::Error, {
                Ok(Condition::from_str(v))
            }
        }
        deserializer.deserialize_str(StringVisitor)
    }
}

