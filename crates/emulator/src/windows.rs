// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::rc::Rc;
use std::cell::RefCell;
use std::time::{Instant, Duration};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use mwx::WindowInterface;
use debugger_ui::DebuggerUI;
use debugger_data::{DebuggerState, DebuggerEvent};
use sameboy_rs::{Key, ReadableCore, WritableCore};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use super::{Emulator, DMG_PALETTE};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const DEBUGGER_WINDOW_WIDTH: u32 = 596;
const DEBUGGER_WINDOW_HEIGHT: u32 = 758;
const SGB_PIXEL_WIDTH: u32 = 256;
const SGB_PIXEL_HEIGHT: u32 = 224;
const SGB_PIXEL_WIDTH_OFFSET: u32 = 48;
const SGB_PIXEL_HEIGHT_OFFSET: u32 = 40;
const GAMEBOY_PIXEL_WIDTH: u32 = 160;
const GAMEBOY_PIXEL_HEIGHT: u32 = 144;
const GAMEBOY_HEIGHT_OFFSET: u32 = 10;
const GAMEBOY_HEIGHT_VBLANK: u32 = 10;
const GAMEBOY_WIDTH_HBLANK: u32 = 4;
const GAMEBOY_WIDTH_HBLANK_OFFSET: u32 = 0;


// Emulator Window ------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct EmulatorWindow {
    ui: Rc<RefCell<Option<DebuggerUI>>>,
    required_pixel_size: (u32, u32),
    title_update: Instant
}

impl EmulatorWindow {
    pub fn build(app: &mut mwx::App<Emulator>, ui: Rc<RefCell<Option<DebuggerUI>>>) -> Option<mwx::handle::Window> {
        app.window()
            .title("emulator")
            .minimum_size((GAMEBOY_PIXEL_WIDTH, GAMEBOY_PIXEL_HEIGHT))
            .resizeable(true)
            .pixels_size((GAMEBOY_PIXEL_WIDTH, GAMEBOY_PIXEL_HEIGHT))
            .clear_color([0.1, 0.1, 0.1, 1.0])
            .priority(0)
            .build(EmulatorWindow {
                ui,
                required_pixel_size: (0, 0),
                title_update: Instant::now().checked_sub(Duration::from_millis(1000)).unwrap_or_else(Instant::now)
            }).ok()
    }

    pub fn pixel_buffer() -> Vec<u32> {
        vec![0; (SGB_PIXEL_WIDTH * SGB_PIXEL_HEIGHT) as usize]
    }

    fn screen_pixel_width(&self, state: &DebuggerState) -> u32 {
        if state.core.is_sgb() {
            SGB_PIXEL_WIDTH

        } else if state.config.opened {
            GAMEBOY_PIXEL_WIDTH + GAMEBOY_WIDTH_HBLANK

        } else {
            GAMEBOY_PIXEL_WIDTH
        }
    }

    fn screen_pixel_height(&self, state: &DebuggerState) -> u32 {
        if state.core.is_sgb() {
            SGB_PIXEL_HEIGHT

        } else if state.config.opened {
            GAMEBOY_PIXEL_HEIGHT + GAMEBOY_HEIGHT_VBLANK + GAMEBOY_HEIGHT_OFFSET

        } else {
            GAMEBOY_PIXEL_HEIGHT
        }
    }

    fn screen_pixel_rect(&self, state: &DebuggerState) -> (usize, usize, usize, usize) {
        if state.core.is_sgb() {
            (0, 0, SGB_PIXEL_WIDTH as usize, SGB_PIXEL_HEIGHT as usize)

        } else if state.config.opened {
            (
                GAMEBOY_WIDTH_HBLANK_OFFSET as usize,
                GAMEBOY_HEIGHT_OFFSET as usize,
                GAMEBOY_PIXEL_WIDTH as usize,
                GAMEBOY_PIXEL_HEIGHT as usize
            )

        } else {
            (0, 0, GAMEBOY_PIXEL_WIDTH as usize, GAMEBOY_PIXEL_HEIGHT as usize)
        }
    }

    fn screen_ui_rect(&self, state: &DebuggerState) -> Option<(usize, usize, usize, usize)> {
        if state.config.opened {
            if state.core.is_sgb() {
                Some((
                    SGB_PIXEL_WIDTH_OFFSET as usize,
                    SGB_PIXEL_HEIGHT_OFFSET as usize,
                    SGB_PIXEL_WIDTH as usize,
                    SGB_PIXEL_HEIGHT as usize
                ))

            } else {
                Some((
                    GAMEBOY_WIDTH_HBLANK_OFFSET as usize,
                    GAMEBOY_HEIGHT_OFFSET as usize,
                    GAMEBOY_PIXEL_WIDTH as usize,
                    (GAMEBOY_HEIGHT_OFFSET + GAMEBOY_PIXEL_HEIGHT) as usize
                ))
            }

        } else {
            None
        }
    }
}

impl WindowInterface<Emulator> for EmulatorWindow {
    fn event(&mut self, window: &mut mwx::Window<DebuggerState>, event: mwx::event::Window) {
        match event {
            mwx::event::Window::Init => {
                let required_pixel_size = (self.screen_pixel_width(window.state), self.screen_pixel_height(window.state));
                window.set_minimum_size((required_pixel_size.0, required_pixel_size.1));
            },
            mwx::event::Window::Tick => {
                if self.title_update.elapsed() > Duration::from_millis(500) {
                    window.set_title(format!("GameBoy Emulator | {}", window.state.description()));
                    self.title_update = Instant::now();
                }

                // GameBoy Input
                window.state.core.set_key_state(Key::B, window.input.key_held(mwx::Key::A));
                window.state.core.set_key_state(Key::A, window.input.key_held(mwx::Key::S));
                window.state.core.set_key_state(Key::Start, window.input.key_held(mwx::Key::Return));
                window.state.core.set_key_state(Key::Select, window.input.key_held(mwx::Key::RShift));
                window.state.core.set_key_state(Key::Left, window.input.key_held(mwx::Key::Left));
                window.state.core.set_key_state(Key::Right, window.input.key_held(mwx::Key::Right));
                window.state.core.set_key_state(Key::Down, window.input.key_held(mwx::Key::Down));
                window.state.core.set_key_state(Key::Up, window.input.key_held(mwx::Key::Up));

                // Forward Frame and Line Advance and Pause/Resume to Debugger
                if window.input.key_pressed_os(mwx::Key::F) {
                    window.event(DebuggerEvent::FrameAdvance(!window.input.key_held(mwx::Key::LShift)));

                } else if window.input.key_pressed_os(mwx::Key::L) {
                    window.event(DebuggerEvent::LineAdvance);

                } else if window.input.key_pressed(mwx::Key::P) {
                    window.event(DebuggerEvent::TogglePause);

                } else if window.input.key_pressed(mwx::Key::R) {
                    window.event(DebuggerEvent::Reset);
                }

                // Re-scale Window
                let rescale = if window.input.key_pressed(mwx::Key::Key1) {
                    Some(1)

                } else if window.input.key_pressed(mwx::Key::Key2) {
                    Some(2)

                } else if window.input.key_pressed(mwx::Key::Key3) {
                    Some(3)

                } else if window.input.key_pressed(mwx::Key::Key4) {
                    Some(4)

                } else if window.input.key_pressed(mwx::Key::Key5) {
                    Some(5)

                } else {
                    None
                };
                if let Some(scale) = rescale {
                    window.set_minimum_size((self.required_pixel_size.0, self.required_pixel_size.1));
                    window.set_size((self.required_pixel_size.0 * scale, self.required_pixel_size.1 * scale));
                }

                // Close Window
                if window.input.key_released(mwx::Key::Escape) {
                    window.close();

                // Open Debugger Window
                } else if window.input.key_released(mwx::Key::Tab) {
                    window.event(DebuggerEvent::Launch);
                }
            },
            mwx::event::Window::RenderUI(ui) => {
                if let Some((x, y, w, h)) = self.screen_ui_rect(window.state) {
                    let (tl, br) = ui.pixel_offset_rect([x as f32, y as f32], [w as f32, h as f32]);
                    if let Some(dbg_ui) = self.ui.borrow_mut().as_mut() {
                        dbg_ui.draw_overlay(window.state, ui, tl, br);
                    }
                }
            },
            mwx::event::Window::RenderPixels(pixels) => {
                // Switch between normal / SGB screen sizes
                let required_pixel_size = (self.screen_pixel_width(window.state), self.screen_pixel_height(window.state));
                if required_pixel_size != self.required_pixel_size {
                    pixels.resize(required_pixel_size);
                    self.required_pixel_size = required_pixel_size;
                }

                // Copy GameBoy screen into pixels window
                let gb_screen = self.screen_pixel_rect(window.state);
                let pixel_width = required_pixel_size.0;
                for (i, pixel) in pixels.frame().chunks_exact_mut(4).enumerate() {
                    let mut x = i % pixel_width as usize;
                    let mut y = i / pixel_width as usize;
                    if x >= gb_screen.0 && x < gb_screen.0 + gb_screen.2 && y >= gb_screen.1 && y < gb_screen.1 + gb_screen.3 {
                        x -= gb_screen.0;
                        y -= gb_screen.1;

                        let p = window.state.pixel_buffer[y * gb_screen.2 + x];
                        pixel.copy_from_slice(&[
                            (p >> 24) as u8,
                            (p >> 16) as u8,
                            (p >> 8) as u8,
                            255
                        ]);
                    } else {
                        pixel.copy_from_slice(&[64, 64, 64, 255]);
                    }
                }
            },
            mwx::event::Window::DroppedFile(path) => {
                window.event(DebuggerEvent::LoadRom(path));
            }
        }
    }
}


// Debugger Window ------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct DebuggerWindow {
    ui: Rc<RefCell<Option<DebuggerUI>>>,
    title_update: Instant
}

impl DebuggerWindow {
    pub fn build(ctx: &mut mwx::App<Emulator>, ui: Rc<RefCell<Option<DebuggerUI>>>) -> Option<mwx::handle::Window> {
        ctx.window()
            .title("debugger")
            .minimum_size((DEBUGGER_WINDOW_WIDTH, DEBUGGER_WINDOW_HEIGHT))
            .clear_color([0.0, 0.0, 0.0, 1.0])
            .priority(1)
            .use_icons(debugger_ui::used_icons_list())
            .build(Self {
                ui,
                title_update: Instant::now().checked_sub(Duration::from_millis(1000)).unwrap_or_else(Instant::now)

            }).ok()
    }
}

impl WindowInterface<Emulator> for DebuggerWindow {
    fn event(&mut self, window: &mut mwx::Window<DebuggerState>, event: mwx::event::Window) {
        match event {
            mwx::event::Window::Init => {
                let mut dbg_ui = DebuggerUI::new(window.state);
                dbg_ui.set_dmg_palette(DMG_PALETTE);
                *self.ui.borrow_mut() = Some(dbg_ui);
            },
            mwx::event::Window::Tick => {
                if self.title_update.elapsed() > Duration::from_millis(500) || window.state.logic.did_step() {
                    window.set_title(format!("GameBoy Debugger | {}", window.state.debug_description(window)));
                    self.title_update = Instant::now();
                }
                if let Some(dbg_ui) = self.ui.borrow_mut().as_mut() {
                    dbg_ui.update(window.state);
                }
            },
            mwx::event::Window::RenderUI(ui) => {
                if let Some(dbg_ui) = self.ui.borrow_mut().as_mut() {
                    dbg_ui.draw(window, ui);
                }
            },
            mwx::event::Window::RenderPixels(_) => {},
            mwx::event::Window::DroppedFile(_) => {}
        }
    }

    fn closed(self, _ctx: &mut mwx::Window<DebuggerState>) {
        self.ui.borrow_mut().take();
    }
}

