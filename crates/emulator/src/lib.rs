// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::rc::Rc;
use std::cell::RefCell;
use std::path::PathBuf;
use std::sync::mpsc::Receiver;
use std::collections::VecDeque;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use mwx::AppInterface;
use debugger_logic::DebuggerLogic;
use debugger_ui::{DebuggerUI, DebuggerContextMenu};
use sameboy_rs::{CallbackCore, ReadableCore, WritableCore, RunnableCore};
use debugger_data::{DebuggerCache, DebuggerConfig, DebuggerEvent, DebuggerState};


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod windows;
mod socket;


// Re-Exports Dependencies ----------------------------------------------------
// ----------------------------------------------------------------------------
pub use sameboy_rs::Model;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use windows::{EmulatorWindow, DebuggerWindow};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const AGB_BOOT_ROM: &[u8] = include_bytes!("./boot/agb_boot.bin");
const CGB_BOOT_ROM: &[u8] = include_bytes!("./boot/cgb_boot.bin");
const DMG_BOOT_ROM: &[u8] = include_bytes!("./boot/dmg_boot.bin");
const MGB_BOOT_ROM: &[u8] = include_bytes!("./boot/mgb_boot.bin");
const SGB2_BOOT_ROM: &[u8] = include_bytes!("./boot/sgb2_boot.bin");
const SGB_BOOT_ROM: &[u8] = include_bytes!("./boot/sgb_boot.bin");

const DMG_PALETTE: [[u8; 3]; 5] = [
    [0x08, 0x18, 0x10],
    [0x39, 0x61, 0x39],
    [0x84, 0xa5, 0x63],
    [0xc6, 0xde, 0x8c],
    [0xd2, 0xe6, 0xa6]
];


// Emulator ------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Emulator {
    state: DebuggerState,
    model: Option<Model>,
    rom: Vec<u8>,

    skip_boot_rom: bool,
    start_with_debugger_opened: bool,

    audio_context: Option<mwx::Audio>,
    socket_handle: Option<Receiver<PathBuf>>,
    emulator_window: Option<mwx::handle::Window>,

    ui: Rc<RefCell<Option<DebuggerUI>>>,
    debugger_window: Option<mwx::handle::Window>,
}

impl Emulator {
    pub fn new(model: Option<Model>, rom_path: Option<PathBuf>, source_dir: PathBuf) -> Self {
        Self {
            state: DebuggerState {
                rom_path,
                pixel_buffer: EmulatorWindow::pixel_buffer(),
                logs: Rc::new(RefCell::new(VecDeque::new())),
                dap: None,
                cache: DebuggerCache::default(),
                core: RunnableCore::from_model(Model::DMG),
                logic: DebuggerLogic::new(source_dir),
                config: DebuggerConfig::default()
            },
            model,
            rom: Vec::new(),

            skip_boot_rom: false,
            start_with_debugger_opened: false,

            audio_context: None,
            socket_handle: None,
            emulator_window: None,

            ui: Rc::new(RefCell::new(None)),
            debugger_window: None
        }
    }

    pub fn set_skip_boot_rom(&mut self, skip: bool) {
        self.skip_boot_rom = skip;
    }

    pub fn set_start_halted(&mut self, halt: bool) {
        if halt {
            self.state.logic.halt();
        }
    }

    pub fn set_start_with_debugger_opened(&mut self, open: bool) {
        self.start_with_debugger_opened = open;
    }

    pub fn set_launch_stdio_dap(&mut self, enabled: bool) {
        if enabled {
            self.state.enable_dap();
            self.start_with_debugger_opened = true;
        }
    }

    pub fn launch(mut self, forward: bool) {
        let forwarded = forward && socket::send(self.state.rom_path.as_ref()).unwrap_or(false);
        if !forwarded {
            self.socket_handle = socket::listen().ok();
            self.run(Some(60)).expect("Emulator failed");
        }
    }
}

impl Emulator {
    fn default_rom() -> Vec<u8> {
        let mut default: Vec<u8> = std::iter::repeat(0x0).take(0x200).collect();
        default[0x100] = 0x76; // halt
        default[0x101] = 0x00; // nop
        default[0x102] = 0x18; // jr
        default[0x103] = 0xFC; // -2
        default
    }

    fn load_rom(&mut self) {
        // Load ROM from path and fallback to a halting default
        if let Some(p) = &self.state.rom_path {
            if let Ok(bytes) = std::fs::read(p) {
                self.rom = bytes;

            } else {
                self.state.rom_path = None;
                self.rom = Self::default_rom();
            }
        } else {
            self.rom = Self::default_rom();
        }
        // Pad ROM to avoid segfaults in sameboy core
        while self.rom.len() < 32768 {
            self.rom.push(0);
        }
        self.state.logic.load_maps(self.state.rom_path.clone());
    }

    fn reset(&mut self) {
        self.reset_with_model(self.state.core.model());
        self.state.pixel_buffer = EmulatorWindow::pixel_buffer();
    }

    fn reset_with_model(&mut self, model: Model) {
        self.state.core = RunnableCore::from_model(model);

        // Display
        self.state.core.set_dmg_palette(DMG_PALETTE);
        self.state.core.set_rgb_encode_callback(Some(Box::new(|_, r, g, b| {
            ((r as u32) << 24) | ((g as u32) << 16) | ((b as u32) << 8)
        })));

        // Audio
        if let Some(audio) = self.audio_context.clone() {
            self.state.core.set_sample_rate(48000);
            self.state.core.set_apu_sample_callback(Some(Box::new(move |_, left, right| {
                audio.push_sample(
                    left as f32 / i16::MAX as f32,
                    right as f32 / i16::MAX as f32
                );
            })));
        }

        // Logs
        let logs = self.state.logs.clone();
        self.state.core.set_log_callback(Some(Box::new(move |core, msg| {
            let pc = core.pc();
            logs.borrow_mut().push_back((
                (core.current_line(), 0, pc, 0),
                msg
            ));
        })));

        // ROM
        match model {
            Model::AGB => self.state.core.set_boot_rom_from_slice(AGB_BOOT_ROM),
            Model::CGB => self.state.core.set_boot_rom_from_slice(CGB_BOOT_ROM),
            Model::DMG => self.state.core.set_boot_rom_from_slice(DMG_BOOT_ROM),
            Model::MGB => self.state.core.set_boot_rom_from_slice(MGB_BOOT_ROM),
            Model::SGB2 => self.state.core.set_boot_rom_from_slice(SGB2_BOOT_ROM),
            Model::SGB => self.state.core.set_boot_rom_from_slice(SGB_BOOT_ROM)
        }
        self.state.core.set_rom_from_slice(&self.rom);

        // Debugger
        if let Some(ui) = &mut *self.ui.borrow_mut() {
            let was_halted = self.state.logic.is_halted();
            self.state.logic.reset(&mut self.state.core);
            self.state.cache.reset();
            if was_halted {
                self.state.logic.halt();
            }
            ui.reset();
        }
    }
}

impl AppInterface for Emulator {
    type Output = ();
    type State = DebuggerState;
    type ContextMenu = DebuggerContextMenu;

    fn state_mut(&mut self) -> &mut DebuggerState {
        &mut self.state
    }

    fn event(&mut self, app: &mut mwx::App<Self>, audio: &mut mwx::Audio, event: mwx::event::App<Self>) {
        match event {
            mwx::event::App::Init => {
                // Setup Audio
                if audio.initialize(48000, 2).is_ok() {
                    self.audio_context = Some(audio.clone());
                }

                // Setup ROM
                self.load_rom();

                // Setup Emulation Core
                let model = self.model.unwrap_or_else(|| self.state.config.model.into());
                self.reset_with_model(model);

                // Create Debugger Window if it was open last time we exited
                self.state.config.system_skip_boot_rom |= self.skip_boot_rom;
                if self.state.config.opened || self.start_with_debugger_opened {
                    app.event(DebuggerEvent::Launch);
                }

                // Create Emulator Window
                self.emulator_window = EmulatorWindow::build(app, self.ui.clone());
            },
            mwx::event::App::Tick => {
                // Check for incoming ROM load requests
                if let Some(handle) = &self.socket_handle {
                    while let Ok(rom_path) = handle.try_recv() {
                        self.state.rom_path = Some(rom_path);
                        self.state.logic.request_reload();
                    }
                }

                // Exit when EmulatorWindow is closed or DAP tells debugger to terminate
                if self.emulator_window.is_none() || self.state.logic.should_terminate() {
                    app.exit();

                // Debugger drives emulation loop when opened
                } else if self.debugger_window.is_some() {
                    // Wait for the UI to be ready to improve startup responsibility
                    if self.ui.borrow().as_ref().map(|ui| ui.is_ready()).unwrap_or(false) {
                        if self.state.logic.should_reload() {
                            self.load_rom();
                        }

                        if self.state.logic.should_reset() {
                            self.reset();
                        }

                        if let Some(model) = self.state.logic.should_switch_model() {
                            self.reset_with_model(model);
                        }

                        self.state.logic.run_frame(
                            &mut self.state.core,
                            &mut self.state.pixel_buffer[..],
                            &mut self.state.dap,
                            self.state.config.system_skip_boot_rom,
                            |silent| audio.set_silent(silent)
                        );
                    }

                // Skip boot rom if desired
                } else if self.skip_boot_rom && !self.state.core.is_boot_rom_finished() {
                    audio.set_silent(true);
                    self.state.core.skip_bootrom();
                    audio.set_silent(false);

                } else {
                    // Always resume emulation core if debugger is not visible
                    if self.state.core.is_debug_stopped() {
                        self.state.core.set_debug_stopped(false);
                    }
                    self.state.core.run_frame(&mut self.state.pixel_buffer[..]);
                }
                self.state.config.opened = self.debugger_window.is_some();
            },
            mwx::event::App::Action(action) => {
                if let Some(ui) = &mut *self.ui.borrow_mut() {
                    ui.action(&mut self.state, action)
                }
            },
            mwx::event::App::WindowClosed(id) => {
                if self.emulator_window.as_ref().map(|h| h.id()) == Some(id) {
                    self.emulator_window.take();

                } else if self.debugger_window.as_ref().map(|h| h.id()) == Some(id) {
                    self.debugger_window.take();
                    self.state.logic.hide(&mut self.state.core);
                }
            },
            mwx::event::App::State(DebuggerEvent::Launch) => if self.debugger_window.is_none() {
                self.debugger_window = DebuggerWindow::build(app, self.ui.clone());
                self.state.logic.show(&mut self.state.core, self.state.config.system_halt_on_debugger);
            },
            mwx::event::App::State(DebuggerEvent::FrameAdvance(sync_vblank)) => if self.debugger_window.is_some() {
                self.state.logic.advance_frames(1, sync_vblank);
            },
            mwx::event::App::State(DebuggerEvent::LineAdvance) => if self.debugger_window.is_some() {
                self.state.logic.advance_lines(1);
            },
            mwx::event::App::State(DebuggerEvent::TogglePause) => if self.debugger_window.is_some() {
                self.state.logic.toggle_pause();
            },
            mwx::event::App::State(DebuggerEvent::Reset) => {
                self.reset();
            },
            mwx::event::App::State(DebuggerEvent::LoadRom(rom_path)) => {
                self.state.rom_path = Some(rom_path);
                self.state.logic.request_reload();
            }
        }
    }

    fn closed(self, _ctx: &mut mwx::App<Self>) -> mwx::Output<()> {
        Ok(())
    }
}

