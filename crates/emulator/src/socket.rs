// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::thread;
use std::error::Error;
use std::path::PathBuf;
use std::time::Duration;
use std::io::{Write, Read};
use std::sync::mpsc::{self, Receiver};
use std::net::{SocketAddr, TcpListener, TcpStream};


// Socket Communcation --------------------------------------------------------
// ----------------------------------------------------------------------------
pub fn listen() -> Result<Receiver<PathBuf>, Box<dyn Error>> {
    let socket = TcpListener::bind("127.0.0.1:0")?;
    std::fs::write(addr_file()?, socket.local_addr()?.to_string())?;
    log::info!("Command receiver listening on {}", socket.local_addr()?.to_string());

    let (tx, rx) = mpsc::channel::<>();
    thread::spawn(move || {
        loop {
            if let Ok((mut conn, _)) = socket.accept() {
                let mut s = String::new();
                if conn.read_to_string(&mut s).is_ok() {
                    let p = PathBuf::from(s);
                    log::info!("Command receiver: {:?}", p);
                    tx.send(p).ok();
                }
            }
        }
    });
    Ok(rx)
}

pub fn send(rom_path: Option<&PathBuf>) -> Result<bool, Box<dyn Error>> {
    if let Some(p) = rom_path {
        let addr: SocketAddr = std::fs::read_to_string(addr_file()?)?.parse()?;
        let mut s = TcpStream::connect_timeout(&addr, Duration::from_millis(25))?;
        s.set_nodelay(true)?;
        s.write_all(p.to_string_lossy().as_bytes())?;
        Ok(true)

    } else {
        Ok(false)
    }
}

fn addr_file() -> Result<PathBuf, Box<dyn Error>> {
    let mut file = dirs::cache_dir().ok_or("Unable to determine cache dir")?;
    file.push("gbd_socket_addr.sock");
    Ok(file)
}

