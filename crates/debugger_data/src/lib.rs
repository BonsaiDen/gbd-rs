// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::rc::Rc;
use std::path::PathBuf;
use std::cell::RefCell;
use std::collections::VecDeque;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::BankedAddress;
use analyzer::{CodeCache, MemoryCache,};


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod config;
mod state;


// Exports --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub use config::DebuggerConfig;
pub use state::DebuggerState;


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub type DebuggerLogs = Rc<RefCell<VecDeque<((u8, u8, BankedAddress, u32), String)>>>;

pub enum DebuggerEvent {
    Launch,
    FrameAdvance(bool),
    LineAdvance,
    TogglePause,
    Reset,
    LoadRom(PathBuf)
}

#[derive(Default)]
pub struct DebuggerCache {
    pub code: CodeCache,
    pub memory: MemoryCache
}

impl DebuggerCache {
    pub fn reset(&mut self) {
        self.code = CodeCache::default();
        self.memory = MemoryCache::default();
    }
}

