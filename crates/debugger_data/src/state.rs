// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::pin::Pin;
use std::path::PathBuf;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use mwx::State;
use dap::DapProtocol;
use sameboy_rs::{BankedAddress, BankedAddressRange, ReadableCore, RunnableCore};
use debugger_logic::{DebuggerLogic, DebuggerBreakpoint, DebuggerWatchpoint};
use analyzer::{Breakpoint, SymbolProvider, Watchpoint};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{DebuggerConfig, DebuggerCache, DebuggerEvent, DebuggerLogs};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct DebuggerState {
    pub rom_path: Option<PathBuf>,
    pub pixel_buffer: Vec<u32>,
    pub logs: DebuggerLogs,
    pub dap: Option<DapProtocol>,
    pub cache: DebuggerCache,
    pub core: Pin<Box<RunnableCore>>,
    pub logic: DebuggerLogic,
    pub config: DebuggerConfig,
}

impl State for DebuggerState {
    type Event = DebuggerEvent;
    type Config = DebuggerConfig;

    fn config_mut(&mut self) -> &mut Self::Config {
        &mut self.config
    }
}

impl DebuggerState {
    pub fn has_rom(&self) -> bool {
        self.rom_path.is_some()
    }

    pub fn rom_base_path(&self) -> Option<PathBuf> {
        self.rom_path.clone().map(|mut p| {
            p.pop();
            p
        })
    }

    pub fn enable_dap(&mut self) {
        self.dap = Some(DapProtocol::stdio());
    }

    pub fn has_dap(&self) -> bool {
        self.dap.is_some()
    }

    pub fn is_waiting_for_dap(&self) -> bool {
        self.dap.as_ref().map(|d| !d.is_connected()).unwrap_or(false)
    }

    pub fn watchpoint_mut(
        &mut self,
        range: BankedAddressRange

    ) -> Option<&mut DebuggerWatchpoint> {
        self.config.watchpoint_list.iter_mut().find(|w| w.start_address() == range.start() && w.end_address() == range.end())
    }

    pub fn breakpoint_mut(&mut self, addr: BankedAddress) -> Option<&mut DebuggerBreakpoint> {
        self.config.breakpoint_list.iter_mut().find(|b| b.address() == addr)
    }

    pub fn description(&self) -> String {
        self.rom_path.as_ref().and_then(|p| p.file_name()).map(|s|
            s.to_string_lossy().to_string()

        ).unwrap_or_else(|| "No ROM loaded".to_string())
    }

    pub fn debug_description(&self, window: &mwx::Window<DebuggerState>) -> String {
        if window.state.is_waiting_for_dap() {
            "Waiting for DAP connection".to_string()

        } else {
            let dap = if window.state.has_dap() { "| DAP " } else { "" };
            if self.logic.is_halted() {
                let pc = window.state.core.pc();
                let frame = self.logic.analyzer.borrow().frame().saturating_sub(self.logic.base_frame());
                let label = self.logic.resolve_symbol_name_relative(pc);
                format!("Halted @ {} {}| +{} frames | {:.2} FPS", label, dap, frame, window.fps())

            } else {
                format!("Running {}| {:.2} FPS", dap, window.fps())
            }
        }
    }
}

