// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::path::PathBuf;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use mwx::Config;
use sameboy_rs::BankedAddress;
use serde::{Serialize, Deserialize};
use debugger_logic::{DebuggerBreakpoint, DebuggerWatchpoint};


// Debugger State -------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct DebuggerConfig {
    #[serde(default)]
    pub model: usize,
    #[serde(default)]
    pub opened: bool,
    #[serde(skip, default)]
    pub tab_switch: usize,

    #[serde(default)]
    pub global_override_disable_halts: bool,
    #[serde(default)]
    pub global_override_disable_sound: bool,

    #[serde(default)]
    pub cpu: bool,
    #[serde(default = "default_true")]
    pub cpu_usage_include_sub: bool,
    #[serde(default = "default_false")]
    pub cpu_overlay_usage: bool,
    #[serde(default = "default_false")]
    pub cpu_average_line_usage: bool,

    #[serde(default)]
    pub code: bool,
    #[serde(default)]
    pub code_offset: (BankedAddress, usize),
    #[serde(default)]
    pub code_follow_pc: bool,
    #[serde(default)]
    pub code_search: String,
    #[serde(default)]
    pub code_overlay_calls: bool,
    #[serde(default)]
    pub code_show_line_values: bool,
    #[serde(default)]
    pub code_show_coverage: bool,
    #[serde(default = "default_true")]
    pub code_jump_on_halt: bool,

    #[serde(default)]
    pub memory: bool,
    #[serde(default)]
    pub memory_offset: (BankedAddress, usize),
    #[serde(default = "default_true")]
    pub memory_outline_changes: bool,
    #[serde(default = "default_true")]
    pub memory_underline_symbols: bool,
    #[serde(default)]
    pub memory_search: String,
    #[serde(default)]
    pub memory_follow_bc: bool,
    #[serde(default)]
    pub memory_follow_de: bool,
    #[serde(default)]
    pub memory_follow_hl: bool,

    #[serde(default)]
    pub display: bool,
    #[serde(default)]
    pub display_darken: bool,
    #[serde(default = "default_true")]
    pub display_enable_bg: bool,
    #[serde(default = "default_true")]
    pub display_enable_oam: bool,
    #[serde(default = "default_false")]
    pub display_show_ly: bool,
    #[serde(default = "default_false")]
    pub display_show_lyc: bool,
    #[serde(default = "default_false")]
    pub display_overlay_access: bool,
    #[serde(default = "default_false")]
    pub display_overlay_sprites: bool,

    #[serde(default)]
    pub tiles: bool,
    #[serde(default = "default_true")]
    pub tiles_use_palette: bool,
    #[serde(default = "default_false")]
    pub tiles_overlay_access: bool,
    #[serde(default = "default_false")]
    pub tiles_log_hdma_transfer: bool,

    #[serde(default)]
    pub bg_map: bool,
    #[serde(default)]
    pub bg_map_darken: bool,
    #[serde(default = "default_false")]
    pub bg_map_overlay_tile_id: bool,
    #[serde(default = "default_false")]
    pub bg_map_overlay_palette: bool,
    #[serde(default = "default_false")]
    pub bg_map_overlay_bank: bool,
    #[serde(default = "default_false")]
    pub bg_map_overlay_access: bool,
    #[serde(default = "default_true")]
    pub bg_map_outline_screen: bool,
    #[serde(default = "default_false")]
    pub bg_map_outline_sprites: bool,

    #[serde(default)]
    pub joypad: bool,
    #[serde(default)]
    pub joypad_emulate_key_bounce: bool,
    #[serde(default)]
    pub joypad_force_inputs_enable: bool,
    #[serde(default)]
    pub joypad_force_inputs_next_frame: bool,
    #[serde(default)]
    pub joypad_force_inputs_alternate: bool,
    #[serde(default)]
    pub joypad_force_up: bool,
    #[serde(default)]
    pub joypad_force_right: bool,
    #[serde(default)]
    pub joypad_force_down: bool,
    #[serde(default)]
    pub joypad_force_left: bool,
    #[serde(default)]
    pub joypad_force_start: bool,
    #[serde(default)]
    pub joypad_force_select: bool,
    #[serde(default)]
    pub joypad_force_a: bool,
    #[serde(default)]
    pub joypad_force_b: bool,

    #[serde(default = "default_true")]
    pub audio_enable: bool,
    #[serde(default = "default_true")]
    pub audio_enable_square_1: bool,
    #[serde(default = "default_true")]
    pub audio_enable_square_2: bool,
    #[serde(default = "default_true")]
    pub audio_enable_wave: bool,
    #[serde(default = "default_true")]
    pub audio_enable_noise: bool,

    #[serde(default)]
    pub system: bool,
    #[serde(default)]
    pub system_skip_boot_rom: bool,
    #[serde(default)]
    pub system_halt_after_boot_rom: bool,
    #[serde(default = "default_true")]
    pub system_overlay_frames: bool,
    #[serde(default = "default_true")]
    pub system_halt_on_debugger: bool,
    #[serde(default)]
    pub system_halt_on_blocked_vram_write: bool,
    #[serde(default)]
    pub system_halt_on_unitialized_vram_read: bool,
    #[serde(default)]
    pub system_halt_on_unitialized_ram_read: bool,
    #[serde(default)]
    pub system_halt_on_echo_ram_access: bool,
    #[serde(default)]
    pub system_halt_on_software_breakpoint: bool,
    #[serde(default)]
    pub system_halt_on_mbc_write: bool,
    #[serde(default)]
    pub system_overlay_mbc_access: bool,
    #[serde(default)]
    pub system_log_mbc_access: bool,

    #[serde(default)]
    pub logs: bool,
    #[serde(default)]
    pub logs_filter: String,

    #[serde(default)]
    pub symbols: bool,
    #[serde(default)]
    pub symbols_offset: usize,

    #[serde(default)]
    pub other: bool,

    #[serde(default)]
    pub watchpoints: bool,
    #[serde(default)]
    pub watchpoints_offset: usize,
    #[serde(default)]
    pub breakpoints_offset: usize,

    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub watchpoint_list: Vec<DebuggerWatchpoint>,

    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub breakpoint_list: Vec<DebuggerBreakpoint>
}

impl Config for DebuggerConfig {
    fn filename() -> Option<PathBuf> {
        if let Some(mut p) = dirs::config_dir() {
            p.push("gbd.toml");
            Some(p)

        } else {
            Some(PathBuf::from(".gbd.toml"))
        }
    }
}

impl Default for DebuggerConfig {
    fn default() -> Self {
        Self {
            model: 0,
            opened: false,
            tab_switch: 0,

            global_override_disable_halts: false,
            global_override_disable_sound: false,

            cpu: true,
            cpu_usage_include_sub: false,
            cpu_overlay_usage: false,
            cpu_average_line_usage: false,

            code: false,
            code_offset: (BankedAddress::zero(), 0),
            code_follow_pc: false,
            code_search: String::new(),
            code_overlay_calls: false,
            code_show_line_values: false,
            code_show_coverage: false,
            code_jump_on_halt: true,

            memory: false,
            memory_offset: (BankedAddress::zero(), 0),
            memory_outline_changes: true,
            memory_underline_symbols: true,
            memory_search: String::new(),
            memory_follow_bc: false,
            memory_follow_de: false,
            memory_follow_hl: false,

            display: false,
            display_darken: false,
            display_enable_bg: true,
            display_enable_oam: true,
            display_show_ly: false,
            display_show_lyc: false,
            display_overlay_access: false,
            display_overlay_sprites: false,

            tiles: false,
            tiles_use_palette: true,
            tiles_overlay_access: false,
            tiles_log_hdma_transfer: false,

            bg_map: false,
            bg_map_darken: false,
            bg_map_overlay_tile_id: false,
            bg_map_overlay_palette: false,
            bg_map_overlay_bank: false,
            bg_map_overlay_access: false,
            bg_map_outline_screen: true,
            bg_map_outline_sprites: false,

            joypad: false,
            joypad_emulate_key_bounce: true,
            joypad_force_inputs_enable: false,
            joypad_force_inputs_next_frame: false,
            joypad_force_inputs_alternate: false,
            joypad_force_up: false,
            joypad_force_right: false,
            joypad_force_down: false,
            joypad_force_left: false,
            joypad_force_start: false,
            joypad_force_select: false,
            joypad_force_a: false,
            joypad_force_b: false,

            audio_enable: true,
            audio_enable_square_1: true,
            audio_enable_square_2: true,
            audio_enable_wave: true,
            audio_enable_noise: true,

            system: false,
            system_skip_boot_rom: false,
            system_halt_after_boot_rom: false,
            system_overlay_frames: true,
            system_halt_on_debugger: true,
            system_halt_on_blocked_vram_write: false,
            system_halt_on_unitialized_vram_read: false,
            system_halt_on_unitialized_ram_read: false,
            system_halt_on_echo_ram_access: false,
            system_halt_on_software_breakpoint: false,
            system_halt_on_mbc_write: false,
            system_overlay_mbc_access: false,
            system_log_mbc_access: false,

            logs: false,
            logs_filter: String::new(),

            symbols: false,
            symbols_offset: 0,

            other: false,

            watchpoints: false,
            watchpoints_offset: 0,
            breakpoints_offset: 0,

            watchpoint_list: Vec::new(),
            breakpoint_list: Vec::new()
        }
    }
}

fn default_false() -> bool {
    false
}

fn default_true() -> bool {
    true
}

