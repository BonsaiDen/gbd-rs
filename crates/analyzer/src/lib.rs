// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::rc::Rc;
use std::cell::RefCell;
use std::collections::{HashMap, HashSet};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::{
    BankedAddress,
    CallbackCore, ReadableCore,
    RunnableCore,
    WordRegisters, WritableCore, WriteCallbackCore
};


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod cpu;
mod cache;
pub use cache::*;
mod types;
pub use self::types::*;


// Re-Exports -----------------------------------------------------------------
// ----------------------------------------------------------------------------
pub use cpu::CPU_STAT_HISTORY;
pub use gb_cpu::{Argument as InstructionArgument, Instruction};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use cpu::AnalyzerCPU;


// Enums ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum ControlFlow {
    Running,
    Halted,
    StepInto(usize),
    StepOver(usize, i32),
    ReturnFromCalls(i32),
    AdvanceFrames(usize),
    AdvanceLines(usize)
}


// Analyzer Logic -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Analyzer<B: 'static + Breakpoint, W: 'static + Watchpoint> {
    control: ControlFlow,
    frame: u32,
    stack_base: u16,
    boot_rom_finished: bool,

    halt_after_boot_rom: bool,
    halt_on_blocked_vram_write: bool,
    halt_on_unitialized_vram_read: bool,
    halt_on_unitialized_ram_read: bool,
    halt_on_echo_ram_access: bool,
    halt_on_software_break: bool,
    halt_on_mbc_write: bool,

    halt_information: Option<HaltInformation>,
    coverage: HashSet<BankedAddress>,
    callframes: Vec<CallFrame>,
    backtraces: Vec<CallBacktrace>,
    breakpoints: HashMap<BankedAddress, B>,
    breakpoints_active: usize,
    watchpoints: HashMap<usize, W>,
    watchpoints_active: bool,
    watchpoints_read_active: bool,
    symbols: SymbolCache,

    messages: Vec<Message>,
    log_hdma_transfer: bool,
    log_mbc_access: bool,

    oam: [u8; 160],
    map_tiles: [u8; 1024],
    ram_initialized: HashSet<BankedAddress>,
    hdma_source: u16,
    hdma_destination: u16,

    cpu: AnalyzerCPU,
    call_infos: HashMap<BankedAddress, CallInformation>,
    branch_infos: HashMap<BankedAddress, BranchInformation>,

    access_reads: usize,
    access_writes: usize,

    mbc_rom_bank: u16,
    mbc_ram_bank: u8,
    mbc_ram_enable: bool,
    mbc_access: HashMap<MbcAccessKey, Vec<MbcAccess>>,
    mbc_access_writes: usize,
    mbc_access_reads: usize,

    vram_update: VramUpdate,
    vram_access: HashMap<VramAccessKey, Vec<VramAccess>>,
    vram_access_writes: usize,
    vram_access_reads: usize,

    display_access: HashMap<DisplayAccessKey, Vec<DisplayAccess>>,
    display_access_writes: usize,
    display_access_reads: usize,

    audio_access: HashMap<AudioAccessKey, Vec<AudioAccess>>,
    audio_access_writes: usize,
    audio_access_reads: usize,

    oam_access_writes: usize,
    joypad_access_reads: usize,
    joypad_access_writes: usize,
    bg_scw_access_writes: usize,
    bg_tile_access_writes: usize,
}

impl<B: 'static + Breakpoint, W: 'static + Watchpoint> Analyzer<B, W> {
    pub fn new() -> Rc<RefCell<Self>> {
        Rc::new(RefCell::new(Self {
            control: ControlFlow::Running,
            frame: 0,
            stack_base: 0x2000,
            boot_rom_finished: false,

            halt_after_boot_rom: false,
            halt_on_blocked_vram_write: false,
            halt_on_unitialized_vram_read: false,
            halt_on_unitialized_ram_read: false,
            halt_on_echo_ram_access: false,
            halt_on_software_break: false,
            halt_on_mbc_write: false,

            halt_information: None,
            coverage: HashSet::with_capacity(32768),
            callframes: Vec::with_capacity(32),
            backtraces: Vec::with_capacity(32),
            breakpoints: HashMap::with_capacity(32),
            breakpoints_active: 0,
            watchpoints: HashMap::with_capacity(32),
            watchpoints_active: false,
            watchpoints_read_active: false,
            symbols: SymbolCache::default(),

            messages: Vec::new(),
            log_hdma_transfer: false,
            log_mbc_access: false,

            oam: [0; 160],
            map_tiles: [0; 1024],
            ram_initialized: HashSet::with_capacity(16384),
            hdma_source: 0,
            hdma_destination: 0,

            cpu: AnalyzerCPU::new(),
            call_infos: HashMap::with_capacity(512),
            branch_infos: HashMap::with_capacity(512),

            access_reads: 0,
            access_writes: 0,

            mbc_rom_bank: 1,
            mbc_ram_bank: 0,
            mbc_ram_enable: false,
            mbc_access: HashMap::with_capacity(64),
            mbc_access_writes: 0,
            mbc_access_reads: 0,

            vram_update: VramUpdate::default(),
            vram_access: HashMap::with_capacity(2048),
            vram_access_writes: 0,
            vram_access_reads: 0,

            display_access: HashMap::with_capacity(512),
            display_access_writes: 0,
            display_access_reads: 0,

            audio_access: HashMap::with_capacity(128),
            audio_access_writes: 0,
            audio_access_reads: 0,

            oam_access_writes: 0,
            joypad_access_reads: 0,
            joypad_access_writes: 0,
            bg_scw_access_writes: 0,
            bg_tile_access_writes: 0,
        }))
    }

    pub fn in_boot_rom(&self) -> bool {
        !self.boot_rom_finished
    }

    pub fn is_halted(&self) -> bool {
        self.control != ControlFlow::Running
    }

    pub fn halt_information(&self) -> Option<HaltInformation> {
        self.halt_information.clone()
    }

    pub fn vram_update(&mut self) -> VramUpdate {
        std::mem::take(&mut self.vram_update)
    }

    pub fn stack_base(&self) -> u16 {
        self.stack_base
    }

    pub fn coverage(&self) -> &HashSet<BankedAddress> {
        &self.coverage
    }

    pub fn backtraces(&self) -> &[CallBacktrace] {
        &self.backtraces
    }

    pub fn frame(&self) -> u32 {
        self.frame
    }

    pub fn call_usage(&self) -> &HashMap<BankedAddress, CallUsage> {
        &self.cpu.usage
    }

    pub fn call_infos(&self) -> &HashMap<BankedAddress, CallInformation> {
        &self.call_infos
    }

    pub fn branch_infos(&self) -> &HashMap<BankedAddress, BranchInformation> {
        &self.branch_infos
    }

    pub fn cpu_line_usage(&self) -> &[f32; 154] {
        &self.cpu.line_usage
    }

    pub fn cpu_average(&self) -> f32 {
        self.cpu.average
    }

    pub fn cpu_stats(&self) -> &[f32; CPU_STAT_HISTORY] {
        &self.cpu.stats
    }

    pub fn access_reads(&self) -> usize {
        self.access_reads
    }

    pub fn access_writes(&self) -> usize {
        self.access_writes
    }

    pub fn mbc_access(&self) -> &HashMap<MbcAccessKey, Vec<MbcAccess>> {
        &self.mbc_access
    }

    pub fn mbc_access_reads(&self) -> usize {
        self.mbc_access_reads
    }

    pub fn mbc_access_writes(&self) -> usize {
        self.mbc_access_writes
    }

    pub fn vram_access(&self) -> &HashMap<VramAccessKey, Vec<VramAccess>> {
        &self.vram_access
    }

    pub fn vram_access_reads(&self) -> usize {
        self.vram_access_reads
    }

    pub fn vram_access_writes(&self) -> usize {
        self.vram_access_writes
    }

    pub fn display_access(&self) -> &HashMap<DisplayAccessKey, Vec<DisplayAccess>> {
        &self.display_access
    }

    pub fn display_access_reads(&self) -> usize {
        self.display_access_reads
    }

    pub fn display_access_writes(&self) -> usize {
        self.display_access_writes
    }

    pub fn audio_access(&self) -> &HashMap<AudioAccessKey, Vec<AudioAccess>> {
        &self.audio_access
    }

    pub fn audio_access_reads(&self) -> usize {
        self.audio_access_reads
    }

    pub fn audio_access_writes(&self) -> usize {
        self.audio_access_writes
    }

    pub fn bg_tile_access_writes(&self) -> usize {
        self.bg_tile_access_writes
    }

    pub fn bg_scw_access_writes(&self) -> usize {
        self.bg_scw_access_writes
    }

    pub fn joypad_access_reads(&self) -> usize {
        self.joypad_access_reads
    }

    pub fn joypad_access_writes(&self) -> usize {
        self.joypad_access_writes
    }

    pub fn oam_access_writes(&self) -> usize {
        self.oam_access_writes
    }

    pub fn current_cycle(&self) -> u32 {
        self.cpu.current_cycle()
    }

    pub fn current_line(&self) -> u8 {
        self.cpu.current_line()
    }

    pub fn current_pixel(&self) -> u8 {
        self.cpu.current_pixel()
    }
}

impl<B: 'static + Breakpoint, W: 'static + Watchpoint> Analyzer<B, W> {
    pub fn set_symbols(&mut self, symbols: &SymbolCache) {
        self.symbols = symbols.clone();
    }

    pub fn begin_frame(&mut self, core: &mut RunnableCore, pause: bool) -> bool {
        let (run, flow) = match self.control {
            ControlFlow::Running => if pause {
                (false, ControlFlow::Running)

            } else {
                core.set_debug_stopped(false);
                (true, ControlFlow::Running)
            },
            ControlFlow::Halted => {
                core.set_debug_stopped(true);
                (false, ControlFlow::Halted)
            },
            ControlFlow::AdvanceFrames(mut frames) => {
                if frames == 0 {
                    core.set_debug_stopped(true);
                    (false, ControlFlow::Halted)

                } else {
                    frames = frames.saturating_sub(1);
                    core.set_debug_stopped(false);
                    (true, ControlFlow::AdvanceFrames(frames))
                }
            },
            flow => {
                // Handled in Debugger::run on a per instruction basis
                core.set_debug_stopped(false);
                (true, flow)
            }
        };
        self.control = flow;
        run
    }

    pub fn reset_coverage(&mut self) {
        self.coverage.clear();
    }

    pub fn reset_call_info_totals(&mut self) {
        for call in self.call_infos.values_mut() {
            call.reset_total();
        }
    }

    pub fn reset_branch_info(&mut self) {
        for branch in self.branch_infos.values_mut() {
            branch.reset();
        }
    }

    pub fn add_breakpoint(&mut self, breakpoint: B) {
        self.breakpoints.insert(breakpoint.address(), breakpoint);
        self.breakpoints_active = self.breakpoints.values().filter(|b| b.active()).count();
    }

    pub fn breakpoints(&self) -> &HashMap<BankedAddress, B> {
        &self.breakpoints
    }

    pub fn get_breakpoint(&self, address: BankedAddress) -> Option<&B> {
        self.breakpoints.get(&address)
    }

    pub fn get_breakpoint_mut(&mut self, address: BankedAddress) -> Option<&mut B> {
        self.breakpoints.get_mut(&address)
    }

    pub fn remove_breakpoint(&mut self, address: BankedAddress) -> Option<B> {
        let b = self.breakpoints.remove(&address);
        self.breakpoints_active = self.breakpoints.values().filter(|b| b.active()).count();
        b
    }

    pub fn add_watchpoint(&mut self, index: usize, watchpoint: W) {
        self.watchpoints.insert(index, watchpoint);
        self.watchpoints_active = self.watchpoints.values().any(|w| w.active());
        self.watchpoints_read_active= self.watchpoints.values().any(|w| w.active() && w.start_value().is_some());
    }

    pub fn remove_watchpoint(&mut self, index: usize) -> Option<W> {
        let w = self.watchpoints.remove(&index);
        self.watchpoints_active = self.watchpoints.values().any(|w| w.active());
        self.watchpoints_read_active= self.watchpoints.values().any(|w| w.active() && w.start_value().is_some());
        w
    }

    pub fn set_analyze_average_line_usage(&mut self, average: bool)  {
        self.cpu.average_line_usage = average;
    }

    pub fn set_analyze_line_usage(&mut self, analyze: bool)  {
        self.cpu.usage_per_line = analyze;
    }

    pub fn set_halt_after_boot_rom(&mut self, halt: bool)  {
        self.halt_after_boot_rom = halt;
    }

    pub fn set_log_hdma_transfer(&mut self, log: bool)  {
        self.log_hdma_transfer = log;
    }

    pub fn set_log_mbc_access(&mut self, log: bool)  {
        self.log_mbc_access = log;
    }

    pub fn set_halt_on_memory_access(
        &mut self,
        blocked_vram_write: bool,
        unitialized_vram_read: bool,
        unitialized_ram_read: bool,
        echo_ram_access: bool,
        mbc_write: bool
    )  {
        self.halt_on_blocked_vram_write = blocked_vram_write;
        self.halt_on_unitialized_vram_read = unitialized_vram_read;
        self.halt_on_unitialized_ram_read = unitialized_ram_read;
        self.halt_on_echo_ram_access = echo_ram_access;
        self.halt_on_mbc_write = mbc_write;
    }

    pub fn set_halt_on_software_break(&mut self, enabled: bool) {
        self.halt_on_software_break = enabled;
    }

    pub fn set_enabled(analyzer: &Rc<RefCell<Self>>, core: &mut RunnableCore, enabled: bool) {
        analyzer.borrow_mut().reset_access_logs();
        if enabled {
            let a = analyzer.clone();
            core.set_debugger_callback(Some(Box::new(move |gb| {
                a.borrow_mut().callback_before_instruction(gb);
            })));

            let a = analyzer.clone();
            core.set_lcd_line_callback(Some(Box::new(move |gb, line| {
                a.borrow_mut().callback_lcd_line(gb, line);
            })));

            let a = analyzer.clone();
            core.set_debugger_read_callback(Some(Box::new(move |gb, addr, value| {
                a.borrow_mut().callback_read_memory(gb, addr, value);
            })));

            let a = analyzer.clone();
            core.set_debugger_write_callback(Some(Box::new(move |gb, addr, value| {
                a.borrow_mut().callback_write_memory(gb, addr, value);
            })));

            let a = analyzer.clone();
            core.set_debugger_mbc_write_callback(Some(Box::new(move |gb, addr, value| {
                a.borrow_mut().callback_mbc_write(gb, addr, value);
            })));

            let a = analyzer.clone();
            core.set_debugger_cycle_callback(Some(Box::new(move |gb, cycles| {
                a.borrow_mut().callback_advance_cycles(gb, cycles);
            })));

            let a = analyzer.clone();
            core.set_debugger_call_callback(Some(Box::new(move |gb, call_addr, interrupt| {
                a.borrow_mut().callback_call(gb, call_addr, interrupt);
            })));

            let a = analyzer.clone();
            core.set_debugger_cond_callback(Some(Box::new(move |gb, addr, taken| {
                a.borrow_mut().callback_cond(gb, addr, taken);
            })));

            let a = analyzer.clone();
            core.set_debugger_jphl_callback(Some(Box::new(move |gb, addr| {
                a.borrow_mut().callback_jphl(gb, addr);
            })));

            let a = analyzer.clone();
            core.set_debugger_brk_callback(Some(Box::new(move |gb| {
                a.borrow_mut().callback_brk(gb);
            })));

            let a = analyzer.clone();
            core.set_debugger_msg_callback(Some(Box::new(move |gb, msg_addr, msg_len| {
                a.borrow_mut().callback_msg(gb, msg_addr, msg_len);
            })));

            let a = analyzer.clone();
            core.set_debugger_ret_callback(Some(Box::new(move |gb| {
                a.borrow_mut().callback_ret(gb);
            })));

        } else {
            core.set_debugger_callback(None);
            core.set_lcd_line_callback(None);
            core.set_debugger_read_callback(None);
            core.set_debugger_write_callback(None);
            core.set_debugger_mbc_write_callback(None);
            core.set_debugger_cycle_callback(None);
            core.set_debugger_call_callback(None);
            core.set_debugger_cond_callback(None);
            core.set_debugger_jphl_callback(None);
            core.set_debugger_msg_callback(None);
            core.set_debugger_ret_callback(None);
        }
    }

    pub fn resume(&mut self) {
        self.control = ControlFlow::Running;
    }

    pub fn halt(&mut self) {
        self.control = ControlFlow::Halted;
        self.halt_information = None;
    }

    pub fn step_into(&mut self, count: usize) {
        self.control = ControlFlow::StepInto(count);
    }

    pub fn step_over(&mut self, count: usize) {
        self.control = ControlFlow::StepOver(count, 0);
    }

    pub fn return_from_calls(&mut self, count: usize) {
        self.control = ControlFlow::ReturnFromCalls(count as i32);
    }

    pub fn advance_frames(&mut self, frames: usize, sync_vblank: bool) {
        if sync_vblank {
            self.control = ControlFlow::AdvanceFrames(frames);

        } else {
            self.control = ControlFlow::AdvanceLines(154);
        }
    }

    pub fn advance_lines(&mut self, lines: usize) {
        self.control = ControlFlow::AdvanceLines(lines);
    }

    pub fn messages(&mut self) -> impl Iterator<Item=Message> + '_ {
        self.messages.drain(0..)
    }
}

impl<B: 'static + Breakpoint, W: 'static + Watchpoint> Analyzer<B, W> {
    fn reset_access_logs(&mut self) {
        self.access_reads = 0;
        self.access_writes = 0;
        self.mbc_access.clear();
        self.mbc_access_reads = 0;
        self.mbc_access_writes = 0;
        self.vram_access.clear();
        self.vram_access_reads = 0;
        self.vram_access_writes = 0;
        self.display_access.clear();
        self.display_access_reads = 0;
        self.display_access_writes = 0;
        self.audio_access.clear();
        self.audio_access_reads = 0;
        self.audio_access_writes = 0;
        self.bg_scw_access_writes = 0;
        self.joypad_access_reads = 0;
        self.joypad_access_writes = 0;
        self.bg_tile_access_writes = 0;
        self.call_infos.clear();
        self.callframes.clear();
        self.backtraces.clear();
    }

    fn halt_with_reason(&mut self, core: &mut WriteCallbackCore, reason: HaltReason) {
        core.set_debug_stopped(true);
        self.control = ControlFlow::Halted;
        self.halt_with_information(core, reason);
    }

    fn halt_with_information(&mut self, core: &WriteCallbackCore, reason: HaltReason) {
        self.halt_information = Some(HaltInformation {
            line: self.cpu.current_line(),
            pixel: self.cpu.current_pixel(),
            frame: self.frame,
            addr: core.pc(),
            reason
        });
    }

    fn log_message(&mut self, core: &WriteCallbackCore, detail: MessageDetail) {
        self.messages.push(Message {
            line: self.cpu.current_line(),
            pixel: self.cpu.current_pixel(),
            frame: self.frame,
            addr: core.pc(),
            detail
        });
    }
}

impl<B: 'static + Breakpoint, W: 'static + Watchpoint> Analyzer<B, W> {
    fn enter_call(&mut self, core: &WriteCallbackCore, caller_addr: BankedAddress, _interrupt: bool, _jphl: bool) {
        let pc = core.pc();
        self.call_infos.entry(pc)
            .or_insert_with(CallInformation::new)
            .begin(
                self.frame,
                self.cpu.current_cycle(),
                self.cpu.current_line(),
                self.cpu.current_pixel(),
                self.callframes.iter().map(|f| f.addr).collect(),
                caller_addr
            );
    }

    fn exit_call(&mut self, addr: BankedAddress) {
        if let Some(call) = self.call_infos.get_mut(&addr) {
            call.exit = Some((self.frame, self.cpu.current_cycle(), self.cpu.current_line(), self.cpu.current_pixel()));
        }
    }

    fn record_software_breakpoint(&mut self, pc: BankedAddress) {
        self.breakpoints.insert(pc, B::from_software(pc));
    }

    fn check_watchpoints(
        &mut self,
        core: &mut WriteCallbackCore,
        addr: BankedAddress,
        read_value: Option<u8>,
        write_value: Option<u8>
    ) {
        if self.watchpoints_active {
            let mut logs = Vec::new();
            let mut halts = Vec::new();
            for w in self.watchpoints.values().filter(|w| w.active()) {
                if address_hit(w, addr) {
                    if let (true, Some(v)) = (w.read(), read_value) {
                        if value_hit(w, v) {
                            if w.log() {
                                logs.push(MessageDetail::WatchpointRead(addr, v));
                            }
                            if w.halt() {
                                halts.push(HaltReason::WatchpointRead(addr, v));
                            }
                        }
                    }
                    if let (true, Some(v)) = (w.write(), write_value) {
                        if value_hit(w, v) {
                            if w.log() {
                                logs.push(MessageDetail::WatchpointWrite(addr, v));
                            }
                            if w.halt() {
                                halts.push(HaltReason::WatchpointWrite(addr, v));
                            }
                        }
                    }
                }
            }
            for l in logs {
                self.log_message(core, l);
            }
            for h in halts {
                self.halt_with_reason(core, h);
            }
        }

        fn address_hit<W: Watchpoint>(w: &W, addr: BankedAddress) -> bool {
            let start_addr = w.start_address();
            if let Some(end_addr) = w.end_address() {
                addr.within_range(start_addr, end_addr)

            } else {
                addr == start_addr
            }
        }

        fn value_hit<W: Watchpoint>(w: &W, value: u8) -> bool {
            if let Some(start_value) = w.start_value() {
                if let Some(end_value) = w.end_value() {
                    value >= start_value && value <= end_value

                } else {
                    value == start_value
                }

            } else {
                true
            }
        }
    }

    fn log_mbc_access(&mut self, core: &WriteCallbackCore, addr: BankedAddress, value: u8) {
        let rom_bank = core.mbc_rom_bank();
        let rom_bank = if rom_bank != self.mbc_rom_bank {
            if self.log_mbc_access {
                self.log_message(
                    core,
                    MessageDetail::MBCBankSwitchROM(self.mbc_rom_bank, rom_bank)
                );
            }
            self.mbc_rom_bank = rom_bank;
            Some(rom_bank)

        } else {
            None
        };

        let ram_bank = core.mbc_ram_bank();
        let ram_bank = if ram_bank != self.mbc_ram_bank {
            if self.log_mbc_access {
                self.log_message(
                    core,
                    MessageDetail::MBCBankSwitchRAM(self.mbc_ram_bank, ram_bank)
                );
            }
            self.mbc_ram_bank = ram_bank;
            Some(ram_bank)

        } else {
            None
        };

        let ram_enable = core.mbc_ram_enable();
        let ram_enable = if ram_enable != self.mbc_ram_enable {
            if self.log_mbc_access {
                if ram_enable {
                    self.log_message(core, MessageDetail::MBCRamEnable(addr, value));

                } else {
                    self.log_message(core, MessageDetail::MBCRamDisable(addr, value));
                }
            }
            self.mbc_ram_enable = ram_enable;
            Some(ram_enable)

        } else {
            None
        };

        let access = self.mbc_access.entry(MbcAccessKey {
            line: self.cpu.current_line(),
            pixel: self.cpu.current_pixel(),
            rom_switch: rom_bank.is_some(),
            ram_switch: ram_bank.is_some(),
            ram_enable: ram_enable.is_some(),

        }).or_insert_with(|| Vec::with_capacity(16));
        access.push(MbcAccess {
            addr,
            rom_bank,
            ram_bank,
            ram_enable,
            by: core.pc(),
            value
        });
        self.mbc_access_writes += 1;
    }

    fn log_vram_access(&mut self, core: &WriteCallbackCore, addr: BankedAddress, value: u8, write: bool) {
        let write_blocked = core.is_vram_write_blocked();
        let access = self.vram_access.entry(VramAccessKey {
            line: self.cpu.current_line(),
            pixel: self.cpu.current_pixel(),
            hdma: core.is_hdma_in_progress(),
            dma: core.is_dma_in_progress(),
            write,
            write_blocked

        }).or_insert_with(|| Vec::with_capacity(16));
        access.push(VramAccess {
            addr,
            by: core.pc(),
            value
        });
        if write {
            self.vram_access_writes += 1;

        } else {
            self.vram_access_reads += 1;
        }
    }

    fn log_display_access(&mut self, core: &WriteCallbackCore, addr: BankedAddress, value: u8, write: bool) {
        let access = self.display_access.entry(DisplayAccessKey {
            line: self.cpu.current_line(),
            pixel: self.cpu.current_pixel(),
            write

        }).or_insert_with(|| Vec::with_capacity(16));
        access.push(DisplayAccess {
            addr,
            by: core.pc(),
            value
        });
        if write {
            self.display_access_writes += 1;

        } else {
            self.display_access_reads += 1;
        }
    }

    fn log_audio_access(&mut self, core: &WriteCallbackCore, addr: BankedAddress, value: u8, write: bool) {
        let access = self.audio_access.entry(AudioAccessKey {
            line: self.cpu.current_line(),
            pixel: self.cpu.current_pixel(),
            write

        }).or_insert_with(|| Vec::with_capacity(16));
        access.push(AudioAccess {
            addr,
            by: core.pc(),
            value
        });
        if write {
            self.audio_access_writes += 1;

        } else {
            self.audio_access_reads += 1;
        }
    }

    fn check_ahead_watchpoint(&self, core: &WriteCallbackCore, regs: WordRegisters) -> Option<(BankedAddress, Option<u8>, Option<u8>)> {
        let op_code = core.read_memory_safe(regs.pc);
        let n = regs.pc.wrapping_add(1);
        let m = regs.pc.wrapping_add(2);
        let a = (regs.af >> 8) as u8;
        let result = match op_code {
            // ld [bc],a
            0x02 => Some((regs.bc, Some(a))),
            // ld [u16],SP // TODO this is an address range
            0x08 => Some((((core.read_memory_safe(n) as u16) << 8) + core.read_memory_safe(m) as u16, Some(regs.sp as u8))),
            // ld [de],a
            0x12 => Some((regs.de, Some(a))),
            // ld [hli],a
            0x22 => Some((regs.hl, Some(a))),
            // ld [hld],a
            0x32 => Some((regs.hl, Some(a))),
            // ld [hl],u8
            0x36 => Some((regs.hl, Some(core.read_memory_safe(n)))),
            // ld [hl],b
            0x70 => Some((regs.hl, Some((regs.bc >> 8) as u8))),
            // ld [hl],c
            0x71 => Some((regs.hl, Some(regs.bc as u8))),
            // ld [hl],d
            0x72 => Some((regs.hl, Some((regs.de >> 8) as u8))),
            // ld [hl],e
            0x73 => Some((regs.hl, Some(regs.de as u8))),
            // ld [hl],h
            0x74 => Some((regs.hl, Some((regs.hl >> 8) as u8))),
            // ld [hl],l
            0x75 => Some((regs.hl, Some(regs.de as u8))),
            // ld [hl],a
            0x77 => Some((regs.hl, Some(a))),
            // ld [a8],a
            0xE0 => Some((0xFF00 + core.read_memory_safe(n) as u16, Some(a))),
            // ld [c],a
            0xE2 => Some((0xFF00 + (regs.bc & 0xFF), Some(a))),
            // ld [u16],a
            0xEA => Some((((core.read_memory_safe(n) as u16) << 8) + core.read_memory_safe(m) as u16, Some(a))),

            // ld a,[bc]
            0x0A => Some((regs.bc, None)),
            // ld a,[de]
            0x1A => Some((regs.de, None)),
            // ld a,[hli]
            // ld a,[hld]
            // ld b,[hl]
            // ld c,[hl]
            // ld d,[hl]
            // ld e,[hl]
            // ld h,[hl]
            // ld l,[hl]
            // ld a,[hl]
            // add a,[hl]
            // adc a,[hl]
            // sub a,[hl]
            // sbc a,[hl]
            // and a,[hl]
            // xor a,[hl]
            // or a,[hl]
            // cp a,[hl]
            0x2A |
            0x3A |
            0x46 |
            0x4E |
            0x56 |
            0x5E |
            0x66 |
            0x6E |
            0x7E |
            0x86 |
            0x8E |
            0x96 |
            0x9E |
            0xA6 |
            0xAE |
            0xB6 |
            0xBE => Some((regs.hl, None)),
            // ld a,[u8]
            0xF0 => Some((0xFF00 + core.read_memory_safe(n) as u16, None)),
            // ld a,[c]
            0xF2 => Some((0xFF00 + (regs.bc & 0xFF), None)),
            // ld a,[u16]
            0xFA =>  Some((((core.read_memory_safe(n) as u16) << 8) + core.read_memory_safe(m) as u16, None)),
            _ => None
        };
        match result {
            Some((addr, Some(write_value))) => {
                Some((core.banked_address(addr), None, Some(write_value)))
            },
            Some((addr, None)) => if self.watchpoints_read_active {
                let read_value = core.read_memory_safe(addr);
                Some((core.banked_address(addr), Some(read_value), None))

            } else {
                Some((core.banked_address(addr), None, None))
            },
            None => None
        }
    }
}

impl<B: 'static + Breakpoint, W: 'static + Watchpoint> Analyzer<B, W> {
    fn callback_brk(&mut self, mut core: WriteCallbackCore) {
        // If software breakpoints are enabled and we just ran into one and there
        // is not yet a breakpoint created, create one and move the PC back to before
        // the instruction (since ld b,b is a noop this works out fine)
        //
        // Afterwards the usual breakpoint logic will take care of the break.
        let pc = core.previous_pc();
        if self.halt_on_software_break && !self.breakpoints.contains_key(&pc) {
            self.record_software_breakpoint(pc);
            core.set_pc(pc.address());
        }
    }

    fn callback_before_instruction(&mut self, mut core: WriteCallbackCore) {
        // Keep track of general instruction coverage
        let pc = core.pc();
        self.coverage.insert(pc);

        // Check for a breakpoint before the next instruction gets executed so
        // we can break in time (i.e. before the instruction is executed)
        if self.breakpoints_active > 0 {
            if let Some(bp) = self.breakpoints.get_mut(&pc) {
                if bp.evaluate(&core, self.halt_on_software_break, &self.symbols) {
                    self.halt_with_reason(&mut core, HaltReason::UserBreakpoint);
                }
            }
        }

        // Check if the upcoming instruction will trigger a watchpoint.
        //
        // During DMA we the CPU does not run actual instructions so those watchpoints can
        // only be handled after the fact in the memory read / write callbacks.
        if self.watchpoints_active && !core.is_hdma_in_progress() && !core.is_dma_in_progress() {
            // This is done so we can break BEFORE the read / write actually happens
            // so we can intercept it.
            let regs = core.word_registers();
            if let Some((addr, read_value, write_value)) = self.check_ahead_watchpoint(&core, regs) {
                self.check_watchpoints(&mut core, addr, read_value, write_value);
            }
        }

        // Detect stack pointer base changes
        let sp = core.sp();
        let sd = sp.abs_diff(self.stack_base);
        if sd > 0x100 || sd % 2 != 0 {
            self.stack_base = sp;
        }

        // Handle per instruction control flow
        self.control = match self.control {
            ControlFlow::Running => if self.halt_after_boot_rom && !self.boot_rom_finished && core.is_boot_rom_finished() {
                core.set_debug_stopped(true);
                self.halt_with_information(&core, HaltReason::BootRomFinished);
                ControlFlow::Halted

            } else {
                ControlFlow::Running
            },
            ControlFlow::Halted => if !core.is_debug_stopped() {
                ControlFlow::Running

            } else {
                ControlFlow::Halted
            },
            ControlFlow::StepInto(mut remaining) => {
                remaining = remaining.saturating_sub(1);
                if remaining == 0 {
                    core.set_debug_stopped(true);
                    ControlFlow::Halted

                } else {
                    ControlFlow::StepInto(remaining)
                }
            },
            ControlFlow::StepOver(mut remaining, call_depth) => {
                if call_depth <= 0 {
                    remaining = remaining.saturating_sub(1);
                }
                if remaining == 0 {
                    core.set_debug_stopped(true);
                    ControlFlow::Halted

                } else {
                    ControlFlow::StepOver(remaining, call_depth)
                }
            },
            ControlFlow::ReturnFromCalls(calls_left) => {
                if calls_left <= 0 || self.backtraces.is_empty() {
                    core.set_debug_stopped(true);
                    ControlFlow::Halted

                } else {
                    ControlFlow::ReturnFromCalls(calls_left)
                }
            },
            ControlFlow::AdvanceFrames(remaining) => ControlFlow::AdvanceFrames(remaining),
            flow => flow
        };

        // Handle bootrom exit
        let boot_rom_finished = core.is_boot_rom_finished();
        if boot_rom_finished != self.boot_rom_finished {
            self.coverage.clear();
            self.boot_rom_finished = boot_rom_finished;
        }
    }

    fn callback_lcd_line(&mut self, mut core: WriteCallbackCore, lcd_line: u8) {
        // Since the UI always renders after vblank, we reset all frame based
        // statistics on the first line after vlbank, this way we can display
        // all access correctly
        if lcd_line == 145 {
            // Save call information from the previous frame for UI access
            for call in self.call_infos.values_mut() {
                call.complete_frame(self.frame, self.cpu.current_cycle());
            }

            // Advance to next frame
            self.frame += 1;
            self.cpu.start_frame();

            // Check for OAM changes
            let oam = core.oam();
            for (old, new) in self.oam.iter_mut().zip(oam) {
                if *old != *new {
                    self.vram_update.oam = true;
                    *old = *new;
                }
            }

            // Reset access stats
            self.access_reads = 0;
            self.access_writes = 0;

            self.mbc_access_reads = 0;
            self.mbc_access_writes = 0;
            self.mbc_access.clear();

            self.vram_access_reads = 0;
            self.vram_access_writes = 0;
            self.vram_access.clear();

            self.display_access_reads = 0;
            self.display_access_writes = 0;
            self.display_access.clear();

            self.audio_access_reads = 0;
            self.audio_access_writes = 0;
            self.audio_access.clear();

            self.bg_tile_access_writes = 0;
            self.bg_scw_access_writes = 0;
            self.joypad_access_reads = 0;
            self.joypad_access_writes = 0;
            self.oam_access_writes = 0;
        }
        self.control = match self.control {
            ControlFlow::AdvanceLines(mut remaining) => {
                remaining = remaining.saturating_sub(1);
                if remaining == 0 {
                    core.set_debug_stopped(true);
                    ControlFlow::Halted

                } else {
                    ControlFlow::AdvanceLines(remaining)
                }
            },
            flow => flow
        };
        self.cpu.start_line(lcd_line);
    }

    fn callback_read_memory(&mut self, mut core: WriteCallbackCore, addr: u16, value: u8) {
        if !self.boot_rom_finished {
            return;
        }
        let addr = core.banked_address(addr);
        match addr.address() {
            // VRAM
            0x8000..=0x9FFF => {
                self.log_vram_access(&core, addr, 0, false);
                if !self.ram_initialized.contains(&addr) {
                    self.log_message(&core, MessageDetail::UnitializedVramRead(addr));
                    if self.halt_on_unitialized_vram_read {
                        self.halt_with_reason(&mut core, HaltReason::UnitializedVramRead(addr));
                    }
                }
            },
            // Work RAM
            0xC000..=0xDFFF => {
                if !self.ram_initialized.contains(&addr) {
                    self.log_message(&core, MessageDetail::UnitializedRamRead(addr));
                    if self.halt_on_unitialized_ram_read {
                        self.halt_with_reason(&mut core, HaltReason::UnitializedRamRead(addr));
                    }
                }
            },
            // Echo RAM
            0xE000..=0xFDFF => {
                self.log_message(&core, MessageDetail::EchoRamRead(addr));
                if self.halt_on_echo_ram_access {
                    self.halt_with_reason(&mut core, HaltReason::EchoRamRead(addr));
                }
            },
            // DMG palette
            0xFF47 | 0xFF48 | 0xFF49 => {
                self.log_display_access(&core, addr, 0, false);
            },
            // CGB palette
            0xFF68 | 0xFF69 | 0xFF6A => {
                self.log_display_access(&core, addr, 0, false);
            },
            // Joypad
            0xFF00 => {
                self.joypad_access_reads += 1;
            },
            // Audio
            0xFF10..=0xFF14 | 0xFF16..=0xFF19 | 0xFF1A..=0xFF1E | 0xFF20..=0xFF23 | 0xFF24 | 0xFF25 | 0xFF26 | 0xFF30..=0xFF3F => {
                self.log_audio_access(&core, addr, 0, false);
            },
            // Display Registers
            0xFF40 | 0xFF41 | 0xFF42 | 0xFF4A | 0xFF4B | 0xFF4F | 0xFF43 | 0xFF44 | 0xFF45=> {
                self.log_display_access(&core, addr, 0, false);
            },
            // HRAM
            0xFF80..=0xFFFE => {
                if !self.ram_initialized.contains(&addr) {
                    self.log_message(&core, MessageDetail::UnitializedRamRead(addr));
                    if self.halt_on_unitialized_ram_read {
                        self.halt_with_reason(&mut core, HaltReason::UnitializedRamRead(addr))
                    }
                }
            },
            _ => {}
        }
        // We only handle DMA watchpoints here since they are not precise on
        // a per instruction basis
        if core.is_hdma_in_progress() || core.is_dma_in_progress() {
            self.check_watchpoints(&mut core, addr, Some(value), None);
        }
        self.access_reads += 1;
    }

    fn callback_mbc_write(&mut self, mut core: WriteCallbackCore, addr: u16, value: u8) {
        let addr = core.banked_address(addr);
        self.log_mbc_access(&core, addr, value);
        if self.halt_on_mbc_write {
            self.halt_with_reason(&mut core, HaltReason::MbcWrite(addr, value));
        }
    }

    fn callback_write_memory(&mut self, mut core: WriteCallbackCore, addr: u16, value: u8) {
        if !self.boot_rom_finished {
            return;
        }
        let addr = core.banked_address(addr);
        match addr.address() {
            // VRAM
            0x8000..=0x9FFF => {
                if addr.address() <= 0x97FF {
                    let tile_index = (addr.offset() - 0x8000) / 16;
                    if addr.bank() == 1 {
                        self.vram_update.bank1 = true;
                        self.vram_update.tiles[tile_index + 384] = true;

                    } else {
                        self.vram_update.bank0 = true;
                        self.vram_update.tiles[tile_index] = true;
                    }
                } else {
                    let index = (addr.offset() - 0x9800) % 1024;
                    if self.map_tiles[index] != value {
                        self.vram_update.map = true;
                        self.vram_update.map_tiles[index] = true;
                        self.map_tiles[index] = value;
                    }
                    self.bg_tile_access_writes += 1;
                }
                self.log_vram_access(&core, addr, value, true);

                // Halt on invalid writes
                if core.is_vram_write_blocked() {
                    self.log_message(&core, MessageDetail::InvalidVramWrite {
                        addr,
                        value,
                        hdma: core.is_hdma_in_progress()
                    });
                    if self.halt_on_blocked_vram_write {
                        self.halt_with_reason(&mut core, HaltReason::InvalidVramWrite(addr, value));
                    }
                }

                // Mark as initialized
                self.ram_initialized.insert(addr);
            },
            // Work RAM
            0xC000..=0xDFFF => {
                self.ram_initialized.insert(addr);
            },
            // Echo RAM
            0xE000..=0xFDFF => {
                self.log_message(&core, MessageDetail::EchoRamWrite(addr, value));
                if self.halt_on_echo_ram_access {
                    self.halt_with_reason(&mut core, HaltReason::EchoRamWrite(addr, value));
                }
            },
            // Joypad
            0xFF00 => {
                self.joypad_access_writes += 1;
            },
            // Audio
            0xFF10..=0xFF14 | 0xFF16..=0xFF19 | 0xFF1A..=0xFF1E | 0xFF20..=0xFF23 | 0xFF24 | 0xFF25 | 0xFF26 | 0xFF30..=0xFF3F => {
                self.log_audio_access(&core, addr, value, true);
            },
            // DMG palette
            0xFF47 | 0xFF48 | 0xFF49 => {
                self.vram_update.palette = true;
                self.log_display_access(&core, addr, value, true);
            },
            // CGB palette
            0xFF68 | 0xFF69 | 0xFF6A => {
                self.vram_update.palette = true;
                self.log_display_access(&core, addr, value, true);
            },
            // Display Registers
            0xFF42 | 0xFF43 | 0xFF4A | 0xFF4B => {
                self.bg_scw_access_writes += 1;
                self.log_display_access(&core, addr, value, true);
            },
            0xFF40 | 0xFF41 | 0xFF4F | 0xFF44 | 0xFF45=> {
                self.log_display_access(&core, addr, value, true);
            },
            // HDMA registers
            0xFF51 => {
                self.hdma_source = (value as u16) << 8 | (self.hdma_source & 0x00FF);
            },
            0xFF52 => {
                self.hdma_source = (value as u16) & 0b1111_0000 | (self.hdma_source & 0xFF00);
            },
            0xFF53 => {
                self.hdma_destination = ((value as u16) & 0b1111) << 8 | (self.hdma_destination & 0x00FF);
            },
            0xFF54 => {
                self.hdma_destination = (value as u16) & 0b1111_0000 | (self.hdma_destination & 0xFF00);
            },
            0xFF55 => if self.log_hdma_transfer {
                if (value >> 7) == 1 {
                    self.log_message(&core, MessageDetail::HDMATransferStart {
                        source: self.hdma_source,
                        destination: 0x8000 + self.hdma_destination,
                        blocks: (value & 0b0111_1111) + 1
                    })

                } else {
                    self.log_message(&core, MessageDetail::GDMATransferStart {
                        source: self.hdma_source,
                        destination: 0x8000 + self.hdma_destination,
                        blocks: (value & 0b0111_1111) + 1
                    })
                }
            },
            // OAM
            0xFE00..=0xFE9F => {
                self.vram_update.oam = true;
                self.oam_access_writes += 1;
            },
            // HRAM
            0xFF80..=0xFFFE => {
                self.ram_initialized.insert(addr);
            },
            _ => {}
        }
        // We only handle DMA watchpoints here since they are not precise on
        // a per instruction basis
        if core.is_hdma_in_progress() || core.is_dma_in_progress() {
            self.check_watchpoints(&mut core, addr, None, Some(value));
        }
        self.access_writes += 1;
    }

    fn callback_advance_cycles(&mut self, core: WriteCallbackCore, cycles: u8) {
        self.cpu.track_cycles(&mut self.callframes, core, cycles);
    }

    fn callback_call(&mut self, core: WriteCallbackCore, addr: u16, interrupt: bool) {
        let addr = core.banked_address(addr);
        self.enter_call(&core, addr, interrupt, false);

        // Update Backtrace
        let regs = core.word_registers();
        if self.backtraces.last().map(|b| b.sp) <= Some(regs.sp) {
            self.backtraces.pop();
        }
        self.backtraces.push(CallBacktrace {
            sp: regs.sp,
            addr
        });

        // Update Callstack
        self.callframes.push(CallFrame {
            addr: core.pc(),
            cycles: 0,
            hl_jumps: Vec::new()
        });

        self.control = match self.control {
            ControlFlow::ReturnFromCalls(calls) => ControlFlow::ReturnFromCalls(calls.saturating_add(1)),
            ControlFlow::StepOver(remaining, depth) => ControlFlow::StepOver(remaining, depth.saturating_add(1)),
            flow => flow
        }
    }

    fn callback_jphl(&mut self, core: WriteCallbackCore, addr: u16) {
        // Treat jump tables as calls (they are most likely used in this way)
        let addr = core.banked_address(addr);
        self.enter_call(&core, addr, false, true);

        // Add the address of the jumped to function to the current callframe
        // this way we can later correctly record the exit infromation for the call
        // stats
        if let Some(frame) = self.callframes.last_mut() {
            frame.hl_jumps.push(core.pc());
        }
    }

    fn callback_cond(&mut self, core: WriteCallbackCore, addr: u16, taken: bool) {
        let addr = core.banked_address(addr);
        self.branch_infos.entry(addr)
            .or_insert_with(BranchInformation::default)
            .record(taken);
    }

    fn callback_ret(&mut self, core: WriteCallbackCore) {
        let regs = core.word_registers();
        if self.backtraces.last().map(|b| b.sp) <= Some(regs.sp) {
            self.backtraces.pop();
        }

        if let Some(frame) = self.callframes.pop() {
            // Accumulate cycles from the dropped call frame on all remaining frames
            let locations: Vec<BankedAddress> = self.callframes.iter().map(|f| f.addr).collect();
            for addr in locations {
                self.cpu.track_call_usage(addr, 0, frame.cycles);
            }
            self.cpu.track_call_usage(frame.addr, frame.cycles, frame.cycles);

            // Record exit of the called function
            self.exit_call(frame.addr);

            // Also record exit for all functions "called" via JPHL during the callframe
            for addr in frame.hl_jumps {
                self.exit_call(addr);
            }
        }
        self.control = match self.control {
            ControlFlow::ReturnFromCalls(calls) => ControlFlow::ReturnFromCalls(calls.saturating_sub(1)),
            ControlFlow::StepOver(remaining, depth) => ControlFlow::StepOver(remaining, depth.saturating_sub(1)),
            flow => flow
        }
    }

    fn callback_msg(&mut self, core: WriteCallbackCore, msg_addr: u16, msg_len: u8) {
        let msg_addr = msg_addr as usize;
        let msg_len = msg_len as usize;
        let rom = core.rom();
        if msg_addr + msg_len < rom.len() {
            let msg = &core.rom()[msg_addr..msg_addr + msg_len];
            if let Ok(payload) = String::from_utf8(msg.to_vec()) {
                self.log_message(&core, MessageDetail::MsgInstruction(payload))
            }
        }
    }
}

