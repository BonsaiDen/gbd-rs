// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::collections::HashMap;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::{BankedAddress, ReadableCore, WriteCallbackCore};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{CallFrame, CallUsage};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub const CPU_STAT_HISTORY: usize = 120;
const TOTAL_LINE_CYCLES: u32 = 912; // @ 8mhz
const TOTAL_FRAME_CYCLES: u32 = TOTAL_LINE_CYCLES * 154; // @ 8mhz
const PIXEL_CYCLES_TIMES_100: u32 = (160 * 100) / TOTAL_LINE_CYCLES;


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct AnalyzerCPU {
    load: f32,
    pub average: f32,
    pub stats: [f32; CPU_STAT_HISTORY],
    pub usage: HashMap<BankedAddress, CallUsage>,
    cycle: u32,
    frame_run_cycles: u32,
    frame_halt_cycles: u32,
    current_line: usize,
    current_line_cycles: u32,
    pub average_line_usage: bool,
    pub line_usage: [f32; 154],
    pub usage_per_line: bool,
}

impl AnalyzerCPU {
    pub fn new() -> Self {
        Self {
            load: 0.0,
            average: 0.0,
            stats: [0.0; CPU_STAT_HISTORY],
            usage: HashMap::new(),
            cycle: 0,
            frame_run_cycles: 0,
            frame_halt_cycles: 0,
            current_line: 0,
            current_line_cycles: 0,
            average_line_usage: false,
            line_usage: [0.0; 154],
            usage_per_line: false
        }
    }

    pub fn start_line(&mut self, line: u8) {
        if self.usage_per_line {
            // Global CPU usage
            let load = (1.0 / TOTAL_LINE_CYCLES as f32 * self.current_line_cycles as f32).min(1.0).max(0.0);
            self.line_usage[self.current_line] = if self.average_line_usage {
                (load + self.line_usage[self.current_line] * 2.0) / 3.0

            } else {
                load
            };

            // Call Usage
            for usage in self.usage.values_mut() {
                let own_cycles = usage.own_line_cycles[self.current_line];
                let own_load = (1.0 / TOTAL_LINE_CYCLES as f32 * own_cycles as f32).min(1.0).max(0.0);
                usage.own_line_usage[self.current_line] = if self.average_line_usage {
                    (own_load + usage.own_line_usage[self.current_line] * 2.0) / 3.0

                } else {
                    own_load
                };
                usage.own_line_cycles[self.current_line] = 0;

                let acc_cycles = usage.acc_line_cycles[self.current_line];
                let acc_load = (1.0 / TOTAL_LINE_CYCLES as f32 * acc_cycles as f32).min(1.0).max(0.0);
                usage.acc_line_usage[self.current_line] = if self.average_line_usage {
                    (acc_load + usage.acc_line_usage[self.current_line] * 2.0) / 3.0

                } else {
                    acc_load
                };
                usage.acc_line_cycles[self.current_line] = 0;
            }
        }
        self.current_line = line as usize;
        self.current_line_cycles = 0;
    }

    pub fn start_frame(&mut self) {
        // Average cpu load across 2 frames
        let load = (1.0 / TOTAL_FRAME_CYCLES as f32 * self.frame_run_cycles as f32).min(1.0).max(0.0);
        self.load = (load + self.load * 2.0) / 3.0;
        self.frame_run_cycles = 0;
        self.frame_halt_cycles = 0;
        for usage in self.usage.values_mut() {
            let own_load = (1.0 / TOTAL_FRAME_CYCLES as f32 * usage.own_cycles as f32).min(1.0).max(0.0);
            let acc_load = (1.0 / TOTAL_FRAME_CYCLES as f32 * usage.acc_cycles as f32).min(1.0).max(0.0);
            usage.own_load = (own_load + usage.own_load * 2.0) / 3.0;
            usage.acc_load = (acc_load + usage.acc_load * 2.0) / 3.0;
            usage.own_cycles = 0;
            usage.acc_cycles = 0;
        }

        // Track the last N frames
        for usage in self.usage.values_mut() {
            for i in 1..CPU_STAT_HISTORY {
                let e = CPU_STAT_HISTORY - i;
                usage.own_stats[e] = usage.own_stats[e - 1];
                usage.acc_stats[e] = usage.acc_stats[e - 1];
            }
            usage.own_stats[0] = usage.own_load;
            usage.acc_stats[0] = usage.acc_load;
            usage.own_average = usage.own_stats.iter().sum::<f32>() / CPU_STAT_HISTORY as f32;
            usage.acc_average = usage.acc_stats.iter().sum::<f32>() / CPU_STAT_HISTORY as f32;
        }
        for i in 1..CPU_STAT_HISTORY {
            let e = CPU_STAT_HISTORY - i;
            self.stats[e] = self.stats[e - 1];
        }
        self.stats[0] = self.load;
        self.average = self.stats.iter().sum::<f32>() / CPU_STAT_HISTORY as f32;
    }

    pub fn track_cycles(
        &mut self,
        callframes: &mut [CallFrame],
        core: WriteCallbackCore,
        cycles: u8
    ) {
        if !core.is_halted() {
            // Track usage per frame (since last vblank)
            self.frame_run_cycles += cycles as u32;

            // Track call cycles for each frame
            if let Some(frame) = callframes.last_mut() {
                frame.cycles += cycles as u32;
            }

            if self.usage_per_line {
                for (i, frame) in callframes.iter().rev().enumerate() {
                    if let Some(call) = self.usage.get_mut(&frame.addr) {
                        if i == 0 {
                            call.own_line_cycles[self.current_line] += cycles as u32;
                        }
                        call.acc_line_cycles[self.current_line] += cycles as u32;
                    }
                }
            }

        } else {
            self.frame_halt_cycles += cycles as u32;
        }

        // Total cycles
        self.cycle = self.cycle.saturating_add(cycles as u32);

        // Total cycles since the start of the line
        self.current_line_cycles += cycles as u32;
    }

    pub fn track_call_usage(&mut self, addr: BankedAddress, own_cycles: u32, acc_cycles: u32) {
        let len = self.usage.len();
        let usage = self.usage.entry(addr).or_insert_with(|| {
            CallUsage::new(len)
        });
        usage.own_cycles += own_cycles;
        usage.acc_cycles += acc_cycles;
    }

    pub fn current_cycle(&self) -> u32 {
        self.cycle
    }

    pub fn current_line(&self) -> u8 {
        self.current_line as u8
    }

    pub fn current_pixel(&self) -> u8 {
        // Since Sameboy does the pixel rendering in batches we need to compute the
        // estimated pixel position ourselves by keeping track of the cycles since
        // the start of the line
        (PIXEL_CYCLES_TIMES_100 * self.current_line_cycles / 100) as u8
    }
}

