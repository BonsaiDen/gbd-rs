// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::hash::Hasher;
use std::hash::BuildHasher;
use std::collections::{HashMap, HashSet};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use gb_cpu::Instruction;
use sameboy_rs::{BankedAddress, ReadableCore, RunnableCore};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{Analyzer, Breakpoint, CallInformation, SymbolProvider, Watchpoint};
use crate::cache::{LineCache, LineAdressOffset, MemoryCache};


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Default)]
struct AddressHasher {
    address: u64
}

impl Hasher for AddressHasher {
    fn write(&mut self, _: &[u8]) {
        unimplemented!()
    }

    fn write_usize(&mut self, i: usize) {
        self.address = i as u64;
    }

    fn write_u32(&mut self, i: u32) {
        self.address = i as u64;
    }

    fn finish(&self) -> u64 {
        self.address
    }
}

struct BuildAddressHasher;
impl BuildHasher for BuildAddressHasher {
    type Hasher = AddressHasher;

    fn build_hasher(&self) -> Self::Hasher {
        AddressHasher::default()
    }
}

#[derive(Debug)]
pub enum CodeDescriptor {
    Label {
        name: String,
        is_child: bool,
        call_access_address: Option<BankedAddress>
    },
    Value(String),
    Comment(String)
}

impl CodeDescriptor {
    fn len(&self) -> usize {
        match self {
            Self::Label { call_access_address: Some(_), .. } => 2,
            _ => 1
        }
    }
}

#[derive(Debug)]
#[allow(clippy::large_enum_variant)]
pub enum CodeMapping {
    Instruction {
        instruction: Instruction,
        postfix: Option<CodeDescriptor>,
        above: Option<CodeDescriptor>
    },
    Message {
        byte_size: usize,
        value: String,
    },
    Data {
        bytes: Vec<u8>,
        postfix: Option<CodeDescriptor>,
        above: Option<CodeDescriptor>
    }
}

impl CodeMapping {
    pub fn above(&self) -> &Option<CodeDescriptor> {
        match self {
            Self::Instruction { above, .. } => above,
            Self::Message { .. } => &None,
            Self::Data { above, ..} => above
        }
    }

    pub fn postfix(&self) -> &Option<CodeDescriptor> {
        match self {
            Self::Instruction { postfix, .. } => postfix,
            Self::Message { .. } => &None,
            Self::Data { postfix, ..} => postfix
        }
    }

    fn size(&self) -> usize {
        match self {
            Self::Instruction { instruction, .. } => instruction.size,
            Self::Message { byte_size, .. } => *byte_size,
            Self::Data { bytes, .. } => bytes.len()
        }
    }

    fn lines(&self) -> usize {
        match self {
            Self::Instruction { above, ..} => above.as_ref().map(|c| c.len()).unwrap_or(0),
            Self::Message { .. } => 0,
            Self::Data { above, .. } => above.as_ref().map(|c| c.len()).unwrap_or(0)
        }
    }
}


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
type LocationStack = Vec<(BankedAddress, Option<CodeDescriptor>)>;

pub struct CodeCache {
    bootrom_finished: bool,
    line_gen: usize,
    symbols_gen: usize,
    combined_ie: u8,
    call_info_count: Option<usize>,
    mapped_rom_banks: HashSet<u16>,
    mapped_data_bytes: HashSet<BankedAddress, BuildAddressHasher>,
    mapping: HashMap<BankedAddress, CodeMapping, BuildAddressHasher>,
    bank_mapping: HashMap<usize, u16, BuildAddressHasher>,
    is_data_map: HashSet<BankedAddress, BuildAddressHasher>,
    line_mapping: HashMap<LineAdressOffset, usize>,
    line_addresses: Vec<LineAdressOffset>,
}

impl LineCache for CodeCache {
    fn line_indicies_gen(&mut self) -> usize {
        self.line_gen
    }

    fn map_line_index(&self, (addr, offset): LineAdressOffset) -> usize {
        if let Some(index) = self.line_mapping.get(&(addr, offset)) {
            *index

        } else {
            // Fallback to linear search of closest index
            for (index, (line_addr, _)) in self.line_addresses.iter().enumerate() {
                if *line_addr >= addr {
                    return index;
                }
            }
            0
        }
    }

    fn get_line_addr(&self, index: usize) -> LineAdressOffset {
        *self.line_addresses.get(index).unwrap_or(&(BankedAddress::zero(), 0))
    }

    fn get_line_addr_opt(&self, index: usize) -> Option<LineAdressOffset> {
        self.line_addresses.get(index).copied()
    }

    fn line_count(&self) -> usize {
        self.line_addresses.len()
    }
}

impl Default for CodeCache {
    fn default() -> Self {
        Self {
            bootrom_finished: false,
            line_gen: 1,
            symbols_gen: 0,
            combined_ie: 0,
            call_info_count: None,
            mapped_rom_banks: HashSet::with_capacity(8),
            mapped_data_bytes: HashSet::with_capacity_and_hasher(8, BuildAddressHasher),
            mapping: HashMap::with_capacity_and_hasher(32768, BuildAddressHasher),
            bank_mapping: HashMap::with_capacity_and_hasher(32768, BuildAddressHasher),
            is_data_map: HashSet::with_capacity_and_hasher(32768, BuildAddressHasher),
            line_mapping: HashMap::with_capacity(32768),
            line_addresses: Vec::with_capacity(32768),
        }
    }
}

impl CodeCache {
    pub fn map_code(&self, addr: BankedAddress) -> &CodeMapping {
        &self.mapping[&addr]
    }

    pub fn map_code_opt(&self, addr: BankedAddress) -> Option<&CodeMapping> {
        self.mapping.get(&addr)
    }

    pub fn update<T: SymbolProvider, B: Breakpoint, W: Watchpoint>(
        &mut self,
        core: &RunnableCore,
        memory_cache: &mut MemoryCache,
        analyzer: &mut Analyzer<B, W>,
        provider: &T
    ) {
        // Get info about current location in code
        let bootrom_toggled = core.is_boot_rom_finished() != self.bootrom_finished;
        let rom_addr = core.banked_address(0x4000);
        let new_bank_mapped = !self.mapped_rom_banks.contains(&rom_addr.bank());
        let combined_ie = self.combined_ie | core.ie();
        let symbols_changed = self.symbols_gen != provider.gen();
        let mut changed = bootrom_toggled || new_bank_mapped || symbols_changed;

        // If we didn't detect any change try and look if we jumped into a previous
        // data mapping
        let mut from_pc = false;
        if !changed {
            let index = core.pc();
            let pc_in_data = self.is_data_map.contains(&index);
            if pc_in_data && !self.mapped_data_bytes.contains(&index) {
                log::info!("PC entered data area, updating code cache...");
                self.mapped_data_bytes.insert(index);
                from_pc = true;
                changed = true;
            }
        }

        let analyzed = if changed {
            if symbols_changed {
                log::info!("Symbols changed, updating code cache...");
            }
            if bootrom_toggled {
                log::info!("Bootrom was toggled, updating code cache...");
            }
            if new_bank_mapped {
                log::info!("New bank mapped, updating code cache...");
            }

            let start = std::time::Instant::now();
            self.combined_ie = combined_ie;
            self.analyze(core, memory_cache, analyzer, provider, from_pc);
            self.mapped_rom_banks.insert(rom_addr.bank());
            log::info!("CodeCache contents updated in {:?}", start.elapsed());
            true

        } else {
            false
        };

        // Re-build line cache if either the analysis updated or the access stats have changed and
        // need attachment
        let call_infos = analyzer.call_infos();
        if analyzed || Some(call_infos.len()) != self.call_info_count {
            self.update_line_indicies(core, call_infos);
            self.call_info_count = Some(call_infos.len());
        }
    }

    fn analyze<T: SymbolProvider, B: Breakpoint, W: Watchpoint>(
        &mut self,
        core: &RunnableCore,
        memory_cache: &mut MemoryCache,
        analyzer: &mut Analyzer<B, W>,
        provider: &T,
        from_pc: bool
    ) {
        // Clean out mapping completely in case symbols changed
        if self.symbols_gen != provider.gen() {
            self.mapping.clear();
            self.symbols_gen = provider.gen();
        }

        // Cleanup after boot rom changed
        let boot_rom_size = core.boot_rom_size() as u16;
        if core.is_boot_rom_finished() != self.bootrom_finished {
            for addr in 0..0x4000 {
                self.mapping.remove(&BankedAddress::new(addr, 0));
            }
            self.bootrom_finished = core.is_boot_rom_finished();
        }

        // Data Symbols
        let mut header_symbols = HashMap::with_capacity_and_hasher(32768, BuildAddressHasher);
        header_symbols.insert(BankedAddress::new(0x104, 0), (Some("Nintendo Logo"), None));
        header_symbols.insert(BankedAddress::new(0x134, 0), (Some("Cartridge Title"), None));
        header_symbols.insert(BankedAddress::new(0x143, 0), (None, Some("Model Flag")));
        header_symbols.insert(BankedAddress::new(0x144, 0), (None, Some("Licensee")));
        header_symbols.insert(BankedAddress::new(0x146, 0), (None, Some("SGB Flag")));
        header_symbols.insert(BankedAddress::new(0x147, 0), (None, Some("Cart Type")));
        header_symbols.insert(BankedAddress::new(0x148, 0), (None, Some("ROM Size")));
        header_symbols.insert(BankedAddress::new(0x149, 0), (None, Some("RAM Size")));
        header_symbols.insert(BankedAddress::new(0x14A, 0), (None, Some("Destination")));
        header_symbols.insert(BankedAddress::new(0x14B, 0), (None, Some("Old Licensee")));
        header_symbols.insert(BankedAddress::new(0x14C, 0), (None, Some("ROM Version")));
        header_symbols.insert(BankedAddress::new(0x14D, 0), (None, Some("Header Checksum")));
        header_symbols.insert(BankedAddress::new(0x14E, 0), (None, Some("Global Checksum")));

        let mut code_locations: LocationStack = Vec::with_capacity(2048);
        if self.bootrom_finished {
            // Attach symbols
            for s in provider.dynamic_symbols().iter().filter(|s| s.is_code()) {
                if s.is_code() {
                    code_locations.push((s.address(), Some(CodeDescriptor::Label {
                        name: s.name().to_string(),
                        is_child: s.is_child(),
                        call_access_address: None
                    })));

                } else if s.is_data() {
                    header_symbols.insert(s.address(), (Some(s.name()), None));
                }
            }
            code_locations.push(
                (BankedAddress::new(0x0100, 0), Some(CodeDescriptor::Comment("Bootrom Exit".to_string())))
            );

        } else {
            code_locations.push(
                (BankedAddress::new(0x0000, 0), Some(CodeDescriptor::Comment("Bootrom Start".to_string())))
            );
        }

        // Analyze from PC if we jumped into a previous data mapping
        let pc = core.pc();
        if from_pc {
            code_locations.push((core.pc(), None));
        }

        // Remove data mappings for the currently active bank
        self.mapping.retain(|addr, m| {
            matches!(m, CodeMapping::Instruction { .. } | CodeMapping::Message { .. }) || addr.bank() != pc.bank()
        });
        self.is_data_map.retain(|addr| addr.bank() != pc.bank());

        // Disassemble code in valid address ranges
        let code_ranges = if self.bootrom_finished {
            vec![(0, 0x7FFF)]

        } else {
            vec![(0x000, boot_rom_size as usize)]
        };
        self.analyze_from(
            core,
            code_locations,
            &code_ranges,
            analyzer,
            // Only resolve symbols after bootrom has finished
            if self.bootrom_finished {
                Some(provider)

            } else {
                None
            },
            memory_cache
        );

        // Map code and data
        for (start, end) in code_ranges {
            // Find Remaining Data Blocks
            let mut addr = start;
            while addr <= end {
                let byte = &memory_cache.bytes[addr];

                // Store bank mapping for later access during line cache update
                self.bank_mapping.insert(addr, byte.addr.bank());

                // Skip over instruction data
                if let Some(m) = self.mapping.get(&byte.addr) {
                    addr += m.size();

                // Collect up to 16 bytes into one data mapping
                } else {
                    let mut memory = memory_cache.bytes[addr..=end].iter().peekable();
                    let mut bytes = Vec::with_capacity(16);
                    let next = memory.next().unwrap();
                    bytes.push(next.value);
                    while let Some(next) = memory.peek() {
                        // Check if there is no instruction mapping
                        if !self.mapping.contains_key(&next.addr) && !header_symbols.contains_key(&next.addr) {
                            // Collect the bytes
                            if bytes.len() < 16 {
                                let next = memory.next().unwrap();
                                bytes.push(next.value);

                            } else {
                                break;
                            }

                        } else {
                            break;
                        }
                    }
                    addr += bytes.len();

                    // Mark memory as data
                    for i in 0..bytes.len() {
                        self.is_data_map.insert(byte.addr.add(i as u16));
                    }

                    // Map actual data
                    let data = match header_symbols.get(&byte.addr) {
                        Some((Some(above), None)) => CodeMapping::Data {
                            bytes,
                            postfix: None,
                            above: Some(CodeDescriptor::Comment(above.to_string()))
                        },
                        Some((None, Some(postfix))) => CodeMapping::Data {
                            bytes,
                            postfix: Some(CodeDescriptor::Comment(postfix.to_string())),
                            above: None
                        },
                        _ => CodeMapping::Data { bytes, postfix: None, above: None }
                    };
                    self.mapping.insert(byte.addr, data);
                }
            }
        }
        self.line_gen += 1;
    }

    pub fn update_line_indicies(
        &mut self,
        core: &RunnableCore,
        access_calls: &HashMap<BankedAddress, CallInformation>
    ) {
        let start = std::time::Instant::now();

        self.line_addresses.clear();
        self.line_mapping.clear();

        // Serialize code and data mapping tables into a linear vector for display purposes
        let mut attachments = false;
        let code_ranges = if self.bootrom_finished {
            vec![(0, 0x7FFF)]

        } else {
            vec![(0x000, core.boot_rom_size())]
        };
        for (start, end) in code_ranges {
            let mut addr = start;
            while addr <= end {
                let bank = self.bank_mapping[&addr];
                //let bank = core.bank_for_address(addr as u16);
                let index = BankedAddress::new(addr as u16, bank);
                if let Some(m) = self.mapping.get_mut(&index) {
                    // Attach call address
                    if let CodeMapping::Instruction { above: Some(CodeDescriptor::Label { call_access_address, ..  }), .. } = m {
                        if access_calls.contains_key(&index) {
                            *call_access_address = Some(index);
                            attachments = true;
                        }
                    }

                    // Insert blank lines into serialized buffer to create space for descriptors
                    for offset in 0..m.lines() {
                        self.line_mapping.insert((index, offset + 1), self.line_addresses.len());
                        self.line_addresses.push((index, offset + 1));
                    }
                    self.line_mapping.insert((index, 0), self.line_addresses.len());
                    self.line_addresses.push((index, 0));
                    addr += m.size();

                } else {
                    addr += 1;
                }
            }
        }
        if attachments {
            self.line_gen += 1;
        }
        log::info!("CodeCache indices updated in {:?}", start.elapsed());
    }

    fn analyze_from<T: SymbolProvider, B: Breakpoint, W: Watchpoint>(
        &mut self,
        core: &RunnableCore,
        mut location_stack: LocationStack,
        address_ranges: &[(usize, usize)],
        analyzer: &mut Analyzer<B, W>,
        provider: Option<&T>,
        memory_cache: &MemoryCache
    ) {
        while let Some((entry_addr, mut desc)) = location_stack.pop() {
            // Check if already visited
            if let Some(mapping) = self.mapping.get_mut(&entry_addr) {
                // But still attach descriptors
                if let CodeMapping::Instruction { above, .. } = mapping {
                    if above.is_none() && desc.is_some() {
                        *above = desc;
                    }
                }
                continue;
            }

            // Check if address is in the set of valid address ranges
            if !address_ranges.iter().any(|(start, end)| (*start..=*end).contains(&entry_addr.offset())) {
                continue;
            }

            // Decode instructions
            let mut instruction_offset = entry_addr.offset();
            while let Some(i) = memory_cache.decode_instruction(instruction_offset) {
                let instruction_address = core.banked_address(instruction_offset as u16);

                // Don't continue into already mapped code from other jumps / calls
                if self.mapping.contains_key(&instruction_address) {
                    break;
                }

                // Inform the analyzer about potential software breakpoints
                if i.code == 0x40 {
                    analyzer.record_software_breakpoint(instruction_address);

                // Decode debug message instructions
                } else if i.code == 0x52 {
                    let l = memory_cache.raw_bytes.len();
                    let r = (instruction_offset + 1).min(l)..(instruction_offset + 7).min(l);
                    if let [0x18, len, 0x64, 0x64, 0x00, 0x00] = &memory_cache.raw_bytes[r] {
                        let msg_start = instruction_offset + 7;
                        let msg_len = len.saturating_sub(4) as usize;
                        self.mapping.insert(instruction_address, CodeMapping::Message {
                            byte_size: 7 + msg_len,
                            value: String::from_utf8_lossy(&memory_cache.raw_bytes[msg_start..msg_start + msg_len]).to_string(),
                        });
                        instruction_offset = msg_start + msg_len;
                        continue;
                    }
                }

                // Follow calls and jumpts
                if let Some(target_addr) = i.address_argument(instruction_offset as u16) {
                    let index = core.banked_address(target_addr);

                    // Resolve instruction targets
                    let name = if let Some(symbol) = provider.and_then(|p| p.resolve_symbol_absolute(index)) {
                        symbol.name().to_string()

                    } else if i.is_call() {
                        format!("call_target_from_{}", instruction_address)

                    } else if i.is_relative_jump() {
                        format!("relative_jump_target_from_{}", instruction_address)

                    } else if i.is_absolute_jump() {
                        format!("absolute_jump_target_from_{}", instruction_address)

                    } else {
                        unreachable!();
                    };
                    location_stack.push((index, Some(CodeDescriptor::Label {
                        call_access_address: None,
                        is_child: name.contains('.'),
                        name
                    })));
                }

                // Insert new instruction
                let end_of_trace = i.is_return() && !i.is_conditional() || i.is_jump() && !i.is_conditional();
                let symbol = provider.and_then(|p| p.resolve_symbol_absolute(instruction_address));
                let fallback_symbol = desc.take();
                let code = CodeMapping::Instruction {
                    postfix: None,
                    above: if let Some(s) = symbol {
                        Some(CodeDescriptor::Label {
                            call_access_address: None,
                            is_child: s.is_child(),
                            name: s.name().to_string()
                        })

                    } else {
                        fallback_symbol
                    },
                    instruction: i
                };
                instruction_offset += code.size();
                self.mapping.insert(instruction_address, code);

                // Stop any decoding beyond uncondiotional returns or jumps
                if end_of_trace {
                    break;
                }
            }
        }
    }
}

