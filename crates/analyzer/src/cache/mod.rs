// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::BankedAddress;


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod memory;
mod code;


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub type LineAdressOffset = (BankedAddress, usize);

pub trait LineCache {
    fn line_indicies_gen(&mut self) -> usize;
    fn map_line_index(&self, addr: LineAdressOffset) -> usize;
    fn get_line_addr(&self, index: usize) -> LineAdressOffset;
    fn get_line_addr_opt(&self, index: usize) -> Option<LineAdressOffset>;
    fn line_count(&self) -> usize;
}


// Re-Exports -----------------------------------------------------------------
// ----------------------------------------------------------------------------
pub use memory::*;
pub use code::*;

