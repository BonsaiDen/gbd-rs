// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use gb_cpu::Instruction;
use sameboy_rs::{BankedAddress, ReadableCore, RunnableCore};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::SymbolProvider;
use crate::cache::{LineCache, LineAdressOffset};


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Default, Clone)]
pub struct MemoryByte {
    pub addr: BankedAddress,
    pub value: u8,
    pub last_value: u8,
    pub changed: bool,
    pub symbol: Option<MemorySymbol>,
}

impl MemoryByte {
    pub fn offset(&self) -> u16 {
        if let Some(symbol) = &self.symbol {
            symbol.offset

        } else {
            0
        }
    }

    pub fn size(&self) -> u16 {
        if let Some(symbol) = &self.symbol {
            symbol.size

        } else {
            0
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum MemorySymbolKind {
    Code,
    Data,
    Register,
    Variable,
    Stack
}

#[derive(Clone)]
pub struct MemorySymbol {
    pub name: String,
    pub kind: MemorySymbolKind,
    // TODO replace with BankedAddressRange
    pub start: u16,
    pub end: u16,
    pub size: u16,
    pub offset: u16,
    pub computed: bool
}

impl MemorySymbol {
    pub fn within_range(&self, range: (u16, u16)) -> bool {
        self.start >= range.0 && self.end <= range.1
    }

    fn next(&self) -> Option<Self> {
        if self.offset < self.size.saturating_sub(1) {
            let mut next = self.clone();
            next.offset += 1;
            Some(next)

        } else {
            None
        }
    }
}


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct MemoryCache {
    pub(crate) raw_bytes: Vec<u8>,
    pub(crate) bytes: Vec<MemoryByte>,
    instructions: Vec<Instruction>,
    line_gen: usize,
    symbols_gen: usize,
    memory_byte_edited: bool,
    last_sp: u16
}

impl LineCache for MemoryCache {
    fn line_indicies_gen(&mut self) -> usize {
        self.line_gen
    }

    fn map_line_index(&self, (addr, _): LineAdressOffset) -> usize {
        addr.offset() / 16
    }

    fn get_line_addr(&self, index: usize) -> LineAdressOffset {
        if let Some(byte) = self.bytes.get(index * 16) {
            (byte.addr, 0)

        } else {
            (BankedAddress::zero(), 0)
        }
    }

    fn get_line_addr_opt(&self, index: usize) -> Option<LineAdressOffset> {
        self.bytes.get(index * 16).map(|byte| (byte.addr, 0))
    }

    fn line_count(&self) -> usize {
        0x10000 / 16
    }
}

impl Default for MemoryCache {
    fn default() -> Self {
        Self {
            raw_bytes: std::iter::repeat(0).take(0x10000).collect(),
            bytes: std::iter::repeat(MemoryByte::default()).take(0x10000).collect(),
            line_gen: 1,
            symbols_gen: 0,
            instructions: gb_cpu::instruction_list(),
            memory_byte_edited: false,
            last_sp: 0xFFFF
        }
    }
}

impl MemoryCache {
    pub fn with_bytes<C: FnMut(u16, &mut MemoryByte)>(&mut self, mut callback: C) {
        for (addr, byte) in self.bytes.iter_mut().enumerate() {
            callback(addr as u16, byte);
        }
    }

    pub fn byte(&self, addr: u16) -> &MemoryByte {
        &self.bytes[addr as usize]
    }

    pub fn set_byte(&mut self, addr: u16, value: u8) {
        self.bytes[addr as usize].value = value;
        self.memory_byte_edited = true;
    }

    pub fn return_pointer(&self, byte: &MemoryByte) -> Option<BankedAddress> {
        if byte.size() == 2 && byte.symbol.as_ref().map(|s| s.kind == MemorySymbolKind::Stack).unwrap_or(false) {
            let (low, high) = if byte.offset() == 0 {
                (byte.addr.address(), byte.addr.address().saturating_add(1))

            } else {
                (byte.addr.address().saturating_sub(1), byte.addr.address())
            };
            Some(BankedAddress::new(
                (self.bytes[high as usize].value as u16) << 8 | self.bytes[low as usize].value as u16,
                0
            ))

        } else {
            None
        }
    }

    pub fn word(&self, byte: &MemoryByte) -> Option<(&MemoryByte, &MemoryByte)> {
        if byte.size() == 2 {
            let (low, high) = if byte.offset() == 0 {
                (byte.addr.address(), byte.addr.address().saturating_add(1))

            } else {
                (byte.addr.address().saturating_sub(1), byte.addr.address())
            };
            Some((
                &self.bytes[low as usize],
                &self.bytes[high as usize]
            ))

        } else {
            None
        }
    }

    pub fn decode_instruction(&self, address: usize) -> Option<Instruction> {
        Instruction::decode(&self.raw_bytes[address..], &self.instructions, false)
    }

    pub fn update<
        T: SymbolProvider,
        U: Fn(BankedAddress) -> Option<(String, MemorySymbolKind, u16)>

    >(&mut self, core: &RunnableCore, provider: &T, symbol_compute: U, stack_base: u16) {
        let start = std::time::Instant::now();

        // Keep track of memory symbols
        let mut memory_symbol: Option<MemorySymbol> = None;
        let symbols_changed = self.symbols_gen != provider.gen();
        self.symbols_gen = provider.gen();

        // Refresh cached state from actual memory contents
        let sp = core.sp();
        for (addr, (byte, raw_byte)) in self.bytes.iter_mut().zip(&mut self.raw_bytes).enumerate() {

            // Cache byte information
            let addr = core.banked_address(addr as u16);
            let value = core.read_memory_safe(addr.address());
            byte.addr = addr;
            byte.changed = value != byte.value;
            byte.last_value = byte.value;
            byte.value = value;

            // Store raw value for quick compares in code cache
            *raw_byte = value;

            // Unmap old stack entries
            if addr.address() >= self.last_sp && addr.address() < stack_base {
                byte.symbol = None;
            }

            // Map Stack Entries
            if addr.address() >= sp && addr.address() < stack_base && stack_base.saturating_sub(sp) >= 2 {
                let index = stack_base.saturating_sub(addr.address()).saturating_sub(stack_base % 2) / 2;
                let offset = if addr.address() % 2 == stack_base % 2 { 0 } else { 1 };
                let start = addr.address().saturating_sub(offset);
                byte.symbol = Some(MemorySymbol {
                    name: format!("Entry #{}", index),
                    kind: MemorySymbolKind::Stack,
                    start,
                    end: start.saturating_add(1),
                    offset,
                    size: 2,
                    computed: false
                });

            } else if symbols_changed {
                // Continue previous symbol
                if let Some(next) = memory_symbol.as_ref().and_then(|s| s.next()) {
                    memory_symbol = Some(next);
                    byte.symbol = memory_symbol.clone();

                // Compute symbol
                } else if let Some((name, kind, size)) = symbol_compute(addr) {
                    memory_symbol = Some(MemorySymbol {
                        name,
                        kind,
                        start: addr.address(),
                        end: addr.address() + size.saturating_sub(1),
                        offset: 0,
                        size,
                        computed: true
                    });
                    byte.symbol = memory_symbol.clone();

                // Lookup new symbol
                } else if let Some(symbol) = provider.resolve_symbol_absolute(addr) {
                    if memory_symbol.as_ref().map(|s| s.name != symbol.name()).unwrap_or(true) {
                        let kind = if symbol.is_code() {
                            MemorySymbolKind::Code

                        } else if symbol.is_variable() {
                            MemorySymbolKind::Variable

                        } else if symbol.is_register() {
                            MemorySymbolKind::Register

                        } else {
                            MemorySymbolKind::Data
                        };
                        memory_symbol = Some(MemorySymbol {
                            name: symbol.name().to_string(),
                            kind,
                            start: addr.address(),
                            end: addr.address() + symbol.size().saturating_sub(1),
                            offset: 0,
                            size: symbol.size(),
                            computed: false
                        });
                        byte.symbol = memory_symbol.clone();
                    }

                } else {
                    byte.symbol = None;
                }
            }
        }
        self.last_sp = sp;
        self.line_gen += 1;
        log::info!("MemoryCache contents updated in {:?}", start.elapsed());
    }

    pub fn was_edited(&mut self) -> bool {
        let edited = self.memory_byte_edited;
        self.memory_byte_edited = false;
        edited
    }
}

