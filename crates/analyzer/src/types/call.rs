// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::collections::HashSet;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::BankedAddress;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::CPU_STAT_HISTORY;


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct CallFrame {
    pub addr: BankedAddress,
    pub cycles: u32,
    pub hl_jumps: Vec<BankedAddress>
}

pub struct CallBacktrace {
    pub sp: u16,
    pub addr: BankedAddress,
}

pub struct CallUsage {
    pub index: usize,
    pub own_cycles: u32,
    pub own_load: f32,
    pub own_average: f32,
    pub own_stats: [f32; CPU_STAT_HISTORY],
    pub own_line_cycles: [u32; 154],
    pub own_line_usage: [f32; 154],
    pub acc_cycles: u32,
    pub acc_load: f32,
    pub acc_average: f32,
    pub acc_stats: [f32; CPU_STAT_HISTORY],
    pub acc_line_cycles: [u32; 154],
    pub acc_line_usage: [f32; 154],
}

impl CallUsage {
    pub(crate) fn new(index: usize) -> Self {
        Self {
            index,
            own_cycles: 0,
            own_load: 0.0,
            own_average: 0.0,
            own_stats: [0.0; CPU_STAT_HISTORY],
            own_line_cycles: [0; 154],
            own_line_usage: [0.0; 154],
            acc_cycles: 0,
            acc_load: 0.0,
            acc_average: 0.0,
            acc_stats: [0.0; CPU_STAT_HISTORY],
            acc_line_cycles: [0; 154],
            acc_line_usage: [0.0; 154],
        }
    }
}

pub struct CallInformation {
    pub enter: Option<(u32, u32, u8, u8)>,
    pub exit: Option<(u32, u32, u8, u8)>,
    pub callers: HashSet<BankedAddress>,
    pub stack: Vec<BankedAddress>,
    frame_calls: usize,
    total_calls: usize,
    previous_cycles: u32
}

impl CallInformation {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        Self {
            enter: None,
            exit: None,
            callers: HashSet::new(),
            stack: Vec::new(),
            frame_calls: 0,
            total_calls: 0,
            previous_cycles: 0,
        }
    }

    pub fn begin(
        &mut self,
        frame: u32,
        cycle: u32,
        line: u8,
        pixel: u8,
        stack: Vec<BankedAddress>,
        caller_addr: BankedAddress
    ) {
        self.exit = None;
        self.enter = Some((frame, cycle, line, pixel));
        self.frame_calls = self.frame_calls.saturating_add(1);
        self.total_calls = self.total_calls().saturating_add(1);
        self.stack = stack;
        self.callers.insert(caller_addr);
    }

    pub fn end(&mut self, frame: u32, cycle: u32, line: u8, pixel: u8) {
        self.exit = Some((frame, cycle, line, pixel));
    }

    pub fn total_calls(&self) -> usize {
        self.total_calls
    }

    pub fn frame_calls(&self) -> usize {
        self.frame_calls
    }

    pub fn frame_lines(&self, current_line: u8) -> u8 {
        let exit_line = if let Some((_, _, exit_line, _)) = self.exit {
            exit_line

        } else {
            current_line
        };
        if let Some((_, _, enter_line, _)) = self.enter {
            if enter_line <= exit_line {
                exit_line.saturating_sub(enter_line)

            } else {
                exit_line.saturating_add(154).saturating_sub(enter_line)
            }

        } else {
            0
        }
    }

    pub fn total_cycles(&self, current_cycle: u32) -> u32 {
        self.previous_cycles + self.frame_cycles(current_cycle)
    }

    pub fn frame_cycles(&self, current_cycle: u32) -> u32 {
        let exit_cycle = if let Some((_, exit_cycle, _, _)) = self.exit {
            exit_cycle

        } else {
            current_cycle
        };
        if let Some((_, enter_cycle, _, _)) = self.enter {
            exit_cycle.saturating_sub(enter_cycle)

        } else {
            0
        }
    }

    pub fn reset_total(&mut self) {
        self.total_calls = 0;
        self.previous_cycles = 0;
        self.callers.clear();
    }

    pub fn complete_frame(&mut self, completed_frame: u32, completed_cycle: u32) {
        self.previous_cycles += self.frame_cycles(completed_cycle);
        self.frame_calls = 0;

        // Reset trace in case we exited during the last frame or any previous
        // otherwise we keep going in order to trace calls that span one or
        // more frames
        let completed = if let Some((exit_frame, _, _, _)) = self.exit {
            exit_frame <= completed_frame

        // For cases were calls span more than one frame and are called for more
        // than 1 frame in a row, we need to clean them up eventually
        } else if let Some((enter_frame, _, _, _)) = self.enter {
            // Reset trace in case we entered more than 1 frame in the past
            enter_frame <= completed_frame.saturating_sub(1)

        } else {
            false
        };

        // Save stats from last completion
        if completed {
            self.enter = None;
            self.exit = None;
        }
    }
}

