// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::BankedAddress;


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct HaltInformation {
    pub line: u8,
    pub pixel: u8,
    pub addr: BankedAddress,
    pub frame: u32,
    pub reason: HaltReason
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum HaltReason {
    BootRomFinished,
    EchoRamRead(BankedAddress),
    EchoRamWrite(BankedAddress, u8),
    MbcWrite(BankedAddress, u8),
    InvalidVramWrite(BankedAddress, u8),
    SoftwareBreakpoint,
    UserBreakpoint,
    UnitializedRamRead(BankedAddress),
    UnitializedVramRead(BankedAddress),
    WatchpointRead(BankedAddress, u8),
    WatchpointWrite(BankedAddress, u8)
}

impl std::fmt::Display for HaltReason {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::BootRomFinished => {
                write!(f, "boot rom has finished")
            },
            Self::EchoRamRead(addr) => {
                write!(f, "read from echo RAM {}", addr)
            },
            Self::EchoRamWrite(addr, value) => {
                write!(f, "write to echo RAM {} (${:0>2X})", addr, value)
            },
            Self::MbcWrite(addr, value) => {
                write!(f, "write to MBC {} (${:0>2X})", addr, value)
            },
            Self::InvalidVramWrite(addr, value) => {
                write!(f, "write to inaccessible VRAM {} (${:0>2X})", addr, value)
            },
            Self::SoftwareBreakpoint => {
                write!(f, "break instruction (ld b,b) hit")
            },
            Self::UserBreakpoint => {
                write!(f, "user defined breakpoint hit")
            },
            Self::UnitializedRamRead(addr) => {
                write!(f, "read from unitialized RAM {}", addr)
            },
            Self::UnitializedVramRead(addr) => {
                write!(f, "read from unitialized VRAM {}", addr)
            },
            Self::WatchpointRead(addr, value) => {
                write!(f, "read from watchpoint {} (${:0>2X})", addr, value)
            },
            Self::WatchpointWrite(addr, value) => {
                write!(f, "write to watchpoint {} (${:0>2X})", addr, value)
            }
        }
    }
}
