// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::{BankedAddress, ReadableCore};


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod access;
pub use access::*;
mod call;
pub use call::*;
mod halt;
pub use halt::*;
mod message;
pub use message::*;
mod symbol;
pub use symbol::*;


// Traits --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait Watchpoint {
    fn start_address(&self) -> BankedAddress;
    fn end_address(&self) -> Option<BankedAddress>;
    fn start_value(&self) -> Option<u8>;
    fn end_value(&self) -> Option<u8>;
    fn active(&self) -> bool;
    fn write(&self) -> bool;
    fn read(&self) -> bool;
    fn log(&self) -> bool;
    fn halt(&self) -> bool;
}

pub trait Breakpoint {
    fn from_software(address: BankedAddress) -> Self where Self: Sized;
    fn active(&self) -> bool;
    fn software(&self) -> bool;
    fn address(&self) -> BankedAddress;
    fn evaluate<T: SymbolProvider>(&mut self, core: &dyn ReadableCore, soft_enabled: bool, provider: &T) -> bool;
}

#[derive(Default)]
pub struct BranchInformation {
    hits: u32,
    jumps: u32
}

impl BranchInformation {
    pub fn stats(&self) -> (u32, u32, f32) {
        if self.hits > 0 {
            (self.hits, self.jumps, 100.0 / self.hits as f32 * self.jumps as f32)

        } else {
            (self.hits, self.jumps, 0.0)
        }
    }

    pub fn reset(&mut self) {
        self.hits = 0;
        self.jumps = 0;
    }

    pub fn record(&mut self, taken: bool) {
        self.hits = self.hits.saturating_add(1);
        if taken {
            self.jumps = self.jumps.saturating_add(1);
        }
    }
}
