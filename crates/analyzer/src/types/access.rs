// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::BankedAddress;


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait AddressAccess {
    fn address(&self) -> BankedAddress;
    fn value(&self) -> u16;
    fn by(&self) -> BankedAddress;
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MbcAccessKey {
    pub line: u8,
    pub pixel: u8,
    pub rom_switch: bool,
    pub ram_switch: bool,
    pub ram_enable: bool
}

pub struct MbcAccess {
    pub addr: BankedAddress,
    pub value: u8,
    pub rom_bank: Option<u16>,
    pub ram_bank: Option<u8>,
    pub ram_enable: Option<bool>,
    pub by: BankedAddress
}

impl AddressAccess for MbcAccess {
    fn address(&self) -> BankedAddress {
        self.addr
    }

    fn value(&self) -> u16 {
        if let Some(b) = self.rom_bank {
            b

        } else if let Some(b) = self.ram_bank {
            b as u16

        } else if let Some(b) = self.ram_enable {
            b as u16

        } else {
            self.value as u16
        }
    }

    fn by(&self) -> BankedAddress {
        self.by
    }
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct VramAccessKey {
    pub line: u8,
    pub pixel: u8,
    pub dma: bool,
    pub hdma: bool,
    pub write: bool,
    pub write_blocked: bool
}

pub struct VramAccess {
    pub addr: BankedAddress,
    pub value: u8,
    pub by: BankedAddress
}

impl AddressAccess for VramAccess {
    fn address(&self) -> BankedAddress {
        self.addr
    }

    fn value(&self) -> u16 {
        self.value as u16
    }

    fn by(&self) -> BankedAddress {
        self.by
    }
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct DisplayAccessKey {
    pub line: u8,
    pub pixel: u8,
    pub write: bool
}

pub struct DisplayAccess {
    pub addr: BankedAddress,
    pub value: u8,
    pub by: BankedAddress
}

impl AddressAccess for DisplayAccess {
    fn address(&self) -> BankedAddress {
        self.addr
    }

    fn value(&self) -> u16 {
        self.value as u16
    }

    fn by(&self) -> BankedAddress {
        self.by
    }
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct AudioAccessKey {
    pub line: u8,
    pub pixel: u8,
    pub write: bool
}

pub struct AudioAccess {
    pub addr: BankedAddress,
    pub value: u8,
    pub by: BankedAddress
}

#[derive(Clone)]
pub struct VramUpdate {
    pub map: bool,
    pub map_tiles: [bool; 1024],
    pub bank0: bool,
    pub bank1: bool,
    pub palette: bool,
    pub oam: bool,
    pub tiles: [bool; 768]
}

impl Default for VramUpdate {
    fn default() -> Self {
        Self {
            map: false,
            map_tiles: [false; 1024],
            bank0: false,
            bank1: false,
            palette: false,
            oam: false,
            tiles: [false; 768]
        }
    }
}

