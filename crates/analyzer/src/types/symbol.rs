// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::fmt;
use std::collections::HashMap;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::BankedAddress;


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait SymbolProvider {
    fn gen(&self) -> usize;
    fn symbols(&self) -> &[Symbol];
    fn dynamic_symbols(&self) -> Vec<&Symbol>;
    fn set_symbol_flags(&mut self, ie: u8, lcdc: u8);
    fn resolve_symbol_by_name(&self, name: &str) -> Option<&Symbol>;
    fn resolve_symbol_absolute(&self, addr: BankedAddress) -> Option<&Symbol>;
    fn resolve_symbol_name_relative(&self, addr: BankedAddress) -> String;
}


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Symbol {
    name: String,
    addr: BankedAddress,
    code_size: u16,
    data_size: u16,
    variable_size: u16,
    register_size: u16,
    user_provided: bool,
    ie_mask: u8
}

impl Symbol {
    pub fn new(
        name: String,
        addr: BankedAddress,
        code_size: u16,
        data_size: u16,
        variable_size: u16,
        register_size: u16,
        user_provided: bool

    ) -> Self {
        Self {
            addr,
            name,
            code_size,
            data_size,
            variable_size,
            register_size,
            user_provided,
            ie_mask: 0
        }
    }

    pub fn into_user_symbol(mut self) -> Self {
        self.user_provided = true;
        self
    }

    pub fn with_address(&self, addr: BankedAddress) -> Self {
        let mut s = self.clone();
        s.addr = addr;
        s
    }

    pub fn with_name(&self, name: String) -> Self {
        let mut s = self.clone();
        s.name = name;
        s
    }

    pub fn with_code_size(&self, size: u16) -> Self {
        let mut s = self.clone();
        s.code_size = size;
        s.variable_size = 0;
        s.data_size = 0;
        s
    }

    pub fn with_data_size(&self, size: u16) -> Self {
        let mut s = self.clone();
        s.code_size = 0;
        s.variable_size = 0;
        s.data_size = size;
        s
    }

    pub fn with_variable_size(&self, size: u16) -> Self {
        let mut s = self.clone();
        s.code_size = 0;
        s.variable_size = size;
        s.data_size = 0;
        s
    }

    pub fn address(&self) -> BankedAddress {
        self.addr
    }

    pub fn size(&self) -> u16 {
        self.code_size.max(self.data_size).max(self.variable_size).max(self.register_size)
    }

    pub fn code_size(&self) -> u16 {
        self.code_size
    }

    pub fn data_size(&self) -> u16 {
        self.data_size
    }

    pub fn variable_size(&self) -> u16 {
        self.variable_size
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn name_string(&self) -> &String {
        &self.name
    }

    pub fn is_user_provided(&self) -> bool {
        self.user_provided
    }

    pub fn is_child(&self) -> bool {
        self.name.contains('.')
    }

    pub fn is_code(&self) -> bool {
        self.code_size > 0
    }

    pub fn is_data(&self) -> bool {
        self.data_size > 0
    }

    pub fn is_variable(&self) -> bool {
        self.variable_size > 0
    }

    pub fn is_register(&self) -> bool {
        self.register_size > 0
    }

    pub fn is_locked(&self) -> bool {
        self.register_size > 0 || self.ie_mask != 0
    }

    fn is_active(&self, ie: u8) -> bool {
        self.ie_mask == 0 || (ie & self.ie_mask) == self.ie_mask
    }
}

impl fmt::Display for Symbol {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {} {}:{}:{}", self.addr.to_symbol_string(), self.name, self.code_size, self.data_size, self.variable_size)
    }
}

#[derive(Default, Clone)]
pub struct SymbolCache {
    gen: usize,
    ie: u8,
    symbol_list: Vec<Symbol>,
    symbol_map: HashMap<String, Symbol>,
    user_symbol_map: HashMap<String, Symbol>,
    address_to_symbol_map: HashMap<BankedAddress, Symbol>,
}

impl SymbolCache {
    pub fn reset(&mut self) {
        self.ie = 0;
    }
}

impl SymbolProvider for SymbolCache {
    fn gen(&self) -> usize {
        self.gen
    }

    fn set_symbol_flags(&mut self, ie: u8, _lcdc: u8) {
        let combined_ie = self.ie | ie;
        if combined_ie != self.ie {
            self.ie = combined_ie;
            self.gen += 1;
        }
    }

    fn symbols(&self) -> &[Symbol] {
        &self.symbol_list
    }

    fn dynamic_symbols(&self) -> Vec<&Symbol> {
        self.symbol_list.iter().filter(|s| s.is_active(self.ie)).collect()
    }

    fn resolve_symbol_by_name(&self, name: &str) -> Option<&Symbol> {
        self.symbol_map.get(name)
    }

    fn resolve_symbol_absolute(&self, addr: BankedAddress) -> Option<&Symbol> {
        self.address_to_symbol_map.get(&addr).filter(|&symbol| symbol.is_active(self.ie))
    }

    fn resolve_symbol_name_relative(&self, addr: BankedAddress) -> String {
        let (name, base_addr) = if let Some(symbol) = self.resolve_symbol_absolute(addr) {
            (Some(&symbol.name), addr.address())

        } else {
            let mut last_address = 0;
            let mut last_name = None;
            for s in &self.symbol_list {
                if s.is_active(self.ie) {
                    if s.addr.address() > addr.address() {
                        break

                    } else if s.addr.bank() == addr.bank() {
                        last_address = s.addr.address();
                        last_name = Some(&s.name);
                    }
                }
            }
            (last_name, last_address)
        };
        if let Some(name) = name {
            let offset = addr.address().saturating_sub(base_addr);
            if offset > 512 {
                addr.to_string()

            } else if offset > 0 {
                format!("{}+${:0>3X}", name, offset)

            } else {
                name.to_string()
            }

        } else {
            addr.to_string()
        }
    }
}

impl SymbolCache {
    pub fn clear(&mut self) {
        self.symbol_map.clear();
        self.user_symbol_map.clear();
    }

    pub fn insert(&mut self, s: Symbol) {
        if s.user_provided {
            self.user_symbol_map.insert(s.name.clone(), s);

        } else {
            self.symbol_map.insert(s.name.clone(), s);
        }
    }

    pub fn remove(&mut self, s: Symbol) {
        if s.user_provided {
            self.user_symbol_map.remove(&s.name);
        }
    }

    pub fn build(&mut self) {
        // Add Interrupt Vectors
        let vectors = [
            ("interrupt_vblank", 0x0040, 1),
            ("interrupt_lcd_stat", 0x0048, 2),
            ("interrupt_timer", 0x0050, 4),
            ("interrupt_serial", 0x0058, 8),
            ("interrupt_joypad", 0x0060, 16)
        ];
        for (name, addr, ie_mask) in vectors {
            self.insert(Symbol {
                name: name.to_string(),
                addr: BankedAddress::new(addr, 0),
                code_size: 8,
                data_size: 0,
                variable_size: 0,
                register_size: 0,
                user_provided: false,
                ie_mask
            });
        }

        // Add IO registers
        for addr in 0xFF00..=0xFFFF {
            if let Some(name) = Self::resolve_io_register_name(addr) {
                self.insert(Symbol {
                    name: name.to_string(),
                    addr: BankedAddress::new(addr, 0),
                    code_size: 0,
                    data_size: 0,
                    variable_size: 0,
                    register_size: 1,
                    user_provided: false,
                    ie_mask: 0
                });
            }
        }

        // Reset internal state
        self.symbol_list.clear();
        self.address_to_symbol_map.clear();
        self.gen += 1;

        // User symbols take priority
        for s in self.user_symbol_map.values() {
            self.address_to_symbol_map.insert(s.addr, s.clone());
            self.symbol_list.push(s.clone());
        }
        for s in self.symbol_map.values() {
            if !self.user_symbol_map.contains_key(&s.name) && !self.address_to_symbol_map.contains_key(&s.addr) {
                self.address_to_symbol_map.insert(s.addr, s.clone());
                self.symbol_list.push(s.clone());
            }
        }

        self.symbol_list.sort_by(|a, b| {
            a.addr.cmp(&b.addr)
        });
    }

    fn resolve_io_register_name(addr: u16) -> Option<&'static str> {
        let name = match addr {
            0xFF00 => "rP1",
            0xFF01 => "rSB",
            0xFF02 => "rSC",
            0xFF04 => "rDIV",
            0xFF05 => "rTIMA",
            0xFF06 => "rTMA",
            0xFF07 => "rTAC",
            0xFF0F => "rIF",
            0xFF10 => "rCH1_SWP",
            0xFF11 => "rCH1_DTY",
            0xFF12 => "rCH1_ENV",
            0xFF13 => "rCH1_FRQ",
            0xFF14 => "rCH1_TRG",
            0xFF16 => "rCH2_DTY",
            0xFF17 => "rCH2_ENV",
            0xFF18 => "rCH2_FRQ",
            0xFF19 => "rCH2_TRG",
            0xFF1A => "rCH3_DAC",
            0xFF1B => "rCH3_LEN",
            0xFF1C => "rCH3_VOL",
            0xFF1D => "rCH3_FRQ",
            0xFF1E => "rCH3_TRG",
            0xFF20 => "rCH4_LEN",
            0xFF21 => "rCH4_ENV",
            0xFF22 => "rCH4_FRQ",
            0xFF23 => "rCH4_TRG",
            0xFF24 => "rSND_VOLUME",
            0xFF25 => "rSND_MIXING",
            0xFF26 => "rSND_MASTER",
            0xFF30 => "rSND_WAVRAM",
            0xFF40 => "rLCDC",
            0xFF41 => "rSTAT",
            0xFF42 => "rSCY",
            0xFF43 => "rSCX",
            0xFF44 => "rLY",
            0xFF45 => "rLYC",
            0xFF46 => "rDMA",
            0xFF47 => "rBGP",
            0xFF48 => "rOBP0",
            0xFF49 => "rOBP1",
            0xFF4A => "rWY",
            0xFF4B => "rWX",
            0xFF51 => "rHDMA1",
            0xFF52 => "rHDMA2",
            0xFF53 => "rHDMA3",
            0xFF54 => "rHDMA4",
            0xFF55 => "rHDMA5",
            0xFF76 => "rPCM12",
            0xFF77 => "rPCM34",
            0xFFFF => "rIE",
            _ => return None
        };
        Some(name)
    }
}

