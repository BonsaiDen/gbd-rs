// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::BankedAddress;


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, PartialEq, Eq)]
pub struct Message {
    pub line: u8,
    pub pixel: u8,
    pub addr: BankedAddress,
    pub frame: u32,
    pub detail: MessageDetail
}

#[derive(Debug, PartialEq, Eq)]
pub enum MessageDetail {
    MsgInstruction(String),
    EchoRamRead(BankedAddress),
    EchoRamWrite(BankedAddress, u8),
    MBCBankSwitchROM(u16, u16),
    MBCBankSwitchRAM(u8, u8),
    MBCRamEnable(BankedAddress, u8),
    MBCRamDisable(BankedAddress, u8),
    GDMATransferStart {
        source: u16,
        destination: u16,
        blocks: u8
    },
    HDMATransferStart {
        source: u16,
        destination: u16,
        blocks: u8
    },
    InvalidVramWrite {
        addr: BankedAddress,
        value: u8,
        hdma: bool
    },
    UnitializedRamRead(BankedAddress),
    UnitializedVramRead(BankedAddress),
    WatchpointRead(BankedAddress, u8),
    WatchpointWrite(BankedAddress, u8),
}

