// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use mwx::ContextMenu;
use tile::{TileID, SpriteID};
use debugger_data::DebuggerState;
use sameboy_rs::{BankedAddress, BankedAddressRange, ReadableCore};
use analyzer::{SymbolProvider, MemoryByte, MemorySymbolKind};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum DebuggerContextMenu {
    JumpCode(BankedAddress),
    ViewMemoryJumpCode(BankedAddress),
    AddWatchpointViewMemory(BankedAddressRange),
    AddWatchpointViewMemoryViewTile(BankedAddressRange, TileID),
    ViewMemory(BankedAddress),
    AddWatchpointViewMemoryJumpCode(BankedAddressRange),
    Sprite(SpriteID),
    Code(BankedAddressRange, bool, bool, bool),
    MemoryByte(MemoryByte)
}

#[derive(Default)]
struct Handler {
    action: Option<DebuggerContextAction>
}

impl Handler {
    fn action(&mut self, action: DebuggerContextAction) {
        self.action = Some(action);
    }
}

pub enum DebuggerContextAction {
    AddWatchpoint(BankedAddressRange),
    ViewMemory(BankedAddress),
    ViewCode(BankedAddress),
    ToggleBreakpoint(BankedAddress),
    EditBreakpoint(BankedAddress),
    RemoveBreakpoint(BankedAddress),
    CreateSymbol(BankedAddress),
    EditSymbol(String),
    GotoTileAddress(BankedAddress),
    GotoCellIndex(usize),
    GotoSpriteIndex(usize)
}

impl ContextMenu<DebuggerState> for DebuggerContextMenu {
    type Action = DebuggerContextAction;

    fn render_ui(&self, window: &mut mwx::Window<DebuggerState>, ui: &mwx::UI) -> Option<DebuggerContextAction> {
        let mut handler = Handler::default();
        match self {
            DebuggerContextMenu::JumpCode(addr) => {
                Self::jump_to_code(&mut handler, window, ui, *addr);
            },
            DebuggerContextMenu::ViewMemoryJumpCode(addr) => {
                Self::view_memory(&mut handler, ui, *addr);
                Self::jump_to_code(&mut handler, window, ui, *addr);
            },
            DebuggerContextMenu::AddWatchpointViewMemory(range) => {
                Self::add_watchpoint(&mut handler, ui, *range);
                Self::view_memory(&mut handler, ui, **range);
            },
            DebuggerContextMenu::AddWatchpointViewMemoryViewTile(range, id) => {
                Self::add_watchpoint(&mut handler, ui, *range);
                Self::view_memory(&mut handler, ui, **range);
                if ui.selectable(id.to_string()) {
                    handler.action(DebuggerContextAction::GotoTileAddress(id.address()));
                }
            },
            DebuggerContextMenu::ViewMemory(addr) => {
                Self::view_memory(&mut handler, ui, *addr);
            },
            DebuggerContextMenu::AddWatchpointViewMemoryJumpCode(range) => {
                Self::add_watchpoint(&mut handler, ui, *range);
                Self::view_memory(&mut handler, ui, **range);
                Self::jump_to_code(&mut handler, window, ui, **range);
            },
            DebuggerContextMenu::Sprite(id) => {
                if ui.selectable(format!("View {}", id)) {
                    handler.action(DebuggerContextAction::GotoSpriteIndex(id.index()));
                }
            },
            DebuggerContextMenu::Code(range, is_jump_or_call, is_code_address, has_breakpoint) => {
                if *is_code_address {
                    Self::breakpoint(&mut handler, ui, **range, *has_breakpoint);

                } else {
                    Self::add_watchpoint(&mut handler, ui, *range);
                }
                Self::symbol(&mut handler, window, ui, **range, true);
                Self::view_memory(&mut handler, ui, **range);
                if *is_jump_or_call {
                    Self::jump_to_code(&mut handler, window, ui, **range);
                }
            },
            DebuggerContextMenu::MemoryByte(byte) => {
                Self::add_watchpoint(&mut handler, ui, byte.addr.with_size(byte.size()));
                Self::symbol(&mut handler, window, ui, byte.addr, byte.symbol.as_ref().map(|b| !b.computed).unwrap_or(false));

                if let Some(addr) = byte.symbol.as_ref().and_then(|s| if s.kind == MemorySymbolKind::Code { Some(s.start) } else { None }) {
                    Self::jump_to_code(&mut handler, window, ui, window.state.core.banked_address(addr));
                }

                if let Some(addr) = window.state.cache.memory.return_pointer(byte) {
                    Self::jump_to_code(&mut handler, window, ui, addr);
                }

                if let Some((low, high)) = window.state.cache.memory.word(byte) {
                    let addr = (high.value as u16) << 8 | low.value as u16;
                    let addr = window.state.core.banked_address(addr);
                    if ui.selectable(format!("View Memory @ {}", addr)) {
                        handler.action(DebuggerContextAction::ViewMemory(addr));
                    }

                } else if window.state.core.vram_tile_range().contains(&byte.addr.address()) {
                    let id = TileID::from_address(byte.addr);
                    if ui.selectable(format!("View {}", id)) {
                        handler.action(DebuggerContextAction::GotoTileAddress(id.address()));
                    }

                } else if window.state.core.bg_map_range().contains(&byte.addr.address()) {
                    let cell_index = byte.addr.address().saturating_sub(window.state.core.bg_map_base()) as usize;
                    if ui.selectable(format!("View BG Cell #{}", cell_index)) {
                        handler.action(DebuggerContextAction::GotoCellIndex(cell_index));
                    }

                } else if window.state.core.oam_range().contains(&byte.addr.address()) {
                    let sprite_index = (byte.addr.address().saturating_sub(window.state.core.oam_base()) / 4) as usize;
                    if ui.selectable(format!("View Sprite #{}", sprite_index)) {
                        handler.action(DebuggerContextAction::GotoSpriteIndex(sprite_index));
                    }
                }
            }
        }
        handler.action
    }
}

impl DebuggerContextMenu {
    fn add_watchpoint(handler: &mut Handler, ui: &mwx::UI, range: BankedAddressRange) {
        if ui.selectable(format!("Add Watchpoint @ {}", range)) {
            handler.action(DebuggerContextAction::AddWatchpoint(range));
        }
    }

    fn view_memory(handler: &mut Handler, ui: &mwx::UI, addr: BankedAddress) {
        if ui.selectable(format!("View Memory @ {}", addr)) {
            handler.action(DebuggerContextAction::ViewMemory(addr));
        }
    }

    fn jump_to_code(handler: &mut Handler, window: &mut mwx::Window<DebuggerState>, ui: &mwx::UI, addr: BankedAddress) {
        let name = window.state.logic.resolve_symbol_name_relative(addr);
        if ui.selectable(format!("Jump to Code @ {}", name)) {
            handler.action(DebuggerContextAction::ViewCode(addr));
        }
    }

    fn breakpoint(handler: &mut Handler, ui: &mwx::UI, addr: BankedAddress, edit: bool) {
        if ui.selectable(format!("Toggle Breakpoint @ {}", addr)) {
            handler.action(DebuggerContextAction::ToggleBreakpoint(addr));

        } else if edit && ui.selectable(format!("Edit Breakpoint @ {}", addr)) {
            handler.action(DebuggerContextAction::EditBreakpoint(addr));

        } else if edit && ui.selectable(format!("Remove Breakpoint @ {}", addr)) {
            handler.action(DebuggerContextAction::RemoveBreakpoint(addr));
        }
    }

    fn symbol(handler: &mut Handler, window: &mut mwx::Window<DebuggerState>, ui: &mwx::UI, addr: BankedAddress, edit: bool) {
        if let Some(name) = window.state.logic.resolve_symbol_absolute(addr).map(|s| s.name().to_string()) {
            if edit && ui.selectable(format!("Edit Symbol {}", name)) {
                handler.action(DebuggerContextAction::EditSymbol(name));
            }

        } else if ui.selectable(format!("Create Symbol @ {}", addr)) {
            handler.action(DebuggerContextAction::CreateSymbol(addr));
        }
    }
}

