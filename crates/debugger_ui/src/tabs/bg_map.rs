// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::time::Instant;
use std::collections::HashSet;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use mwx::Texture;
use analyzer::{VramAccess, VramAccessKey};
use debugger_data::{DebuggerConfig, DebuggerState};
use mwx::imgui::{StyleColor, DrawListMut};
use sameboy_rs::{ReadableCore, RunnableCore, BankedAddress};
use tile::{PaletteColors, MapID, Tile, TileID, TilePalette, TileUsage};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{
    DebuggerUI,
    DebuggerContextMenu,
    tabs::{DebuggerTab, tiles::Tab as TilesTab},
    util::{self, ReadWriteAverage},
    view::Extensions
};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const SCREEN_RECT_COLOR: [f32; 4] = [1.0, 1.0, 0.0, 1.0];
const WINDOW_RECT_COLOR: [f32; 4] = [0.0, 1.0, 1.0, 1.0];
const MAP_TILE_OVERLAY_COLOR: [f32; 4] = [1.0, 1.0, 1.0, 1.0];
const VRAM_BG_TILES_PER_ROW: usize = 32;
const VRAM_BG_MAP_TEXTURE_WIDTH: u32 = 512 + 32;
const VRAM_BG_MAP_TEXTURE_HEIGHT: u32 = 512 + 32;


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Tab {
    vram_gen: usize,
    vram_palette: Option<PaletteColors>,
    vram_bg_map_texture: Texture,
    scroll_rw_avg: ReadWriteAverage,
    tile_rw_avg: ReadWriteAverage,
    highlight_timer: Instant,
    highlight_cell: Option<(usize, usize, u16)>
}

impl Tab {
    pub fn new() -> Self {
        Self {
            vram_gen: 0,
            vram_palette: None,
            vram_bg_map_texture: Texture::new(VRAM_BG_MAP_TEXTURE_WIDTH, VRAM_BG_MAP_TEXTURE_HEIGHT, [0, 0, 0, 0]),
            scroll_rw_avg: ReadWriteAverage::default(),
            tile_rw_avg: ReadWriteAverage::default(),
            highlight_timer: Instant::now(),
            highlight_cell: None,
        }
    }

    pub fn goto_cell_index(&mut self, s: &mut DebuggerState, index: usize) {
        s.config.bg_map = true;
        s.config.tab_switch = 2;
        self.highlight_timer = Instant::now();
        self.highlight_cell = Some((
            index % VRAM_BG_TILES_PER_ROW,
            index / VRAM_BG_TILES_PER_ROW,
            0
        ));
    }
}

impl DebuggerTab for Tab {
    type Partner = TilesTab;

    fn title(&self) -> &'static str {
        "BG"
    }

    fn id(&self) -> &'static str {
        "TAB_BG_MAP"
    }

    fn select<'a>(&self, state: &'a mut DebuggerConfig) -> &'a mut bool {
        &mut state.bg_map
    }

    fn reset(&mut self, _s: &mut DebuggerState, _ui: &mut DebuggerUI) {
        self.vram_bg_map_texture = Texture::new(VRAM_BG_MAP_TEXTURE_WIDTH, VRAM_BG_MAP_TEXTURE_HEIGHT, [0, 0, 0, 0]);
    }

    fn update(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI) {
        // Average read / writes of scroll / window registers
        self.scroll_rw_avg.update(
            s.logic.is_halted(),
            s.logic.did_step(),
            0,
            s.logic.analyzer.borrow().bg_scw_access_writes()
        );

        // Average read / writes of tiles
        self.tile_rw_avg.update(
            s.logic.is_halted(),
            s.logic.did_step(),
            0,
            s.logic.analyzer.borrow().bg_tile_access_writes()
        );
    }

    fn refresh(&mut self, s: &mut DebuggerState, dbg_ui: &mut DebuggerUI, ui: &mwx::UI) {
        if dbg_ui.vram_gen() != self.vram_gen {
            let core = &s.core;
            let palette = TilePalette::colors(&dbg_ui.vram_dmg_palette, &s.core, s.config.tiles_use_palette);
            dbg_ui.vram_update.palette = self.vram_palette != Some(palette);

            self.refresh_bg_map_texture(dbg_ui, core, ui, &palette);
            self.vram_palette = Some(palette);
            self.vram_gen = dbg_ui.vram_gen();
        }
    }

    fn draw_content(&mut self, s: &mut DebuggerState, dbg_ui: &mut DebuggerUI, ui: &mwx::UI, tiles: &mut TilesTab) {
        let w = ui.window_content_region_max()[0];
        ui.split(2, "TAB_BG_MAP_INFOS");
        ui.set_current_column_width(w * 0.7);
        ui.header("Options", None);
        ui.checkbox("Overlay Access", &mut s.config.bg_map_overlay_access);
        ui.checkbox("Overlay Tile", &mut s.config.bg_map_overlay_tile_id);
        ui.same_line();
        if s.core.is_cgb() {
            if ui.checkbox("Overlay Palette", &mut s.config.bg_map_overlay_palette) {
                s.config.bg_map_overlay_bank = false;
            }
            ui.same_line();
            if ui.checkbox("Overlay Bank", &mut s.config.bg_map_overlay_bank) {
                s.config.bg_map_overlay_palette = false;
            }

        } else if ui.checkbox("Use Palette", &mut s.config.tiles_use_palette) {
            dbg_ui.refresh_vram();
        }
        ui.checkbox("Dim Map", &mut s.config.bg_map_darken);
        ui.same_line();
        ui.checkbox("Overlay Screen Rect", &mut s.config.bg_map_outline_screen);
        ui.same_line();
        ui.checkbox("Overlay Sprites", &mut s.config.bg_map_outline_sprites);
        ui.next_split();

        ui.header("Registers", Some(&self.scroll_rw_avg.format_writes()));
        dbg_ui.io_reg(s, ui, 0x42, "SCY");
        dbg_ui.io_reg(s, ui, 0x43, "SCX");
        dbg_ui.io_reg(s, ui, 0x4A, "WY");
        dbg_ui.io_reg(s, ui, 0x4B, "WX");

        ui.split(1, "TAB_BG_MAP_MAP");
        ui.header("VRAM BG Map", Some(&self.tile_rw_avg.format_writes()));
        ui.child_window("TAB_BG_MAP_WINDOW").size([0.0, VRAM_BG_MAP_TEXTURE_HEIGHT as f32 + 12.0 + 1.0]).build(|| {
            self.draw_vram_bg_map(s, ui, tiles);
        });
    }

    fn draw_overlay(&mut self, s: &mut DebuggerState, ui: &mwx::UI, _: &mut TilesTab, tl: [f32; 2], _br: [f32; 2]) {
        // VRAM Access Overlay
        if s.config.bg_map_overlay_access {
            let (addrs, banks) = if let Some((x, y, bank)) = self.highlight_cell {
                let addr = MapID::from_xy(x, y, s.core.lcdc()).address().address();
                (addr..addr + 1, bank..bank + 1)

            } else {
                (s.core.bg_map_range(), 0..2)
            };
            util::draw_vram_access_overlay(s, ui, tl, addrs, banks);
        }
    }
}

impl Tab {
    fn draw_vram_bg_map(&mut self, s: &mut DebuggerState, ui: &mwx::UI, tiles: &mut TilesTab) {
        // Screen Setup
        let bl = ui.cursor_screen_pos();
        let tl = [bl[0] + 2.0 + 14.0, bl[1] + 1.0 + 12.0];
        let gl = [tl[0] - 1.0, tl[1] - 1.0];
        let br = [tl[0] + VRAM_BG_MAP_TEXTURE_WIDTH as f32, tl[1] + VRAM_BG_MAP_TEXTURE_HEIGHT as f32];

        // Adjustments for pixel grid of bg map
        let scale = 1.0625;
        let wrapping = 512.0 + 32.0;
        let scy = (s.core.read_io_register(0x42) as f32 * scale * 2.0).round();
        let scx = (s.core.read_io_register(0x43) as f32 * scale * 2.0).round();
        {
            let d = ui.get_window_draw_list();

            // Index Grid
            let c = ui.style_color(StyleColor::Text);
            for i in 0..32 {
                d.add_text([bl[0] + 17.0 + i as f32 * 17.0, bl[1]], c, format!("{:0>2X}", i));
                d.add_text([bl[0], bl[1] + 15.0 + i as f32 * 17.0], c, format!("{:0>2X}", i));
                d.add_text([bl[0] + VRAM_BG_MAP_TEXTURE_WIDTH as f32 + 17.0, bl[1] + 15.0 + i as f32 * 17.0], c, format!("{:0>2X}", i));
            }

            // Map Background
            let mut color = ui.style_color(StyleColor::Header);
            color[3] = 0.1;
            d.add_rect_filled_multicolor(gl, br, color, color, color, color);

            // Overlay tile texture
            d.add_image(self.vram_bg_map_texture.id(ui), tl, br).build();

            //
            if s.config.bg_map_darken {
                let color = [0.0, 0.0, 0.0, 0.80];
                d.add_rect_filled_multicolor(gl, br, color, color, color, color);
            }

            // Screen Outline
            if s.config.bg_map_outline_screen {
                draw_window_rectangle(&d, tl, scx, scy, scale, SCREEN_RECT_COLOR);

                // Window Outline
                if s.core.is_window_enabled() {
                    let wcy = (s.core.read_io_register(0x4A) as f32 * scale * 2.0).round();
                    let wcx = (s.core.read_io_register(0x4B) as f32 * scale * 2.0).round();
                    draw_window_rectangle(&d, tl, wcx, wcy, scale, WINDOW_RECT_COLOR);
                }
            }
        }

        // Select Cells
        let vram = s.core.vram();
        let lcdc = s.core.lcdc();
        if let Some((x, y, _)) = util::cell_selection(
            ui, tl, br, 0,
            &self.highlight_timer,
            &mut self.highlight_cell,
            |_| {}, |x, y, bank| {
                let id = MapID::from_xy(x, y, lcdc);
                let addr = id.address().address();
                let tile_id = vram[id.tile_index()];
                let attr = if s.core.is_cgb() { vram[id.attr_index()] } else { 0 };
                let id = TileID::from_bg_cell(tile_id, lcdc, attr);
                DebuggerContextMenu::AddWatchpointViewMemoryViewTile(BankedAddress::new(addr, bank).into(), id)
            }) {
            let id = MapID::from_xy(x, y, lcdc);
            let addr = id.address().address();
            let tile_id = vram[id.tile_index()];
            let attr = if s.core.is_cgb() { vram[id.attr_index()] } else { 0 };
            ui.text_tooltip(&format!(
                "BG Cell #{} (${:0>4X})\n     X: {: >2}\n     Y: {: >2}\n  Tile: {:0>2X} ({}){}",
                (x + y * 32),
                addr,
                x,
                y,
                tile_id,
                tile_id,
                if s.core.is_cgb() {
                    format!(
                        "\n  Bank: {}\n  Palette: {}\n  Priority: {}\n  V-Flip: {}\n  H-Flip: {}",
                        (attr & 0b1000) >> 3,
                        attr & 0b111,
                        (attr & 0b1000_0000) >> 7,
                        (attr & 0b0100_0000) >> 6,
                        (attr & 0b0010_0000) >> 5,
                    )

                } else {
                    "".to_string()
                }
            ));
            for (_key @ VramAccessKey { line, pixel, hdma, write, .. }, values) in s.logic.analyzer.borrow().vram_access() {
                let prefix = match (*write, *hdma) {
                    (true, true) => "HDMA WR",
                    (true, false) => "VRAM WR",
                    (false, true) => "HDMA RD",
                    (false, false) => "VRAM RD"
                };
                util::overlay_pixel_tooltip(s, ui, *line, *pixel, prefix, values.iter().filter(|v| {
                    v.addr.address() == addr
                }));
            }
        }

        // Palette / Bank Overlay
        if s.config.bg_map_overlay_tile_id || s.config.bg_map_overlay_palette || s.config.bg_map_overlay_bank {
            let vram = s.core.vram();
            let d = ui.get_window_draw_list();
            let is_color = s.core.is_cgb() ;
            for y in 0..32 {
                for x in 0..32 {
                    let id = MapID::from_xy(x, y, lcdc);
                    let v = if is_color {
                        let attr = vram[id.attr_index()];
                        if s.config.bg_map_overlay_palette {
                            attr & 0b111

                        } else if s.config.bg_map_overlay_bank {
                            (attr & 0b1000) >> 3

                        } else if s.config.bg_map_overlay_tile_id {
                            255

                        } else {
                            vram[id.tile_index()]
                        }

                    } else if s.config.bg_map_overlay_tile_id {
                        vram[id.tile_index()]

                    } else {
                        0
                    };
                    let t = format!("{:0>2X}", v);
                    let width = if t.len() == 2 { 13.0} else { 6.0 };
                    let p = [ tl[0] + 17.0 * x as f32, tl[1] + 17.0 * y as f32, ];
                    let c = [0.0, 0.0, 0.0, 0.85];
                    d.add_rect_filled_multicolor(
                        [p[0] - 1.0, p[1] - 1.0],
                        [p[0] + width, p[1] + 9.0],
                        c, c, c, c
                    );
                    d.add_text([p[0] - 1.0, p[1] - 3.0], MAP_TILE_OVERLAY_COLOR, t);
                }
            }
        }

        // Access overlay
        if s.config.bg_map_overlay_access {
            let d = ui.get_window_draw_list();
            let base = s.core.bg_map_base();
            let range = s.core.bg_map_range();
            for (_key @ VramAccessKey { hdma, write, .. }, write_or_reads) in s.logic.analyzer.borrow().vram_access() {
                for VramAccess { addr, .. } in write_or_reads {
                    if range.contains(&addr.address()) {
                        let index = addr.address() - base;
                        let x = index % 32;
                        let y = index / 32;
                        let rtl = [tl[0] + x as f32 * 17.0 - 1.0, tl[1] + y as f32 * 17.0 - 1.0];
                        let rbd = [rtl[0] + 4.0, rtl[1] + 4.0];
                        let color = util::access_color(*write, *hdma);
                        d.add_rect_filled_multicolor(rtl, rbd, color, color, color, color);
                    }
                }
            }
        }

        // Sprites Outlines
        if s.config.bg_map_outline_sprites {
            let d = ui.get_window_draw_list();
            d.with_clip_rect(gl, br, || {
                tiles.draw_sprite_overlay(s, ui, &d, |x, y, size| {
                    let x = (scx + ((x - 8.0) * scale * 2.0)) % wrapping;
                    let y = (scy + ((y - 16.0) * scale * 2.0)) % wrapping;
                    ([
                        (tl[0] + x).round(),
                        (tl[1] + y).round()
                    ], [
                        (tl[0] + x + 8.0 * scale * 2.0).round(),
                        (tl[1] + y + size * scale * 2.0).round()
                    ])
                });
            });
        }
    }

    fn refresh_bg_map_texture(
        &mut self,
        dbg_ui: &mut DebuggerUI,
        core: &RunnableCore,
        ui: &mwx::UI,
        palette: &PaletteColors
    ) {
        let lcdc = core.lcdc();
        let vram = core.vram();
        let bg_offset = if (lcdc & 8) != 0 { 0x1C00 } else { 0x1800};
        let attr_offset = if (lcdc & 8) != 0 { 0x3C00 } else { 0x3800 };
        let is_color = core.is_cgb();

        // Reset usage in case of updates so tiles don't get stuck on the last usage
        let vram_update = &mut dbg_ui.vram_update;
        if vram_update.map {
            let mut unmapped_tiles = HashSet::with_capacity(32);
            for (tile_index, usage) in dbg_ui.vram_tile_usage.iter_mut().enumerate() {
                if let TileUsage::Background(_pal) = usage {
                    unmapped_tiles.insert(tile_index);
                }
            }

            if is_color {
                for (index, tile_id) in vram[bg_offset..bg_offset + 0x400].iter().enumerate() {
                    let attr = vram[attr_offset + index];
                    let pal = attr & 0b0000_0111;
                    let tile_index = TileID::from_bg_cell(*tile_id, lcdc, attr).index();
                    let usage = TileUsage::Background(pal);
                    if dbg_ui.vram_tile_usage[tile_index] != usage {
                        dbg_ui.vram_tile_usage[tile_index] = usage;
                        vram_update.tiles[tile_index] = true;
                    }
                    unmapped_tiles.remove(&tile_index);
                }

            } else {
                for (_, tile_id) in vram[bg_offset..bg_offset + 0x400].iter().enumerate() {
                    let tile_index = TileID::from_bg_cell(*tile_id, lcdc, 0).index();
                    let usage = TileUsage::Background(0);
                    if dbg_ui.vram_tile_usage[tile_index] != usage {
                        dbg_ui.vram_tile_usage[tile_index] = usage;
                        vram_update.tiles[tile_index] = true;
                    }
                    unmapped_tiles.remove(&tile_index);
                }
            }
            for tile_index in unmapped_tiles {
                dbg_ui.vram_tile_usage[tile_index] = TileUsage::Unused;
                vram_update.tiles[tile_index] = true;
            }
        }

        if vram_update.palette || vram_update.map || vram_update.bank0 || vram_update.bank1 {
            let mut buffer = self.vram_bg_map_texture.handle(ui);
            for (index, tile_id) in vram[bg_offset..bg_offset + 0x400].iter().enumerate() {
                let attr = if is_color { vram[attr_offset + index] } else { 0 };
                let tile_index = TileID::from_bg_cell(*tile_id, lcdc, attr).index();
                if vram_update.palette || vram_update.tiles[tile_index] || vram_update.map_tiles[index] {
                    let usage = if is_color {
                        TileUsage::Background(vram[attr_offset + index] & 0b111)

                    } else {
                        dbg_ui.vram_tile_usage[tile_index]
                    };
                    let range = Tile::vram_range(tile_index);
                    Tile::draw(
                        usage,
                        palette,
                        is_color,
                        &vram[range],
                        buffer.width(),
                        buffer.bytes_mut(),
                        ((attr >> 5) & 1 == 1, (attr >> 6) & 1 == 1),
                        (index % VRAM_BG_TILES_PER_ROW, index / VRAM_BG_TILES_PER_ROW)
                    );
                    buffer.refresh();
                }
            }
        }
    }
}

fn draw_window_rectangle(d: &DrawListMut, tl: [f32; 2], scx: f32, scy: f32, scale: f32, color: [f32; 4]) {
    let wrapping = 512.0 + 32.0;
    let sw = 160.0 * scale * 2.0;
    let sh = 144.0 * scale * 2.0;
    let wrapped_line_x = |mut x: f32, mut y: f32, w: f32| {
        x %= wrapping;
        y %= wrapping;
        if x + sw > wrapping {
            d.add_line([tl[0], tl[1] + y], [tl[0] + (x + w) % wrapping, tl[1] + y], color).build();
            d.add_line([tl[0] + x, tl[1] + y], [tl[0] + (x + w).min(wrapping), tl[1] + y], color).build();

        } else {
            d.add_line([tl[0] + x, tl[1] + y], [tl[0] + x + w, tl[1] + y], color).build();
        }
    };

    let wrapped_line_y = |mut x: f32, mut y: f32, h: f32| {
        x %= wrapping;
        y %= wrapping;
        if y + sh > wrapping {
            d.add_line([tl[0] + x, tl[1]], [tl[0] + x, tl[1] + (y + h) % wrapping], color).build();
            d.add_line([tl[0] + x, tl[1] + y], [tl[0] + x, tl[1] + (y + h).min(wrapping)], color).build();

        } else {
            d.add_line([tl[0] + x, tl[1] + y], [tl[0] + x, tl[1] + y + h], color).build();
        }
    };
    wrapped_line_x(scx, scy, sw);
    wrapped_line_x(scx, scy + sh - 1.0, sw);
    wrapped_line_y(scx, scy, sh);
    wrapped_line_y(scx + sw - 1.0, scy, sh);
}

