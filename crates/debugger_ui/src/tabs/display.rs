// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use tile::TilePalette;
use analyzer::DisplayAccessKey;
use sameboy_rs::{ReadableCore, WritableCore};
use debugger_data::{DebuggerConfig, DebuggerState};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{
    DebuggerUI,
    tabs::{DebuggerTab, tiles::Tab as TilesTab},
    util::{self, ReadWriteAverage},
    view::Extensions
};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const LY_OVERLAY_LINE_COLOR: [f32; 4] = [1.0, 0.0, 0.0, 1.0];
const LYC_OVERLAY_LINE_COLOR: [f32; 4] = [1.0, 0.0, 1.0, 1.0];
const DISPLAY_ACCESS_WRITE_COLOR: [f32; 4] = [1.0, 0.25, 0.0, 1.0];
const DISPLAY_ACCESS_READ_COLOR: [f32; 4] = [0.25, 1.0, 0.0, 1.0];


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Tab {
    rw_avg: ReadWriteAverage
}

impl Tab {
    pub fn new() -> Self {
        Self {
            rw_avg: ReadWriteAverage::default()
        }
    }
}

impl DebuggerTab for Tab {
    type Partner = TilesTab;

    fn title(&self) -> &'static str {
        "Display"
    }

    fn id(&self) -> &'static str {
        "TAB_DISPLAY"
    }

    fn select<'a>(&self, state: &'a mut DebuggerConfig) -> &'a mut bool {
        &mut state.display
    }

    fn update(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI) {
        // Average read / writes of all memory
        let analyzer = s.logic.analyzer.borrow();
        self.rw_avg.update(
            s.logic.is_halted(),
            s.logic.did_step(),
            analyzer.display_access_reads(),
            analyzer.display_access_writes()
        );

        s.core.set_display_background_enable(s.config.display_enable_bg);
        s.core.set_display_oam_enable(s.config.display_enable_oam);
    }

    fn draw_content(&mut self, s: &mut DebuggerState, dbg_ui: &mut DebuggerUI, ui: &mwx::UI, _: &mut TilesTab) {
        ui.split(2, "TAB_DISPLAY_SETTINGS_COLUMNS");
        ui.header("Access", Some(&self.rw_avg.format()));
        ui.checkbox("Overlay Access", &mut s.config.display_overlay_access);
        ui.checkbox("Overlay LY Outline", &mut s.config.display_show_ly);
        ui.checkbox("Overlay LYC Outline", &mut s.config.display_show_lyc);
        ui.checkbox("Overlay Sprites", &mut s.config.display_overlay_sprites);
        ui.next_split();

        ui.header("Rendering", None);
        ui.checkbox("Enable BG Rendering", &mut s.config.display_enable_bg);
        ui.checkbox("Enable Sprite Rendering", &mut s.config.display_enable_oam);
        ui.checkbox("Dim Screen", &mut s.config.display_darken);
        ui.next_split();

        let palettes = TilePalette::colors(&dbg_ui.vram_dmg_palette, &s.core, true);

        ui.split(2, "TAB_DISPLAY_INFO_COLUMNS");
        ui.header("LCDC Register", None);
        dbg_ui.io_reg_binary(s, ui, 0x40, "LCDC");
        let lcdc = s.core.lcdc();
        ui.text(format!("    7    LCD & PPU Enabled: {}", if (lcdc & 0x80) != 0 { "On" } else { "Off" }));
        ui.text(format!("    6        Win Tile Area: {}", if (lcdc & 0x40) != 0 { "$9C00-$9FFF" } else { "$9800-$9BFF" }));
        ui.text(format!("    5           Win Enable: {}", if (lcdc & 0x20) != 0 { "On" } else { "Off" }));
        ui.text(format!("    4   BG & Win Tile Data: {}", if (lcdc & 0x10) != 0 { "$8000-$8FFF" } else { "$8800-$97FF" }));
        ui.text(format!("    3         BG Tile Area: {}", if (lcdc & 8) != 0 { "$9C00-$9FFF" } else { "$9800-$9BFF" }));
        ui.text(format!("    2          Object Size: {}", if (lcdc & 4) != 0 { "8x16" } else { "8x8" }));
        ui.text(format!("    1        Object Enable: {}", if (lcdc & 2) != 0 { "On" } else { "Off" }));
        ui.text(format!("    0    BG & Win Priority: {}", if (lcdc & 1) != 0 { "On" } else { "Off" }));

        ui.header("STAT Register", None);
        dbg_ui.io_reg_binary(s, ui, 0x41, "STAT");
        let stat = s.core.read_io_register(0x41);
        ui.text(format!("    6        LYC=LY IE: {}", if (stat & 0x40) != 0 { "On"} else { "Off" }));
        ui.text(format!("    4    Mode 2 OAM IE: {}", if (stat & 0x20) != 0 { "On"} else { "Off" }));
        ui.text(format!("    4 Mode 1 VBlank IE: {}", if (stat & 0x10) != 0 { "On"} else { "Off" }));
        ui.text(format!("    3 Mode 0 HBlank IE: {}", if (stat & 0x8) != 0 { "On"} else { "Off" }));
        ui.text(format!("    2      LYC=LY Flag: {}", if (stat & 0x4) != 0 { "Equal"} else { "Different" }));
        ui.text(format!("  1-0        Mode Flag: {}", if stat & 0b11 == 0 {
            "HBlank"

        } else if stat & 0b11 == 1 {
            "VBlank"

        } else if stat & 0b11 == 2 {
            "Search"

        } else {
            "Transfer"
        }));

        ui.header("Palette BG", None);
        if s.core.is_cgb() {
            util::draw_color(ui, palettes.1.0[0], 0, false);
            util::draw_color(ui, palettes.1.0[1], 1, false);
            util::draw_color(ui, palettes.1.0[2], 2, false);
            util::draw_color(ui, palettes.1.0[3], 3, false);
            util::draw_color(ui, palettes.1.0[4], 4, false);
            util::draw_color(ui, palettes.1.0[5], 5, false);
            util::draw_color(ui, palettes.1.0[6], 6, false);
            util::draw_color(ui, palettes.1.0[7], 7, false);

        } else {
            util::draw_color(ui, palettes.0[0], 0, false);
        }
        ui.next_split();

        ui.header("Other Registers", None);
        dbg_ui.io_reg(s, ui, 0x44, "LY");
        dbg_ui.io_reg(s, ui, 0x45, "LYC");
        dbg_ui.io_reg(s, ui, 0x46, "DMA");
        dbg_ui.io_reg_binary(s, ui, 0x47, "BGP");
        dbg_ui.io_reg_binary(s, ui, 0x48, "OBJ0");
        dbg_ui.io_reg_binary(s, ui, 0x49, "OBJ1");
        ui.new_line();
        ui.new_line();
        ui.new_line();

        ui.header("GBC Registers", None);
        if s.core.is_cgb() {
            dbg_ui.io_reg_ext(s, ui, 0x4F, 0x1, " VBNK");
            // TODO need to fetch values from the core directly since
            // io readout doesn't work here
            dbg_ui.io_reg_ext(s, ui, 0x51, 0xFF, "HDMA1");
            dbg_ui.io_reg_ext(s, ui, 0x52, 0xFF, "HDMA2");
            dbg_ui.io_reg_ext(s, ui, 0x53, 0b0000_1111, "HDMA3");
            dbg_ui.io_reg_ext(s, ui, 0x54, 0xFF, "HDMA4");
            dbg_ui.io_reg_binary(s, ui, 0x55, "HDMA5");

        } else {
            ui.new_line();
            ui.new_line();
            ui.new_line();
            ui.new_line();
            ui.new_line();
            ui.new_line();
        }
        ui.new_line();

        ui.header("Palette OBJ", None);
        if s.core.is_cgb() {
            util::draw_color(ui, palettes.1.1[0], 0, true);
            util::draw_color(ui, palettes.1.1[1], 1, true);
            util::draw_color(ui, palettes.1.1[2], 2, true);
            util::draw_color(ui, palettes.1.1[3], 3, true);
            util::draw_color(ui, palettes.1.1[4], 4, true);
            util::draw_color(ui, palettes.1.1[5], 5, true);
            util::draw_color(ui, palettes.1.1[6], 6, true);
            util::draw_color(ui, palettes.1.1[7], 7, true);

        } else {
            util::draw_color(ui, palettes.0[1], 0, true);
            util::draw_color(ui, palettes.0[2], 1, true);
        }
    }

    fn draw_overlay(&mut self, s: &mut DebuggerState, ui: &mwx::UI, tiles: &mut TilesTab, tl: [f32; 2], br: [f32; 2]) {
        // LY
        if s.config.display_show_ly {
            let line = s.logic.analyzer.borrow().current_line();
            util::draw_overlay_textline(ui, tl, line, "LY", LY_OVERLAY_LINE_COLOR);
        }

        // LYC
        if s.config.display_show_ly {
            let stat = s.core.read_io_register(0x41);
            if (stat & 0x40) != 0 {
                let line = s.core.read_io_register(0x45);
                util::draw_overlay_textline(ui, tl, line, "LYC", LYC_OVERLAY_LINE_COLOR);
            }
        }

        // VRAM Access Overlay
        if s.config.display_overlay_access {
            let draw_list = ui.get_background_draw_list();
            for (DisplayAccessKey { line, pixel, write }, values) in s.logic.analyzer.borrow().display_access() {
                let color = if *write {
                    DISPLAY_ACCESS_WRITE_COLOR

                } else {
                    DISPLAY_ACCESS_READ_COLOR
                };
                if util::draw_overlay_pixel(ui, &draw_list, tl, *pixel as f32, *line as f32, color) {
                    if *write {
                        util::overlay_pixel_tooltip(s, ui, *line, *pixel, "WR", values.iter());

                    } else {
                        util::overlay_pixel_tooltip(s, ui, *line, *pixel, "RD", values.iter());
                    }
                }
            }
        }

        // Sprite Outline Overlay
        if s.config.display_overlay_sprites {
            let draw_list = ui.get_background_draw_list();
            draw_list.with_clip_rect(tl, br, || {
                let scale = ui.pixel_scale();
                tiles.draw_sprite_overlay(s, ui, &draw_list, |x, y, size| {
                    let x = (x - 8.0) * scale;
                    let y = (y - 16.0) * scale;
                    ([
                        (tl[0] + x).round(),
                        (tl[1] + y).round()
                    ], [
                        (tl[0] + x + 8.0 * scale).round(),
                        (tl[1] + y + size * scale).round()
                    ])
                });
            });
        }
    }
}

