// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::{Key, WritableCore};
use debugger_data::{DebuggerConfig, DebuggerState};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{
    DebuggerUI,
    tabs::DebuggerTab,
    util::ReadWriteAverage,
    view::Extensions
};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Tab {
    audio_rw_avg: ReadWriteAverage,
    joypad_rw_avg: ReadWriteAverage,
    input_frame: u32,
    first_update: bool
}

impl Tab {
    pub fn new() -> Self {
        Self {
            audio_rw_avg: ReadWriteAverage::default(),
            joypad_rw_avg: ReadWriteAverage::default(),
            input_frame: 255,
            first_update: true
        }
    }
}

impl DebuggerTab for Tab {
    type Partner = ();

    fn title(&self) -> &'static str {
        "Other"
    }

    fn id(&self) -> &'static str {
        "TAB_OTHER"
    }

    fn select<'a>(&self, state: &'a mut DebuggerConfig) -> &'a mut bool {
        &mut state.other
    }

    fn update(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI) {
        // Average read / writes of all memory
        self.audio_rw_avg.update(
            s.logic.is_halted(),
            s.logic.did_step(),
            s.logic.analyzer.borrow().audio_access_reads(),
            s.logic.analyzer.borrow().audio_access_writes()
        );
        self.joypad_rw_avg.update(
            s.logic.is_halted(),
            s.logic.did_step(),
            s.logic.analyzer.borrow().joypad_access_reads(),
            s.logic.analyzer.borrow().joypad_access_writes()
        );

        // Audio
        if !s.config.audio_enable || s.config.global_override_disable_sound {
            s.core.set_audio_channel_enable(0, false);
            s.core.set_audio_channel_enable(1, false);
            s.core.set_audio_channel_enable(2, false);
            s.core.set_audio_channel_enable(3, false);

        } else {
            s.core.set_audio_channel_enable(0, s.config.audio_enable_square_1);
            s.core.set_audio_channel_enable(1, s.config.audio_enable_square_2);
            s.core.set_audio_channel_enable(2, s.config.audio_enable_wave);
            s.core.set_audio_channel_enable(3, s.config.audio_enable_noise);
        }

        // Input Override
        if s.logic.in_boot_rom() {
            return;
        }

        let frame = s.logic.analyzer.borrow().frame();
        if frame != self.input_frame {
            if s.config.joypad_force_inputs_next_frame {
                s.config.joypad_force_inputs_enable = false;
            }
            self.input_frame = frame;
        }

        if self.first_update {
            s.config.joypad_force_inputs_enable = false;
            self.first_update = false;
        }

        let on = !s.config.joypad_force_inputs_alternate || frame % 2 == 0;
        if s.config.joypad_force_inputs_enable && on {
            s.core.set_key_state(Key::A, s.config.joypad_force_a);
            s.core.set_key_state(Key::B, s.config.joypad_force_b);
            s.core.set_key_state(Key::Start, s.config.joypad_force_start);
            s.core.set_key_state(Key::Select, s.config.joypad_force_select);
            s.core.set_key_state(Key::Left, s.config.joypad_force_left);
            s.core.set_key_state(Key::Right, s.config.joypad_force_right);
            s.core.set_key_state(Key::Down, s.config.joypad_force_down);
            s.core.set_key_state(Key::Up, s.config.joypad_force_up);
        }

        s.core.set_emulate_joypad_bouncing(s.config.joypad_emulate_key_bounce);
    }

    fn draw_content(&mut self, s: &mut DebuggerState, dbg_ui: &mut DebuggerUI, ui: &mwx::UI, _: &mut Self::Partner) {
        ui.header("Joypad", Some(&self.joypad_rw_avg.format()));
        ui.split(2, "OTHER_TAB_JOYPAD_INNER");
        ui.checkbox("Emulate joypad bouncing", &mut s.config.joypad_emulate_key_bounce);
        ui.checkbox("Enable Input Override", &mut s.config.joypad_force_inputs_enable);
        ui.separate_split();
        ui.disabled(!s.config.joypad_force_inputs_enable, || {
            ui.checkbox("Only for one Frame", &mut s.config.joypad_force_inputs_next_frame);
            ui.checkbox("Only for every other Frame", &mut s.config.joypad_force_inputs_alternate);
            ui.checkbox("Up", &mut s.config.joypad_force_up);
            ui.same_line_with_pos(64.0);
            ui.checkbox("Right", &mut s.config.joypad_force_right);
            ui.same_line_with_pos(128.0);
            ui.checkbox("Down", &mut s.config.joypad_force_down);
            ui.same_line_with_pos(192.0);
            ui.checkbox("Left", &mut s.config.joypad_force_left);
            ui.checkbox("A", &mut s.config.joypad_force_a);
            ui.same_line_with_pos(64.0);
            ui.checkbox("B", &mut s.config.joypad_force_b);
            ui.checkbox("Start", &mut s.config.joypad_force_start);
            ui.same_line_with_pos(64.0);
            ui.checkbox("Select", &mut s.config.joypad_force_select);
        });
        ui.next_split();
        dbg_ui.io_reg_binary(s, ui, 0x00, "JOYP");

        ui.split(1, "OTHER_TAB_AUDIO");
        ui.header("Audio", Some(&self.audio_rw_avg.format()));
        ui.split(2, "OTHER_TAB_AUDIO_INNER");
        ui.checkbox("Enable Output", &mut s.config.audio_enable);
        ui.separate_split();
        ui.disabled(!s.config.audio_enable, || {
            ui.checkbox("Enable Square 1 Channel", &mut s.config.audio_enable_square_1);
            ui.checkbox("Enable Square 2 Channel", &mut s.config.audio_enable_square_2);
            ui.checkbox("Enable Wave Channel", &mut s.config.audio_enable_wave);
            ui.checkbox("Enable Noise Channel", &mut s.config.audio_enable_noise);
        });
        ui.next_split();
        dbg_ui.io_reg(s, ui, 0x24, "VOL");
        dbg_ui.io_reg_binary(s, ui, 0x25, "L/R");
        dbg_ui.io_reg(s, ui, 0x26, "ON");
    }
}

