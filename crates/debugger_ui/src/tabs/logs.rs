// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::collections::VecDeque;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::BankedAddress;
use debugger_data::{DebuggerConfig, DebuggerState};
use analyzer::{Message, MessageDetail, SymbolProvider};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{
    icons,
    DebuggerUI,
    DebuggerContextMenu,
    tabs::DebuggerTab,
    view::Extensions
};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const MAX_MESSAGES: usize = 256;


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
type LogLine = (String, (BankedAddress, u8, u8, u32), LogMessage);
enum LogMessage {
    Info(String),
    Warn(String),
    Error(String)
}

impl LogMessage {
    fn inner(&self) -> &str {
        match self {
            Self::Info(s) | Self::Warn(s) | Self::Error(s) => s
        }
    }
}


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Tab {
    lines: VecDeque<LogLine>,
    was_halted: bool,
    new_message_count: usize
}

impl Tab {
    pub fn new() -> Self {
        Self {
            lines: VecDeque::new(),
            was_halted: false,
            new_message_count: 0
        }
    }

    fn log(&mut self, s: &mut DebuggerState, (line, pixel, addr, frame): (u8, u8, BankedAddress, u32), message: LogMessage) {
        // Foward to a possibly attached DAP
        s.logic.forward_message_to_dap(addr, message.inner().to_string());

        // Append message to UI buffer
        self.lines.push_front((
            addr.to_string(),
            (addr, line, pixel, frame),
            message
        ));
        self.new_message_count = (self.new_message_count + 1).min(MAX_MESSAGES);
        if self.lines.len() > MAX_MESSAGES {
            self.lines.pop_back();
        }
    }
}

impl DebuggerTab for Tab {
    type Partner = ();

    fn title(&self) -> &'static str {
        "Logs"
    }

    fn dynamic_title(&self, state: &DebuggerConfig) -> Option<String> {
        if self.new_message_count == 0 || state.logs {
            None

        } else {
            Some(format!("Logs ({})", self.new_message_count))
        }
    }

    fn id(&self) -> &'static str {
        "TAB_LOGS"
    }

    fn select<'a>(&self, state: &'a mut DebuggerConfig) -> &'a mut bool {
        &mut state.logs
    }

    fn update(&mut self, s: &mut DebuggerState, dbg_ui: &mut DebuggerUI) {
        // Log analyzer messages
        let messages: Vec<Message> = s.logic.analyzer.borrow_mut().messages().collect();
        for m in messages {
            let msg = match m.detail {
                MessageDetail::MsgInstruction(payload) => {
                    LogMessage::Info(format!("MSG: {}", payload))
                },
                MessageDetail::EchoRamRead(addr) => {
                    LogMessage::Error(format!("Read from echo RAM {}", addr))
                },
                MessageDetail::EchoRamWrite(addr, value) => {
                    LogMessage::Error(format!("Write to echo RAM {} (${:0>2X})", addr, value))
                },
                MessageDetail::MBCBankSwitchROM(from, to) => {
                    LogMessage::Info(format!("MBC ROM bank switched from {} to {}", from, to))
                },
                MessageDetail::MBCBankSwitchRAM(from, to) => {
                    LogMessage::Info(format!("MBC RAM bank switched from {} to {}", from, to))
                },
                MessageDetail::MBCRamEnable(addr, value) => {
                    LogMessage::Info(format!("MBC RAM enabled (${:0>2X} written to {})", value, addr))
                },
                MessageDetail::MBCRamDisable(addr, value) => {
                    LogMessage::Info(format!("MBC RAM disabled (${:0>2X} written to {})", value, addr))
                },
                MessageDetail::GDMATransferStart { source, destination, blocks } => {
                    LogMessage::Info(format!("GDMA transfer ${:0>4} bytes from ${:0>4X} to ${:0>4X}", blocks as u16 * 16, source, destination))
                },
                MessageDetail::HDMATransferStart { source, destination, blocks } => {
                    LogMessage::Info(format!("HDMA transfer ${:0>4} bytes from ${:0>4X} to ${:0>4X}", blocks as u16 * 16, source, destination))
                },
                MessageDetail::InvalidVramWrite { addr, value, hdma } => {
                    if hdma {
                        LogMessage::Error(format!("HDMA to inaccessible VRAM {} (${:0>2X})", addr, value))

                    } else {
                        LogMessage::Error(format!("Write to inaccessible VRAM {} (${:0>2X})", addr, value))
                    }
                },
                MessageDetail::UnitializedRamRead(addr) => {
                    LogMessage::Error(format!("read from unitialized RAM {}", addr))
                },
                MessageDetail::UnitializedVramRead(addr) => {
                    LogMessage::Error(format!("read from unitialized VRAM {}", addr))
                },
                MessageDetail::WatchpointRead(addr, value) => {
                    LogMessage::Error(format!("Read from watchpoint {} (${:0>2X})", addr, value))
                },
                MessageDetail::WatchpointWrite(addr, value) => {
                    LogMessage::Error(format!("Write to watchpoint {} (${:0>2X})", addr, value))
                }
            };
            self.log(s, (m.line, m.pixel, m.addr, m.frame), msg);
        }

        // Log halt reasons
        if s.logic.analyzer.borrow().is_halted() {
            if !self.was_halted {
                let information = s.logic.analyzer.borrow().halt_information();
                if let Some(i) = information {
                    self.log(s, (i.line, i.pixel, i.addr, i.frame), LogMessage::Info(format!("HALT: {}", i.reason)));
                }
            }
            self.was_halted = true;

        } else {
            self.was_halted = false;
        }

        // Log core messages
        for (info, msg) in dbg_ui.logs.borrow_mut().drain(0..) {
            self.log(s, info, LogMessage::Warn(msg));
        }
    }

    fn draw_content(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI, ui: &mwx::UI, _: &mut Self::Partner) {
        let avail = ui.content_region_avail();
        ui.input_text(icons::FILTER, &mut s.config.logs_filter).build();
        ui.same_line_with_pos(avail[0] - 32.0);
        if ui.button_with_size(icons::CLEAR_ENTRIES, [32.0, 0.0]) {
            self.lines.clear();
        }
        ui.separator();

        let avail = ui.content_region_avail();
        ui.child_window("TAB_LOGS_MESSAGES").size(avail).build(|| {
            for (loc, (addr, line, pixel, frame), m) in self.lines.iter().filter(|(_, _, m)| {
                s.config.logs_filter.is_empty() || m.inner().contains(&s.config.logs_filter)
            }) {
                let menu = || { ui.tooltip(|| {
                    let name = s.logic.resolve_symbol_name_relative(*addr);
                    ui.text(format!("Location: {}\n   Frame: {}\n    Line: {}\n   Pixel: {}", name, frame, line, pixel))
                })};
                if ui.text_custom(loc).underline().build_with_tooltip(menu) {
                    ui.open_context_menu(DebuggerContextMenu::ViewMemoryJumpCode(*addr));
                }
                ui.same_line();
                match m {
                    LogMessage::Info(s) => ui.text_colored([0.0, 0.5, 1.0, 1.0], format!("{} {}", icons::LOG_INFO, s)),
                    LogMessage::Warn(s) => ui.text_colored([1.0, 1.0, 0.0, 1.0], format!("{} {}", icons::LOG_WARN, s)),
                    LogMessage::Error(s) => ui.text_colored([1.0, 0.0, 0.0, 1.0], format!("{} {}", icons::LOG_ERROR, s))
                }
            }
        });

        // Mark messages as seen
        self.new_message_count = 0;
    }
}

