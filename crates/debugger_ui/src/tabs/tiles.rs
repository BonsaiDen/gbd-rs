// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::time::Instant;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use mwx::Texture;
use analyzer::{VramAccess, VramAccessKey};
use debugger_data::{DebuggerConfig, DebuggerState};
use sameboy_rs::{ReadableCore, RunnableCore, BankedAddress};
use tile::{PaletteColors, Sprite, Tile, TileID, TilePalette, TileUsage};
use mwx::imgui::{self, DrawListMut, MouseButton, StyleColor};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{
    DebuggerUI,
    DebuggerContextMenu,
    tabs::DebuggerTab,
    util::{self, ReadWriteAverage},
    view::Extensions
};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const SPRITE_TILE_OUTLINE_COLOR: [f32; 4] = [1.0, 0.5, 0.0, 1.0];
const SPRITE_OUTLINE_COLOR: [f32; 4] = [1.0, 1.0, 0.0, 1.0f32];
const VRAM_TILE_TEXTURE_WIDTH: u32 = 256 + 16;
const VRAM_TILE_TEXTURE_HEIGHT: u32 = 384 + 24;


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Tab {
    vram_gen: usize,
    vram_palette: Option<PaletteColors>,

    vram_sprite_tiles: [u8; 40],
    vram_bank_zero_texture: Texture,
    vram_bank_one_texture: Texture,

    vram_sprite_texture_size: bool,
    vram_sprite_textures: Vec<(usize, u8, Texture)>,

    vram_rw_avg: ReadWriteAverage,
    oam_rw_avg: ReadWriteAverage,
    highlight_timer: Instant,
    highlight_cell: Option<(usize, usize, u16)>,
    highlight_sprite_timer: Instant,
    highlight_sprite: Option<(usize, usize, u16)>
}

impl Tab {
    pub fn new() -> Self {
        Self {
            vram_gen: 0,
            vram_palette: None,

            vram_sprite_tiles: [255; 40],
            vram_bank_zero_texture: Texture::new(VRAM_TILE_TEXTURE_WIDTH, VRAM_TILE_TEXTURE_HEIGHT, [0, 0, 0, 0]),
            vram_bank_one_texture: Texture::new(VRAM_TILE_TEXTURE_WIDTH, VRAM_TILE_TEXTURE_HEIGHT, [0, 0, 0, 0]),

            vram_sprite_texture_size: false,
            vram_sprite_textures: Self::sprite_textures(16),

            vram_rw_avg: ReadWriteAverage::default(),
            oam_rw_avg: ReadWriteAverage::default(),
            highlight_timer: Instant::now(),
            highlight_cell: None,
            highlight_sprite_timer: Instant::now(),
            highlight_sprite: None
        }
    }

    pub fn goto_tile(&mut self, s: &mut DebuggerState, id: TileID) {
        s.config.tiles = true;
        s.config.tab_switch = 2;
        self.highlight_cell = Some((id.x(), id.y(), id.bank()));
        self.highlight_timer = Instant::now();
    }

    pub fn goto_sprite_index(&mut self, s: &mut DebuggerState, index: usize) {
        s.config.tiles = true;
        s.config.tab_switch = 2;
        let sprite = Sprite::map_from_oam_index(s.core.oam(), index, s.core.is_cgb());
        self.highlight_sprite_timer = Instant::now();
        self.highlight_sprite = Some((index, sprite.tile_id().index(), sprite.bank()));
    }

    pub fn draw_sprite_overlay<C: Fn(f32, f32, f32) -> ([f32; 2], [f32; 2])>(
        &mut self,
        s: &mut DebuggerState,
        ui: &mwx::UI,
        d: &imgui::DrawListMut,
        position: C
    ) {
        let core = &s.core;
        let text_height = ui.calc_text_size("L")[1];
        let size_16 = core.is_sprite_size_16();
        let sprite_size = if size_16 { [8.0, 16.0] } else { [8.0, 8.0] };
        let oam = core.oam();
        let is_color = core.is_cgb();
        for index in 0..40 {
            let sprite = Sprite::map_from_oam_index(oam, index, is_color);
            if sprite.active() {
                let (tl, br) = position(sprite.x() as f32, sprite.y() as f32, sprite_size[1]);
                if ui.mouse_inside_rect(tl, br) && ui.is_mouse_clicked(MouseButton::Right) {
                    ui.open_context_menu(DebuggerContextMenu::Sprite(sprite.id()));
                }
                let color = self.draw_sprite(
                    ui, d, tl, br,
                    sprite,

                ).unwrap_or(SPRITE_OUTLINE_COLOR);
                d.add_rect(tl, br, color).build();
                d.add_text([tl[0], tl[1] - text_height], color, format!("{}", index));
            }
        }
    }
}

impl DebuggerTab for Tab {
    type Partner = ();

    fn title(&self) -> &'static str {
        "Tiles"
    }

    fn id(&self) -> &'static str {
        "TAB_TILES"
    }

    fn select<'a>(&self, state: &'a mut DebuggerConfig) -> &'a mut bool {
        &mut state.tiles
    }

    fn reset(&mut self, _s: &mut DebuggerState, _ui: &mut DebuggerUI) {
        self.vram_sprite_textures = Self::sprite_textures(16);
        self.vram_bank_zero_texture = Texture::new(VRAM_TILE_TEXTURE_WIDTH, VRAM_TILE_TEXTURE_HEIGHT, [0, 0, 0, 0]);
        self.vram_bank_one_texture = Texture::new(VRAM_TILE_TEXTURE_WIDTH, VRAM_TILE_TEXTURE_HEIGHT, [0, 0, 0, 0]);
    }

    fn update(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI) {
        // Average read / writes of VRAM
        self.vram_rw_avg.update(
            s.logic.is_halted(),
            s.logic.did_step(),
            s.logic.analyzer.borrow().vram_access_reads(),
            s.logic.analyzer.borrow().vram_access_writes()
        );

        // Average read / writes of OAM
        self.oam_rw_avg.update(
            s.logic.is_halted(),
            s.logic.did_step(),
            0,
            s.logic.analyzer.borrow().oam_access_writes()
        );

        // HDMA Transfer Logs
        let mut analyzer = s.logic.analyzer.borrow_mut();
        analyzer.set_log_hdma_transfer(s.config.tiles_log_hdma_transfer);
    }

    fn refresh(&mut self, s: &mut DebuggerState, dbg_ui: &mut DebuggerUI, ui: &mwx::UI) {
        if dbg_ui.vram_gen() != self.vram_gen {
            let core = &s.core;
            let palette = TilePalette::colors(&dbg_ui.vram_dmg_palette, core, s.config.tiles_use_palette);
            dbg_ui.vram_update.palette = self.vram_palette != Some(palette);

            self.refresh_sprite_textures(dbg_ui, core, ui, &palette);
            self.refresh_bank_texture(dbg_ui, core, ui, &palette, 0);
            if core.is_cgb() {
                self.refresh_bank_texture(dbg_ui, core, ui, &palette, 1);
            }
            self.vram_palette = Some(palette);
            self.vram_gen = dbg_ui.vram_gen();
        }
    }

    fn draw_content(&mut self, s: &mut DebuggerState, dbg_ui: &mut DebuggerUI, ui: &mwx::UI, _: &mut Self::Partner) {
        ui.checkbox("Overlay Access", &mut s.config.tiles_overlay_access);
        ui.same_line();
        ui.disabled(s.core.is_cgb(), || {
            if ui.checkbox("Use Palette", &mut s.config.tiles_use_palette) {
                dbg_ui.refresh_vram();
            }
        });
        ui.same_line();
        ui.checkbox("Log HDMA Transfer", &mut s.config.tiles_log_hdma_transfer);

        ui.header("VRAM Tile Banks", Some(&self.vram_rw_avg.format()));
        ui.child_window("TAB_TILES_BANKS_WINDOW").size([0.0, VRAM_TILE_TEXTURE_HEIGHT as f32 + 12.0 + 1.0]).build(|| {
            ui.split(2, "TILES_TAB_COLUMNS");
            ui.set_current_column_width(VRAM_TILE_TEXTURE_WIDTH as f32 + 6.0 + 14.0);
            self.draw_vram_bank_tiles(s, ui, 0);
            if s.core.is_cgb() {
                ui.next_split();
                self.draw_vram_bank_tiles(s, ui, 1);
            }
        });

        self.draw_sprite_grid(s, ui);
    }

    fn draw_overlay(&mut self, s: &mut DebuggerState, ui: &mwx::UI, _: &mut Self::Partner, tl: [f32; 2], _br: [f32; 2]) {
        // VRAM Access Overlay
        if s.config.tiles_overlay_access {
            let (addrs, banks) = if let Some((x, y, bank)) = self.highlight_cell {
                let id = TileID::from_xy(x, y, bank);
                let addr = id.address().address();
                let bank = id.bank();
                (addr..addr + 16, bank..bank + 1)

            } else {
                (s.core.vram_tile_range(), 0..2)
            };
            util::draw_vram_access_overlay(s, ui, tl, addrs, banks);
        }
    }
}

impl Tab {
    fn sprite_textures(height: u32) -> Vec<(usize, u8, Texture)> {
        std::iter::repeat(()).take(40).map(|_| (512, 0, Texture::new(16, height, [0, 0, 0, 0]))).collect()
    }

    fn draw_vram_bank_tiles(&mut self, s: &mut DebuggerState, ui: &mwx::UI, bank: u16) {
        let bl = ui.cursor_screen_pos();
        let tl = [bl[0] + 2.0 + 14.0, bl[1] + 1.0 + 12.0];
        let gl = [tl[0] - 1.0, tl[1] - 1.0];
        let br = [tl[0] + VRAM_TILE_TEXTURE_WIDTH as f32, tl[1] + VRAM_TILE_TEXTURE_HEIGHT as f32];
        {
            let d = ui.get_window_draw_list();

            // Index Grid
            let c = ui.style_color(StyleColor::Text);
            for x in 0..16 {
                d.add_text([bl[0] + 17.0 + x as f32 * 17.0, bl[1]], c, format!("{:0>2X}", x));
            }
            for y in 0..32 {
                d.add_text([bl[0], bl[1] + 15.0 + y as f32 * 17.0], c, format!("{:0>2X}", y));
            }

            // Grid Background
            let color = ui.style_color(StyleColor::Header);
            d.add_rect_filled_multicolor(gl, br, color, color, color, color);

            // Overlay tile texture
            if bank == 1 {
                d.add_image(self.vram_bank_one_texture.id(ui), tl, br).build();

            } else {
                d.add_image(self.vram_bank_zero_texture.id(ui), tl, br).build();
            }
        }

        // Select Tiles
        let size_16 = s.core.is_sprite_size_16();
        if let Some((x, y, bank)) = util::cell_selection(
            ui, tl, br, bank,
            &self.highlight_timer,
            &mut self.highlight_cell,
            |d| {
                // Highlight tiles from current sprite selection
                if let Some((_, tile_id, b)) = self.highlight_sprite {
                    if b == bank {
                        let x = tile_id % 16;
                        let y = tile_id / 16;
                        let rtl = [tl[0] + x as f32 * 17.0 - 1.0, tl[1] + y as f32 * 17.0 - 1.0];
                        let rbr = if size_16 {
                            [rtl[0] + 35.0, rtl[1] + 18.0]

                        } else {
                            [rtl[0] + 18.0, rtl[1] + 18.0]
                        };
                        d.add_rect(rtl, rbr, SPRITE_TILE_OUTLINE_COLOR).build();
                    }
                }

            }, |x, y, bank| {
                DebuggerContextMenu::AddWatchpointViewMemory(TileID::from_xy(x, y, bank).range())
            }
        ) {
            let id = TileID::from_xy(x, y, bank);
            let range = id.range();
            ui.text_tooltip(&format!("{} ({})", id, range));
            for (_key @ VramAccessKey { line, pixel, hdma, write, .. }, values) in s.logic.analyzer.borrow().vram_access() {
                let prefix = match (*write, *hdma) {
                    (true, true) => "HDMA WR",
                    (true, false) => "VRAM WR",
                    (false, true) => "HDMA RD",
                    (false, false) => "VRAM RD"
                };
                util::overlay_pixel_tooltip(s, ui, *line, *pixel, prefix, values.iter().filter(|v| {
                    range.contains(v.addr)
                }));
            }
        }

        // Access overlay
        if s.config.tiles_overlay_access {
            let d = ui.get_window_draw_list();
            for (_key @ VramAccessKey { hdma, write, .. }, write_or_reads) in s.logic.analyzer.borrow().vram_access() {
                for VramAccess { addr, .. } in write_or_reads {
                    if addr.bank() == bank && addr.address() < 0x9800 {
                        let index = (addr.offset() - 0x8000) / Tile::BYTES_PER_TILE;
                        let x = index % 16;
                        let y = index / 16;
                        let rtl = [tl[0] + x as f32 * 17.0 - 1.0, tl[1] + y as f32 * 17.0 - 1.0];
                        let rbd = [rtl[0] + 4.0, rtl[1] + 4.0];
                        let color = util::access_color(*write, *hdma);
                        d.add_rect_filled_multicolor(rtl, rbd, color, color, color, color);
                    }
                }
            }
        }
    }

    fn draw_sprite_grid(&mut self, s: &mut DebuggerState, ui: &mwx::UI) {
        let is_color = s.core.is_cgb();
        let oam = s.core.oam();
        let size_16 = s.core.is_sprite_size_16();

        ui.header("Sprites", Some(&self.oam_rw_avg.format_writes()));

        let p = ui.cursor_pos();
        for index in 0..40 {
            let sprite = Sprite::map_from_oam_index(oam, index, is_color);

            // Make inactive sprites half transparent
            let alpha = if sprite.active() { 1.0 } else { 0.25 };
            let a = ui.push_style_var(imgui::StyleVar::Alpha(alpha));

            // Text
            let ox = ((index % 10) as f32 * 59.25).floor();
            let oy = ((index / 10) as f32 * 47.5).floor();
            ui.set_cursor_pos([p[0] + ox, p[1] + oy- 2.0]);
            ui.text(format!("{: >3}", index));
            let sp = ui.cursor_screen_pos();
            ui.set_cursor_pos([p[0] + ox, p[1] + oy + 10.0]);
            ui.text(format!("x{:0>2X}", sprite.x()));
            ui.set_cursor_pos([p[0] + ox, p[1] + oy + 22.0]);
            ui.text(format!("y{:0>2X}", sprite.y()));
            a.pop();

            // Texture
            let height = if size_16 { 33.0 } else { 17.0 };
            let d = ui.get_window_draw_list();
            let tl = [sp[0] + ox + 28.0, sp[1] - 16.0];
            let br = [tl[0] + 16.0, tl[1] + height];

            let gl = [tl[0] - 1.0, tl[1] - 1.0];
            let gb = [br[0] + 1.0, br[1] + 1.0];
            let tint = if sprite.active() { [1.0, 1.0, 1.0, 1.0] } else { [0.3, 0.3, 0.3, 1.0] };
            let bg = if sprite.active() { ui.style_color(StyleColor::Header) } else { [0.25, 0.25, 0.25, 1.0] };

            d.add_rect_filled_multicolor(tl, br, bg, bg, bg, bg);
            d.add_image(self.vram_sprite_textures[index].2.id(ui), tl, br).col(tint).build();

            // Context Menu
            let hover = ui.mouse_inside_rect(tl, br);
            if hover && ui.is_mouse_clicked(MouseButton::Right) {
                ui.open_context_menu(DebuggerContextMenu::ViewMemory(BankedAddress::new((0xFE00 + index * 4) as u16, 0)));
            }

            // Outline and Tooltip
            let color = self.draw_sprite(
                ui, &d, gl, gb,
                sprite

            ).unwrap_or(tint);
            d.add_rect(gl, gb, color).build();
        }
    }

    #[allow(clippy::too_many_arguments)]
    fn draw_sprite(
        &mut self,
        ui: &mwx::UI,
        d: &DrawListMut,
        tl: [f32; 2],
        br: [f32; 2],
        sprite: Sprite

    ) -> Option<[f32; 4]> {
        let hover = ui.mouse_inside_rect(tl, br);
        if hover {
            ui.text_tooltip(&sprite.to_string());
            if ui.is_mouse_clicked(MouseButton::Left) {
                if Some(sprite.index()) == self.highlight_sprite.map(|(i, _, _)| i) {
                    self.highlight_sprite = None

                } else {
                    self.highlight_sprite = Some((sprite.index(), sprite.tile_id().index(), sprite.bank()));
                }
            }
        }
        if Some(sprite.index()) == self.highlight_sprite.map(|(i, _, _)| i) {
            if let Some(t) = util::highlight_factor(&self.highlight_sprite_timer, 5.0) {
                let mut c = SPRITE_TILE_OUTLINE_COLOR;
                c[3] = 0.85 * t;
                let s = (4.0 * t).floor();
                d.add_rect_filled_multicolor(
                    [tl[0] - s, tl[1] - s],
                    [br[0] + s, br[1] + s],
                    c, c, c, c
                );
            }
            Some(SPRITE_TILE_OUTLINE_COLOR)

        } else if hover {
            Some(SPRITE_TILE_OUTLINE_COLOR)

        } else {
            None
        }
    }
}

impl Tab {
    fn refresh_bank_texture(
        &mut self,
        dbg_ui: &mut DebuggerUI,
        core: &RunnableCore,
        ui: &mwx::UI,
        palette: &PaletteColors,
        bank: usize
    ) {
        let is_color = core.is_cgb();
        let vram_update = &dbg_ui.vram_update;
        if vram_update.palette || vram_update.oam || vram_update.map || vram_update.bank0 && bank == 0 || vram_update.bank1 && bank == 1 {
            let mut buffer = if bank == 0 {
                self.vram_bank_zero_texture.handle(ui)

            } else {
                self.vram_bank_one_texture.handle(ui)
            };
            let (tile_base, range) = Tile::bank_vram_range(bank);
            for (tile_id, tile_data) in core.vram()[range].chunks(Tile::BYTES_PER_TILE).enumerate() {
                let tile_index = tile_base + tile_id;
                if vram_update.palette || vram_update.tiles[tile_index] {
                    Tile::draw(
                        dbg_ui.vram_tile_usage[tile_index],
                        palette,
                        is_color,
                        tile_data,
                        buffer.width(),
                        buffer.bytes_mut(),
                        (false, false),
                        (tile_id % Tile::TILES_PER_ROW, tile_id / Tile::TILES_PER_ROW)
                    );
                    buffer.refresh();
                }
            }
        }
    }

    fn refresh_sprite_textures(
        &mut self,
        dbg_ui: &mut DebuggerUI,
        core: &RunnableCore,
        ui: &mwx::UI,
        palette: &PaletteColors
    ) {
        let is_color = core.is_cgb();
        let lcdc = core.lcdc();
        let size_16 = (lcdc & 4) != 0;

        // Check for tiles used by OAM and their corresponding palettes
        let vram_update = &mut dbg_ui.vram_update;
        if (lcdc & 2) != 0 && vram_update.oam {
            for (index, sprite) in core.oam().chunks(4).enumerate() {
                if let [y, _, tile_id, attr] = *sprite {
                    let tile_index = TileID::from_sprite(tile_id, attr, is_color).index();
                    if y > 0 {
                        // Mark tile as being used by a sprite
                        let usage = if is_color {
                            TileUsage::Sprite(attr & 0b111)

                        } else {
                            TileUsage::Sprite((attr >> 4) & 0x1)
                        };
                        dbg_ui.vram_tile_usage[tile_index] = usage;
                        if size_16 {
                            dbg_ui.vram_tile_usage[tile_index.wrapping_add(1)] = usage;
                        }

                        // Check if tile index used by sprite changed and if to mark the tile as dirty
                        if self.vram_sprite_tiles[index] != tile_index as u8 {
                            self.vram_sprite_tiles[index] = tile_index as u8;
                            vram_update.tiles[tile_index] = true;
                        }

                    // Mark the tile as dirty in case it was previously used
                    } else if self.vram_sprite_tiles[index] != 255 {
                        self.vram_sprite_tiles[index] = 255;
                        vram_update.tiles[tile_index] = true;
                    }
                }
            }
        }

        // Re-size textures if required
        let mut resized = false;
        if size_16 != self.vram_sprite_texture_size {
            self.vram_sprite_textures = Self::sprite_textures(if size_16 { 33 } else { 17 });
            self.vram_sprite_texture_size = size_16;
            resized = true;
        }

        if (lcdc & 2) != 0 || resized {
            let vram = core.vram();
            let is_color = core.is_cgb();
            for (index, sprite) in core.oam().chunks(4).enumerate() {
                if let [_, _, tile_id, attr] = *sprite {
                    let tile_index = TileID::from_sprite(tile_id, attr, is_color).index();
                    let texture = &mut self.vram_sprite_textures[index];
                    if texture.0 != tile_index || texture.1 != attr || vram_update.palette || vram_update.tiles[tile_index] || resized {
                        let mut buffer = texture.2.handle(ui);
                        let flip = ((attr >> 5) & 1 == 1, (attr >> 6) & 1 == 1);
                        let usage = if is_color {
                            TileUsage::Sprite(attr & 0b111)

                        } else {
                            TileUsage::Sprite((attr >> 4) & 0x1)
                        };
                        let (top, bottom) = if flip.1 {
                            (tile_index.wrapping_add(1), tile_index)

                        } else {
                            (tile_index, tile_index.wrapping_add(1))
                        };
                        Tile::draw(
                            usage,
                            palette,
                            is_color,
                            &vram[Tile::vram_range(top)],
                            buffer.width(),
                            buffer.bytes_mut(),
                            flip,
                            (0, 0)
                        );
                        if size_16 {
                            Tile::draw(
                                usage,
                                palette,
                                is_color,
                                &vram[Tile::vram_range(bottom)],
                                buffer.width(),
                                buffer.bytes_mut(),
                                flip,
                                (0, 1)
                            );
                        }
                        buffer.refresh();
                        texture.1 = attr;
                        texture.0 = tile_index;
                    }
                }
            }
        }
    }
}
