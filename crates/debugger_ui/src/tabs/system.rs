// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::time::{Duration, Instant};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use analyzer::{MbcAccessKey, SymbolProvider};
use sameboy_rs::{BankedAddress, ReadableCore};
use debugger_data::{DebuggerConfig, DebuggerState};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{
    DebuggerUI,
    DebuggerContextMenu,
    tabs::DebuggerTab,
    util::{self, ReadWriteAverage},
    view::Extensions
};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const MBC_ACCESS_BANK_SWITCH_COLOR: [f32; 4] = [0.0, 1.0, 0.25, 1.0];
const MBC_ACCESS_RAM_ENABLE_COLOR: [f32; 4] = [0.0, 0.25, 1.0, 1.0];
const MBC_ACCESS_OTHER_COLOR: [f32; 4] = [1.0, 1.0, 1.0, 1.0];


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Tab {
    interrupt_scan_time: Instant,
    interrupt_vblank: Option<BankedAddress>,
    interrupt_stat: Option<BankedAddress>,
    interrupt_timer: Option<BankedAddress>,
    interrupt_serial: Option<BankedAddress>,
    interrupt_joypad: Option<BankedAddress>,
    mbc_rw_avg: ReadWriteAverage
}

impl Tab {
    pub fn new() -> Self {
        Self {
            interrupt_scan_time: Instant::now(),
            interrupt_vblank: None,
            interrupt_stat: None,
            interrupt_timer: None,
            interrupt_serial: None,
            interrupt_joypad: None,
            mbc_rw_avg: ReadWriteAverage::default()
        }
    }

    fn int_handler(
        &mut self,
        s: &mut DebuggerState,
        ui: &mwx::UI,
        (base_addr, name, bit): (u16, &str, u8),
        target: Option<BankedAddress>
    ) {
        let ie = s.core.ie();
        let enabled = (ie >> bit) & 1 == 1;
        ui.text(format!("${:0>4X} {}: {}", base_addr, name, enabled as u8));
        ui.same_line();
        if let Some(addr) = target {
            let handler = s.logic.resolve_symbol_name_relative(addr);
            if ui.text_custom(&handler).underline().build() {
                ui.open_context_menu(DebuggerContextMenu::ViewMemoryJumpCode(addr));
            }

        } else {
            ui.text("<unknown>");
        }
    }

    fn resolve_interrupt_handler(
        &self,
        s: &mut DebuggerState,
        addr: u16,
        bit: u8

    ) -> Option<BankedAddress> {
        let ie = s.core.ie();
        if (ie >> bit) & 1 == 1 {
            let last_address = (addr + 8) as usize;
            let mut instruction_address = addr as usize;
            while let Some(i) = s.cache.memory.decode_instruction(instruction_address) {
                if i.is_jump() && !i.is_conditional() {
                    return i.address_argument(instruction_address as u16).map(|addr| BankedAddress::new(addr, 0));
                }
                instruction_address += i.size;
                if instruction_address >= last_address {
                    break;
                }
            }
            None

        } else {
            None
        }
    }
}

impl DebuggerTab for Tab {
    type Partner = ();

    fn title(&self) -> &'static str {
        "System"
    }

    fn id(&self) -> &'static str {
        "TAB_SYSTEM"
    }

    fn select<'a>(&self, state: &'a mut DebuggerConfig) -> &'a mut bool {
        &mut state.system
    }

    fn reset(&mut self, _s: &mut DebuggerState, _ui: &mut DebuggerUI) {
        // TODO scan immediately?
        // self.interrupt_scan_time = Instant::now().checked_sub
    }

    fn update(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI) {
        // Scan Memory for Interrupt handlers
        if s.logic.did_step() || !s.logic.is_halted() && self.interrupt_scan_time.elapsed() > Duration::from_millis(1000) {
            self.interrupt_vblank = self.resolve_interrupt_handler(s, 0x0040, 0);
            self.interrupt_stat = self.resolve_interrupt_handler(s, 0x0048, 1);
            self.interrupt_timer = self.resolve_interrupt_handler(s, 0x0050, 2);
            self.interrupt_serial = self.resolve_interrupt_handler(s, 0x0058, 3);
            self.interrupt_joypad = self.resolve_interrupt_handler(s, 0x0060, 4);
            self.interrupt_scan_time = Instant::now();
        }

        // Average read / writes for MBC
        self.mbc_rw_avg.update(
            s.logic.is_halted(),
            s.logic.did_step(),
            s.logic.analyzer.borrow().mbc_access_reads(),
            s.logic.analyzer.borrow().mbc_access_writes()
        );

        // Halting
        let mut analyzer = s.logic.analyzer.borrow_mut();
        let active = !s.config.global_override_disable_halts;
        analyzer.set_halt_on_memory_access(
            active && s.config.system_halt_on_blocked_vram_write,
            active && s.config.system_halt_on_unitialized_vram_read,
            active && s.config.system_halt_on_unitialized_ram_read,
            active && s.config.system_halt_on_echo_ram_access,
            active && s.config.system_halt_on_mbc_write
        );

        // Software breakpoints
        analyzer.set_halt_on_software_break(
            active && s.config.system_halt_on_software_breakpoint
        );

        // MBC Logging
        analyzer.set_log_mbc_access(s.config.system_log_mbc_access);
    }

    fn draw_content(&mut self, s: &mut DebuggerState, dbg_ui: &mut DebuggerUI, ui: &mwx::UI, _: &mut Self::Partner) {
        ui.split(2, "SYSTEM_TAB_COLUMNS");
        ui.header("General", None);
        ui.checkbox("Skip Boot Rom / Logo", &mut s.config.system_skip_boot_rom);
        ui.checkbox("Overlay Frames", &mut s.config.system_overlay_frames);
        ui.next_split();

        ui.header("Halt Options", None);
        ui.checkbox("Halt when opening debugger", &mut s.config.system_halt_on_debugger);
        ui.checkbox("Halt after Boot Rom / Logo", &mut s.config.system_halt_after_boot_rom);
        ui.separate_split();
        ui.checkbox("Halt on blocked VRAM write", &mut s.config.system_halt_on_blocked_vram_write);
        ui.checkbox("Halt on uninitialized VRAM read", &mut s.config.system_halt_on_unitialized_vram_read);
        ui.checkbox("Halt on uninitialized RAM read", &mut s.config.system_halt_on_unitialized_ram_read);
        ui.checkbox("Halt on echo RAM access", &mut s.config.system_halt_on_echo_ram_access);
        ui.separate_split();
        ui.checkbox("Halt on MBC writes", &mut s.config.system_halt_on_mbc_write);
        ui.checkbox("Halt on BRK (ld b,b) instruction", &mut s.config.system_halt_on_software_breakpoint);
        ui.next_split();

        ui.split(1, "SYSTEM_TAB_INTERRUPTS");
        ui.header("Interrupts", None);
        ui.text(format!("         IME: {}", s.core.ime() as u8));
        self.int_handler(s, ui, (0x0040, "VBlank", 0), self.interrupt_vblank);
        self.int_handler(s, ui, (0x0048, "  STAT", 1), self.interrupt_stat);
        self.int_handler(s, ui, (0x0050, " Timer", 2), self.interrupt_timer);
        self.int_handler(s, ui, (0x0058, "Serial", 3), self.interrupt_serial);
        self.int_handler(s, ui, (0x0060, "Joypad", 4), self.interrupt_joypad);

        ui.split(3, "SYSTEM_TAB_MISC");
        ui.header("Timer Registers", None);
        dbg_ui.io_reg(s, ui, 0x04, "DIV");
        dbg_ui.io_reg(s, ui, 0x05, "TIMA");
        dbg_ui.io_reg(s, ui, 0x06, "TMA");
        dbg_ui.io_reg(s, ui, 0x07, "TAC");
        ui.new_line();
        ui.next_split();

        ui.header("GBC Registers", None);
        ui.text(format!("WRAM Bank: {:0>2X}", s.core.gbc_ram_bank()));
        ui.next_split();

        let name = s.core.mbc_name();
        if let Some(name) = name {
            ui.header(&format!("MBC: {}", name), Some(&self.mbc_rw_avg.format_writes()));

        } else {
            ui.header("MBC: None", Some(&self.mbc_rw_avg.format_writes()));
        }
        ui.disabled(name.is_none(), || {
            ui.checkbox("Overlay Access", &mut s.config.system_overlay_mbc_access);
            ui.checkbox("Log Access", &mut s.config.system_log_mbc_access);
        });
        if name.is_some() {
            ui.separate_split();
            ui.text(format!("  ROM Bank: {:0>2X}", s.core.mbc_rom_bank()));
            ui.text(format!("  RAM Bank: {:0>2X}", s.core.mbc_ram_bank()));
            ui.text(format!("  RAM Size: {}", s.core.mbc_ram_size()));
            ui.text(format!("RAM Access: {}", if s.core.mbc_ram_enable() { "Enabled" } else { "Disabled" }));
        }
        ui.next_split();
    }

    fn draw_overlay(&mut self, s: &mut DebuggerState, ui: &mwx::UI, _: &mut Self::Partner, tl: [f32; 2], _br: [f32; 2]) {
        if s.config.system_overlay_frames {
            let d = ui.get_background_draw_list();
            let analyzer = s.logic.analyzer.borrow();
            let frame = analyzer.frame();
            if analyzer.is_halted() {
                let relativ_frame = frame.saturating_sub(s.logic.base_frame());
                d.add_text([tl[0], tl[1] - 13.0], [1.0, 0.0, 0.0, 1.0], format!("HALTED | FRAME = {} / +{}", frame, relativ_frame));

            } else {
                d.add_text([tl[0], tl[1] - 13.0], [1.0, 1.0, 1.0, 1.0], format!("FRAME = {}", frame));
            }
        }

        // VRAM Access Overlay
        if s.config.system_overlay_mbc_access {
            let d = ui.get_background_draw_list();
            for (MbcAccessKey { line, pixel, rom_switch, ram_switch, ram_enable }, values) in s.logic.analyzer.borrow().mbc_access() {
                let color = if *rom_switch || *ram_switch {
                    MBC_ACCESS_BANK_SWITCH_COLOR

                } else if *ram_enable {
                    MBC_ACCESS_RAM_ENABLE_COLOR

                } else {
                    MBC_ACCESS_OTHER_COLOR
                };
                if util::draw_overlay_pixel(ui, &d, tl, *pixel as f32, *line as f32, color) {
                    if *rom_switch {
                        util::overlay_pixel_tooltip(s, ui, *line, *pixel, "ROM Bank", values.iter());

                    } else if *ram_switch {
                        util::overlay_pixel_tooltip(s, ui, *line, *pixel, "RAM Bank", values.iter());

                    } else if *ram_enable {
                        util::overlay_pixel_tooltip(s, ui, *line, *pixel, "RAM Enable", values.iter());

                    } else {
                        util::overlay_pixel_tooltip(s, ui, *line, *pixel, "WR", values.iter());
                    }
                }
            }
        }
    }
}

