// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use tile::TileID;
use debugger_data::{DebuggerConfig, DebuggerState};
use mwx::imgui::{TabItem, TabItemFlags};


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub mod bg_map;
pub mod code;
pub mod cpu;
pub mod display;
pub mod logs;
pub mod memory;
pub mod other;
pub mod system;
pub mod symbols;
pub mod tiles;
pub mod watchpoints;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{DebuggerUI, DebuggerContextAction};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const STATUS_HEIGHT: f32 = 22.0;


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
impl DebuggerTab for () {
    type Partner = ();

    fn title(&self) -> &str {
        unimplemented!()
    }
    fn id(&self) -> &str {
        unimplemented!()
    }

    fn select<'a>(&self, _: &'a mut DebuggerConfig) -> &'a mut bool {
        unimplemented!()
    }

    fn draw_content(&mut self, _: &mut DebuggerState, _: &mut DebuggerUI, _: &mwx::UI, _: &mut Self::Partner) {
        unimplemented!()
    }
}

trait DebuggerTab {
    type Partner: DebuggerTab;

    fn title(&self) -> &str;
    fn dynamic_title(&self, _state: &DebuggerConfig) -> Option<String> {
        None
    }
    fn id(&self) -> &str;
    fn select<'a>(&self, state: &'a mut DebuggerConfig) -> &'a mut bool;
    fn update(&mut self, _s: &mut DebuggerState, _ui: &mut DebuggerUI) {}
    fn refresh(&mut self, _s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI, _ui: &mwx::UI) {}
    fn reset(&mut self, _s: &mut DebuggerState, _ui: &mut DebuggerUI) {}
    fn draw(&mut self, s: &mut DebuggerState, dbg_ui: &mut DebuggerUI, ui: &mwx::UI, partner: &mut Self::Partner) {
        let title = if let Some(title) = self.dynamic_title(&s.config) {
            title

        } else {
            self.title().to_string()
        };

        // Deactivate all tabs during tab switch
        let f = if s.config.tab_switch > 0 && *self.select(&mut s.config) { TabItemFlags::SET_SELECTED } else { TabItemFlags::empty() };
        *self.select(&mut s.config) = false;

        // Refresh for textures etc.
        self.refresh(s, dbg_ui, ui);

        let id = self.id().to_string();
        TabItem::new(format!("{}###{}", title, id)).flags(f).build(ui, || {
            let avail = ui.content_region_avail();
            ui.child_window(&id).size([avail[0], avail[1] - STATUS_HEIGHT]).build(|| {
                if !dbg_ui.first_draw {
                    self.draw_content(s, dbg_ui, ui, partner);
                }
            });

            // Mark the currently open tab as selected one tab switch is complete
            if s.config.tab_switch == 0 {
                *self.select(&mut s.config) = true;
            }
        });
    }
    fn draw_content(&mut self, s: &mut DebuggerState, dbg_ui: &mut DebuggerUI, ui: &mwx::UI, partner: &mut Self::Partner);
    fn draw_overlay(&mut self, _s: &mut DebuggerState, _ui: &mwx::UI, _partner: &mut Self::Partner, _tl: [f32; 2], _br: [f32; 2]) {}
}


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct DebuggerTabs {
    cpu: cpu::Tab,
    memory: memory::Tab,
    code: code::Tab,
    display: display::Tab,
    bg_map: bg_map::Tab,
    tiles: tiles::Tab,
    symbols: symbols::Tab,
    watchpoints: watchpoints::Tab,
    system: system::Tab,
    other: other::Tab,
    logs: logs::Tab,
    reset: bool
}

impl Default for DebuggerTabs {
    fn default() -> Self {
        Self {
            cpu: cpu::Tab::new(),
            memory: memory::Tab::new(),
            code: code::Tab::new(),
            display: display::Tab::new(),
            bg_map: bg_map::Tab::new(),
            tiles: tiles::Tab::new(),
            symbols: symbols::Tab::new(),
            watchpoints: watchpoints::Tab::new(),
            system: system::Tab::new(),
            other: other::Tab::new(),
            logs: logs::Tab::new(),
            reset: true
        }
    }
}

impl DebuggerTabs {
    pub fn reset(&mut self) {
        self.reset = true;
    }

    pub fn update(&mut self, s: &mut DebuggerState, dbg_ui: &mut DebuggerUI) {
        if self.reset {
            self.cpu.reset(s, dbg_ui);
            self.memory.reset(s, dbg_ui);
            self.code.reset(s, dbg_ui);
            self.display.reset(s, dbg_ui);
            self.bg_map.reset(s, dbg_ui);
            self.tiles.reset(s, dbg_ui);
            self.symbols.reset(s, dbg_ui);
            self.watchpoints.reset(s, dbg_ui);
            self.system.reset(s, dbg_ui);
            self.other.reset(s, dbg_ui);
            self.logs.reset(s, dbg_ui);
            self.reset = false;
        }

        self.cpu.update(s, dbg_ui);
        self.memory.update(s, dbg_ui);
        self.code.update(s, dbg_ui);
        self.display.update(s, dbg_ui);
        self.bg_map.update(s, dbg_ui);
        self.tiles.update(s, dbg_ui);
        self.symbols.update(s, dbg_ui);
        self.watchpoints.update(s, dbg_ui);
        self.system.update(s, dbg_ui);
        self.other.update(s, dbg_ui);
        self.logs.update(s, dbg_ui);
    }

    pub fn action(&mut self, s: &mut DebuggerState, action: DebuggerContextAction) {
        match action {
            DebuggerContextAction::AddWatchpoint(range) => self.watchpoints.add_watchpoint(s, range),
            DebuggerContextAction::ViewMemory(addr) => self.memory.goto_address(s, addr),
            DebuggerContextAction::ViewCode(addr) => self.code.goto_address(s, addr),
            DebuggerContextAction::ToggleBreakpoint(addr) => self.cpu.toggle_breakpoint(s, addr),
            DebuggerContextAction::EditBreakpoint(addr) => self.cpu.edit_breakpoint(s, addr),
            DebuggerContextAction::RemoveBreakpoint(addr) => self.cpu.remove_breakpoint(addr),
            DebuggerContextAction::CreateSymbol(addr) => self.symbols.create_symbol(s, addr),
            DebuggerContextAction::EditSymbol(name) => self.symbols.edit_symbol(s, &name),
            DebuggerContextAction::GotoTileAddress(addr) => self.tiles.goto_tile(s, TileID::from_address(addr)),
            DebuggerContextAction::GotoCellIndex(index) => self.bg_map.goto_cell_index(s, index),
            DebuggerContextAction::GotoSpriteIndex(index)=> self.tiles.goto_sprite_index(s, index)
        }
    }

    pub fn draw_content(&mut self, s: &mut DebuggerState, dbg_ui: &mut DebuggerUI, ui: &mwx::UI) {
        self.cpu.draw(s, dbg_ui, ui, &mut ());
        self.code.draw(s, dbg_ui, ui, &mut self.cpu);
        self.memory.draw(s, dbg_ui, ui, &mut ());
        self.display.draw(s, dbg_ui, ui, &mut self.tiles);
        self.bg_map.draw(s, dbg_ui, ui, &mut self.tiles);
        self.tiles.draw(s, dbg_ui, ui, &mut ());
        self.symbols.draw(s, dbg_ui, ui, &mut ());
        self.watchpoints.draw(s, dbg_ui, ui, &mut ());
        self.system.draw(s, dbg_ui, ui, &mut ());
        self.other.draw(s, dbg_ui, ui, &mut ());
        self.logs.draw(s, dbg_ui, ui, &mut ());
    }

    pub fn draw_overlay(&mut self, s: &mut DebuggerState, ui: &mwx::UI, tl: [f32; 2], br: [f32; 2]) {
        self.cpu.draw_overlay(s, ui, &mut (), tl, br);
        self.code.draw_overlay(s, ui, &mut self.cpu, tl, br);
        self.memory.draw_overlay(s, ui, &mut (), tl, br);
        self.display.draw_overlay(s, ui, &mut self.tiles, tl, br);
        self.tiles.draw_overlay(s, ui, &mut (), tl, br);
        self.bg_map.draw_overlay(s, ui, &mut self.tiles, tl, br);
        self.symbols.draw_overlay(s, ui, &mut (), tl, br);
        self.watchpoints.draw_overlay(s, ui, &mut (), tl, br);
        self.system.draw_overlay(s, ui, &mut (), tl, br);
        self.other.draw_overlay(s, ui, &mut (), tl, br);
        self.logs.draw_overlay(s, ui, &mut (), tl, br);
    }
}

