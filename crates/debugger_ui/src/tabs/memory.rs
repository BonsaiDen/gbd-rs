// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::time::{Duration, Instant};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use tile::{TileID, SpriteID};
use debugger_logic::DebuggerLogic;
use mwx::imgui::{MouseButton, DrawListMut};
use sameboy_rs::{BankedAddress, ReadableCore};
use debugger_data::{DebuggerConfig, DebuggerState};
use analyzer::{MemorySymbolKind, MemoryByte, MemoryCache, MemorySymbol, SymbolProvider};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{
    icons,
    DebuggerUI,
    DebuggerContextMenu,
    tabs::DebuggerTab,
    util::{LayoutData, ReadWriteAverage},
    view::{AutoCompleteTextInput, Extensions, HexByteInput, HexByteResult, ScrollView, ScrollViewProvider}
};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const OUTLINE_BYTE_COLOR: [f32; 4] = [1.0, 0.5, 0.0, 1.0];
const MEMORY_BYTE_CHANGED_COLOR: [f32; 4] = [1.0, 1.0, 0.0, 1.0];

const MEMORY_BYTE_SYMBOL_VARIABLE: [f32; 4] = [0.0, 0.6, 0.7, 1.0];
const MEMORY_BYTE_SYMBOL_ARRAY: [f32; 4] = [0.7, 0.0, 0.5, 1.0];
const MEMORY_BYTE_SYMBOL_LABEL: [f32; 4] = [0.0, 0.9, 0.1, 1.0];
const MEMORY_BYTE_SYMBOL_DATA: [f32; 4] = [1.0, 0.9, 0.0, 1.0];
const MEMORY_BYTE_SYMBOL_STACK: [f32; 4] = [0.5, 0.5, 0.5, 1.0];
const MEMORY_BYTE_SYMBOL_REGISTER: [f32; 4] = [0.7, 0.2, 0.2, 1.0];


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
struct InnerTab {
    hover_address_range: Option<(u16, u16)>,
    outline_addr: Option<u16>,
    next_hover_address_range: Option<(u16, u16)>,
    temporary_hover_address_range: Option<(u16, u16)>,
    current_hover_address_range: Option<(u16, u16)>,
    jump_target_address: Option<BankedAddress>,
    edit_byte_address: Option<u16>,
    edit_byte_input: HexByteInput,
    edit_byte_write: Option<(u16, u8)>,
    memory_initialized: bool,
    memory_scan_time: Instant,
}

pub struct Tab {
    rw_avg: ReadWriteAverage,
    inner: InnerTab,
    symbols_gen: usize,
    goto_input: AutoCompleteTextInput,
    view: ScrollView<MemoryCache>
}

impl Tab {
    pub fn new() -> Self {
        Self {
            rw_avg: ReadWriteAverage::default(),
            inner: InnerTab {
                outline_addr: None,
                jump_target_address: None,
                hover_address_range: None,
                next_hover_address_range: None,
                temporary_hover_address_range: None,
                current_hover_address_range: None,
                edit_byte_address: None,
                edit_byte_input: HexByteInput::new("MEMORY", 2),
                edit_byte_write: None,
                memory_initialized: false,
                memory_scan_time: Instant::now(),
            },
            symbols_gen: 0,
            goto_input: AutoCompleteTextInput::new(),
            view: ScrollView::new()
        }
    }

    pub fn format_memory_byte(s: &DebuggerState, byte: &MemoryByte) -> String {
        InnerTab::format_byte_value(s, byte)
    }

    pub fn goto_address(&mut self, s: &mut DebuggerState, addr: BankedAddress) {
        if !s.config.memory {
            s.config.memory = true;
            s.config.tab_switch = 2;
        }
        self.inner.jump_target_address = Some(addr);
    }
}

impl DebuggerTab for Tab {
    type Partner = ();

    fn title(&self) -> &'static str {
        "Memory"
    }

    fn id(&self) -> &'static str {
        "TAB_MEMORY"
    }

    fn select<'a>(&self, state: &'a mut DebuggerConfig) -> &'a mut bool {
        &mut state.memory
    }

    fn reset(&mut self, _s: &mut DebuggerState, _ui: &mut DebuggerUI) {
        self.inner.memory_initialized = false;
    }

    fn update(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI) {
        // Average read / writes of all memory
        self.rw_avg.update(
            s.logic.is_halted(),
            s.logic.did_step(),
            s.logic.analyzer.borrow().access_reads(),
            s.logic.analyzer.borrow().access_writes()
        );

        // Set edited byte value
        if let Some((addr, value)) = self.inner.edit_byte_write.take() {
            s.core.write_memory(addr, value);
            s.cache.memory.set_byte(addr, value);
        }

        // Wait for debugger to be ready before updating cache for the first time
        if s.logic.is_ready() {
            self.inner.update(s, self.symbols_gen != s.logic.gen());
            self.symbols_gen = s.logic.gen();
        }

        // Set initial scroll offset from restore app state
        self.view.update(&mut s.cache.memory, s.config.memory_offset);
        if let Some(addr) = self.inner.jump_target_address.take() {
            self.view.scroll_to_with_highlight(&s.cache.memory, (addr, 0), true);
        }
    }

    fn draw_content(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI, ui: &mwx::UI, _: &mut Self::Partner) {
        // Follow 16 bit registers
        let outline_addr = self.draw_follow_registers(s, ui);
        if let Some(addr) = outline_addr {
            self.view.set_follow_offset(&s.cache.memory, Some((BankedAddress::new(addr, 0), 0)));

        } else {
            self.view.set_follow_offset(&s.cache.memory, None);
        }
        self.inner.outline_addr = outline_addr;
        ui.same_line();

        // Goto Field
        self.draw_goto(s, ui);

        // Options
        // TODO support outlining breakpoints and watchpoints
        ui.checkbox("Outline changes", &mut s.config.memory_outline_changes);
        ui.same_line();
        ui.checkbox("Underline symbols", &mut s.config.memory_underline_symbols);

        // Goto Shortcuts
        self.draw_shortcuts(s, ui);

        // Memory Viewer
        ui.header("Section   Address     Bytes", Some(&self.rw_avg.format()));
        let (offset, follow) = self.view.draw(s, ui, &mut self.inner);
        if let Some(offset) = offset {
            s.config.memory_offset = offset;
        }
        self.inner.hover_address_range = self.inner.next_hover_address_range;
        self.inner.current_hover_address_range = self.inner.temporary_hover_address_range;
        self.inner.temporary_hover_address_range = None;

        if follow.is_none() {
            s.config.memory_follow_bc = false;
            s.config.memory_follow_de = false;
            s.config.memory_follow_hl = false;
        }
    }
}

impl Tab {
    fn draw_goto(&mut self, s: &mut DebuggerState, ui: &mwx::UI) {
        ui.text(icons::SEARCH);
        ui.same_line();
        if self.goto_input.build(ui, false, &s.logic, |_| true, true) {
            let mut goto = self.goto_input.placeholder();
            if goto.is_empty() {
                goto = self.goto_input.value();
            }
            if goto.is_empty() {
                self.view.return_to_anchor();

            } else if let Some(symbol) = s.logic.resolve_symbol_by_name(goto) {
                self.view.scroll_to_with_highlight(
                    &s.cache.memory,
                    (symbol.address(), 0),
                    self.goto_input.placeholder().is_empty()
                );

            } else if let Some(addr) = DebuggerLogic::parse_address(goto) {
                self.view.scroll_to_with_highlight(
                    &s.cache.memory,
                    (addr, 0),
                    false
                );
            }
            // Reset Input
            if ui.is_key_released(mwx::imgui::Key::Enter) {
                self.view.anchor();
                self.goto_input.clear();
            }
        }
    }

    fn draw_follow_registers(&mut self, s: &mut DebuggerState, ui: &mwx::UI) -> Option<u16> {
        let mut outline_addr = None;
        let regs = s.core.word_registers();
        if ui.checkbox("Follow BC", &mut s.config.memory_follow_bc) {
            s.config.memory_follow_de = false;
            s.config.memory_follow_hl = false;
        }
        if s.config.memory_follow_bc {
            outline_addr = Some(regs.bc);
        }
        ui.same_line();
        if ui.checkbox("Follow DE", &mut s.config.memory_follow_de) {
            s.config.memory_follow_bc = false;
            s.config.memory_follow_hl = false;
        }
        if s.config.memory_follow_de {
            outline_addr = Some(regs.de);
        }
        ui.same_line();
        if ui.checkbox("Follow HL", &mut s.config.memory_follow_hl) {
            s.config.memory_follow_bc = false;
            s.config.memory_follow_de = false;
        }
        if s.config.memory_follow_hl {
            outline_addr = Some(regs.hl);
        }
        outline_addr
    }

    fn draw_shortcuts(&mut self, s: &DebuggerState, ui: &mwx::UI) {
        let shortcuts = [
            ("ROM0", 0x0000), ("ROMX", 0x4000), ("VRAM", 0x8000), ("EXRAM", 0xA000),
            ("WRAM0", 0xC000), ("WRAMX", 0xD000), ("ECHO", 0xE000), ("OAM", 0xFE00),
            ("IO", 0xFF00), ("HRAM", 0xFF80)
        ];
        for (section, addr) in shortcuts {
            if ui.button(section) {
                self.goto_input.set_value(&format!("{:0>4X}", addr));
                self.view.scroll_to_with_highlight(&s.cache.memory, (BankedAddress::new(addr, 0), 0), false);
            }
            ui.same_line();
        }
        ui.new_line();
    }
}

impl ScrollViewProvider<MemoryCache> for InnerTab {
    fn cache<'a>(&self, s: &'a DebuggerState) -> &'a MemoryCache {
        &s.cache.memory
    }

    fn highlight_rects(&self, layout: &LayoutData, width: f32, ox: f32, oy: f32, (addr, _): (BankedAddress, usize)) -> Vec<([f32; 2], [f32; 2], f32, f32)> {
        let column = addr.address() % 16;
        let indent = if column < 8 { 22.0 } else { 28.0 } * layout.width_unit;
        let ctl = [ox + indent + (column as f32 * 3.0 * layout.width_unit) - 4.0, oy - 3.0];
        vec![(
            [0.0, oy - 3.0],
            [width, oy + layout.height_line + 2.0],
            5.0,
            0.10
        ), (
            ctl,
            [ctl[0] + layout.width_unit * 2.0 + 7.0, ctl[1] + layout.height_line + 5.0],
            7.5,
            0.75
        )]
    }

    fn draw_line(
        &mut self,
        s: &mut DebuggerState,
        ui: &mwx::UI,
        d: &DrawListMut,
        layout: &LayoutData,
        (ox, oy, _, line_index): (f32, f32, f32, usize)
    ) {
        let core = &s.core;
        let outline = s.config.memory_outline_changes;
        let underline = s.config.memory_underline_symbols;
        let addr = core.banked_address(line_index as u16 * 16);
        let (section, readable, writeable, outlineable) = match addr.address() {
            0x0000..=0x3FFF => (" ROM0", true, true, true),
            0x4000..=0x7FFF => (" ROMX", true, true, true),
            0x8000..=0x9FFF => (" VRAM", true, !core.is_vram_write_blocked(), true),
            0xA000..=0xBFFF => ("EXRAM", true, true, true),
            0xC000..=0xCFFF => ("WRAM0", true, true, true),
            0xD000..=0xDFFF => ("WRAMX", true, true, true),
            0xE000..=0xFDFF => (" ECHO", true, false, false),
            0xFE00..=0xFE9F => ("  OAM", false, !core.is_vram_write_blocked(), true),
            0xFEA0..=0xFEFF => ("  ---", false, false, false),
            0xFF00..=0xFF7F => ("   IO", true, true, true),
            0xFF80..=0xFFFF => (" HRAM", true, true, true)
        };

        // Draw Line Info
        let prefix = format!("{}    {}", section, addr);
        d.add_text([ox, oy], layout.color_text, prefix);

        // Draw Bytes
        for i in 0..16 {
            self.draw_byte(
                s,
                ui,
                layout, d,
                [ox, oy],
                (underline, outline && outlineable),
                (readable, writeable),
                addr.address(), i
            );
        }
    }
}

impl InnerTab {
    fn update(&mut self, s: &mut DebuggerState, symbols_changed: bool) {
        if s.logic.did_step() ||
            !self.memory_initialized ||
            !s.logic.is_halted() && s.config.memory && self.memory_scan_time.elapsed() > Duration::from_millis(1000) ||
            s.cache.memory.was_edited() ||
            symbols_changed {

            let vram_tile_range = s.core.vram_tile_range();
            let bg_map_base = s.core.bg_map_base();
            let bg_map_range = s.core.bg_map_range();
            let oam_range = s.core.oam_range();
            let stack_base = s.logic.analyzer.borrow().stack_base();
            s.cache.memory.update(&s.core, &s.logic, |addr| {
                if vram_tile_range.contains(&addr.address()) {
                    if addr.address() % 16 == 0 {
                        Some((TileID::from_address(addr).to_string(), MemorySymbolKind::Data, 16))

                    } else {
                        None
                    }

                } else if bg_map_range.contains(&addr.address()) {
                    let map_index = addr.address().saturating_sub(bg_map_base);
                    let x = map_index % 32;
                    let y = map_index / 32;
                    Some((format!("BG Cell #{}[{}][{}]", map_index, x, y), MemorySymbolKind::Data, 1))

                } else if oam_range.contains(&addr.address()) {
                    if addr.address() % 4 == 0 {
                        Some((SpriteID::from_address(addr).to_string(), MemorySymbolKind::Data, 4))

                    } else {
                        None
                    }

                } else {
                    None
                }

            }, stack_base);
            self.memory_scan_time = Instant::now();
            self.memory_initialized = true;
        }
    }

    #[allow(clippy::too_many_arguments)]
    fn draw_byte(
        &mut self,
        s: &mut DebuggerState,
        ui: &mwx::UI,
        layout: &LayoutData,
        d: &DrawListMut,
        [ox, oy]: [f32; 2],
        (underline, outline): (bool, bool),
        (readable, writeable): (bool, bool),
        row_addr: u16,
        column: u16
    ) {
        let indent = if column < 8 { 22.0 } else { 28.0 } * layout.width_unit;
        let tl = [ox + indent + (column as f32 * 3.0 * layout.width_unit), oy];
        let br = [tl[0] + layout.width_unit * 2.0, tl[1] + layout.height_line + 1.0];
        let rtl = [tl[0] - 4.0, tl[1] - 3.0];
        let rbr = [br[0] + 3.0, br[1] + 1.0];
        let otl = [tl[0] - 3.0, tl[1] - 2.0];
        let obr = [br[0] + 2.0, br[1]];

        let addr = row_addr + column;
        if readable || writeable {
            let byte = s.cache.memory.byte(addr);
            let hover = ui.mouse_inside_rect(rtl, rbr) && !ui.has_context_menu_open();
            if hover {
                self.temporary_hover_address_range = byte.symbol.as_ref().map(|s| (s.start, s.end));

                let c = layout.color_text_selected;
                d.add_rect_filled_multicolor(rtl, rbr, c, c, c, c);
                if ui.is_mouse_clicked(MouseButton::Right) {
                    ui.open_context_menu(DebuggerContextMenu::MemoryByte(byte.clone()));

                } else if ui.is_mouse_double_clicked(MouseButton::Left) {
                    self.edit_byte_input.set_value(byte.value as u16);
                    self.edit_byte_address = Some(addr);
                    if let Some(symbol) = &byte.symbol {
                        self.next_hover_address_range = Some((symbol.start, symbol.end));
                    }

                } else if ui.is_mouse_clicked(MouseButton::Left) {
                    self.next_hover_address_range = if let Some(symbol) = &byte.symbol {
                        let range = Some((symbol.start, symbol.end));
                        if self.next_hover_address_range == range {
                            None

                        } else {
                            range
                        }

                    } else {
                        None
                    };
                }
            }

            // Edit
            if self.edit_byte_address == Some(addr) {
                let c = layout.color_frame_bg;
                d.add_rect_filled_multicolor(rtl, rbr, c, c, c, c);
                let old_pos = ui.cursor_screen_pos();
                ui.set_cursor_screen_pos([rtl[0], rtl[1] + 1.0]);
                match self.edit_byte_input.build_edit(ui) {
                    Some(HexByteResult::Value(v)) => {
                        self.edit_byte_write = Some((addr, v as u8));
                        self.edit_byte_address = None;
                    },
                    Some(_) => {
                        self.edit_byte_address = None;
                    },
                    None => {}
                }
                ui.set_cursor_screen_pos(old_pos);

            // View
            } else {
                if hover {
                    ui.text_tooltip(&Self::format_memory_byte(s, byte));
                }

                // Fade out non selected / hovered symbols
                let selected = self.hover_address_range.and_then(|r| {
                    byte.symbol.as_ref().map(|s| s.within_range(r))

                }).unwrap_or(false);
                let hovered = self.current_hover_address_range.and_then(|r| {
                    byte.symbol.as_ref().map(|s| s.within_range(r))

                }).unwrap_or(false);

                let a = match (selected, self.hover_address_range.is_some(), hovered) {
                    (true, _, _) | (_, _, true) => 1.0,
                    (false, true, _) => 0.15,
                    _ => 1.0
                };

                // Underline symbols
                if let (Some(symbol), true) = (&byte.symbol, underline) {
                    let sx = if addr <= symbol.start { 2.0 } else { 0.0 };
                    let ex = if symbol.offset < symbol.size.saturating_sub(1) && column == 7 {
                        layout.width_unit * 6.0

                    } else if addr >= symbol.end {
                        -2.0

                    } else {
                        0.0
                    };
                    let (mut c, thickness) = match (symbol.kind, symbol.size) {
                        (MemorySymbolKind::Data, _) => (MEMORY_BYTE_SYMBOL_DATA, 1.0),
                        (MemorySymbolKind::Code, _) => (MEMORY_BYTE_SYMBOL_LABEL, 1.0),
                        (MemorySymbolKind::Register, _) => (MEMORY_BYTE_SYMBOL_REGISTER, 2.0),
                        (MemorySymbolKind::Stack, _) => (MEMORY_BYTE_SYMBOL_STACK, 2.0),
                        (MemorySymbolKind::Variable, s) => {
                            if s > 2 {
                                (MEMORY_BYTE_SYMBOL_ARRAY, 2.0)

                            } else {
                                (MEMORY_BYTE_SYMBOL_VARIABLE, 2.0)
                            }
                        }
                    };

                    c[3] = a;
                    d.add_rect_filled_multicolor(
                        [rtl[0] + sx, tl[1] + layout.height_line + 1.0 - thickness + 1.0],
                        [rbr[0] + ex, br[1] + 1.0],
                        c, c, c, c
                    );
                }

                // Outline changed bytes
                if byte.changed && outline {
                    let mut c = MEMORY_BYTE_CHANGED_COLOR;
                    c[3] = a;
                    d.add_rect(otl, obr, c).build();
                }

                // Outline followed bytes
                if self.outline_addr == Some(addr) {
                    let mut c = OUTLINE_BYTE_COLOR;
                    c[3] = a;
                    d.add_rect(rtl, rbr, c).build();
                }

                let mut c = if writeable { layout.color_text } else { layout.color_text_disabled };
                c[3] = a;
                d.add_text(tl, c, format!("{:0>2X}", byte.value));
            }

        } else {
            d.add_text(tl, layout.color_text_disabled, "--");
        }
    }

    fn format_memory_byte(s: &DebuggerState,byte: &MemoryByte) -> String {
        format!(
            " Address: {}{}{}\n{}",
            byte.addr,
            Self::format_byte_symbol(&byte.symbol),
            if let Some(symbol) = &byte.symbol {
                format!("\n    Size: {} byte", symbol.size)

            } else {
                "".to_string()
            },
            Self::format_byte_value(s, byte)
        )
    }

    fn format_byte_symbol(symbol: &Option<MemorySymbol>) -> String {
        if let Some(symbol) = symbol {
            match (symbol.kind, symbol.size) {
                (MemorySymbolKind::Data, 1) => format!("\n    Data: {}", symbol.name),
                (MemorySymbolKind::Data, _) => format!("\n    Data: {}[{}]", symbol.name, symbol.offset),
                (MemorySymbolKind::Code, _) => format!("\n    Code: {}", symbol.name),
                (MemorySymbolKind::Register, _) => format!("\nRegister: {}", symbol.name),
                (MemorySymbolKind::Stack, _) => format!("\n   Stack: {}", symbol.name),
                (MemorySymbolKind::Variable, s) => {
                    if s > 2 {
                        format!("\n   Array: {}[{}]", symbol.name, symbol.offset)

                    } else if s == 2 && symbol.offset == 0 {
                        format!("\nVariable: {}[high]", symbol.name)

                    } else if s == 2 && symbol.offset == 1 {
                        format!("\nVariable: {}[low]", symbol.name)

                    } else {
                        format!("\nVariable: {}", symbol.name)
                    }
                }
            }

        } else {
            "".to_string()
        }
    }

    fn format_byte_value(s: &DebuggerState, byte: &MemoryByte) -> String {
        if let Some((old, new)) = Self::word_value(s, byte) {
            format!(
                " Pointer: {}\n     Old: {:0>4X} ({: >5}) ({:0>16b})\n     New: {:0>4X} ({: >5}) ({:0>16b}",
                s.logic.resolve_symbol_name_relative(BankedAddress::new(new, 0)),
                old,
                old,
                old,
                new,
                new,
                new
            )

        } else {
            let old = byte.last_value;
            let new = byte.value;
            format!(
                "     Old: {:0>2X} ({: >3}) ({:0>8b})\n     New: {:0>2X} ({: >3}) ({:0>8b}",
                old,
                old,
                old,
                new,
                new,
                new
            )
        }
    }

    fn word_value(s: &DebuggerState, byte: &MemoryByte) -> Option<(u16, u16)> {
        if let Some((low, high)) = s.cache.memory.word(byte) {
            Some((
                (high.last_value as u16) << 8 | low.last_value as u16,
                (high.value as u16) << 8 | low.value as u16
            ))

        } else {
            None
        }
    }
}

