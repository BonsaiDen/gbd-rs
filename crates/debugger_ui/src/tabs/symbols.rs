// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use mwx::imgui::StyleColor;
use debugger_logic::DebuggerLogic;
use analyzer::{SymbolProvider, Symbol};
use sameboy_rs::{BankedAddress, ReadableCore};
use debugger_data::{DebuggerConfig, DebuggerState};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{
    icons,
    DebuggerUI,
    DebuggerContextMenu,
    tabs::DebuggerTab,
    util::LayoutData,
    view::{AutoCompleteTextInput, EditableInputHelper, Extensions, SimpleScrollView, SimpleScrollViewProvider},
};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct InnerTab {
    helper: EditableInputHelper,
    edited_symbol: Option<Symbol>,
    removed_symbol: Option<Symbol>
}

pub struct Tab {
    inner: InnerTab,
    goto_input: AutoCompleteTextInput,
    scroll_to_symbol: Option<(String, bool, bool)>,
    view: SimpleScrollView
}

impl Tab {
    pub fn new() -> Self {
        Self {
            inner: InnerTab {
                helper: EditableInputHelper::new(),
                edited_symbol: None,
                removed_symbol: None
            },
            goto_input: AutoCompleteTextInput::new(),
            scroll_to_symbol: None,
            view: SimpleScrollView::new()
        }
    }

    pub fn create_symbol(&mut self, s: &mut DebuggerState, addr: BankedAddress) {
        if !s.config.symbols {
            s.config.symbols = true;
            s.config.tab_switch = 2;
        }
        let name = format!("user_symbol_{}", s.logic.symbols().len());
        self.scroll_to_symbol = Some((name.clone(), true, true));
        self.inner.edited_symbol = Some(Symbol::new(name, addr, 0, 1, 0, 0, true).into_user_symbol());
    }

    pub fn edit_symbol(&mut self, s: &mut DebuggerState, name: &str) {
        if !s.config.symbols {
            s.config.symbols = true;
            s.config.tab_switch = 2;
        }
        self.scroll_to_symbol = Some((name.to_string(), true, true));
    }
}

impl DebuggerTab for Tab {
    type Partner = ();

    fn title(&self) -> &'static str {
        "Symbols"
    }

    fn id(&self) -> &'static str {
        "TAB_SYMBOLS"
    }

    fn select<'a>(&self, state: &'a mut DebuggerConfig) -> &'a mut bool {
        &mut state.symbols
    }

    fn update(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI) {
        self.view.update(s.config.symbols_offset);

        // Update symbols flags
        s.logic.set_symbol_flags(s.core.ie(), s.core.lcdc());

        // Remove user symbols
        if let Some(symbol) = self.inner.removed_symbol.take() {
            s.logic.remove_user_symbol(symbol);
        }

        // Re-Insert edited symbols as user symbols
        if let Some(symbol) = self.inner.edited_symbol.take() {
            s.logic.insert_user_symbol(symbol);
        }

        // Scroll to
        if let Some((target, edit, anchor)) = self.scroll_to_symbol.take() {
            let t = target.to_lowercase();
            let symbols = s.logic.dynamic_symbols();
            for (index, s) in symbols.iter().enumerate() {
                if t == s.name().to_lowercase() {
                    if edit && !s.is_locked() {
                        self.inner.helper.edit_value(index, 1, s.name().to_string());
                    }
                    self.view.scroll_to_with_highlight(index, anchor);
                    break;
                }
            }
        }
    }

    fn draw_content(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI, ui: &mwx::UI, _: &mut Self::Partner) {
        // Create Symbols
        if ui.button("Add User Symbol") {
            self.create_symbol(s, BankedAddress::zero());
        }

        // Goto
        ui.same_line();
        ui.text("                   ");
        ui.same_line();
        ui.text(icons::SEARCH);
        ui.same_line();
        if self.goto_input.build(ui, false, &s.logic, |_| true, true) {
            let mut goto = self.goto_input.placeholder();
            if goto.is_empty() {
                goto = self.goto_input.value();
            }
            if goto.is_empty() {
                self.view.return_to_anchor();
            }
            if let Some(symbol) = s.logic.resolve_symbol_by_name(goto) {
                self.scroll_to_symbol = Some((symbol.name().to_string(), false, false));

            } else if let Some(addr) = DebuggerLogic::parse_address(goto) {
                if let Some(symbol) = s.logic.resolve_symbol_absolute(addr) {
                    self.scroll_to_symbol = Some((symbol.name().to_string(), false, false));
                }
            }
            if ui.is_key_released(mwx::imgui::Key::Enter) {
                self.view.anchor();
                self.goto_input.clear();
            }
        }

        ui.header("Address   Name                                        Code   Data   RAM", None);
        let symbols = s.logic.dynamic_symbols();
        s.config.symbols_offset = self.view.draw(
            ui,
            &symbols,
            &s.logic,
            &mut self.inner
        );
    }
}

impl SimpleScrollViewProvider<&Symbol> for InnerTab {
    fn context_menu(&self, ui: &mwx::UI, symbol: &&Symbol) {
        if symbol.is_code() {
            ui.open_context_menu(
                DebuggerContextMenu::AddWatchpointViewMemoryJumpCode(symbol.address().with_size(symbol.size()))
            );

        } else {
            ui.open_context_menu(
                DebuggerContextMenu::AddWatchpointViewMemory(symbol.address().with_size(symbol.size()))
            );
        }
    }

    fn draw_line<S: SymbolProvider>(
        &mut self,
        ui: &mwx::UI,
        _: &S,
        layout: &LayoutData,
        size: [f32; 2],
        index: usize,
        symbol: &&Symbol
    ) {
        let _id = ui.push_id(&format!("symbol_{}", index));
        let disabled = symbol.is_locked();
        let _inactive = if disabled {
            Some((
                ui.push_style_color(StyleColor::Button, [0.5, 0.5, 0.5, 1.0]),
                ui.push_style_color(StyleColor::FrameBg, [0.5, 0.5, 0.5, 1.0])
            ))

        } else if !symbol.is_user_provided() {
            let bc = ui.style_color(StyleColor::Button);
            let bg = ui.style_color(StyleColor::FrameBg);
            Some((
                ui.push_style_color(mwx::imgui::StyleColor::Button, [bc[0] * 0.3, bc[1] * 0.3, bc[2] * 0.3, 1.0]),
                ui.push_style_color(mwx::imgui::StyleColor::FrameBg, [bg[0] * 0.3, bg[1] * 0.3, bg[2] * 0.3, 1.0])
            ))

        } else {
            None
        };

        // Editable Field
        let mut x = 3.0;
        ui.move_cursor([x, 0.0]);

        if let Some(v) = self.helper.string_input(ui, index, 0, layout.width_unit * 8.0, &symbol.address().to_label_string(), disabled) {
            if let Some(addr) = DebuggerLogic::parse_address(&v) {
                self.edited_symbol = Some(symbol.with_address(addr));
            }
        }

        x += layout.width_unit * 10.0;
        ui.same_line_with_pos(x);
        if let Some(v) = self.helper.string_input(ui, index, 1, layout.width_unit * 42.0, symbol.name(), disabled) {
            if !v.is_empty() {
                self.edited_symbol = Some(symbol.with_name(v));
                self.removed_symbol = Some((*symbol).clone());
            }
        }
        x += layout.width_unit * 44.0;
        ui.same_line_with_pos(x);
        if let Some(v) = self.helper.string_input(ui, index, 2, layout.width_unit * 5.0, &format!("{:0>4X}", symbol.code_size()), disabled) {
            if let Ok(v) = u16::from_str_radix(&v, 16) {
                self.edited_symbol = Some(symbol.with_code_size(v));
            }
        }
        x += layout.width_unit * 7.0;
        ui.same_line_with_pos(x);
        if let Some(v) = self.helper.string_input(ui, index, 3, layout.width_unit * 5.0, &format!("{:0>4X}", symbol.data_size()), disabled) {
            if let Ok(v) = u16::from_str_radix(&v, 16) {
                self.edited_symbol = Some(symbol.with_data_size(v));
            }
        }
        x += layout.width_unit * 7.0;
        ui.same_line_with_pos(x);
        if let Some(v) = self.helper.string_input(ui, index, 4, layout.width_unit * 5.0, &format!("{:0>4X}", symbol.variable_size()), disabled) {
            if let Ok(v) = u16::from_str_radix(&v, 16) {
                self.edited_symbol = Some(symbol.with_variable_size(v));
            }
        }

        // Remove Button
        if symbol.is_user_provided() {
            ui.same_line_with_pos(size[0] - 24.0);
            if ui.button_with_size(icons::DELETE_ENTRY, [24.0, 0.0]) {
                self.removed_symbol = Some((*symbol).clone());
            }
        }
    }
}

