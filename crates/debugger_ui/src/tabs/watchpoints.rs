// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use analyzer::SymbolProvider;
use debugger_logic::DebuggerWatchpoint;
use debugger_data::{DebuggerConfig, DebuggerState};
use sameboy_rs::{BankedAddress, BankedAddressRange};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{
    icons,
    DebuggerUI,
    DebuggerContextMenu,
    tabs::DebuggerTab,
    util::LayoutData,
    view::{EditableInputHelper, Extensions, SimpleScrollView, SimpleScrollViewProvider},
};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct InnerTab {
    helper: EditableInputHelper,
    edited_watchpoint: Option<DebuggerWatchpoint>,
    removal_index: Option<usize>
}

pub struct Tab {
    inner: InnerTab,
    scroll_to_watchpoint: Option<usize>,
    watchpoint_index: usize,
    view: SimpleScrollView
}

impl Tab {
    pub fn new() -> Self {
        Self {
            inner: InnerTab {
                helper: EditableInputHelper::new(),
                edited_watchpoint: None,
                removal_index: None
            },
            scroll_to_watchpoint: None,
            watchpoint_index: 0,
            view: SimpleScrollView::new()
        }
    }

    pub fn add_watchpoint(
        &mut self,
        s: &mut DebuggerState,
        range: BankedAddressRange
    ) {
        if let Some(w) = s.watchpoint_mut(range) {
            self.scroll_to_watchpoint = Some(w.index);

        } else {
            let w = DebuggerWatchpoint::new(self.watchpoint_index, range.start(), range.end());
            s.logic.analyzer.borrow_mut().add_watchpoint(self.watchpoint_index, w.clone());
            s.config.watchpoint_list.push(w);
            self.scroll_to_watchpoint = Some(self.watchpoint_index);
            self.watchpoint_index += 1;
        }
        if !s.config.watchpoints {
            s.config.watchpoints = true;
            s.config.tab_switch = 2;
        }
    }
}

impl DebuggerTab for Tab {
    type Partner = ();

    fn title(&self) -> &'static str {
        "Watch"
    }

    fn id(&self) -> &'static str {
        "TAB_WATCHPOINT"
    }

    fn select<'a>(&self, state: &'a mut DebuggerConfig) -> &'a mut bool {
        &mut state.watchpoints
    }

    fn reset(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI) {
        self.watchpoint_index = 0;
        for w in &mut s.config.watchpoint_list {
            s.logic.analyzer.borrow_mut().add_watchpoint(self.watchpoint_index, w.clone());
            w.index = self.watchpoint_index;
            self.watchpoint_index += 1;
        }
    }

    fn update(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI) {
        // Update ScrollView
        self.view.update(s.config.watchpoints_offset);
        if let Some(target) = self.scroll_to_watchpoint.take() {
            for (index, w) in s.config.watchpoint_list.iter().enumerate() {
                if w.index == target {
                    self.view.scroll_to_with_highlight(index, true);
                    break;
                }
            }
        }

        // Re-Insert edited watchpoints
        if let Some(w) = self.inner.edited_watchpoint.take() {
            for wl in &mut s.config.watchpoint_list {
                if wl.index == w.index {
                    *wl = w.clone();
                    break;
                }
            }
            s.logic.analyzer.borrow_mut().add_watchpoint(w.index, w);
        }

        // Remove Watchpoints
        if let Some(index) = self.inner.removal_index.take() {
            self.inner.helper.stop_editing(index);
            s.config.watchpoint_list.retain(|w| w.index != index);
            s.logic.analyzer.borrow_mut().remove_watchpoint(index);
        }
    }

    fn draw_content(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI, ui: &mwx::UI, _: &mut Self::Partner) {
        if ui.button("Add Watchpoint") {
            self.add_watchpoint(s, BankedAddress::new(0x4000, 0).into());
        }

        ui.header("On   Address                                     Value    WR   RD   Log  Halt", None);
        s.config.symbols_offset = self.view.draw(
            ui,
            &s.config.watchpoint_list,
            &s.logic,
            &mut self.inner
        );
    }
}

impl SimpleScrollViewProvider<DebuggerWatchpoint> for InnerTab {
    fn context_menu(&self, ui: &mwx::UI, w: &DebuggerWatchpoint) {
        ui.open_context_menu(DebuggerContextMenu::ViewMemory(w.start_address));
    }

    fn draw_line<S: SymbolProvider>(
        &mut self,
        ui: &mwx::UI,
        provider: &S,
        layout: &LayoutData,
        size: [f32; 2],
        _: usize,
        w: &DebuggerWatchpoint
    ) {
        let _id = ui.push_id(&format!("watchpoint_{}", w.index));
        {
            // Grey out inactive watchpoints
            let _inactive = if !w.active {
                Some((
                    ui.push_style_color(mwx::imgui::StyleColor::Button, [0.5, 0.5, 0.5, 1.0]),
                    ui.push_style_color(mwx::imgui::StyleColor::CheckMark, [1.0, 1.0, 1.0, 1.0]),
                    ui.push_style_color(mwx::imgui::StyleColor::FrameBg, [0.5, 0.5, 0.5, 1.0])
                ))

            } else {
                None
            };

            // Display Information
            let mut x = 3.0;
            ui.move_cursor([x, 0.0]);
            if let Some(b) = ui.borrowed_checkbox("##active", w.active) {
                let mut w = w.clone();
                w.active = b;
                self.edited_watchpoint = Some(w);
            }
            x += layout.width_unit * 5.0;
            ui.same_line_with_pos(x);
            if let Some(v) = self.helper.address_input(
                ui, w.index, 0, layout.width_unit * 42.0,
                (provider, w.start_address),
                || w.to_address_label(),
                || w.to_address_string(),
                |s| s.is_variable(),
                false
            ) {
                let mut w = w.clone();
                w.update_address(&v);
                self.edited_watchpoint = Some(w);
            }
            x += layout.width_unit * 44.0;
            ui.same_line_with_pos(x);
            if let Some(v) = self.helper.string_input(
                ui, w.index, 1, layout.width_unit * 7.0,
                &w.to_value_label(),
                false
            ) {
                let mut w = w.clone();
                w.update_value(&v);
                self.edited_watchpoint = Some(w);
            }
            x += layout.width_unit * 9.0;
            ui.same_line_with_pos(x - 2.0);
            if let Some(b) = ui.borrowed_checkbox("##write", w.write) {
                let mut w = w.clone();
                w.write = b;
                if !w.read {
                    w.read = true;
                }
                self.edited_watchpoint = Some(w);
            }
            x += layout.width_unit * 5.0;
            ui.same_line_with_pos(x - 2.0);
            if let Some(b) = ui.borrowed_checkbox("##read", w.read) {
                let mut w = w.clone();
                w.read = b;
                if !w.write {
                    w.write = true;
                }
                self.edited_watchpoint = Some(w);
            }
            x += layout.width_unit * 5.0;
            ui.same_line_with_pos(x + 1.0);
            if let Some(b) = ui.borrowed_checkbox("##log", w.log) {
                let mut w = w.clone();
                w.log = b;
                if !w.halt {
                    w.halt = true;
                }
                self.edited_watchpoint = Some(w);
            }
            x += layout.width_unit * 5.0;
            ui.same_line_with_pos(x + 4.0);
            if let Some(b) = ui.borrowed_checkbox("##halt", w.halt) {
                let mut w = w.clone();
                w.halt = b;
                if !w.log {
                    w.log = true;
                }
                self.edited_watchpoint = Some(w);
            }
        }

        // Mark for removal
        ui.same_line_with_pos(size[0] - 24.0);
        if ui.button_with_size(icons::DELETE_ENTRY, [24.0, 0.0]) {
            self.removal_index = Some(w.index);
        }
    }
}

