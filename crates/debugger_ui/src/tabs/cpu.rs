// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::rc::Rc;
use std::cell::RefCell;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use debugger_logic::DebuggerBreakpoint;
use sameboy_rs::{BankedAddress, ReadableCore};
use analyzer::{SymbolProvider, CPU_STAT_HISTORY};
use debugger_data::{DebuggerConfig, DebuggerState};
use mwx::imgui::{self, StyleColor};
use mwx::icon_str;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{
    icons,
    DebuggerUI,
    DebuggerContextMenu,
    tabs::DebuggerTab,
    util::{self, LayoutData, COLOR_MAP},
    view::{Extensions, EditableInputHelper, Plot, PlotBuilder, SimpleScrollView, SimpleScrollViewProvider},
};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const TOTAL_USAGE_INDEX: usize = 2048;


// Types ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub type PlotEntry<'a> = (BankedAddress, &'a [f32; CPU_STAT_HISTORY], String, f32, usize);


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct InnerTab {
    helper: EditableInputHelper,
    edited_breakpoint: Option<DebuggerBreakpoint>,
    removal_index: Option<(usize, BankedAddress)>
}

pub struct Tab {
    inner: InnerTab,
    plot_entry_hover_index: Rc<RefCell<Option<usize>>>,
    plot_entry_selected: Option<usize>,
    scroll_to_breakpoint_address: Option<(BankedAddress, bool)>,
    view: SimpleScrollView
}

impl Tab {
    pub fn new() -> Self {
        Self {
            inner: InnerTab {
                helper: EditableInputHelper::new(),
                edited_breakpoint: None,
                removal_index: None
            },
            plot_entry_hover_index: Rc::new(RefCell::new(None)),
            plot_entry_selected: None,
            scroll_to_breakpoint_address: None,
            view: SimpleScrollView::new()
        }
    }

    pub fn add_breakpoint(
        &mut self,
        s: &mut DebuggerState,
        addr: BankedAddress,
    ) {
        // Don't override exisiting breakpoints, instead just highlight them
        if s.breakpoint_mut(addr).is_none() {
            s.config.breakpoint_list.push(DebuggerBreakpoint::new(addr, None, None));
        }
        self.scroll_to_breakpoint_address = Some((addr, false));
    }

    pub fn toggle_breakpoint(
        &mut self,
        s: &mut DebuggerState,
        addr: BankedAddress,
    ) {
        if let Some(bp) = s.logic.analyzer.borrow_mut().get_breakpoint_mut(addr) {
            if bp.software {
                bp.active = !bp.active;
                return;
            }
        }
        if let Some(bp) = s.breakpoint_mut(addr) {
            bp.active = !bp.active;
            bp.changed = Some(addr);

        } else {
            self.add_breakpoint(s, addr);
        }
    }

    pub fn edit_breakpoint(&mut self, s: &mut DebuggerState, addr: BankedAddress) {
        if !s.config.cpu {
            s.config.cpu = true;
            s.config.tab_switch = 2;
        }
        self.scroll_to_breakpoint_address = Some((addr, true));
    }

    pub fn remove_breakpoint(&mut self, addr: BankedAddress) {
        self.inner.removal_index = Some((0, addr))
    }
}

impl DebuggerTab for Tab {
    type Partner = ();

    fn title(&self) -> &'static str {
        "CPU"
    }

    fn id(&self) -> &'static str {
        "TAB_CPU"
    }

    fn select<'a>(&self, state: &'a mut DebuggerConfig) -> &'a mut bool {
        &mut state.cpu
    }

    fn reset(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI) {
        for b in &mut s.config.breakpoint_list {
            b.changed = Some(b.address);
        }
    }

    fn update(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI) {
        // Update ScrollView
        self.view.update(s.config.breakpoints_offset);
        if let Some((address, edit)) = self.scroll_to_breakpoint_address.take() {
            let analyzer = s.logic.analyzer.borrow();
            let mut breakpoints: Vec<&DebuggerBreakpoint> = analyzer.breakpoints().values().collect();
            breakpoints.sort_by(|a, b| a.address.cmp(&b.address));
            for (index, b) in breakpoints.iter().enumerate() {
                if b.address == address {
                    if edit {
                        self.inner.helper.start_editing(index);
                    }
                    self.view.scroll_to_with_highlight(index, true);
                    break;
                }
            }
        }

        // Remove Breakpoints
        let mut analyzer = s.logic.analyzer.borrow_mut();
        if let Some((index, address)) = self.inner.removal_index.take() {
            analyzer.remove_breakpoint(address);
            s.config.breakpoint_list.retain(|b| b.address != address);
            self.inner.helper.stop_editing(index);
        }

        // Re-insert edited breakpoint
        if let Some(b) = self.inner.edited_breakpoint.take() {
            if let Some(address) = b.changed {
                if b.software {
                    // Software breakpoints can only change their active state
                    if let Some(bp) = analyzer.get_breakpoint_mut(address) {
                        bp.active = b.active;
                    }

                } else {
                    // User breakpoints are overwritten
                    for bl in &mut s.config.breakpoint_list {
                        if bl.address == address {
                            *bl = b;
                            break;
                        }
                    }
                }
            }
        }

        // Update Analyzer
        analyzer.set_analyze_line_usage(s.config.cpu_overlay_usage);
        analyzer.set_analyze_average_line_usage(s.config.cpu_average_line_usage);

        // Sync changed user breakpoints to analyzer
        for b in &mut s.config.breakpoint_list {
            if let Some(previous_address) = b.changed.take() {
                analyzer.remove_breakpoint(previous_address);
                analyzer.add_breakpoint(b.clone());
            }
        }
    }

    fn draw_content(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI, ui: &mwx::UI, _: &mut Self::Partner) {
        ui.split(1, "TAB_CPU_USAGE");
        ui.header(icon_str!(NF_FA_AREA_CHART " Usage (last 120 frames, >= 1% only)"), None);
        ui.checkbox("Include inner calls", &mut s.config.cpu_usage_include_sub);
        ui.same_line();
        ui.checkbox("Overlay usage per line", &mut s.config.cpu_overlay_usage);
        ui.disabled(!s.config.cpu_overlay_usage, || {
            ui.same_line();
            ui.checkbox("Use average for lines", &mut s.config.cpu_average_line_usage);
        });

        let avail = ui.content_region_avail();
        {
            // Generate plot data
            let analyzer = s.logic.analyzer.borrow();
            let mut plot_entries: Vec<PlotEntry> = analyzer.call_usage().iter().filter(|(_, call)| {
                if s.config.cpu_usage_include_sub {
                    call.acc_average >= 0.01

                } else {
                    call.own_average >= 0.01
                }

            }).map(|(addr, call)| {
                let (stats, average) = if s.config.cpu_usage_include_sub {
                    (&call.acc_stats, call.acc_average)

                } else {
                    (&call.own_stats, call.own_average)
                };
                (*addr, stats, s.logic.resolve_symbol_name_relative(*addr), average, call.index)

            }).collect();

            // Sort by usage descending
            plot_entries.sort_by(|a, b| {
                let a = (a.3 * 100.0) as u32;
                let b = (b.3 * 100.0) as u32;
                b.cmp(&a)
            });

            // Draw CPU usage plot
            let mut plot = ui.plot("cpu_usage");
            plot.size([avail[0], (avail[1] * 0.5).floor()]);
            plot.x_label("Frames");
            plot.x_range(-120.0, 0.0, 20.0);
            plot.y_label("%");
            plot.y_range(0.0, 100.0, 10.0);

            // TODO fix percentage alignment in text width formatting
            let total = format!("Total {:.0}%", analyzer.cpu_average() * 100.0);
            let len = total.len();

            self.plot_stats(&mut plot, &total, analyzer.cpu_stats(), TOTAL_USAGE_INDEX, None);
            for (addr, stats, label, average, index) in &plot_entries {
                let percent = format!("{:.0}%", average * 100.0);
                // TODO fix percentage alignment in text width formatting
                let label = format!("{}{: >2$}", label, percent, len);
                self.plot_stats(&mut plot, &label, stats, *index, Some(*addr));
            }

            *self.plot_entry_hover_index.borrow_mut() = self.plot_entry_selected.map_or(self.plot_entry_selected, Some);
            if let Some(index) = plot.build() {
                if index > 0 {
                    let plot_entry = plot_entries.get(index.saturating_sub(1)).unwrap();
                    if ui.is_mouse_clicked(imgui::MouseButton::Left) {
                        if self.plot_entry_selected == Some(plot_entry.4) {
                            self.plot_entry_selected = None;

                        } else {
                            self.plot_entry_selected = Some(plot_entry.4);
                        }

                    } else if ui.is_mouse_clicked(imgui::MouseButton::Right) {
                        ui.open_context_menu(DebuggerContextMenu::ViewMemoryJumpCode(plot_entry.0));
                    }
                    *self.plot_entry_hover_index.borrow_mut() = Some(plot_entry.4);

                } else {
                    if ui.is_mouse_clicked(imgui::MouseButton::Left) {
                        self.plot_entry_selected = None;
                    }
                    *self.plot_entry_hover_index.borrow_mut() = None;
                }
            }
        }

        // Breakpoints
        ui.set_cursor_pos([0.0, (avail[1] * 0.5).floor() + 52.0]);
        ui.disabled(s.has_dap(), || {
            if ui.button("Add Breakpoint") {
                self.add_breakpoint(s, s.core.pc());
            }
            ui.header("On   Breakpoint Address                          Condition", None);
            self.draw_breakpoints(s, ui);
        });
    }

    fn draw_overlay(&mut self, s: &mut DebuggerState, ui: &mwx::UI, _: &mut Self::Partner, tl: [f32; 2], br: [f32; 2]) {
        if s.config.display_darken {
            let d = ui.get_background_draw_list();
            let color = [0.0, 0.0, 0.0, 0.80];
            d.add_rect_filled_multicolor(tl, br, color, color, color, color);
        }

        if s.config.cpu_overlay_usage {
            let d = ui.get_background_draw_list();

            // Render usage for each line
            let analyzer = s.logic.analyzer.borrow();
            for (line, usage) in analyzer.cpu_line_usage().iter().enumerate() {

                // Show total CPU usage for this line
                if let Some(hover_index) = *self.plot_entry_hover_index.borrow() {
                    if hover_index == TOTAL_USAGE_INDEX {
                        let width = (160.0 * usage).ceil().floor();
                        let mut color = COLOR_MAP[0];
                        if line < 144 {
                            color[3] = 0.5;
                        }
                        util::draw_overlay_line(ui, &d, tl, 0.0, line as f32, width, color);
                        continue;
                    }
                }

                // Display stacking function call usage for this line
                let mut offset = 0.0f32;
                for (addr, call) in analyzer.call_usage().iter().filter(|(_, call)| {
                    if s.config.cpu_usage_include_sub {
                        call.acc_average >= 0.01

                    } else {
                        call.own_average >= 0.01
                    }
                }) {
                    if let Some(hover_index) = *self.plot_entry_hover_index.borrow() {
                        if hover_index != call.index {
                            continue;
                        }
                    }
                    let (usage, average) = if s.config.cpu_usage_include_sub {
                        (call.acc_line_usage[line], call.acc_average)

                    } else {
                        (call.own_line_usage[line], call.own_average)
                    };

                    let width = (160.0 * usage).ceil().floor();
                    let mut color = util::address_color(*addr, call.index);
                    if line < 144 {
                        color[3] = 0.5;
                    }
                    let (tl, br) = util::draw_overlay_line(ui, &d, tl, offset, line as f32, width, color);
                    offset += width;
                    if ui.mouse_inside_rect(tl, br) {
                        let label = s.logic.resolve_symbol_name_relative(*addr);
                        util::draw_overlay_tooltip(ui, &format!(
                            "LY = {}\n# {}\n Line Usage: {:.0}%\nFrame Usage: {:.0}%",
                            line,
                            label,
                            usage * 100.0,
                            average * 100.0
                        ));
                    }
                }
            }
        }
    }
}

impl Tab {
    fn draw_breakpoints(&mut self, s: &mut DebuggerState, ui: &mwx::UI) {
        let analyzer = s.logic.analyzer.borrow();
        let mut breakpoints: Vec<&DebuggerBreakpoint> = analyzer.breakpoints().values().collect();
        breakpoints.sort_by(|a, b| a.address.cmp(&b.address));
        s.config.breakpoints_offset = self.view.draw(
            ui,
            &breakpoints,
            &s.logic,
            &mut self.inner
        );
    }

    fn plot_stats(&self, plot: &mut PlotBuilder, label: &str, stats: &[f32; CPU_STAT_HISTORY], index: usize, addr: Option<BankedAddress>) {
        let points: Vec<(f32, f32)> = stats.iter().enumerate().map(|(i, s)| {
            (-(i as f32), s * 100.0)

        }).collect();

        let color = if let Some(addr) = addr {
            util::address_color(addr, index)

        } else {
            COLOR_MAP[0]
        };
        plot.line(label, self.plot_entry_selected.is_none() || self.plot_entry_selected == Some(index), color, points);
    }
}

impl SimpleScrollViewProvider<&DebuggerBreakpoint> for InnerTab {
    fn context_menu(&self, ui: &mwx::UI, breakpoint: &&DebuggerBreakpoint) {
        ui.open_context_menu(DebuggerContextMenu::ViewMemoryJumpCode(breakpoint.address));
    }

    fn draw_line<S: SymbolProvider>(
        &mut self,
        ui: &mwx::UI,
        provider: &S,
        layout: &LayoutData,
        size: [f32; 2],
        index: usize,
        b: &&DebuggerBreakpoint
    ) {
        let _id = ui.push_id(&format!("breakpoint_{}", index));
        let last_address = b.address;
        {
            // Grey out inactive watchpoints
            let _inactive = if !b.active {
                Some((
                    ui.push_style_color(mwx::imgui::StyleColor::Button, [0.5, 0.5, 0.5, 1.0]),
                    ui.push_style_color(mwx::imgui::StyleColor::FrameBg, [0.5, 0.5, 0.5, 1.0])
                ))

            } else if b.software {
                let bc = ui.style_color(StyleColor::Button);
                let bg = ui.style_color(StyleColor::FrameBg);
                Some((
                    ui.push_style_color(mwx::imgui::StyleColor::Button, [bc[0] * 0.5, bc[1] * 0.5, bc[2] * 0.5, 1.0]),
                    ui.push_style_color(mwx::imgui::StyleColor::FrameBg, [bg[0] * 0.5, bg[1] * 0.5, bg[2] * 0.5, 1.0])
                ))
            } else {
                None
            };

            // Display Information
            let mut x = 3.0;
            ui.move_cursor([x, 0.0]);
            if let Some(a) = ui.borrowed_checkbox("##active", b.active) {
                let mut b = (*b).clone();
                b.active = a;
                b.changed = Some(last_address);
                self.edited_breakpoint = Some(b);
            }
            x += layout.width_unit * 5.0;
            ui.same_line_with_pos(x);
            if let Some(v) = self.helper.address_input(
                ui, index, 0, layout.width_unit * 42.0,
                (provider, b.address),
                || b.address_string(),
                || b.address_string(),
                |s| s.is_code(),
                b.software
            ) {
                let mut b = (*b).clone();
                b.update_address(&v);
                b.changed = Some(last_address);
                self.edited_breakpoint = Some(b);
            }
            x += layout.width_unit * 44.0;
            ui.same_line_with_pos(x);

            if let Some(v) = self.helper.string_input(
                ui, index, 1, layout.width_unit * 28.0,
                b.as_str(),
                b.software
            ) {
                let mut b = (*b).clone();
                b.update_condition(&v);
                b.changed = Some(last_address);
                self.edited_breakpoint = Some(b);
            }
        }

        // Mark for removal
        ui.same_line_with_pos(size[0] - 24.0);
        if !b.software && ui.button_with_size(icons::DELETE_ENTRY, [24.0, 0.0]) {
            self.removal_index = Some((index, b.address));
        }
    }
}

