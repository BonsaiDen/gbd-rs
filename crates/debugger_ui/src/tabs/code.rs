// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::ops::Range;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use debugger_logic::DebuggerLogic;
use mwx::imgui::{DrawListMut, MouseButton};
use sameboy_rs::{BankedAddress, ReadableCore};
use debugger_data::{DebuggerConfig, DebuggerState};
use analyzer::{
    Breakpoint, CodeDescriptor, CodeMapping, CodeCache, CallInformation,
    Instruction, InstructionArgument,
    LineCache, SymbolProvider
};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{
    icons,
    DebuggerUI,
    DebuggerContextMenu,
    tabs::{DebuggerTab, memory::Tab as MemoryTab, cpu::Tab as CPUTab},
    util::{self, LayoutData, COLOR_MAP_SMALL},
    view::{AutoCompleteTextInput, Extensions, ScrollView, ScrollViewProvider}
};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const LAYOUT_INDENT_UNITS: f32 = 21.0;


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
type LineSpan = ((usize, bool), (usize, bool), (usize, usize), [f32; 4]);

struct InnerTabData {
    selected_call_address: Option<BankedAddress>,
    hovered_call_address: Option<BankedAddress>,
    jump_target_address: Option<BankedAddress>,
}

struct InnerTab {
    data: InnerTabData,
    toggle_breakpoint: Option<BankedAddress>,
    span_gen: usize,
    span_range: Range<usize>,
    span_cache: Vec<LineSpan>
}

pub struct Tab {
    inner: InnerTab,
    goto_input: AutoCompleteTextInput,
    jump_on_halt: bool,
    view: ScrollView<CodeCache>
}

impl Tab {
    pub fn new() -> Self {
        Self {
            inner: InnerTab {
                data: InnerTabData {
                    selected_call_address: None,
                    hovered_call_address: None,
                    jump_target_address: None,
                },
                toggle_breakpoint: None,
                span_gen: 0,
                span_range: (0..0),
                span_cache: Vec::with_capacity(32),
            },
            jump_on_halt: true,
            goto_input: AutoCompleteTextInput::new(),
            view: ScrollView::new()
        }
    }

    pub fn goto_address(&mut self, s: &mut DebuggerState, addr: BankedAddress) {
        if !s.config.code {
            s.config.code = true;
            s.config.tab_switch = 2;
        }
        self.inner.data.jump_target_address = Some(addr);
    }
}

impl DebuggerTab for Tab {
    type Partner = CPUTab;

    fn title(&self) -> &'static str {
        "Code"
    }

    fn id(&self) -> &'static str {
        "TAB_CODE"
    }

    fn select<'a>(&self, state: &'a mut DebuggerConfig) -> &'a mut bool {
        &mut state.code
    }

    fn reset(&mut self, _s: &mut DebuggerState, _ui: &mut DebuggerUI) {
        self.jump_on_halt = true;
    }

    fn update(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI) {
        // Wait for debugger to be ready before updating cache for the first time
        if s.logic.is_ready() {
            self.inner.update(s);
        }

        // Jump to PC on halt
        if s.logic.is_halted() {
            if s.config.code_jump_on_halt && self.jump_on_halt {
                let pc = s.core.pc();
                self.goto_address(s, pc);
            }
            self.jump_on_halt = false;

        } else {
            self.jump_on_halt = true;
        }

        self.view.update(&mut s.cache.code, s.config.code_offset);
        if let Some(addr) = self.inner.data.jump_target_address.take() {
            self.view.scroll_to_with_highlight(&s.cache.code, (addr, 0), true);
        }
    }

    fn draw_content(&mut self, s: &mut DebuggerState, _dbg_ui: &mut DebuggerUI, ui: &mwx::UI, cpu: &mut CPUTab) {
        // Controls
        ui.checkbox("Follow PC", &mut s.config.code_follow_pc);
        ui.same_line();
        ui.checkbox("Jump to PC on HALT", &mut s.config.code_jump_on_halt);
        if s.config.code_follow_pc {
            let pc = s.core.pc();
            self.view.set_follow_offset(&s.cache.code, Some((pc, 0)));

        } else {
            self.view.set_follow_offset(&s.cache.code, None);
        }
        ui.same_line();

        // Goto Label
        self.draw_goto(s, ui);

        // Call Stats
        if ui.button("Reset Calls") {
            s.logic.analyzer.borrow_mut().reset_call_info_totals();
        }
        ui.same_line();
        if ui.button("Reset Branches") {
            s.logic.analyzer.borrow_mut().reset_branch_info();
        }
        ui.same_line();
        if ui.button("Reset Coverage") {
            s.logic.analyzer.borrow_mut().reset_coverage();
        }
        ui.checkbox("Overlay Calls Traces", &mut s.config.code_overlay_calls);
        ui.same_line();
        ui.checkbox("Inline Memory Values", &mut s.config.code_show_line_values);
        ui.same_line();
        ui.checkbox("Instruction Coverage", &mut s.config.code_show_coverage);

        // Code View
        ui.header("   Address            Layout", None);

        // TODO implement an actual fix for when the cache is temporarily empty
        // after a reset
        if s.cache.code.line_count() > 0 {
            let (offset, follow) = self.view.draw(s, ui, &mut self.inner);
            if let Some(offset) = offset {
                s.config.code_offset = offset;
            }
            if follow.is_none() {
                s.config.code_follow_pc = false;
            }

            // Toggle Breakpoint via address DoubleClick
            if let Some(addr) = self.inner.toggle_breakpoint.take() {
                cpu.toggle_breakpoint(s, addr);
            }
        }
    }

    fn draw_overlay(&mut self, s: &mut DebuggerState, ui: &mwx::UI, _: &mut CPUTab, tl: [f32; 2], _: [f32; 2]) {
        // Call Trace Overlay
        if s.config.code_overlay_calls {
            let analyzer = s.logic.analyzer.borrow();
            let current_cycle = analyzer.current_cycle();
            let current_line = analyzer.current_line();
            let current_pixel = analyzer.current_pixel();
            let d = ui.get_background_draw_list();

            let mut hovered_call = None;
            let unfocussed = self.inner.data.selected_call_address.is_none() && self.inner.data.hovered_call_address.is_none();
            for (addr, call) in analyzer.call_infos().iter() {
                let hovered = self.inner.data.hovered_call_address == Some(*addr);
                let selected = self.inner.data.selected_call_address == Some(*addr);
                let alpha = if hovered || selected || unfocussed { 1.0 } else { 0.125 };

                // Line Spans
                if let Some((_, _, first_line, first_pixel)) = call.enter {
                    if selected || hovered {
                        let ((from_line, from_pixel), (to_line, to_pixel)) = if let Some((_, _, last_line, last_pixel)) = call.exit {
                            (
                                (first_line, first_pixel),
                                (last_line, last_pixel),
                            )

                        } else {
                            (
                                (first_line, first_pixel),
                                (current_line, current_pixel)
                            )
                        };

                        let mut line = from_line;
                        loop {
                            let (start, end) = if line == from_line && line == to_line {
                                (from_pixel, to_pixel)

                            } else if line == from_line {
                                (from_pixel, 160)

                            } else if line == to_line {
                                (0, to_pixel)

                            } else {
                                (0, 160)
                            };
                            util::draw_overlay_line_segment(
                                ui, &d, tl,
                                start as f32,
                                line as f32,
                                end.saturating_sub(start) as f32,
                                [0.0, 1.0, 0.0, 1.0]
                            );
                            if line == to_line {
                                break;
                            }
                            line = (line + 1) % 155;
                        }
                    }
                }

                // Hover / Selected Details
                if let Some((_, _, first_line, first_pixel)) = call.enter {
                    if util::draw_overlay_pixel(ui, &d, tl, first_pixel as f32, first_line as f32, [1.0, 1.0, 0.0, alpha]) {
                        hovered_call = Some(*addr);
                        ui.text_tooltip(
                            &format_call(call, *addr, &s.logic, current_cycle, current_line, true, false, true)
                        );
                        if ui.is_mouse_clicked(MouseButton::Left) {
                            if selected {
                                self.inner.data.selected_call_address = None;

                            } else {
                                self.inner.data.selected_call_address = Some(*addr);
                            }

                        } else if ui.is_mouse_clicked(MouseButton::Right) {
                            ui.open_context_menu(DebuggerContextMenu::JumpCode(*addr));
                        }
                    }
                    if let Some((_, _, last_line, last_pixel)) = call.exit {
                        if util::draw_overlay_pixel(ui, &d, tl, last_pixel as f32, last_line as f32, [0.3, 0.3, 1.0, alpha]) {
                            hovered_call = Some(*addr);
                            ui.text_tooltip(
                                &format_call(call, *addr, &s.logic, current_cycle, current_line, false, true, true)
                            );
                            if ui.is_mouse_clicked(MouseButton::Left) {
                                if selected {
                                    self.inner.data.selected_call_address = None;

                                } else {
                                    self.inner.data.selected_call_address = Some(*addr);
                                }

                            } else if ui.is_mouse_clicked(MouseButton::Right) {
                                ui.open_context_menu(DebuggerContextMenu::JumpCode(*addr));
                            }
                        }
                    }
                }
            }
            self.inner.data.hovered_call_address = hovered_call;
        }
    }
}

#[allow(clippy::too_many_arguments)]
fn format_call<T: SymbolProvider>(
    call: &CallInformation,
    addr: BankedAddress,
    provider: &T,
    current_cycle: u32,
    current_line: u8,
    enter: bool,
    exit: bool,
    show_stack: bool

) -> String {
    let cycles = call.frame_cycles(current_cycle);
    let lines = call.frame_lines(current_line);
    let name = provider.resolve_symbol_name_relative(addr);
    let extra = if show_stack {
        let stack: Vec<String> = call.stack.iter().enumerate().map(|(index, addr)| {
            format!("{: >2}. {}", index + 1, provider.resolve_symbol_name_relative(*addr))

        }).collect();
        if stack.is_empty() {
            "".to_string()

        } else {
            format!("\n\nCallstack:\n  {}", stack.join("\n  "))
        }
    } else {
        let mut callers: Vec<BankedAddress> = call.callers.iter().copied().collect();
        if callers.is_empty() {
            "".to_string()

        } else {
            callers.sort();
            let callers = callers.into_iter().map(|addr| provider.resolve_symbol_name_relative(addr)).collect::<Vec<String>>().join("\n  ");
            format!("\n\nCallers:\n  {}", callers)
        }
    };
    match (enter, call.enter, exit, call.exit) {
        (true, Some((first_frame, _, first_line, first_pixel)), true, Some((last_frame, _, last_line, _))) => {
            format!(
                "{}x{} {}\n  {} cycles, {} lines\n  entered on frame #{} (line {})\n  exited on frame #{} (line {}){}",
                first_line, first_pixel, name, cycles, lines, first_frame, first_line, last_frame, last_line, extra
            )
        },
        (true, Some((first_frame, _, first_line, first_pixel)), _, _) => {
            format!(
                "{}x{} {}\n  {} cycles, {} lines\n  enter frame #{}{}",
                first_line, first_pixel, name, cycles, lines, first_frame, extra
            )
        },
        (_, _, true, Some((last_frame, _, last_line, last_pixel))) => {
            format!(
                "{}x{} {}\n  {} cycles, {} lines\n  exit frame #{}{}",
                last_line, last_pixel, name, cycles, lines, last_frame, extra
            )
        }
        _ => name
    }
}

impl Tab {
    fn draw_goto(&mut self, s: &mut DebuggerState, ui: &mwx::UI) {
        ui.text(icons::SEARCH);
        ui.same_line();
        if self.goto_input.build(ui, false, &s.logic, |s| s.is_code(), true) {
            let mut goto = self.goto_input.placeholder();
            if goto.is_empty() {
                goto = self.goto_input.value();
            }
            if goto.is_empty() {
                self.view.return_to_anchor();

            } else if let Some(symbol) = s.logic.resolve_symbol_by_name(goto) {
                self.view.scroll_to_with_highlight(
                    &s.cache.code,
                    (symbol.address(), 0),
                    self.goto_input.placeholder().is_empty()
                );

            } else if let Some(addr) = DebuggerLogic::parse_address(goto) {
                self.view.scroll_to_with_highlight(
                    &s.cache.code,
                    (addr, 0),
                    false
                );
            }
            // Reset Input
            if ui.is_key_released(mwx::imgui::Key::Enter) {
                self.view.anchor();
                self.goto_input.clear();
            }
        }
    }
}

impl ScrollViewProvider<CodeCache> for InnerTab {
    fn cache<'a>(&self, s: &'a DebuggerState) -> &'a CodeCache {
        &s.cache.code
    }

    fn highlight_rects(&self, layout: &LayoutData, width: f32, ox: f32, oy: f32, (_, _): (BankedAddress, usize)) -> Vec<([f32; 2], [f32; 2], f32, f32)> {
        vec![(
            [ox, oy],
            [width, oy + layout.height_line + 1.0],
            5.0,
            0.5
        )]
    }

    fn draw_line(
        &mut self,
        s: &mut DebuggerState,
        ui: &mwx::UI,
        d: &DrawListMut,
        layout: &LayoutData,
        (ox, oy, width, line_index): (f32, f32, f32, usize)
    ) {
        let pc = s.core.pc();
        let indent = LAYOUT_INDENT_UNITS * layout.width_unit + 4.0;
        let postfix_indent = ox + indent + 10.0 * layout.width_unit + 2.0;
        let (addr, offset) = s.cache.code.get_line_addr(line_index);

        // Display Above Line Info
        if offset > 0 {
            if let Some(desc) = s.cache.code.map_code(addr).above() {
                self.data.draw_descriptor(s, ui, d, layout, (ox, oy, ox + indent, width), (addr, offset), desc);
            }

        } else {
            // Highlight line if at current PC location
            if pc == addr {
                let tl = [ox, oy];
                let br = [width, oy + layout.height_line + 1.0];
                let mut c = layout.color_text_selected;
                c[3] = 0.25;
                d.add_rect_filled_multicolor(tl, br, c, c, c, c);
            }

            let mut toggle_click = false;
            let covered = {
                let has_dap = s.has_dap();
                let analyzer = s.logic.analyzer.borrow();
                let bp = analyzer.get_breakpoint(addr);
                let hover = || {
                    if ui.is_mouse_double_clicked(MouseButton::Left) && !has_dap {
                        toggle_click = true;
                    }
                    if let Some(bp) = bp {
                        let active = if bp.active() { "active" } else { "inactive" };
                        let location = s.logic.resolve_symbol_name_relative(bp.address);
                        ui.text_tooltip(&format!(
                            "Breakpoint ({})\n   Location: {}\n  Condition: {}",
                            active,
                            location,
                            bp.as_str()
                        ));
                    }
                };

                // Address Text
                if ui.text_custom(&format!("  {}", addr)).height(1.0).with_position(d, [ox, oy]).build_with_tooltip(hover) {
                    if has_dap || bp.map(|b| b.software()).unwrap_or(false) {
                        ui.open_context_menu(DebuggerContextMenu::Code(addr.into(), false, true, false));

                    } else {
                        ui.open_context_menu(DebuggerContextMenu::Code(addr.into(), false, true, bp.is_some()));
                    }
                }

                // Breakpoint indicator
                if let Some(bp) = bp {
                    let tl = [ox + 2.0, oy + 2.0];
                    let br = [tl[0] + 10.0, tl[1] + 10.0];
                    let c = if bp.active() { [0.6, 0.0, 0.0, 1.0] } else { [0.8, 0.8, 0.8, 1.0] };
                    d.add_rect_filled_multicolor(tl, br, c, c, c, c);
                }
                analyzer.coverage().contains(&addr)
            };
            if toggle_click {
                self.toggle_breakpoint = Some(addr);
            }

            // Line Layout
            let mapping = s.cache.code.map_code(addr);
            let (tx, postfix) = match mapping {
                CodeMapping::Instruction { instruction, .. } => {
                    // Coverage indicator
                    if s.config.code_show_coverage {
                        let tl = [ox + indent + layout.width_unit * 7.0, oy];
                        let br = [tl[0] + 3.0, tl[1] + layout.height_unit];
                        let c = if covered { [0.0, 0.8, 0.0, 1.0] } else { [0.8, 0.0, 0.0, 1.0] };
                        d.add_rect_filled_multicolor(tl, br, c, c, c, c);
                    }
                    self.data.draw_instruction(s, ui, d, instruction, addr, layout, (ox, oy, indent))
                },
                CodeMapping::Message { value, .. } => {
                    let text = format!("{: <8}{}", "MSG", value);
                    d.add_text([ox + indent, oy], [1.0, 1.0, 0.0, 1.0], &text);
                    ((text.len() as f32 + 0.5) * layout.width_unit, None)
                },
                CodeMapping::Data { bytes, .. } => {
                    let bytes = bytes.iter().map(|b| format!("{:0>2X}", b)).collect::<Vec<String>>().join(" ");
                    let text = format!("db {}", bytes);
                    d.add_text([ox + indent, oy], layout.color_text, &text);
                    ((text.len() as f32 + 0.5) * layout.width_unit, None)
                }
            };
            if let Some(desc) = postfix.as_ref().or_else(|| mapping.postfix().as_ref()) {
                self.data.draw_descriptor(
                    s, ui, d, layout,
                    (ox, oy, (ox + indent + tx + layout.width_unit * 0.5).max(postfix_indent), width),
                    (addr, offset),
                    desc
                );
            }
        }
    }

    fn draw_overlay(
        &mut self,
        s: &mut DebuggerState,
        d: &DrawListMut,
        layout: &LayoutData,
        (ox, oy): (f32, f32),
        (from, to): (usize, usize),
    ) {
        // TODO add config options for jump context range
        let line_gen = s.cache.code.line_indicies_gen();
        let line_range = from.saturating_sub(96)..to.saturating_add(96);
        if line_gen != self.span_gen || self.span_range != line_range {
            self.span_cache.clear();
            for source_line in line_range.clone() {
                let (source_addr, offset) = s.cache.code.get_line_addr(source_line);
                if offset == 0 {
                    if let Some(CodeMapping::Instruction { instruction, .. }) = s.cache.code.map_code_opt(source_addr) {
                        if instruction.is_jump() {
                            if let Some(target_addr) = instruction.address_argument(source_addr.address()) {
                                let target_line = s.cache.code.map_line_index((BankedAddress::new(target_addr, source_addr.bank()), offset));
                                let target_line_clipped = target_line.max(from).min(to);
                                let source_line_clipped = source_line.max(from).min(to);
                                let source = source_line.saturating_sub(from);
                                let target = target_line.saturating_sub(from);
                                self.span_cache.push((
                                    (source, source_line == source_line_clipped),
                                    (target, target_line == target_line_clipped),
                                    (target_line.min(source_line), source_line.max(target_line)),
                                    COLOR_MAP_SMALL[target_addr as usize % COLOR_MAP_SMALL.len()]
                                ));
                            }
                        }
                    }
                }
            }
            self.span_gen = line_gen;
            self.span_range = line_range;
        }

        let xl = ox + (LAYOUT_INDENT_UNITS - 11.0) * layout.width_unit;
        let xr = ox + LAYOUT_INDENT_UNITS * layout.width_unit;
        let oh = (layout.height_unit * 0.4).floor();
        let xm = ox + ((LAYOUT_INDENT_UNITS - 1.5) * layout.width_unit).floor();
        let mut stacking = [0u8; 512];
        for ((source, source_visible), (target, target_visible), (sl, tl), color) in &self.span_cache {
            // Compute stack depth for visible lines
            let mut depth = 0;
            for l in *sl..=*tl {
                // Offset spans into the buffer, for very far away targets we will not correctly
                // compute the full depth though
                let i = l.saturating_sub(from).min(511);

                // Find required depth for this span
                depth = depth.max(stacking[i]);

                // Increase depth for all other spans
                if (from..to).contains(&l) {
                    stacking[i] = stacking[i].saturating_add(1);
                }
            }

            // Vertical line
            let x = (xm - depth as f32 * 4.0).max(xl).floor();
            let sy = oy + *source as f32 * layout.height_unit + if *source_visible { oh } else { 0.0 };
            let ty = oy + *target as f32 * layout.height_unit + if *target_visible { oh } else { 0.0 };
            d.add_line([x, sy], [x, ty], *color).build();

            // Circle at source
            if *source_visible {
                d.add_line([x, sy], [xr - 1.0, sy], *color).build();
                d.add_circle([xr - 3.0, sy + 1.0], 3.0, *color).filled(true).build();
            }

            // Triangle at target
            if *target_visible {
                d.add_line([x, ty], [xr - 1.0, ty], *color).build();
                d.add_triangle(
                    // Top
                    [xr - 6.0, ty - 3.0],
                    // Bottom
                    [xr - 6.0, ty + 3.0],
                    // Right
                    [xr, ty],
                    *color

                ).filled(true).build();
            }
        }
    }
}

impl InnerTab {
    fn update(&mut self, s: &mut DebuggerState) {
        let mut analyzer = s.logic.analyzer.borrow_mut();
        s.cache.code.update(
            &s.core,
            &mut s.cache.memory,
            &mut *analyzer,
            &s.logic
        );
    }
}

impl InnerTabData {
    #[allow(clippy::too_many_arguments)]
    fn draw_instruction(
        &mut self,
        s: &DebuggerState,
        ui: &mwx::UI,
        d: &DrawListMut,
        instruction: &Instruction,
        addr: BankedAddress,
        layout: &LayoutData,
        (ox, oy, indent): (f32, f32, f32)

    ) -> (f32, Option<CodeDescriptor>) {
        // Detect and special case software BRK instructions
        let is_brk = instruction.code == 0x40 && s.config.system_halt_on_software_breakpoint;
        let (mnemonic, color) = if is_brk {
            ("BRK", [1.0, 1.0, 0.0, 1.0])

        } else {
            (instruction.name, layout.color_text)
        };

        // Render Instruction Name
        let name = format!("{: <8}", mnemonic);
        d.add_text([ox + indent, oy], color, &name);

        let mut tx = name.len() as f32 * layout.width_unit;
        if is_brk {
            (tx, None)

        } else {
            // Render Instruction Arguments
            let mut postfix = None;
                if instruction.is_conditional() {

                    }

            for (index, arg) in instruction.layout.iter().filter(|arg| *arg != &InstructionArgument::Unused).enumerate() {
                if index > 0 {
                    d.add_text([ox + indent + tx, oy], layout.color_text, ", ");
                    tx += layout.width_unit * 2.0;
                }

                // Resolve target address of argument
                let addr_target = if arg.is_user_provided() {
                    instruction.memory_argument().or_else(|| {
                        instruction.address_argument(addr.address())

                    }).map(|addr| {
                        (s.core.banked_address(addr), instruction.is_call() || instruction.is_jump())
                    })

                } else {
                    None
                };

                // Keep track of conditional branching
                let addr_branch = if let (InstructionArgument::Flag(_), true) = (arg, instruction.is_conditional()) {
                    Some((addr, instruction.is_jump(), instruction.is_call()))

                } else {
                    None
                };

                // Resolve symbol name of the target address
                let symbol_name = addr_target.and_then(|(addr, _)| {
                    s.logic.resolve_symbol_absolute(addr).map(|s| s.name_string())
                });

                // Display argument
                let label = arg.to_string(instruction.value, symbol_name);
                if let Some(p) = self.draw_argument(
                    s, ui,
                    d, layout, [ox + indent + tx, oy], &label,
                    addr_target,
                    addr_branch
                ) {
                    postfix = Some(p);
                }
                tx += label.len() as f32 * layout.width_unit;
            }
            (tx, postfix)
        }
    }

    #[allow(clippy::too_many_arguments)]
    fn draw_descriptor(
        &mut self,
        s: &DebuggerState,
        ui: &mwx::UI,
        d: &DrawListMut,
        layout: &LayoutData,
        (ox, oy, tx, width): (f32, f32, f32, f32),
        (addr, offset): (BankedAddress, usize),
        desc: &CodeDescriptor
    ) {
        let tl = [ox, oy];
        let p = [tx, oy];
        match (desc, offset) {
            (CodeDescriptor::Label { call_access_address, name, is_child, .. }, 1) => {
                let br = [width, oy + layout.height_line + 1.0];
                let hover = !is_child && ui.mouse_inside_rect(tl, br) ;
                let selected = self.selected_call_address == Some(addr);
                if hover {
                    // Tooltip
                    if let Some(addr) = call_access_address {
                        let analyzer = s.logic.analyzer.borrow();
                        let calls = analyzer.call_infos();
                        let current_cycle = analyzer.current_cycle();
                        let current_line = analyzer.current_line();
                        if let Some(call) = calls.get(addr) {
                            ui.text_tooltip(
                                &format_call(call, *addr, &s.logic, current_cycle, current_line, true, true, false)
                            );
                        }
                    }

                    //Selection
                    if ui.is_mouse_clicked(MouseButton::Left) {
                        self.selected_call_address = if selected {
                            None

                        } else {
                            Some(addr)
                        };
                    }
                }

                // Highlight
                let c = [1.0, 1.0, 0.0, if *is_child { 0.5 } else { 1.0 }];
                if hover || selected {
                    let mut bg = c;
                    bg[3] = if selected && hover { 0.025 } else { 0.1 };
                    d.add_rect_filled_multicolor(tl, br, bg, bg, bg, bg);
                    d.add_rect(tl, br, c).build();
                }

                // Separators
                let hy = (oy + layout.height_unit * 0.5 - 1.0).floor();
                if !is_child {
                    d.add_line([ox, hy], [tx - 4.0, hy], c).build();
                }
                d.add_line([tx + ui.calc_text_size(name)[0] + 4.0, hy], [width, hy], c).build();

                // Name
                d.add_text(p, c, name);
            },
            (CodeDescriptor::Label { call_access_address: Some(index), .. }, 2) => {
                let analyzer = s.logic.analyzer.borrow();

                let calls = analyzer.call_infos();
                let cycle = analyzer.current_cycle();
                if let Some(call) = calls.get(index) {
                    d.add_text(p, [1.0, 1.0, 0.0, 1.0], format!(
                        "{: >3} / {: >5} calls, {: >6} / {} cycles",
                        call.frame_calls(),
                        call.total_calls(),
                        call.frame_cycles(cycle),
                        call.total_cycles(cycle)
                    ));
                }
            },
            (CodeDescriptor::Comment(l), _) => d.add_text(p, layout.color_text_disabled, format!("; {}", l)),
            (CodeDescriptor::Value(l), _) => d.add_text(p, [0.4, 0.8, 0.0, 1.0], format!(" = {}", l)),
            _ => {}
        }
    }

    #[allow(clippy::too_many_arguments)]
    fn draw_argument(
        &mut self,
        s: &DebuggerState,
        ui: &mwx::UI,
        d: &DrawListMut,
        layout: &LayoutData,
        tl: [f32; 2],
        label: &str,
        target: Option<(BankedAddress, bool)>,
        branch: Option<(BankedAddress, bool, bool)>

    ) -> Option<CodeDescriptor> {
        if let Some((addr, is_jump_or_call)) = target {
            let hover = || {
                if ui.is_mouse_double_clicked(MouseButton::Left) && is_jump_or_call {
                    self.jump_target_address = Some(addr);
                }
                if !is_jump_or_call {
                    let byte = s.cache.memory.byte(addr.address());
                    ui.text_tooltip(&MemoryTab::format_memory_byte(s, byte));

                } else {
                    ui.text_tooltip("Double click jump to code");
                }
            };

            // Show memory values inline
            let (postfix, range) = if s.config.code_show_line_values && !is_jump_or_call {
                let byte = s.cache.memory.byte(addr.address());
                (Some(CodeDescriptor::Value(format!("{:0>2X} ({})", byte.value, byte.value))), addr.with_size(byte.size()))

            } else {
                (None, addr.into())
            };

            // Context Text
            if ui.text_custom(label).underline().with_position(d, tl).build_with_tooltip(hover) {
                ui.open_context_menu(DebuggerContextMenu::Code(range, is_jump_or_call, false, false));
            }
            postfix

        } else if let Some((addr, jump, call)) = branch {
            let analyzer = s.logic.analyzer.borrow();
            let branches = analyzer.branch_infos();
            let (hits, jumps, percentage) = if let Some(info) = branches.get(&addr) {
                info.stats()

            } else {
                (0, 0, 0.0)
            };
            let hover = || {
                let handler = if jump {
                    "  Jumps"

                } else if call {
                    "  Calls"

                } else {
                    "Returns"
                };
                ui.text_tooltip(
                    &format!("Conditional @ {}\n   Hits: {}\n{}: {} ({:.2}%)", addr, hits, handler, jumps, percentage)
                );
            };
            ui.text_custom(label).underline().with_position(d, tl).build_with_tooltip(hover);
            Some(CodeDescriptor::Comment(format!("{:.2}%", percentage)))

        } else {
            d.add_text(tl, layout.color_text, label);
            None
        }
    }
}

