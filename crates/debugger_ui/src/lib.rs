// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::rc::Rc;
use std::cell::RefCell;
use std::time::{Duration, Instant};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use dap::Debuggee;
use analyzer::VramUpdate;
use tile::{Tile, TileUsage};
use debugger_data::{DebuggerLogs, DebuggerState};
use sameboy_rs::{BankedAddress, Model, ReadableCore, WritableCore, RunnableCore};
use mwx::{
    FileDialog,
    nfde::{FilterableDialogBuilder, DefaultPathDialogBuilder},
    imgui::{self, Condition, TabBar, StyleColor}
};


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod context_menu;
mod icons;
mod util;
mod tabs;
mod view;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use tabs::DebuggerTabs;
use view::{Extensions, HexBuildResult, HexByteInput};
pub use context_menu::{DebuggerContextAction, DebuggerContextMenu};
pub use icons::used_icons_list;


// Debugger UI ----------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct DebuggerUI {
    first_draw: bool,
    tabs: Rc<RefCell<DebuggerTabs>>,
    logs: DebuggerLogs,

    register_a: HexByteInput,
    register_bc: HexByteInput,
    register_de: HexByteInput,
    register_hl: HexByteInput,
    register_sp: HexByteInput,
    register_pc: HexByteInput,

    vram_gen: usize,
    vram_refresh_time: Instant,
    vram_dmg_palette: [(u32, [f32; 4], [u8; 4]); 5],
    vram_tile_usage: [TileUsage; 768],
    vram_update: VramUpdate,
}

impl DebuggerUI {
    pub fn new(s: &mut DebuggerState) -> Self {
        s.config.tab_switch = 2;
        Self {
            first_draw: true,
            tabs: Rc::new(RefCell::new(DebuggerTabs::default())),
            logs: s.logs.clone(),

            register_a: HexByteInput::new("REGISTER_A", 2),
            register_bc: HexByteInput::new("REGISTER_BC", 4),
            register_de: HexByteInput::new("REGISTER_DE", 4),
            register_hl: HexByteInput::new("REGISTER_HL", 4),
            register_sp: HexByteInput::new("REGISTER_SP", 4),
            register_pc: HexByteInput::new("REGISTER_PC", 4),

            vram_gen: 1,
            vram_tile_usage: [TileUsage::default(); Tile::COUNT * 2],
            vram_dmg_palette: [
                (0b00, [1.00, 1.00, 1.00, 1.0], [255, 255, 255, 255]),
                (0b01, [0.50, 0.50, 0.50, 1.0], [128, 128, 128, 255]),
                (0b10, [0.25, 0.25, 0.25, 1.0], [ 64,  64,  64, 255]),
                (0b11, [0.00, 0.00, 0.00, 1.0], [  0,   0,   0, 255]),
                (0b11, [0.00, 0.00, 0.00, 0.0], [  0,   0,   0,   0]),
            ],
            vram_update: VramUpdate::default(),
            vram_refresh_time: Instant::now()
        }
    }

    pub fn is_ready(&self) -> bool {
        !self.first_draw
    }

    pub fn vram_gen(&self) -> usize {
        self.vram_gen
    }

    pub fn refresh_vram(&mut self) {
        self.vram_gen += 1;
    }

    pub fn set_dmg_palette(&mut self, palette: [[u8; 3]; 5]) {
        for i in 0..4 {
            let p = palette[3 - i];
            self.vram_dmg_palette[i].2 = [
                p[2],
                p[1],
                p[0],
                255
            ];
            self.vram_dmg_palette[i].1 = [
                p[0] as f32 / 255.0,
                p[1] as f32 / 255.0,
                p[2] as f32 / 255.0,
                1.0
            ];
        }
    }

    pub fn action(&mut self, s: &mut DebuggerState, action: DebuggerContextAction) {
        self.tabs.borrow_mut().action(s, action);
    }

    pub fn reset(&mut self) {
        self.tabs.borrow_mut().reset();
    }

    pub fn update(&mut self, s: &mut DebuggerState) {
        self.tabs.clone().borrow_mut().update(s, self);
    }

    pub fn draw(&mut self, window: &mut mwx::Window<DebuggerState>, ui: &mwx::UI) {
        let s = ui.clone_style();
        let padding = s.window_padding;
        let window_size = ui.io().display_size;
        let _c = ui.push_style_var(imgui::StyleVar::WindowBorderSize(0.0));
        let _p = ui.push_style_var(imgui::StyleVar::WindowPadding([padding[0], 4.0]));
        ui.window("Debugger")
            .title_bar(false)
            .resizable(false)
            .movable(false)
            .collapsible(false)
            .position([0.0, 0.0], Condition::Always)
            .size(window_size, Condition::Always)
            .build(|| {
                let _p = ui.push_style_var(imgui::StyleVar::WindowPadding(padding));
                self.draw_main_window(window, ui);
            });

        // Switch over to a new tab
        window.state.config.tab_switch = window.state.config.tab_switch.saturating_sub(1);
        if window.state.config.tab_switch == 0 {
            self.first_draw = false;
        }
    }

    pub fn draw_overlay(&mut self, s: &mut DebuggerState, ui: &mwx::UI, tl: [f32; 2], br: [f32; 2]) {
        self.tabs.borrow_mut().draw_overlay(s, ui, tl, br);
    }

    pub fn io_reg(&mut self, s: &DebuggerState, ui: &mwx::UI, index: u8, name: &str) {
        let v = s.core.read_io_register(index);
        self.register(ui, 0xFF00 + index as u16, &format!("{: >4}: {:0>2X} ({: >3})", name, v, v));
    }

    pub fn io_reg_ext(&mut self, s: &DebuggerState, ui: &mwx::UI, index: u8, mask: u8, name: &str) {
        let v = s.core.read_io_register(index) & mask;
        self.register(ui, 0xFF00 + index as u16, &format!("{: >4}: {:0>2X} ({: >3})", name, v, v));
    }

    pub fn io_reg_binary(&mut self, s: &DebuggerState, ui: &mwx::UI, index: u8, name: &str) {
        let v = s.core.read_io_register(index);
        self.register(ui, 0xFF00 + index as u16, &format!("{: >4}: {:0>2X} ({: >3}) ({:0>4b}_{:0>4b})", name, v, v, v >> 4, v & 0xF));
    }

    pub fn register(&mut self, ui: &mwx::UI, addr: u16, content: &str) {
        if ui.text_custom(&format!("${:0>4X}", addr)).underline().build() {
            ui.open_context_menu(DebuggerContextMenu::AddWatchpointViewMemory(BankedAddress::new(addr, 0).into()))
        }
        ui.same_line_with_spacing(0.0, 6.0);
        ui.text(content);
    }
}

impl DebuggerUI {
    fn draw_main_window(&mut self, window: &mut mwx::Window<DebuggerState>, ui: &mwx::UI) {
        ui.split(1, "DEBUGGER_CONTROLS");
        let _s = ui.push_style_var(imgui::StyleVar::ItemSpacing([4.0, 4.0]));
        if window.state.logic.is_halted() {
            if ui.button_with_shortcut(window, icons::RESUME_EMULATION, "Resume emulation", mwx::Key::P, false) {
                window.state.logic.resume(1);
            }

        } else if ui.button_with_shortcut(window, icons::PAUSE_EMULATION, "Pause emulation", mwx::Key::P, false) {
            window.state.logic.pause(1);
        }
        ui.same_line();
        if ui.button_with_shortcut(window, icons::STEP_INTO, "Step (into calls)", mwx::Key::I, false) {
            window.state.logic.step_in(1);
        }
        ui.same_line();
        if ui.button_with_shortcut(window, icons::STEP_OVER, "Step (over calls)", mwx::Key::N, true) {
            window.state.logic.next(1);
        }
        ui.same_line();
        if ui.button_with_shortcut(window, icons::STEP_OUT, "Step (out of current call)", mwx::Key::U, false) {
            window.state.logic.step_out(1);
        }
        ui.same_line();
        if ui.button_with_shortcut(window, icons::ADVANCE_FRAME, "Advance one frame\n+ Shift: Advance 144 lines", mwx::Key::F, true) {
            window.state.logic.advance_frames(1, !window.input.key_held(mwx::Key::LShift));
        }
        ui.same_line();
        if ui.button_with_shortcut(window, icons::ADVANCE_LINE, "Advance one line", mwx::Key::L, true) {
            window.state.logic.advance_lines(1);
        }

        ui.same_line_with_spacing(0.0, 20.0);
        if ui.button_with_shortcut(window, icons::RESET_EMULATION, "Reset emulation", mwx::Key::R, false) {
            window.state.logic.request_reset();
        }
        ui.same_line();
        ui.enabled(window.state.has_rom(), || {
            if ui.button_with_shortcut(window, icons::RELOAD_ROM, "Reload ROM", mwx::Key::O, false) {
                window.state.logic.request_reload();
            }
        });

        ui.same_line_with_spacing(0.0, 20.0);
        if window.state.config.global_override_disable_halts {
            if ui.button_with_shortcut(window, icons::HALT_INACTIVE, "HALT checks are inactive\nClick to enable them", mwx::Key::H, false) {
                window.state.config.global_override_disable_halts = false;
            }

        } else if ui.button_with_shortcut(window, icons::HALT_ACTIVE, "HALT checks are active\nClick to disable them", mwx::Key::H, false) {
            window.state.config.global_override_disable_halts = true;
        }
        ui.same_line();
        if window.state.config.global_override_disable_sound {
            if ui.button_with_shortcut(window, icons::SOUND_MUTED, "Sound channels are inactive\nClick to enable them", mwx::Key::M, false) {
                window.state.config.global_override_disable_sound = false;
            }

        } else if ui.button_with_shortcut(window, icons::SOUND_UNMUTED, "Sound channels are active\nClick to disable them", mwx::Key::M, false) {
            window.state.config.global_override_disable_sound = true;
        }
        self.draw_main_window_inner(window, ui);
    }

    fn draw_main_window_inner(&mut self, window: &mut mwx::Window<DebuggerState>, ui: &mwx::UI) {

        // Model Selector
        let width = ui.window_content_region_max()[0];
        let unit = ui.calc_text_size("#")[0];

        ui.same_line_with_pos(width - unit * 12.0);
        if ui.button_with_shortcut(window, icons::LOAD_ROM, "Load ROM from file system", mwx::Key::S, false) {
            if let Ok(Some(rom_path)) = ui.open_file(|nfd| {
                let nfd = if let Some(dir) = window.state.rom_path.as_ref().and_then(|p| p.parent()) {
                    nfd.default_path(&dir)?
                } else {
                    nfd
                };
                nfd.add_filter("GameBoy ROMs", "gb,gbc")
            }) {
                window.state.rom_path = Some(rom_path);
                window.state.logic.request_reload();
            }
        }

        ui.same_line_with_pos(width - unit * 6.0);
        let _item_width = ui.push_item_width(unit * 8.0);
        if ui.combo("##MODEL", &mut window.state.config.model, &["DMG", "MGB", "CGB", "AGB", "SGB", "SGB2"], |l| (*l).into()) {
            window.state.logic.request_model_switch(Model::from(window.state.config.model));
        }
        ui.separator();

        // Refresh textures etc.
        self.vram_dmg_palette[4].1 = ui.style_color(StyleColor::Header);
        if self.vram_refresh_time.elapsed() > Duration::from_millis(50) {
            self.refresh_vram();
            self.vram_update = window.state.logic.analyzer.borrow_mut().vram_update();
            self.vram_refresh_time = Instant::now();
        }

        ui.split(1, "DEBUGGER_TABS");
        TabBar::new("DEBUGGER_TABS_BAR").build(ui, || {
            let color = ui.style_color(StyleColor::Tab);
            let c = ui.push_style_color(StyleColor::Tab, [color[0] * 0.2, color[1] * 0.2, color[2] * 0.2, 1.0 ]);
            self.tabs.clone().borrow_mut().draw_content(window.state, self, ui);
            c.pop();
        });

        {
            // CPU Status Bar
            let core = &mut window.state.core;
            let unit = ui.calc_text_size("#")[0];
            let c = ui.cursor_pos();
            ui.separate_split();
            ui.text(format!(
                "Z   N   H   C    A:    BC:      DE:      HL:      SP:      PC:      LN: {: >3} PX: {: >3}",
                core.current_line(),
                core.current_pixel()
            ));

            // Flags
            let mut regs = core.word_registers();
            flag_box(core, ui, c, 1.5 * unit, regs.af, 7);
            flag_box(core, ui, c, 5.5 * unit, regs.af, 6);
            flag_box(core, ui, c, 9.5 * unit, regs.af, 5);
            flag_box(core, ui, c, 13.5 * unit, regs.af, 4);

            // Registers
            register_pos(ui, c, 19.5 * unit);
            if let HexBuildResult::Value(a) = self.register_a.build(ui, regs.af >> 8) {
                regs.af = regs.af & 0xFF | a << 8;
                core.set_word_registers(regs);
            }
            register_pos(ui, c, 26.5 * unit);
            match self.register_bc.build(ui, regs.bc) {
                HexBuildResult::Value(bc) => {
                    regs.bc = bc;
                    core.set_word_registers(regs);
                },
                HexBuildResult::ContextMenu => {
                    ui.open_context_menu(DebuggerContextMenu::ViewMemory(BankedAddress::new(regs.bc, 0)));
                },
                _ => {}
            }
            register_pos(ui, c, 35.5 * unit);
            match self.register_de.build(ui, regs.de) {
                HexBuildResult::Value(de) => {
                    regs.de = de;
                    core.set_word_registers(regs);
                },
                HexBuildResult::ContextMenu => {
                    ui.open_context_menu(DebuggerContextMenu::ViewMemory(BankedAddress::new(regs.de, 0)));
                },
                _ => {}
            }
            register_pos(ui, c, 44.5 * unit);
            match self.register_hl.build(ui, regs.hl) {
                HexBuildResult::Value(hl) => {
                    regs.hl = hl;
                    core.set_word_registers(regs);
                },
                HexBuildResult::ContextMenu => {
                    ui.open_context_menu(DebuggerContextMenu::ViewMemory(BankedAddress::new(regs.hl, 0)));
                },
                _ => {}
            }
            register_pos(ui, c, 53.5 * unit);
            match self.register_sp.build(ui, regs.sp) {
                HexBuildResult::Value(sp) => {
                    regs.sp = sp;
                    core.set_word_registers(regs);
                },
                HexBuildResult::ContextMenu => {
                    ui.open_context_menu(DebuggerContextMenu::ViewMemory(BankedAddress::new(regs.sp, 0)));
                },
                _ => {}
            }
            register_pos(ui, c, 62.5 * unit);
            match self.register_pc.build(ui, regs.pc) {
                HexBuildResult::Value(pc) => {
                    regs.pc = pc;
                    core.set_word_registers(regs);
                },
                HexBuildResult::ContextMenu => {
                    ui.open_context_menu(DebuggerContextMenu::ViewMemoryJumpCode(BankedAddress::new(regs.pc, 0)));
                },
                _ => {}
            }
        }
    }
}

fn register_pos(ui: &imgui::Ui, c: [f32; 2], p: f32) {
    ui.set_cursor_pos([c[0] + p, c[1] + 5.0]);
}

fn flag_box(core: &mut RunnableCore, ui: &imgui::Ui, c: [f32; 2], p: f32, af: u16, bit: u8) {
    let _p = ui.push_style_var(imgui::StyleVar::FramePadding([0.0, 0.0]));
    let mut flag = (af as u8 >> bit) & 1 == 1;
    ui.set_cursor_pos([c[0] + p, c[1] + 5.0]);
    if ui.checkbox(format!("##flag_{}", bit), &mut flag) {
        let mut regs = core.word_registers();
        regs.af ^= 1 << bit;
        core.set_word_registers(regs);
    }
}

