// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::ops::Range;
use std::time::Instant;
use std::collections::HashSet;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::BankedAddress;
use debugger_data::DebuggerState;
use analyzer::{AddressAccess, VramAccessKey, SymbolProvider};
use mwx::imgui::{self, DrawListMut, MouseButton, StyleColor};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{DebuggerContextMenu, view::Extensions};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub const COLOR_MAP: [[f32; 4]; 9] = [
    [0.0, 0.749, 1.0, 1.0],     // Blues::DeepSkyBlue,
    [1.0, 0.0, 0.0, 1.0],       // Reds::Red,
    [0.498, 1.0, 0.0, 1.0],     // Greens::Green2,
    [1.0, 1.0, 0.0, 1.0],       // Yellows::Yellow,
    [0.0, 1.0, 1.0, 1.0],       // Cyans::Cyan,
    [1.0, 0.647, 0.0, 1.0],     // Oranges::Orange,
    [1.0, 0.0, 1.0, 1.0],       // Purples::Magenta,
    [0.541, 0.168, 0.886, 1.0], // Purples::BlueViolet,
    //[0.5, 0.5, 0.5, 1.0],       // Grays::Gray50,
    [0.823, 0.705, 0.549, 1.0], // Browns::Tan
];

pub const COLOR_MAP_SMALL: [[f32; 4]; 6] = [
    [1.0, 0.0, 0.0, 1.0],       // Reds::Red,
    [0.498, 1.0, 0.0, 1.0],     // Greens::Green2,
    [1.0, 0.647, 0.0, 1.0],     // Oranges::Orange,
    [0.0, 1.0, 1.0, 1.0],       // Cyans::Cyan,
    [1.0, 0.0, 1.0, 1.0],       // Purples::Magenta,
    [0.541, 0.168, 0.886, 1.0], // Purples::BlueViolet,
];

const HIGHLIGHT_TIME: f32 = 2.0;

const TILE_HIGHLIGHT_COLOR: [f32; 4] = [1.0, 0.5, 0.0, 1.0];
const LINE_PIXEL_WIDTH: u32 = 160;

const HDMA_WRITE_COLOR: [f32; 4] = [0.5, 0.0, 1.0, 1.0];
const HDMA_READ_COLOR: [f32; 4] = [1.0, 1.0, 0.0, 1.0];

const VRAM_WRITE_COLOR: [f32; 4] = [1.0, 0.0, 0.0, 1.0];
const VRAM_READ_COLOR: [f32; 4] = [0.0, 1.0, 0.0, 1.0];


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct ReadWriteAverage {
    halted: bool,
    count_read: usize,
    count_written: usize,
    average_read: f32,
    average_written: f32,
    last_read: usize,
    last_written: usize
}

impl ReadWriteAverage {
    pub fn update(&mut self, halted: bool, step: bool, read: usize, written: usize) {
        if !halted || step {
            self.average_read = (read as f32 + self.average_read * self.count_read as f32) / (self.count_read + 1) as f32;
            self.average_written = (written as f32 + self.average_written * self.count_written as f32) / (self.count_written + 1) as f32;
            self.count_read = (self.count_read + 1).min(120);
            self.count_written = (self.count_written + 1).min(120);
            self.last_read = read;
            self.last_written = written;
        }
        self.halted = halted;
    }

    pub fn format(&self) -> String {
        format!("RD: {: >5} WR: {: >5}", self.read(), self.written())
    }

    pub fn format_writes(&self) -> String {
        format!("WR: {: >5}", self.written())
    }

    fn read(&self) -> usize {
        if self.halted {
            self.last_read

        } else {
            self.average_read.round() as usize
        }
    }

    fn written(&self) -> usize {
        if self.halted {
            self.last_written

        } else {
            self.average_written.round() as usize
        }
    }
}

pub struct LayoutData {
    pub width_unit: f32,
    pub height_line: f32,
    pub height_unit: f32,
    pub color_text: [f32; 4],
    pub color_text_disabled: [f32; 4],
    pub color_text_selected: [f32; 4],
    pub color_bg: [f32; 4],
    pub color_frame_bg: [f32; 4],
    pub color_border: [f32; 4]
}

impl LayoutData {
    pub fn new(ui: &imgui::Ui) -> Self {
        let height_line = ui.text_line_height();
        Self {
            width_unit: ui.calc_text_size("#")[0],
            height_line,
            height_unit: height_line + 5.0,
            color_text: ui.style_color(StyleColor::Text),
            color_text_disabled: ui.style_color(StyleColor::TextDisabled),
            color_text_selected: ui.style_color(StyleColor::TextSelectedBg),
            color_bg: ui.style_color(StyleColor::WindowBg),
            color_frame_bg: ui.style_color(StyleColor::FrameBg),
            color_border: ui.style_color(StyleColor::Border)
        }
    }
}


// Utilities ------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub fn address_color(addr: BankedAddress, index: usize) -> [f32; 4] {
    let index = ((addr.offset() / (index + 1)) % (COLOR_MAP.len() - 1)) + 1;
    COLOR_MAP[index]
}

pub fn highlight_factor(timer: &Instant, scale: f32) -> Option<f32> {
    let t = timer.elapsed().as_secs_f32();
    let e = (HIGHLIGHT_TIME - t).max(0.0);
    let r = 1.0 / HIGHLIGHT_TIME * e;
    if r > 0.0 {
        Some(r * (t * scale).sin().abs() * 2.0)

    } else {
        None
    }
}

#[allow(clippy::too_many_arguments)]
pub fn cell_selection<T: FnMut(&imgui::DrawListMut), U: Fn(usize, usize, u16) -> DebuggerContextMenu>(
    ui: &mwx::UI,
    tl: [f32; 2],
    br: [f32; 2],
    bank: u16,
    highlight_timer: &Instant,
    highlight_cell: &mut Option<(usize, usize, u16)>,
    mut custom: T,
    context_menu: U

) -> Option<(usize, usize, u16)> {
    // Cell indexing
    let grid_position = if ui.is_mouse_hovering_rect(tl, br) {
        let mp = ui.io().mouse_pos;
        Some((
            ((mp[0] - tl[0]) / 17.0).floor() as usize,
            ((mp[1] - tl[1]) / 17.0).floor() as usize,
            bank,
            true
        ))

    } else if let Some((x, y, b)) = *highlight_cell {
        if b == bank {
            Some((x, y, bank, false))

        } else {
            None
        }

    } else {
        None
    };

    // Highlight selected tile
    let d = ui.get_window_draw_list();
    if let Some((x, y, b)) = *highlight_cell {
        if b == bank {
            let rtl = [tl[0] + x as f32 * 17.0 - 1.0, tl[1] + y as f32 * 17.0 - 1.0];
            let rbr = [rtl[0] + 18.0, rtl[1] + 18.0];
            if let Some(t) = highlight_factor(highlight_timer, 5.0) {
                let mut c = TILE_HIGHLIGHT_COLOR;
                c[3] = 0.85 * t;

                let s = (4.0 * t).floor();
                d.add_rect_filled_multicolor(
                    [rtl[0] - s, rtl[1] - s],
                    [rbr[0] + s, rbr[1] + s],
                    c, c, c, c
                );
            }
            d.add_rect(rtl, rbr, TILE_HIGHLIGHT_COLOR).build();
        }
    }

    // Custom highlight
    custom(&d);

    // Highlight currently hovered tile / show info tooltip
    if let Some((x, y, bank, hover)) = grid_position {
        if ui.is_mouse_hovering_rect(tl, br) && ui.is_mouse_clicked(MouseButton::Right) && hover {
            ui.open_context_menu(context_menu(x, y, bank));
        }

        let rtl = [tl[0] + x as f32 * 17.0 - 1.0, tl[1] + y as f32 * 17.0 - 1.0];
        let rbr = [rtl[0] + 18.0, rtl[1] + 18.0];
        d.add_rect(rtl, rbr, TILE_HIGHLIGHT_COLOR).build();
        let context_open = ui.has_context_menu_open();
        if ui.is_mouse_clicked(MouseButton::Left) && hover && !context_open {
            if *highlight_cell == Some((x, y, bank)) {
                *highlight_cell = None;

            } else {
                *highlight_cell = Some((x, y, bank));
            }
        }
        if hover && !context_open {
            Some((x, y, bank))

        } else {
            None
        }

    } else {
        None
    }
}

pub fn draw_color(ui: &imgui::Ui, color: [[u8; 4]; 4], index: usize, sprite: bool) {
    let p = ui.cursor_screen_pos();
    let t = ui.cursor_pos();
    ui.set_cursor_pos([t[0] - 2.0, t[1]]);
    ui.text(format!("{: >2}.", index));
    if sprite {
        color_box(ui, p, color[1], 0);
        color_box(ui, p, color[2], 1);
        color_box(ui, p, color[3], 2);

    } else {
        color_box(ui, p, color[0], 0);
        color_box(ui, p, color[1], 1);
        color_box(ui, p, color[2], 2);
        color_box(ui, p, color[3], 3);
    }
}

pub fn draw_overlay_tooltip(ui: &mwx::UI, text: &str) {
    let _p = ui.push_style_var(imgui::StyleVar::WindowPadding([1.0, 1.0]));
    let _b = ui.push_style_var(imgui::StyleVar::PopupBorderSize(0.0));
    ui.text_tooltip(text);
}

pub fn draw_overlay_line(ui: &mwx::UI, d: &DrawListMut, origin: [f32; 2], x: f32, y: f32, width: f32, color: [f32; 4]) -> ([f32; 2], [f32; 2]) {
    let s = ui.pixel_scale();
    let tl = [origin[0] + x.min(160.0) * s, origin[1] + y * s];
    let br = [origin[0] + (x + width).min(160.0) * s, origin[1] + y * s + 1.0 * s];
    d.add_rect_filled_multicolor(tl, br, color, color, color, color);
    (tl, br)
}

pub fn draw_overlay_line_segment(
    ui: &mwx::UI,
    d: &DrawListMut,
    origin: [f32; 2],
    x: f32, y: f32,
    width: f32,
    color: [f32; 4]

) -> ([f32; 2], [f32; 2]) {
    let s = ui.pixel_scale();
    let [px, py] = [origin[0], origin[1] + 0.5];
    let tl = [px + x.min(160.0) * s, py + y * s];
    let br = [px + (x + width).min(160.0) * s, py + y * s];
    d.add_line(tl, br, color).build();
    (tl, br)
}

pub fn draw_overlay_textline(ui: &mwx::UI, origin: [f32; 2], line: u8, text: &str, color: [f32; 4]) {
    let text_height = ui.calc_text_size("L")[1];
    let d = ui.get_background_draw_list();
    let s = ui.pixel_scale();
    let (tl, br) = (
        [origin[0], origin[1] + line as f32 * s],
        [origin[0] + LINE_PIXEL_WIDTH as f32 * s, origin[1] + (line as f32 + 1.0) * s]
    );
    d.add_rect(tl, br, color).build();
    d.add_text([tl[0], tl[1] - text_height], color, format!("{} = {}", text, line));
}

pub fn draw_vram_access_overlay(s: &DebuggerState, ui: &mwx::UI, tl: [f32; 2], addrs: Range<u16>, banks: Range<u16>) {
    let draw_list = ui.get_background_draw_list();
    for (VramAccessKey { line, pixel, write, hdma, .. }, values) in s.logic.analyzer.borrow().vram_access() {
        if values.iter().filter(|v| addrs.contains(&v.addr.address()) && banks.contains(&v.addr.bank())).count() == 0 {
            continue;
        }
        let color = access_color(*write, *hdma);
        if draw_overlay_pixel(ui, &draw_list, tl, *pixel as f32, *line as f32, color) {
            let prefix = match (*write, *hdma) {
                (true, true) => "HDMA WR",
                (true, false) => "VRAM WR",
                (false, true) => "HDMA RD",
                (false, false) => "VRAM RD"
            };
            overlay_pixel_tooltip(s, ui, *line, *pixel, prefix, values.iter());
        }
    }
}

pub fn draw_overlay_pixel(ui: &mwx::UI, d: &DrawListMut, origin: [f32; 2], x: f32, y: f32, color: [f32; 4]) -> bool {
    let s = ui.pixel_scale();
    let [x, y] = [origin[0] + x.min(160.0) * s, origin[1] + y.min(154.0) * s];
    let [cx, cy] = [x + 0.5, y + 0.5];

    let inner_size = (s * 0.65).max(1.0).min(3.5);
    let imin = [(cx - inner_size).max(0.0), (cy - inner_size).max(0.0)];
    let imax = [cx + inner_size, cy + inner_size];

    let border_size = (s * 0.5).max(1.0).min(2.0);
    let bmin = [(imin[0] - border_size).max(0.0), (imin[1] - border_size).max(0.0)];
    let bmax = [imax[0] + border_size, imax[1] + border_size];
    let bg = [0.0, 0.0, 0.0, 0.5];
    d.add_rect_filled_multicolor(bmin, bmax, bg, bg, bg, bg);

    let border_size = 1.0;
    let bmin = [(imin[0] - border_size).max(0.0), (imin[1] - border_size).max(0.0)];
    let bmax = [imax[0] + border_size, imax[1] + border_size];
    let bg = [color[0] * 0.5, color[1] * 0.5, color[2] * 0.5, 1.0];
    d.add_rect_filled_multicolor(bmin, bmax, bg, bg, bg, bg);

    d.add_rect_filled_multicolor(imin, imax, color, color, color, color);
    ui.mouse_inside_rect(bmin, bmax)
}

pub fn overlay_pixel_tooltip<'a, T: AddressAccess + 'a>(
    s: &DebuggerState,
    ui: &mwx::UI,
    line: u8,
    pixel: u8,
    prefix: &str,
    access: impl Iterator<Item=&'a T>
) {
    let mut addrs: Vec<(BankedAddress, BankedAddress)> = access.map(|a| (a.address(), a.by())).collect();
    addrs.sort_unstable();
    if addrs.is_empty() {
        return;
    }

    let address_info = map_ranges(&addrs).into_iter().map(|(start, end, count)| {
        if count == 1 {
            if start == end {
                format_address(start, &s.logic)

            } else {
                format!("{}-{}", start, end)
            }

        } else if start == end {
            format!("{}(x{})", format_address(start, &s.logic), count)

        } else {
            format!("{}-${}(x{})", start, end, count)
        }

    }).collect::<Vec<String>>().join(", ");

    let mut accessors = HashSet::new();
    for (_, addr) in addrs {
        accessors.insert(addr);
    }
    let mut accessors: Vec<BankedAddress> = accessors.drain().collect();
    accessors.sort_unstable();

    let accessor_info = accessors.into_iter().map(|addr| {
        s.logic.resolve_symbol_name_relative(addr)

    }).collect::<Vec<String>>().join(", ");

    ui.text_tooltip(
        &format!("{} @ {}\n   BY: {}\n   LN: {}\n   PX: {}", prefix, address_info, accessor_info, line, pixel)
    );
}

fn format_address<S: SymbolProvider>(addr: BankedAddress, provider: &S) -> String {
    match provider.resolve_symbol_absolute(addr) {
        Some(symbol) => format!("{} ({})", symbol.name(), addr),
        None => format!("{}", addr)
    }
}

fn map_ranges(addrs: &[(BankedAddress, BankedAddress)]) -> Vec<(BankedAddress, BankedAddress, usize)>  {
    let mut addrs = addrs.iter().copied();
    let start = addrs.next().unwrap().0;
    let mut ranges = vec![(start, start, 1)];
    for (next, _) in addrs {
        let prev = ranges.last().unwrap().1;
        if next == prev.add(1) {
            ranges.last_mut().unwrap().2 += 1;
            ranges.last_mut().unwrap().1 = next;

        } else if next != prev {
            ranges.push((next, next, 1));

        } else {
            ranges.last_mut().unwrap().2 += 1;
        }
    }
    ranges
}

pub fn access_color(write: bool, hdma: bool) -> [f32; 4] {
    if write {
        if hdma {
            HDMA_WRITE_COLOR

        } else {
            VRAM_WRITE_COLOR
        }

    } else if hdma {
        HDMA_READ_COLOR

    } else {
        VRAM_READ_COLOR
    }
}

fn color_box(ui: &imgui::Ui, p: [f32; 2], hex: [u8; 4], index: usize) {
    let c = bgr_color_to_f32(hex);
    let unit = ui.calc_text_size("F")[0];
    let list = ui.get_window_draw_list();
    let w = 45.0;
    let h = 13.0;
    let border = [1.0, 1.0, 1.0, 1.0];
    let offset = 5.0 + 8.0 * index as f32;
    let tl = [p[0] + unit * offset, p[1] + 1.0];
    let br = [p[0] + unit * offset + w, p[1] + 1.0 + h];
    list.add_rect_filled_multicolor(tl, br, c, c, c, c);
    list.add_rect(tl, br, border).build();
    if ui.mouse_inside_rect(tl, br) {
        ui.tooltip(|| ui.text(format!(
            "R ${:0>2X} ({})\nG ${:0>2X} ({})\nB ${:0>2X} ({})",
            hex[2],
            hex[2],
            hex[1],
            hex[1],
            hex[0],
            hex[0],
        )));
    }
}

fn bgr_color_to_f32(color: [u8; 4]) -> [f32; 4] {
    [
        color[2] as f32 / 255.0,
        color[1] as f32 / 255.0,
        color[0] as f32 / 255.0,
        color[3] as f32 / 255.0
    ]
}

