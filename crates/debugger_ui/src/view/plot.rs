// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::imgui::StyleColor;


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait Plot<'a> {
    fn plot<'b>(&'a self, id: &'b str) -> PlotBuilder<'a, 'b>;
}

impl<'a> Plot<'a> for mwx::UI<'a> {
    fn plot<'b>(&'a self, id: &'b str) -> PlotBuilder<'a, 'b> {
        PlotBuilder {
            id,
            ui: self,
            size: [0.0; 2],
            x_label: None,
            y_label: None,
            x_range: (0.0, 100.0, 10.0),
            y_range: (0.0, 100.0, 10.0),
            lines: Vec::new()
        }
    }
}


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
struct PlotLine {
    label: String,
    active: bool,
    color: [f32; 4],
    points: Vec<(f32, f32)>
}

struct LegendItem<'a> {
    label: &'a str,
    color: [f32; 4],
    tl: [f32; 2],
    br: [f32; 2],
    active: bool,
    hovered: bool
}


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct PlotBuilder<'a, 'b> {
    ui: &'a mwx::UI<'a>,
    id: &'b str,
    size: [f32; 2],
    x_label: Option<&'b str>,
    y_label: Option<&'b str>,
    x_range: (f32, f32, f32),
    y_range: (f32, f32, f32),
    lines: Vec<PlotLine>
}

impl<'a, 'b> PlotBuilder<'a, 'b> {
    pub fn size(&mut self, size: [f32; 2]) -> &mut Self {
        self.size = size;
        self
    }

    pub fn x_label(&mut self, label: &'b str) -> &mut Self {
        self.x_label = Some(label);
        self
    }

    pub fn y_label(&mut self, label: &'b str) -> &mut Self {
        self.y_label = Some(label);
        self
    }

    pub fn x_range(&mut self, from: f32, to: f32, step: f32) -> &mut Self {
        self.x_range = (from, to, step);
        self
    }

    pub fn y_range(&mut self, from: f32, to: f32, step: f32) -> &mut Self {
        self.y_range = (from, to, step);
        self
    }

    pub fn line<S: Into<String>>(&mut self, label: S, active: bool, color: [f32; 4], points: Vec<(f32, f32)>) -> &mut Self {
        self.lines.push(PlotLine {
            label: label.into(),
            active,
            color,
            points
        });
        self
    }

    pub fn build(&mut self) -> Option<usize> {
        let th = self.ui.calc_text_size("L")[1];
        let bc = self.ui.style_color(StyleColor::Button);
        let bg = self.ui.style_color(StyleColor::WindowBg);
        let hc = self.ui.style_color(StyleColor::TextSelectedBg);
        let tick_color = self.ui.style_color(StyleColor::TextDisabled);
        let text_color = self.ui.style_color(StyleColor::Text);
        let label_spacing = 2.0;
        let tick_width = 5.0;
        let mut hovered = None;
        self.ui.child_window(format!("{}_window", self.id))
            .size(self.size)
            .build(|| {
                let d = self.ui.get_window_draw_list();
                let p = self.ui.cursor_screen_pos();
                let [x_size, y_size] = self.ui.content_region_avail();
                let mut x_offset = if let Some(label) = self.y_label {
                    (self.ui.calc_text_size(label)[0] + label_spacing * 2.0).floor()

                } else {
                    0.0
                };
                x_offset += x_offset
                    .max(self.ui.calc_text_size(&format!("{:.0}", self.y_range.0))[0])
                    .max(self.ui.calc_text_size(&format!("{:.0}", self.y_range.1))[0]);

                let mut y_offset = if let Some(label) = self.x_label {
                    (self.ui.calc_text_size(label)[1] + label_spacing).floor()

                } else {
                    0.0
                };
                y_offset += y_offset
                    .max(self.ui.calc_text_size(&format!("{:.0}", self.x_range.0))[1])
                    .max(self.ui.calc_text_size(&format!("{:.0}", self.x_range.1))[1]);

                let y_size = (y_size - y_offset).floor();
                let x_size = (x_size - x_offset).floor();

                // Y Ticks
                let mut y = self.y_range.0;
                while y <= self.y_range.1 {
                    let y_percent = y / (self.y_range.1 - self.y_range.0);
                    let y_relative = (y_size * y_percent).floor().abs();
                    let y_pos = p[1] + y_size - y_relative;
                    d.add_rect_filled_multicolor(
                        [p[0] + x_offset, y_pos - 1.0],
                        [p[0] + x_offset + tick_width, y_pos + 1.0],
                        tick_color,
                        tick_color,
                        tick_color,
                        tick_color
                    );

                    let label = format!("{:.0}", y);
                    let label_width = self.ui.calc_text_size(&label)[0];
                    d.add_text(
                        [p[0] + x_offset - label_width - label_spacing, y_pos - th * 0.5],
                        text_color,
                        label
                    );
                    y += self.y_range.2;
                }

                // X Ticks
                let mut x = self.x_range.0;
                while x <= self.x_range.1 {
                    let y_pos = p[1] + y_size + label_spacing * 0.5;
                    let x_percent = x / (self.x_range.1 - self.x_range.0);
                    let x_relative = (x_size * x_percent).floor();
                    let x_pos = p[0] + x_offset + if x_relative <= 0.0 {
                        x_size + x_relative

                    } else {
                        x_relative
                    };
                    d.add_rect_filled_multicolor(
                        [x_pos - 1.0, y_pos - tick_width],
                        [x_pos + 1.0, y_pos],
                        tick_color,
                        tick_color,
                        tick_color,
                        tick_color
                    );

                    let label = format!("{:.0}", x);
                    let label_width = self.ui.calc_text_size(&label)[0];
                    d.add_text(
                        [x_pos - (label_width * 0.5).floor(), y_pos],
                        text_color,
                        label
                    );

                    x += self.x_range.2;
                }

                // Y Legend
                d.add_line(
                    [p[0] + x_offset - 1.0, p[1]],
                    [p[0] + x_offset - 1.0, p[1] + y_size],
                    bc

                ).build();
                if let Some(label) = self.y_label {
                    d.add_text(
                        [p[0], p[1] + y_size * 0.5 - th * 0.5],
                        text_color,
                        label
                    );
                }

                // X Legend
                d.add_line(
                    [p[0] + x_offset, p[1] + y_size],
                    [p[0] + x_offset + x_size, p[1] + y_size],
                    bc

                ).build();
                if let Some(label) = self.x_label {
                    let width = self.ui.calc_text_size(label)[0];
                    d.add_text(
                        [
                            p[0] + x_offset + (x_size * 0.5 - width * 0.5),
                            p[1] + y_size + th + label_spacing * 0.5
                        ],
                        text_color,
                        label
                    );
                }

                // Legend Item Hover
                let ox = p[0] + x_offset;
                let oy = p[1] + y_size;
                let legend_spacing = 5.0;
                let mut legend_width = 0.0f32;
                let legend_items = {
                    let ox = ox + legend_spacing * 3.0;
                    let oy = p[1] + legend_spacing * 3.0;
                    let mp = self.ui.io().mouse_pos;
                    let mut legend_items = Vec::new();
                    for (i, line) in self.lines.iter().enumerate() {
                        let label_width = self.ui.calc_text_size(&line.label)[0] + label_spacing;
                        let y_pos = oy + i as f32 * th;
                        let tl = [ox, y_pos];
                        let br = [ox + th + label_spacing + label_width, y_pos + th];
                        let item = LegendItem {
                            label: &line.label,
                            color: line.color,
                            tl,
                            br,
                            active: line.active,
                            hovered: !self.ui.has_context_menu_open() && mp[0] >= tl[0] && mp[1] >= tl[1] && mp[0] < br[0] && mp[1] < br[1]
                        };
                        if item.hovered {
                            hovered = Some(i);
                        }
                        legend_items.push(item);
                        legend_width = legend_width.max(label_width);
                    }
                    legend_items
                };

                // Lines
                {
                    let xr = self.x_range.1 - self.x_range.0;
                    let yr = self.y_range.1 - self.y_range.0;
                    let hovered_active = hovered.and_then(|i| self.lines.get(i)).map(|line| line.active).unwrap_or(false);
                    for (i, line) in self.lines.iter().enumerate() {
                        let color = if line.active && (Some(i) == hovered || hovered.is_none() || !hovered_active) {
                            line.color

                        } else {
                            let mut color = tick_color ;
                            color[3] = 0.1;
                            color
                        };
                        for pair in line.points.windows(2) {
                            if let [(px, py), (nx, ny)] = pair {
                                let px = x_size * px / xr;
                                let py = y_size * py / yr;
                                let nx = x_size * nx / xr;
                                let ny = y_size * ny / yr;
                                d.add_line(
                                    [ox + x_size + px, oy - py - 1.0],
                                    [ox + x_size + nx, oy - ny - 1.0],
                                    color

                                ).build();
                            }
                        }
                    }
                }

                // Legend
                if !legend_items.is_empty() {
                    let legend_width = legend_width + legend_spacing * 3.0 + th;
                    let legend_height = self.lines.len() as f32 * th;
                    let legend_height = legend_height + legend_spacing * 2.0;
                    let ox = ox + legend_spacing * 2.0;
                    let oy = p[1] + legend_spacing * 2.0;
                    d.add_rect_filled_multicolor(
                        [ox, oy],
                        [ox + legend_width, oy + legend_height],
                        bg,
                        bg,
                        bg,
                        bg
                    );
                    d.add_rect(
                        [ox, oy],
                        [ox + legend_width, oy + legend_height],
                        tick_color,

                    ).build();
                    for item in legend_items {
                        let x = ox + label_spacing * 2.0;
                        let mut item_box_color = item.color;
                        let mut item_text_color = text_color;
                        if !item.active {
                            item_box_color[3] = 0.1;
                            item_text_color = tick_color;
                        }
                        if item.hovered {
                            d.add_rect_filled_multicolor(item.tl, item.br, hc, hc, hc, hc);
                        }
                        d.add_rect_filled_multicolor(
                            [x + 2.0, item.tl[1] + 2.0],
                            [x + th - 2.0, item.tl[1] + th - 2.0],
                            item_box_color,
                            item_box_color,
                            item_box_color,
                            item_box_color
                        );
                        d.add_text([x + th + label_spacing, item.tl[1]], item_text_color, item.label);
                    }
                }
            });
        hovered
    }
}

