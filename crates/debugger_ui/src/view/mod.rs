// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use mwx::KeyName;
use mwx::imgui::{
    DrawListMut, DragDropFlags, MouseButton, StyleColor, StyleVar, Ui
};
use debugger_data::DebuggerState;


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod input;
mod scroll_view;
mod plot;


// Exports --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub use input::*;
pub use scroll_view::*;
pub use plot::*;


// Imgui Extensions -----------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct CustomText<'a, 'b> {
    ui: &'b Ui,
    text: &'b str,
    height: f32,
    pos: Option<(&'b DrawListMut<'a>, [f32; 2])>,
    underlined: bool
}

impl<'a: 'b, 'b> CustomText<'a, 'b> {
    pub fn underline(mut self) -> Self {
        self.underlined = true;
        self
    }

    pub fn with_position(mut self, d: &'b DrawListMut<'a>, tl: [f32; 2]) -> Self {
        self.pos = Some((d, tl));
        self
    }

    pub fn height(mut self, h: f32) -> Self {
        self.height = h;
        self
    }

    pub fn build(self) -> bool {
        self.build_with_tooltip(|| {})
    }

    pub fn build_with_tooltip<C: FnMut()>(self, callback: C) -> bool {
        if let Some((d, tl)) = self.pos {
            let clicked = self.draw(d, tl, callback);
            let c = self.ui.style_color(StyleColor::Text);
            d.add_text(tl, c, self.text);
            clicked

        } else {
            let clicked = self.draw(
                &self.ui.get_window_draw_list(),
                self.ui.cursor_screen_pos(),
                callback
            );
            self.ui.text(self.text);
            clicked
        }
    }

    fn draw<C: FnMut()>(&self, d: &DrawListMut, tl: [f32; 2], mut callback: C) -> bool {
        let size = self.ui.calc_text_size(self.text);
        let br = [tl[0] + size[0], tl[1] + size[1] + self.height];
        let hover = self.ui.mouse_inside_rect(tl, br);
        let clicked = if hover {
            let c = self.ui.style_color(StyleColor::TextSelectedBg);
            d.add_rect_filled_multicolor(tl, br, c, c, c, c);
            callback();
            self.ui.is_mouse_clicked(MouseButton::Right)

        } else {
            false
        };
        if self.underlined {
            let c = self.ui.style_color(StyleColor::Header);
            d.add_rect_filled_multicolor([tl[0], tl[1] + size[1]], [br[0], br[1] + 1.0], c, c, c, c);
            let c = self.ui.style_color(StyleColor::TitleBgActive);
            d.add_rect_filled_multicolor([tl[0], tl[1] + size[1] + 1.0], [br[0], br[1] + 2.0], c, c, c, c);
        }
        clicked
    }
}

pub trait Extensions {
    fn borrowed_checkbox(&self, id: &str, value: bool) -> Option<bool>;
    fn button_with_shortcut(&self, window: &mwx::Window<DebuggerState>, text: &str, desc: &str, key: mwx::Key, repeat: bool) -> bool;
    fn text_custom<'a, 'b>(&'a self, text: &'b str) -> CustomText<'a, 'b>;
    fn mouse_inside_rect(&self, tl: [f32; 2], br: [f32; 2]) -> bool;
    fn move_cursor(&self, offset: [f32; 2]);
    fn split(&self, count: i32, id: &str);
    fn next_split(&self);
    fn separate_split(&self);
    fn header<S: AsRef<str>>(&self, title: S, side_title: Option<S>);
    fn sub_header<S: AsRef<str>, C: FnMut()>(&self, s: S, callback: C);
    fn sortable_list<T, C: FnMut(usize, &mut T)>(&self, items: &mut [T], size: Option<[f32; 2]>, item_height: Option<f32>, callback: C);
}

impl Extensions for Ui {
    fn borrowed_checkbox(&self, id: &str, mut value: bool) -> Option<bool> {
        if self.checkbox(id, &mut value) {
            Some(value)

        } else {
            None
        }
    }

    fn button_with_shortcut(&self, window: &mwx::Window<DebuggerState>, text: &str, desc: &str, key: mwx::Key, repeat: bool) -> bool {
        let b = self.button_with_size(text, [32.0, 0.0]);
        let capture = self.io().want_capture_keyboard;
        if self.is_item_hovered() {
            self.tooltip(|| self.text(format!("{}\nShortcut: {}", desc, key.as_name())));
        }
        if b || repeat && window.input.key_pressed_os(key) && !capture {
            true

        } else {
            window.input.key_pressed(key) && !capture
        }
    }

    fn text_custom<'a, 'b>(&'a self, text: &'b str) -> CustomText<'a, 'b> {
        CustomText {
            ui: self,
            text,
            height: 0.0,
            pos: None,
            underlined: false
        }
    }

    fn mouse_inside_rect(&self, tl: [f32; 2], br: [f32; 2]) -> bool {
        let mp = self.io().mouse_pos;
        mp[0] >= tl[0] && mp[1] >= tl[1] && mp[0] < br[0] && mp[1] < br[1]
    }

    fn move_cursor(&self, offset: [f32; 2]) {
        let c = self.cursor_pos();
        self.set_cursor_pos([c[0] + offset[0], c[1] + offset[1]]);
    }

    fn split(&self, count: i32, id: &str) {
        let _p = self.push_style_var(StyleVar::ItemSpacing([0.0, 0.0]));
        self.columns(count, id, false);
    }

    fn next_split(&self) {
        let _p = self.push_style_var(StyleVar::ItemSpacing([0.0, 0.0]));
        self.next_column();
    }

    fn separate_split(&self) {
        let d = self.get_window_draw_list();
        let [mut width, _] = self.content_region_avail();
        let columns = self.column_count();
        if columns > 1 && self.current_column_index() < columns -1 {
            width -= 3.0;
        }

        let bg = self.style_color(StyleColor::TitleBgActive);
        let sc = self.cursor_screen_pos();
        d.add_line([sc[0] + 0.0, sc[1]], [sc[0] + width, sc[1]], bg).build();
        self.move_cursor([0.0, 5.0]);
    }

    fn header<S: AsRef<str>>(&self, title: S, side_title: Option<S>) {
        let draw_list = self.get_window_draw_list();

        // General layout
        let [mut width, _] = self.content_region_avail();
        let [_, height] = self.calc_text_size(&title);

        // Color and Style
        let color = self.style_color(StyleColor::Text);
        let style = self.clone_style();

        // Handle rendering in tight columns
        let columns = self.column_count();
        if columns > 1 && self.current_column_index() < columns -1 {
            width -= 3.0;
        }

        // Primary header
        let [pw, ph] = style.frame_padding;
        let sc = self.cursor_screen_pos();
        let bg = self.style_color(StyleColor::Header);
        let bt = [sc[0] + width, sc[1] + height + ph * 3.0];
        draw_list.add_rect_filled_multicolor(sc, bt, bg, bg, bg, bg);

        // Underlining bar
        let bg = self.style_color(StyleColor::TitleBgActive);
        draw_list.add_rect_filled_multicolor([sc[0], bt[1] - 2.0], bt, bg, bg, bg, bg);

        // Title and Side Title
        draw_list.add_text([sc[0] + pw, sc[1] + ph], color, title);
        if let Some(side_title) = side_title {
            let [tw, _] = self.calc_text_size(&side_title);
            draw_list.add_text([sc[0] + width - pw - tw, sc[1] + ph], color, side_title);
        }

        // Correclty place cursor for next itmes
        self.move_cursor([0.0, height + ph * 3.0 + style.item_spacing[1]]);
    }

    fn sub_header<S: AsRef<str>, C: FnMut()>(&self, s: S, mut callback: C) {
        let style = self.clone_style();
        let [pw, ph] = style.frame_padding;
        {
            let draw_list = self.get_window_draw_list();
            let [width, _] = self.content_region_avail();
            let [_, height] = self.calc_text_size(&s);

            let color = self.style_color(StyleColor::Text);
            let sc = self.cursor_screen_pos();

            let bg = self.style_color(StyleColor::Header);
            let bt = [sc[0] + width, sc[1] + height + ph * 2.0];
            draw_list.add_rect_filled_multicolor([sc[0], bt[1] - 1.0], bt, bg, bg, bg, bg);
            draw_list.add_text([sc[0] + pw, sc[1] + ph], color, s);
            self.move_cursor([0.0, height + ph * 3.0 + style.item_spacing[1]]);
        }
        self.indent_by(pw);
        callback();
        self.unindent_by(pw);
        self.move_cursor([0.0, style.item_spacing[1]]);
    }

    fn sortable_list<T, C: FnMut(usize, &mut T)>(&self, items: &mut [T], size: Option<[f32; 2]>, item_height: Option<f32>, mut callback: C) {
        if items.is_empty() {
            return;
        }

        let style = self.clone_style();
        let [pw, ph] = style.frame_padding;
        let [_, ih] = style.item_spacing;
        let mut size = size.unwrap_or_else(|| self.content_region_avail());
        let bg = self.style_color(StyleColor::Header);
        let bg = [bg[0] * 0.5, bg[1] * 0.5, bg[2] * 0.5, bg[3] * 0.5];
        let handle = self.style_color(StyleColor::Header);
        let handle_active = self.style_color(StyleColor::HeaderActive);
        let item_height = item_height.unwrap_or_else(|| {
            self.text_line_height() + ph * 2.0
        });
        size[1] = size[1].min(items.len() as f32 * (item_height + ph * 0.5) + ih + style.window_padding[1] * 2.0);

        let mut shift = None;
        self.child_window("SORTABLE_LIST").size(size).build(|| {
            let width = self.window_content_region_max()[0];
            for (index, item) in items.iter_mut().enumerate() {
                let id = self.push_id(format!("{}", index));
                let tc = self.cursor_pos();
                let ts = self.cursor_screen_pos();

                // Drag handle
                let draw_list = self.get_window_draw_list();
                let dragging = self.is_mouse_dragging(mwx::imgui::MouseButton::Left);
                if dragging {
                    self.invisible_button("DRAG_BUTTON", [if dragging { width } else { pw * 3.0 }, item_height]);

                } else {
                    self.move_cursor([-pw, 0.0]);
                    self.invisible_button("DRAG_BUTTON", [if dragging { width + pw } else { pw * 4.0 }, item_height]);
                }

                // Drag / Drop logic
                let mut item_dragged = false;

                if let Some(tooltip) = self.drag_drop_source_config("DRAG_SOURCE").flags(DragDropFlags::SOURCE_ALLOW_NULL_ID).begin_payload(index) {
                    self.text("Move list item");
                    item_dragged = true;
                    tooltip.end();
                }
                if let Some(target) = self.drag_drop_target() {
                    if let Some(Ok(source)) = target.accept_payload::<usize, _>("DRAG_SOURCE", DragDropFlags::empty()) {
                        shift = Some((source.data, index));
                    }
                    target.pop();
                }

                // Handle and hover effect rendering
                let c = if item_dragged || self.is_item_hovered() {
                    draw_list.add_rect_filled_multicolor(ts, [ts[0] + width + pw * 2.0, ts[1] + item_height], bg, bg, bg, bg);
                    handle_active

                } else {
                    handle
                };
                if !item_dragged {
                    draw_list.add_rect_filled_multicolor(ts, [ts[0] + pw, ts[1] + item_height], c, c, c, c);
                }

                // Render Item
                self.set_cursor_pos([tc[0] + pw * 2.0, tc[1]]);
                self.disabled(item_dragged, || {
                    callback(index, item);
                });

                self.set_cursor_pos([tc[0], tc[1] + item_height + ih]);
                id.pop();
            }
        });
        if let Some((from, to)) = shift {
            items.swap(from, to);
        }
    }
}

