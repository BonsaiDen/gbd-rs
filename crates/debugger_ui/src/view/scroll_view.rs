// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::time::Instant;
use std::marker::PhantomData;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::BankedAddress;
use analyzer::{LineCache, SymbolProvider};
use mwx::imgui::{DrawListMut, Key, MouseButton, StyleVar};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{
    DebuggerState,
    util::{self, LayoutData},
    view::Extensions
};


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const GOTO_HIGHLIGHT_COLOR: [f32; 4] = [1.0, 0.5, 0.0, 1.0];


// Types ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
type ScrollOffset = (BankedAddress, usize);


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait ScrollViewProvider<T: LineCache> {
    fn cache<'a>(&self, s: &'a DebuggerState) -> &'a T;
    fn highlight_rects(&self, layout: &LayoutData, width: f32, ox: f32, oy: f32, offset: ScrollOffset) -> Vec<([f32; 2], [f32; 2], f32, f32)>;
    fn draw_line(&mut self, s: &mut DebuggerState, ui: &mwx::UI, d: &DrawListMut, layout: &LayoutData, dim: (f32, f32, f32, usize));
    fn draw_overlay(&mut self, _s: &mut DebuggerState, _d: &DrawListMut, _layout: &LayoutData, _offset: (f32, f32), _lines: (usize, usize)) {}
}

pub trait SimpleScrollViewProvider<U> {
    fn draw_line<S: SymbolProvider>(&mut self, ui: &mwx::UI, provider: &S, layout: &LayoutData, size: [f32; 2], index: usize, item: &U);
    fn context_menu(&self, ui: &mwx::UI, item: &U);
}


// Helpers --------------------------------------------------------------------
// ----------------------------------------------------------------------------
fn did_user_scroll(ui: &mwx::UI) -> bool {
    ui.io().mouse_wheel != 0.0 || ui.mouse_drag_delta_with_button(MouseButton::Left)[1].abs() > 0.0
}


// ScrollView with LineCache --------------------------------------------------
// ----------------------------------------------------------------------------
pub struct ScrollView<T: LineCache> {
    line_indicies_gen: usize,
    view_line: Option<usize>,
    anchor_line: usize,
    anchor_offset: Option<ScrollOffset>,
    target_offset: Option<(usize, Option<ScrollOffset>, bool, bool, bool)>,
    follow_offset: Option<ScrollOffset>,
    follow_offset_last: Option<ScrollOffset>,
    highlight_timer: Instant,
    highlight: Option<(usize, ScrollOffset)>,
    t: PhantomData<T>
}

impl<T: LineCache> ScrollView<T> {
    pub fn new() -> Self {
        Self {
            line_indicies_gen: 0,
            view_line: None,
            anchor_line: 0,
            anchor_offset: None,
            highlight_timer: Instant::now(),
            highlight: None,
            target_offset: None,
            follow_offset: None,
            follow_offset_last: None,
            t: PhantomData
        }
    }

    pub fn set_follow_offset(&mut self, cache: &T, offset: Option<ScrollOffset>) {
        if let Some(offset) = offset {
            self.follow_offset = Some(offset);
            if self.follow_offset != self.follow_offset_last {
                let line = cache.map_line_index(offset);
                self.target_offset = Some((line, None, true, true, false));
                self.follow_offset_last = Some(offset);
            }

        } else {
            self.follow_offset_last = None;
            self.follow_offset = None;
        }
    }

    pub fn scroll_to_with_highlight(&mut self, cache: &T, offset: ScrollOffset, anchor: bool) {
        let line = cache.map_line_index(offset);
        if Some(line) != self.view_line {
            self.target_offset = Some((line, Some(offset), true, anchor, false));
        }
    }

    pub fn return_to_anchor(&mut self) {
        self.highlight = None;
        if Some(self.anchor_line) != self.view_line {
            self.target_offset = Some((self.anchor_line, None, false, false, false));
        }
    }

    pub fn anchor(&mut self) {
        self.anchor_line = self.view_line.unwrap_or(0);
    }

    pub fn update(&mut self, cache: &mut T, offset: ScrollOffset) {
        // Keep scrolling stable in case the content of the view has changed
        let gen = cache.line_indicies_gen();
        if gen != self.line_indicies_gen {
            if self.follow_offset.is_none() {
                // Fallback to the provided offset so we use it as the initial scroll offset
                let line = cache.map_line_index(self.anchor_offset.unwrap_or(offset));
                self.target_offset = Some((line, None, false, true, true));

            } else {
                self.follow_offset_last = None;
            }
            self.line_indicies_gen = gen;

        } else {
            self.anchor_offset = Some(offset);
        }
    }

    pub fn draw<P: ScrollViewProvider<T>>(
        &mut self,
        s: &mut DebuggerState,
        ui: &mwx::UI,
        provider: &mut P

    ) -> (Option<ScrollOffset>, Option<ScrollOffset>) {
        let layout = LayoutData::new(ui);
        let avail = ui.content_region_avail();
        let view_line_count = ((avail[1] - 6.0) / layout.height_unit).floor() as usize;
        let max_line_count = provider.cache(s).line_count();
        if self.handle_scroll( ui, view_line_count, max_line_count) {
            self.follow_offset = None;
        }

        let mut view_line = 0;
        ui.child_window("SCROLL_VIEW_LINE").border(true).size([avail[0], view_line_count as f32 * layout.height_unit + 6.0]).build(|| {
            // Force window to have the full "virtual" height of the view contents
            let [screen_x, screen_y] = ui.cursor_screen_pos();
            ui.set_cursor_pos([0.0, max_line_count as f32 * layout.height_unit]);

            // Compute current view line
            let view_y = ui.scroll_y();
            view_line = (view_y / layout.height_unit).floor() as usize;

            // Update scrolling with new target
            if let Some((target_line, highlight_offset, should_offset, should_anchor, is_scroll)) = self.target_offset.take() {
                // Setup highlight and disable following of a desired offset
                if let Some(highlight_offset) = highlight_offset {
                    self.highlight_timer = Instant::now();
                    self.highlight = Some((target_line, highlight_offset));
                    self.follow_offset = None;
                }

                // Only jump if the line is not already visible
                if target_line < view_line || target_line >= view_line + view_line_count || is_scroll {
                    // Offet the target if desired
                    let target_line = if should_offset { target_line.saturating_sub(4) } else { target_line };
                    let target_line = target_line.min(max_line_count.saturating_sub(view_line_count));
                    ui.set_scroll_y(target_line as f32 * layout.height_unit);

                    // Override line to avoid visual jumping
                    view_line = target_line;
                }

                // Set Anchor
                if should_anchor {
                    self.anchor_line = view_line;
                }
            }

            // Move anchor with scroll wheel or scrollbar is dragged
            if did_user_scroll(ui) {
                self.anchor_line = view_line;
            }

            // Draw lines
            let d = ui.get_window_draw_list();
            let width = ui.window_content_region_max()[0] + layout.width_unit * 2.0;
            let ox = screen_x + 0.0;
            for l in 0..view_line_count {
                let line_index = l + view_line;
                if line_index < max_line_count {
                    let oy = screen_y - 3.0 + l as f32 * layout.height_unit + view_y;

                    // Highlight line
                    if let Some((line, offset)) = self.highlight {
                        if line == line_index {
                            for (tl, br, speed, alpha) in provider.highlight_rects(&layout, width, ox, oy, offset) {
                                if let Some(t) = util::highlight_factor(&self.highlight_timer, speed) {
                                    let mut c = GOTO_HIGHLIGHT_COLOR;
                                    c[3] = alpha * t;
                                    d.add_rect_filled_multicolor(tl, br, c, c, c, c);

                                } else {
                                    self.highlight = None;
                                }
                            }
                        }
                    }
                    provider.draw_line(s, ui, &d, &layout, (ox, oy, width, line_index))
                }
            }

            // Render View Overlay
            provider.draw_overlay(
                s,
                &d,
                &layout,
                (ox, screen_y - 3.0 + view_y),
                (view_line, (view_line + view_line_count).min(max_line_count))
            );
        });
        self.view_line = Some(view_line);
        (provider.cache(s).get_line_addr_opt(view_line), self.follow_offset)
    }

    fn handle_scroll(&mut self, ui: &mwx::UI, line_count: usize, max_lines: usize) -> bool {
        if ui.io().want_text_input {
            false

        } else if ui.io().mouse_wheel != 0.0 {
            true

        } else if ui.is_key_pressed(Key::DownArrow) {
            let s = if ui.io().key_shift { 4 } else { 1 };
            self.target_offset = Some((self.view_line.unwrap_or(0) + s, None, false, true, true));
            true

        } else if ui.is_key_pressed(Key::UpArrow) {
            let s = if ui.io().key_shift { 4 } else { 1 };
            self.target_offset = Some((self.view_line.unwrap_or(0).saturating_sub(s), None, false, true, true));
            true

        } else if ui.is_key_pressed(Key::PageDown) {
            self.target_offset = Some((self.view_line.unwrap_or(0) + line_count, None, false, true, true));
            true

        } else if ui.is_key_pressed(Key::PageUp) {
            self.target_offset = Some((self.view_line.unwrap_or(0).saturating_sub(line_count), None, false, true, true));
            true

        } else if ui.is_key_pressed(Key::End) {
            self.target_offset = Some((max_lines.saturating_sub(line_count), None, false, true, true));
            true

        } else if ui.is_key_pressed(Key::Home) {
            self.target_offset = Some((0, None, false, true, true));
            true

        } else {
            false
        }
    }
}

// ScrollView for UI Components -----------------------------------------------
// ----------------------------------------------------------------------------
pub struct SimpleScrollView {
    highlight_timer: Instant,
    highlight: Option<usize>,
    scroll_line_view: usize,
    scroll_line_last: Option<usize>,
    scroll_line_anchor: usize,
    scroll_line_target: Option<(usize, bool, bool, bool)>,
}

impl SimpleScrollView {
    pub fn new() -> Self {
        Self {
            highlight_timer: Instant::now(),
            highlight: None,
            scroll_line_view: 0,
            scroll_line_last: None,
            scroll_line_anchor: 0,
            scroll_line_target: None,
        }
    }

    pub fn update(&mut self, base_line: usize) {
        if self.scroll_line_last.is_none() && self.scroll_line_target.is_none() {
            self.scroll_line_target = Some((base_line, false, false, true));
        }
    }

    pub fn anchor(&mut self) {
        self.scroll_line_anchor = self.scroll_line_view;
    }

    pub fn return_to_anchor(&mut self) {
        self.scroll_line_target = Some((self.scroll_line_anchor, false, false, true));
        self.highlight = None;
    }

    pub fn scroll_to_with_highlight(&mut self, target_line: usize, anchor: bool) {
        self.scroll_line_target = Some((target_line, true, true, anchor));
    }

    pub fn draw<S: SymbolProvider, T: SimpleScrollViewProvider<U>, U>(
        &mut self,
        ui: &mwx::UI,
        items: &[U],
        symbol_provider: &S,
        provider: &mut T

    ) -> usize {
        let layout = LayoutData::new(ui);
        let avail = ui.content_region_avail();
        let item_height = layout.height_unit + 2.0;
        let max_line_count = items.len();
        let view_line_count = ((avail[1] - 6.0) / item_height).floor() as usize;
        ui.child_window("SCROLL_VIEW_SIMPLE").size([avail[0], view_line_count as f32 * item_height + 6.0]).build(|| {
            // Force window to have the full "virtual" height of the view contents
            let [screen_x, screen_y] = ui.cursor_screen_pos();
            ui.set_cursor_pos([0.0, (max_line_count + 1) as f32 * item_height]);

            // Compute current view line
            let view_y = ui.scroll_y();
            self.scroll_line_view = (view_y / item_height).floor() as usize;

            // Handle force scroll override
            if let Some((target_line, highlight, should_offset, should_anchor)) = self.scroll_line_target.take() {
                if highlight {
                    self.highlight_timer = Instant::now();
                    self.highlight = Some(target_line);
                }
                if target_line < self.scroll_line_view || target_line >= self.scroll_line_view + view_line_count {
                    let target_line = if should_offset { target_line.saturating_sub(4) } else { target_line };
                    let target_line = target_line.min(max_line_count.saturating_sub(view_line_count));
                    ui.set_scroll_y(target_line as f32 * item_height);
                    self.scroll_line_view = target_line;
                }
                if should_anchor {
                    self.scroll_line_anchor = self.scroll_line_view;
                }
            }

            // Move anchor with scroll wheel
            if did_user_scroll(ui) {
                self.scroll_line_anchor = self.scroll_line_view;
            }

            let width = ui.window_content_region_max()[0];
            let ox = screen_x;
            for l in 0..view_line_count {
                let line_index = l + self.scroll_line_view;
                if line_index < max_line_count {
                    let oy = screen_y + l as f32 * item_height + view_y;
                    ui.set_cursor_screen_pos([ox, oy]);
                    let mut line_alpha = 1.0;
                    if self.highlight == Some(line_index) {
                        let tl = ui.cursor_screen_pos();
                        if let Some(t) = util::highlight_factor(&self.highlight_timer, 5.0) {
                            let br = [tl[0] + width, tl[1] + 19.0];
                            let d = ui.get_window_draw_list();
                            let mut c = GOTO_HIGHLIGHT_COLOR;
                            line_alpha = 0.5;
                            c[3] = 0.5 * t;
                            d.add_rect_filled_multicolor(tl, br, c, c, c, c);

                        } else {
                            self.highlight = None;
                        }
                    }

                    let tl = ui.cursor_screen_pos();
                    let br = [tl[0] + width - 24.0, tl[1] + item_height];
                    if ui.mouse_inside_rect(tl, br) && ui.is_mouse_clicked(MouseButton::Right) {
                        provider.context_menu(ui, &items[line_index]);
                    }

                    let _a = ui.push_style_var(StyleVar::Alpha(line_alpha));
                    provider.draw_line(ui, symbol_provider, &layout, [width, item_height], line_index, &items[line_index]);
                }
            }
        });
        self.scroll_line_last = Some(self.scroll_line_view);
        self.scroll_line_view
    }
}

