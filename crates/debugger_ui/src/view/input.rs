// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::cmp::Ordering;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use analyzer::{Symbol, SymbolProvider};
use sameboy_rs::BankedAddress;
use mwx::imgui::InputTextFlags;
use mwx::imgui::{self, InputTextCallback, InputTextCallbackHandler, StyleColor};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::util::LayoutData;
use crate::view::Extensions;


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct EditableInputHelper {
    edit_next: Option<(usize, u8)>,
    edit_index: Option<(usize, u8)>,
    edit_value: String,
    edit_focus: bool,
    edit_input: AutoCompleteTextInput,
}

impl EditableInputHelper {
    pub fn new() -> Self {
        Self {
            edit_next: None,
            edit_index: None,
            edit_focus: false,
            edit_value: String::with_capacity(128),
            edit_input: AutoCompleteTextInput::new(),
        }
    }

    pub fn edit_value(&mut self, index: usize, typ: u8, value: String) {
        self.edit_focus = true;
        self.edit_index = Some((index, typ));
        self.edit_value = value;
    }

    pub fn start_editing(&mut self, index: usize) {
        self.edit_next = Some((index, 0));
    }

    pub fn stop_editing(&mut self, index: usize) {
        if self.edit_index.map(|(i, _)| i) == Some(index) {
            self.edit_index = None;
        }
    }

    #[allow(clippy::too_many_arguments)]
    pub fn address_input<
        T: SymbolProvider,
        L: Fn() -> String,
        V: Fn() -> String,
        F: Fn(&Symbol) -> bool
    >(
        &mut self,
        ui: &mwx::UI,
        index: usize,
        typ: u8,
        width: f32,
        (provider, addr): (&T, BankedAddress),
        format_label: L,
        format_value: V,
        symbol_filter: F,
        disabled: bool

    ) -> Option<String> {
        if self.edit_index == Some((index, typ)) {
            let _w = ui.push_item_width(width);
            if self.edit_focus {
                ui.set_keyboard_focus_here();
                self.edit_focus = false;
            }
            if ui.is_key_released(mwx::imgui::Key::Escape) {
                self.edit_index = None;
            }
            self.edit_input.build(ui, true, provider, &symbol_filter, false);

            // Stop editing if clicking outside
            let (tl, br) = (ui.item_rect_min(), ui.item_rect_max());
            if ui.is_any_mouse_down() && !ui.mouse_inside_rect(tl, br) {
                self.edit_index = None;
            }

            if ui.is_key_released(mwx::imgui::Key::Enter) {
                self.edit_index = None;
                let value = self.edit_input.value();
                // Match and replace symbol names with their address
                if let Some(symbol) = provider.resolve_symbol_by_name(value) {
                    if symbol_filter(symbol) {
                        return Some(symbol.address().to_label_string());
                    }
                }
                Some(value.to_string())

            } else {
                None
            }

        } else {
            let label = format!("{}({})", format_label(), provider.resolve_symbol_name_relative(addr));
            let label = format!("{}##editable_input_button_{}", label, typ);
            ui.disabled(disabled, || {
                if ui.button_with_size(label, [width, 0.0]) || self.edit_next == Some((index, typ)) {
                    self.edit_focus = true;
                    self.edit_input.set_value(&format_value());
                    self.edit_index = Some((index, typ));
                    self.edit_next = None;
                }
            });
            None
        }
    }

    pub fn string_input(
        &mut self,
        ui: &mwx::UI,
        index: usize,
        typ: u8,
        width: f32,
        label: &str,
        disabled: bool

    ) -> Option<String> {
        if self.edit_index == Some((index, typ)) {
            let _w = ui.push_item_width(width);
            let mut was_focused = false;
            if self.edit_focus {
                ui.set_keyboard_focus_here();
                self.edit_focus = false;
                was_focused = true;
            }
            if ui.is_key_released(mwx::imgui::Key::Escape) {
                self.edit_index = None;
            }

            ui.input_text("##editable_input", &mut self.edit_value).auto_select_all(true).build();

            // Stop editing if clicking outside
            let (tl, br) = (ui.item_rect_min(), ui.item_rect_max());
            if ui.is_any_mouse_down() && !ui.mouse_inside_rect(tl, br) && !was_focused {
                self.edit_index = None;
            }

            if ui.is_key_released(mwx::imgui::Key::Enter) {
                self.edit_index = None;
                Some(self.edit_value.clone())

            } else {
                None
            }

        } else {
            ui.disabled(disabled, || {
                if ui.button_with_size(format!("{}##editable_input_button_{}", label, typ), [width, 0.0]) {
                    self.edit_focus = true;
                    self.edit_value = label.to_string();
                    self.edit_index = Some((index, typ));
                }
            });
            None
        }
    }
}

struct CompletionFilter;
impl InputTextCallbackHandler for CompletionFilter {
    fn char_filter(&mut self, c: char) -> Option<char> {
        match c {
            '0'..='9' | 'A'..='Z' | 'a'..='z' | ':' | '-' | '_' => Some(c),
            _ => None
        }
    }
}

struct HexNumberFilter {
    width: usize
}

impl InputTextCallbackHandler for HexNumberFilter {
    fn char_filter(&mut self, c: char) -> Option<char> {
        match c {
            '0'..='9' | 'A'..='F' | 'a'..='f' => Some(c),
            _ => None
        }
    }

    fn on_always(&mut self, mut d: imgui::TextCallbackData) {
        let size = d.str().len();
        if size > self.width {
            d.remove_chars(self.width, size.saturating_sub(self.width));
        }
    }
}

pub enum HexBuildResult {
    None,
    ContextMenu,
    Value(u16),
}

pub enum HexByteResult {
    Value(u16),
    Closed
}

pub struct HexByteInput {
    id: String,
    width: usize,
    value: String,
    editing: bool,
    focus: bool
}

impl HexByteInput {
    pub fn new(id: &str, width: usize) -> Self {
        Self {
            id: format!("##HEX_BYTE_INPUT_{}_{}", width, id),
            width,
            value: String::with_capacity(width),
            editing: false,
            focus: false
        }
    }

    pub fn set_value(&mut self, value: u16) {
        self.value = format!("{:0>width$X}", value, width=self.width);
    }

    pub fn build(&mut self, ui: &imgui::Ui, value: u16) -> HexBuildResult {
        if self.editing {
            let _p = ui.push_style_var(imgui::StyleVar::FramePadding([4.0, 0.0]));
            ui.move_cursor([-4.0, 0.0]);
            match self.build_inner(ui) {
                Some(HexByteResult::Value(v)) => {
                    self.editing = false;
                    HexBuildResult::Value(v)
                },
                Some(HexByteResult::Closed) => {
                    self.editing = false;
                    HexBuildResult::None
                },
                _ => HexBuildResult::None
            }

        } else {
            let text = format!("{:0>width$X}", value, width=self.width);
            let size = ui.calc_text_size(&text);
            let tl = ui.cursor_pos();
            let br = [tl[0] + size[0], tl[1] + size[1]];
            let mut result = HexBuildResult::None;
            if ui.mouse_inside_rect(tl, br) {
                let d = ui.get_window_draw_list();
                let c = ui.style_color(StyleColor::TextSelectedBg);
                d.add_rect_filled_multicolor(tl, br, c, c, c, c);
                ui.tooltip(|| ui.text("Click to edit"));
                if ui.is_mouse_clicked(imgui::MouseButton::Left) {
                    self.set_value(value);
                    self.focus = true;
                    self.editing = true;
                }
                if ui.is_mouse_clicked(imgui::MouseButton::Right) {
                    result = HexBuildResult::ContextMenu;
                }
            }
            ui.text(text);
            result
        }
    }

    pub fn build_edit(&mut self, ui: &imgui::Ui) -> Option<HexByteResult> {
        let _p = ui.push_style_var(imgui::StyleVar::FramePadding([4.0, 2.0]));
        self.build_inner(ui)
    }

    fn build_inner(&mut self, ui: &imgui::Ui) -> Option<HexByteResult> {
        let _item_width = ui.push_item_width(ui.calc_text_size("0")[0] * self.width as f32 + 7.0);
        if self.focus {
            ui.set_keyboard_focus_here();
            self.focus = false;
        }
        ui.input_text(&self.id, &mut self.value)
            .auto_select_all(true)
            .no_horizontal_scroll(true)
            .always_insert_mode(true)
            .callback(InputTextCallback::CHAR_FILTER | InputTextCallback::ALWAYS, HexNumberFilter {
                width: self.width
            })
            .build();

        let (tl, br) = (ui.item_rect_min(), ui.item_rect_max());
        let focus = ui.is_item_focused();
        if ui.is_any_mouse_down() && !ui.mouse_inside_rect(tl, br) || focus && ui.is_key_released(imgui::Key::Escape) {
            Some(HexByteResult::Closed)

        } else if focus && ui.is_key_released(imgui::Key::Enter) {
            if let Ok(v) = u16::from_str_radix(&self.value, 16) {
                Some(HexByteResult::Value(v))

            } else {
                Some(HexByteResult::Closed)
            }

        } else {
            None
        }
    }
}

pub struct AutoCompleteTextInput {
    value: String,
    last_value: String,
    placeholder: String,
    matches: Vec<String>,
    match_index: usize
}

impl AutoCompleteTextInput {
    pub fn new() -> Self {
        Self {
            value: String::with_capacity(64),
            last_value: String::new(),
            placeholder: String::new(),
            matches: Vec::new(),
            match_index: 0
        }
    }

    pub fn clear(&mut self) {
        self.value.clear();
        self.last_value = self.value.clone();
        self.placeholder.clear();
    }

    pub fn set_value(&mut self, value: &str) {
        self.value.clear();
        self.value.push_str(value);
        self.last_value = self.value.clone();
        self.placeholder.clear();
    }

    pub fn value(&self) -> &str {
        &self.value
    }

    pub fn placeholder(&self) -> &str {
        &self.placeholder
    }

    pub fn build<S: SymbolProvider, F: Fn(&Symbol) -> bool>(
        &mut self,
        ui: &imgui::Ui,
        auto_select_all: bool,
        provider: &S,
        symbol_filter: F,
        fill: bool

    ) -> bool {
        let before = ui.cursor_pos();
        let before_screen = ui.cursor_screen_pos();

        // Build underlying imgui text input
        let mut flags = InputTextFlags::empty();
        if auto_select_all {
            flags |= InputTextFlags::AUTO_SELECT_ALL;
        }
        let _item_width = if fill {
            Some(ui.push_item_width(ui.content_region_avail()[0]))

        } else {
            None
        };
        let input = ui.input_text("##auto_complete_input", &mut self.value)
            .flags(flags)
            .callback(InputTextCallback::CHAR_FILTER, CompletionFilter);

        let mut changed = false;
        if input.build() && self.value != self.last_value {
            self.last_value = self.value.clone();

            let value = self.value.to_lowercase();
            let mut ranking: Vec<(usize, String)> = provider
                .dynamic_symbols()
                .into_iter()
                .filter(|s| symbol_filter(s))
                .map(|s| s.name().to_string())
                .filter_map(|name| {
                    let l = name.to_lowercase();
                    if l.starts_with(&value) {
                        Some((l.chars().zip(value.chars()).take_while(|(a, b)| a == b).count(), name))

                    } else {
                        None
                    }

                }).filter(|(s, _)| *s > 0).collect();

            ranking.sort_by(|(a_score, a_label), (b_score, b_label)| {
                match a_score.cmp(b_score) {
                    Ordering::Equal => {
                        b_label.len().cmp(&a_label.len())
                    },
                    c => c
                }
            });

            if ranking.is_empty() {
                self.matches.clear();
                self.placeholder.clear();

            } else {
                self.match_index = 0;
                self.matches = ranking.into_iter().rev().take(10).map(|(_, s)| s).collect();
                self.placeholder = self.matches[0].clone();
            }
            changed = true;
        }

        // Hide auto complete if no longer focussed
        if !ui.is_item_clicked() && ui.is_mouse_clicked(imgui::MouseButton::Left) && !self.placeholder.is_empty() {
            self.placeholder.clear();
            changed = true;
        }
        if !ui.is_item_focused() || ui.is_item_focused() && ui.is_key_released(imgui::Key::Escape) {
            self.placeholder.clear();
        }

        if ui.is_item_focused() && ui.is_key_released(imgui::Key::Enter) {
            changed = true;
        }

        let input_size = ui.item_rect_size();
        if !self.placeholder.is_empty() {
            let mut color = ui.style_color(StyleColor::Text);
            color[0] *= 0.5;
            color[1] *= 0.5;
            color[2] *= 0.5;
            {
                let after = ui.cursor_pos();
                let _c = ui.push_style_color(StyleColor::Text, color);
                let overlay = format!("{: >width$}{}", "", &self.placeholder[self.value.len().min(self.placeholder.len())..], width=self.value.len());
                ui.set_cursor_pos([before[0] + 4.0, before[1] + 3.0]);
                ui.text(overlay);
                ui.same_line();
                ui.move_cursor([-4.0, -3.0]);
                ui.text("");
                ui.set_cursor_pos(after);
            }
            {
                let layout = LayoutData::new(ui);
                let d = ui.get_foreground_draw_list();
                let bg = layout.color_bg;
                let sel = layout.color_text_selected;
                let tl = [before_screen[0], before_screen[1] + input_size[1]];
                let br = [
                    before_screen[0] + input_size[0],
                    before_screen[1] + input_size[1] + layout.height_unit * self.matches.len() as f32 + 4.0
                ];
                d.add_rect_filled_multicolor(tl, br, bg, bg, bg, bg);
                d.add_rect(tl, br, layout.color_border).build();
                let mut p = [tl[0] + 3.0, tl[1] + 3.0];
                for (i, m) in self.matches.iter().enumerate() {
                    if self.match_index == i {
                        d.add_rect_filled_multicolor(
                            p,
                            [br[0] - 3.0, p[1] + layout.height_unit - 2.0],
                            sel, sel, sel, sel
                        );
                    }
                    d.add_text(p, layout.color_text, m);
                    p[1] += layout.height_unit;
                }
                let shift = ui.io().key_shift;
                if (ui.is_key_pressed(imgui::Key::Tab) && !shift) || ui.is_key_pressed(imgui::Key::DownArrow) {
                    self.match_index = (self.match_index + 1) % self.matches.len();
                    self.placeholder = self.matches[self.match_index].clone();
                    changed = true;

                }
                if (ui.is_key_pressed(imgui::Key::Tab) && shift) || ui.is_key_pressed(imgui::Key::UpArrow){
                    self.match_index = (self.match_index + self.matches.len()).saturating_sub(1) % self.matches.len();
                    self.placeholder = self.matches[self.match_index].clone();
                    changed = true;
                }
                if ui.is_key_released(mwx::imgui::Key::Enter) {
                    self.value.clear();
                    self.value.push_str(&self.matches[self.match_index]);
                    self.placeholder.clear();
                    changed = true;
                }
            }
        }
        changed
    }
}

