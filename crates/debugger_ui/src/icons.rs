// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use mwx::icons;


// Icons ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub const GRAPH: &str = icons::NF_FA_AREA_CHART;
pub const SEARCH: &str = icons::NF_OCT_SEARCH;
pub const FILTER: &str = icons::NF_MDI_FILTER;
pub const DELETE_ENTRY: &str = icons::NF_OCT_X;
pub const CLEAR_ENTRIES: &str = icons::NF_MDI_BOOKMARK_REMOVE;
pub const LOG_INFO: &str = icons::NF_FA_INFO_CIRCLE;
pub const LOG_WARN: &str = icons::NF_FA_WARNING;
pub const LOG_ERROR: &str = icons::NF_FA_EXCLAMATION_CIRCLE;
pub const RESUME_EMULATION: &str = icons::NF_MDI_PLAY;
pub const PAUSE_EMULATION: &str = icons::NF_MDI_PAUSE;
pub const STEP_INTO: &str = icons::NF_MDI_DEBUG_STEP_INTO;
pub const STEP_OVER: &str = icons::NF_MDI_DEBUG_STEP_OVER;
pub const STEP_OUT: &str = icons::NF_MDI_DEBUG_STEP_OUT;
pub const ADVANCE_FRAME: &str = icons::NF_MDI_ARRANGE_BRING_FORWARD;
pub const ADVANCE_LINE: &str = icons::NF_MDI_PLAYLIST_PLUS;
pub const RESET_EMULATION: &str = icons::NF_MDI_REDO_VARIANT;
pub const RELOAD_ROM: &str = icons::NF_MDI_RELOAD;
pub const LOAD_ROM: &str = icons::NF_MDI_FOLDER_OPEN;
pub const HALT_ACTIVE: &str = icons::NF_OCT_STOP;
pub const HALT_INACTIVE: &str = icons::NF_MDI_OCTAGON_OUTLINE;
pub const SOUND_MUTED: &str = icons::NF_MDI_VOLUME_OFF;
pub const SOUND_UNMUTED: &str = icons::NF_MDI_VOLUME_HIGH;

pub fn used_icons_list<'a>() -> &'a [&'a str] {
    &[
        GRAPH,
        SEARCH,
        FILTER,
        DELETE_ENTRY,
        CLEAR_ENTRIES,
        LOG_INFO,
        LOG_WARN,
        LOG_ERROR,
        RESUME_EMULATION,
        PAUSE_EMULATION,
        STEP_INTO,
        STEP_OVER,
        STEP_OUT,
        ADVANCE_FRAME,
        ADVANCE_LINE,
        RESET_EMULATION,
        RELOAD_ROM,
        LOAD_ROM,
        HALT_ACTIVE,
        HALT_INACTIVE,
        SOUND_MUTED,
        SOUND_UNMUTED
    ]
}

