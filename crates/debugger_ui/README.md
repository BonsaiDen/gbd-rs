# Debugger UI

The Debugger UI for controlling and displaying the information of the underlying debugger logic.

Written using [imgui-rs](https://github.com/imgui-rs/imgui-rs/).

