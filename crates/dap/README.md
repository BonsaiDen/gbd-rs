# Debug Adapter Protocol

A simple crate which implements a stdio based version of the [Debug Adapter Protocol](https://microsoft.github.io/debug-adapter-protocol/).

It provides traits and other support functions to simplify the implementation of the actual protocol logic.

