// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::RunnableCore;


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod debuggee;
mod protocol;


// Re-Exports -----------------------------------------------------------------
// ----------------------------------------------------------------------------
pub use debuggee::*;
pub use protocol::*;


// Handler --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct DapHandler;
impl DapHandler {
    pub fn before_frame<T: Debuggee>(dap: &mut DapProtocol, d: &mut T, core: &mut RunnableCore) {
        dap.handle(|req| {
            match req.command.clone() {
                DapCommand::Initialize { lines_start_at_1, columns_start_at_1, .. } => {
                    req.respond(Ok(DapResponse::Initialize {
                        supports_restart_request: true,
                        //supports_write_memory_request: true,
                        //supports_read_memory_request: true,
                        //supports_disassemble_request: true,
                        supports_conditional_breakpoints: true,
                        supports_hit_conditional_breakpoints: true,
                        supports_set_variable: true,
                        supports_completions_request: true
                    }));
                    req.event(DapEvent::Initialized {});

                    // Our source map locations already are 1 index, so for
                    // clients that require 0 indexing we need to subtract these
                    if !lines_start_at_1 {
                        d.set_line_offset(1);
                    }
                    if !columns_start_at_1 {
                        d.set_column_offset(1);
                    }
                },
                DapCommand::Launch { .. } => {
                    d.start();
                    req.respond(Ok(DapResponse::Launch {}));
                },
                DapCommand::Completions { text, column } => {
                    let targets = d.completions(text, column);
                    req.respond(Ok(DapResponse::Completions {
                        targets
                    }));
                },
                DapCommand::Evaluate { expression, .. } => {
                    let result = d.evaluate(core, expression);
                    req.respond(Ok(DapResponse::Evaluate {
                        result,
                        variables_reference: 0
                    }));
                },
                DapCommand::Restart {  } => {
                    req.respond(Ok(DapResponse::Restart {}));
                    d.restart();
                },
                DapCommand::Pause { thread_id } => {
                    req.respond(Ok(DapResponse::Pause {}));
                    d.pause(thread_id);
                },
                DapCommand::StepIn { thread_id } => {
                    req.respond(Ok(DapResponse::StepIn {}));
                    d.step_in(thread_id);
                },
                DapCommand::StepOut { thread_id } => {
                    req.respond(Ok(DapResponse::StepOut {}));
                    d.step_out(thread_id);
                },
                DapCommand::Next { thread_id } => {
                    req.respond(Ok(DapResponse::Next {}));
                    d.next(thread_id);
                },
                DapCommand::Continue { thread_id } => {
                    req.respond(Ok(DapResponse::Continue {}));
                    d.resume(thread_id);
                },
                DapCommand::SetVariable { variables_reference, name, value } => {
                    req.respond(Ok(DapResponse::SetVariable {
                        value: d.set_variable(core, variables_reference, name, value)
                    }));
                },
                DapCommand::SetBreakpoints { source, breakpoints, .. } => {
                    req.respond(Ok(DapResponse::SetBreakpoints {
                        breakpoints: d.set_breakpoints(source, breakpoints)
                    }));
                },
                DapCommand::Threads => {
                    req.respond(Ok(DapResponse::Threads {
                        threads: d.threads()
                    }));
                },
                DapCommand::StackTrace { thread_id } => {
                    req.respond(Ok(DapResponse::StackTrace {
                        stack_frames: d.stack_frames(core, thread_id)
                    }));
                },
                DapCommand::Scopes { frame_id } => {
                    req.respond(Ok(DapResponse::Scopes {
                        scopes: d.scopes(frame_id)
                    }));
                },
                DapCommand::Variables { variables_reference } => {
                    req.respond(Ok(DapResponse::Variables {
                        variables: d.variables(core, variables_reference)
                    }));
                },
                DapCommand::Disconnect { terminate_debuggee, .. }=> {
                    if terminate_debuggee {
                        d.terminate();
                        req.event(DapEvent::Exited {
                            exit_code: 0
                        });
                    }
                    req.respond(Ok(DapResponse::Disconnect {}));
                }
            }
        });
    }

    pub fn after_frame<T: Debuggee>(dap: &mut DapProtocol, d: &mut T) {
        if d.was_started() {
            for t in d.threads() {
                dap.event(DapEvent::Thread {
                    reason: "started".to_string(),
                    thread_id: t.id
                });
            }
        }

        if d.was_paused() {
            for t in d.threads() {
                dap.event(DapEvent::Stopped {
                    reason: "pause".to_string(),
                    description: "Paused".to_string(),
                    thread_id: t.id,
                    hit_breakpoint_ids: Vec::new()
                });
            }
        }
        if d.was_resumed() {
            for t in d.threads() {
                dap.event(DapEvent::Continued {
                    thread_id: t.id
                });
            }
        }

        if let Some((reason, bid)) = d.has_stopped() {
            for t in d.threads() {
                dap.event(DapEvent::Stopped {
                    reason: reason.clone(),
                    description: "Stopped".to_string(),
                    thread_id: t.id,
                    hit_breakpoint_ids: bid.map(|id| {
                        vec![id]

                    }).unwrap_or_else(Vec::new)
                });
            }
        }

        for e in d.events() {
            dap.event(e);
        }
    }
}

