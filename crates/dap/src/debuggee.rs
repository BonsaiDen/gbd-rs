// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::RunnableCore;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use super::protocol::{
    DapBreakpoint, DapThread, DapScope, DapSource, DapStackFrame, DapVariable, DapSourceBreakpoint,
    DapEvent, DapCompletionItem
};


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait Debuggee {
    fn set_line_offset(&mut self, offset: usize);
    fn set_column_offset(&mut self, offset: usize);

    fn was_started(&mut self) -> bool;
    fn was_paused(&mut self) -> bool;
    fn was_resumed(&mut self) -> bool;
    fn has_stopped(&mut self) -> Option<(String, Option<usize>)>;

    fn start(&mut self);
    fn restart(&mut self);
    fn step_in(&mut self, thread_id: u8);
    fn step_out(&mut self, thread_id: u8);
    fn next(&mut self, thread_id: u8);
    fn pause(&mut self, thread_id: u8);
    fn resume(&mut self, thread_id: u8);
    fn terminate(&mut self);

    fn completions(&self, text: String, column: usize) -> Vec<DapCompletionItem>;
    fn evaluate(&mut self, core: &mut RunnableCore, expression: String) -> String;

    fn events(&mut self) -> Vec<DapEvent>;

    fn set_breakpoints(&mut self, source: DapSource, breakpoints: Vec<DapSourceBreakpoint>) -> Vec<DapBreakpoint>;
    fn set_variable(&mut self, core: &mut RunnableCore, var_id: usize, name: String, value: String) -> String;

    fn threads(&self) -> Vec<DapThread>;
    fn stack_frames(&self, core: &mut RunnableCore, thread_id: u8) -> Vec<DapStackFrame>;
    fn scopes(&self, frame_id: usize) -> Vec<DapScope>;
    fn variables(&self, core: &mut RunnableCore, var_id: usize) -> Vec<DapVariable>;
}

