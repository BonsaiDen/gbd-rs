// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::error::Error;
use std::thread::{self, JoinHandle};
use std::sync::mpsc::{self, Receiver};
use std::io::{stdin, stdout, Read, Write};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use serde::{Serialize, Deserialize};


// Structs & Enums ------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Serialize, Deserialize)]
struct DaqSequence {
    seq: usize,
    command: String
}

pub struct DapRequest {
    seq: usize,
    pub command: DapCommand,
    command_name: String,
    response: Option<DapResponseMessage>,
    events: Vec<DapEvent>
}

impl DapRequest {
    pub fn respond(&mut self, result: Result<DapResponse, String>) {
        self.response = Some(match result {
            Ok(inner) => DapResponseMessage {
                request_seq: self.seq,
                success: true,
                command: self.command_name.clone(),
                message: None,
                inner: Some(inner)
            },
            Err(s) => DapResponseMessage {
                request_seq: self.seq,
                success: false,
                command: self.command_name.clone(),
                message: Some(s),
                inner: None
            }
        });
    }

    pub fn event(&mut self, event: DapEvent) {
        self.events.push(event);
    }
}

#[derive(Debug, Clone, Deserialize)]
#[serde(tag = "command", content = "arguments", rename_all = "camelCase")]
pub enum DapCommand {
    Initialize {
        #[serde(rename="clientId")]
        client_id: String,
        clientname: String,
        #[serde(rename="adapterID")]
        adapter_id: String,
        #[serde(rename="pathFormat")]
        path_format: String,
        #[serde(rename="columnsStartAt1")]
        columns_start_at_1: bool,
        #[serde(rename="linesStartAt1")]
        lines_start_at_1: bool,
        #[serde(rename="supportsRunInTerminalRequest")]
        supports_run_in_terminal_request: bool,
        #[serde(rename="supportsVariableType")]
        supports_variable_type: bool
    },
    Completions {
        text: String,
        column: usize
    },
    Evaluate {
        expression: String,
        context: String
    },
    Scopes {
        #[serde(rename="frameId")]
        frame_id: usize
    },
    StackTrace {
        #[serde(rename="threadId")]
        thread_id: u8,
    },
    SetVariable {
        #[serde(rename="variablesReference")]
        variables_reference: usize,
        name: String,
        value: String
    },
    SetBreakpoints {
        source: DapSource,
        breakpoints: Vec<DapSourceBreakpoint>,
        #[serde(rename="sourceModified")]
        source_modified: bool,
        lines: Vec<usize>
    },
    Restart,
    Continue {
        #[serde(rename="threadId")]
        thread_id: u8,
    },
    Pause {
        #[serde(rename="threadId")]
        thread_id: u8,
    },
    Next {
        #[serde(rename="threadId")]
        thread_id: u8,
    },
    StepIn {
        #[serde(rename="threadId")]
        thread_id: u8,
    },
    StepOut {
        #[serde(rename="threadId")]
        thread_id: u8,
    },
    Launch {
        #[serde(rename="type")]
        typ: String,
        request: String,
        program: String,
        name: String
    },
    Variables {
        #[serde(rename="variablesReference")]
        variables_reference: usize
    },
    Threads,
    Disconnect {
        restart: bool,
        #[serde(rename="terminateDebuggee")]
        terminate_debuggee: bool
    }
}

#[derive(Debug, Serialize)]
#[serde(tag = "command", content = "body", rename_all = "camelCase")]
pub enum DapResponse {
    Initialize {
        #[serde(rename="supportsRestartRequest")]
        supports_restart_request: bool,
        //#[serde(rename="supportsWriteMemoryRequest")]
        //supports_write_memory_request: bool,
        //#[serde(rename="supportsReadMemoryRequest")]
        //supports_read_memory_request: bool,
        //#[serde(rename="supportsDisassembleRequest")]
        //supports_disassemble_request: bool,
        #[serde(rename="supportsConditionalBreakpoints")]
        supports_conditional_breakpoints: bool,
        #[serde(rename="supportsHitConditionalBreakpoints")]
        supports_hit_conditional_breakpoints: bool,
        #[serde(rename="supportsSetVariable")]
        supports_set_variable: bool,
        #[serde(rename="supportsCompletionsRequest")]
        supports_completions_request: bool
    },
    Completions {
        targets: Vec<DapCompletionItem>
    },
    Evaluate {
        result: String,
        #[serde(rename="variablesReference")]
        variables_reference: usize
    },
    Restart {},
    StepIn {},
    StepOut {},
    Next {},
    Continue {},
    Pause {},
    Launch {},
    Scopes {
        scopes: Vec<DapScope>
    },
    StackTrace {
        #[serde(rename="stackFrames")]
        stack_frames: Vec<DapStackFrame>
    },
    SetVariable {
        value: String
    },
    SetBreakpoints {
        breakpoints: Vec<DapBreakpoint>
    },
    Variables {
        variables: Vec<DapVariable>
    },
    Threads {
        threads: Vec<DapThread>
    },
    Disconnect {}
}

#[derive(Serialize)]
#[serde(tag = "event", content = "body", rename_all = "camelCase")]
pub enum DapEvent {
    Initialized {
    },
    Output {
        category: String,
        output: String,
        source: Option<DapSource>,
        line: Option<usize>,
        column: Option<usize>
    },
    Thread {
        reason: String,
        #[serde(rename="threadId")]
        thread_id: u8,
    },
    Stopped {
        reason: String,
        description: String,
        #[serde(rename="threadId")]
        thread_id: u8,
        #[serde(rename="hitBreakpointsIds")]
        hit_breakpoint_ids: Vec<usize>
    },
    Continued {
        #[serde(rename="threadId")]
        thread_id: u8
    },
    Exited {
        #[serde(rename="exitCode")]
        exit_code: u8
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct DapCompletionItem {
    pub label: String
}

#[derive(Debug, Clone, Serialize)]
pub struct DapVariable {
    pub name: String,
    pub value: String,
    #[serde(rename="variablesReference")]
    pub variables_reference: usize
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DapSource {
    pub name: String,
    pub path: String
}

#[derive(Debug, Clone, Serialize)]
pub struct DapScope {
    pub name: String,
    pub expensive: bool,
    #[serde(rename="presentationHint")]
    pub presentation_hint: String,
    #[serde(rename="variablesReference")]
    pub variables_reference: usize
}

#[derive(Debug, Clone, Deserialize)]
pub struct DapSourceBreakpoint {
    pub line: usize,
    #[serde(default)]
    pub condition: Option<String>,
    #[serde(rename="hitCondition", default)]
    pub hit_condition: Option<String>
}

#[derive(Debug, Clone, Serialize)]
pub struct DapBreakpoint {
    pub id: Option<usize>,
    pub verified: bool,
    pub message: Option<String>,
    pub source: DapSource,
    pub line: usize,
    pub column: usize
}

#[derive(Debug, Clone, Serialize)]
pub struct DapThread {
    pub id: u8,
    pub name: String
}

#[derive(Debug, Clone, Serialize)]
pub struct DapStackFrame {
    pub id: usize,
    pub name: String,
    pub source: DapSource,
    pub line: usize,
    pub column: usize
}

#[derive(Debug, Serialize)]
pub struct DapResponseMessage {
    request_seq: usize,
    success: bool,
    message: Option<String>,
    command: String,
    #[serde(flatten)]
    inner: Option<DapResponse>
}

#[derive(Serialize)]
pub struct DapEventMessage {
    #[serde(rename="type")]
    typ: String,
    #[serde(flatten)]
    inner: DapEvent
}


// STDIN Protocol -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct DapProtocol {
    #[allow(unused)]
    reader: JoinHandle<()>,
    requests: Receiver<DapRequest>,
    connected: bool,
    events: Vec<DapEvent>
}

impl DapProtocol {
    pub fn stdio() -> Self {
        let (request_tx, requests) = mpsc::channel::<DapRequest>();
        let reader = thread::spawn(move || {
            let mut buf: [u8; 512] = [0; 512];
            let mut p = DapParser::new();
            while let Ok(n) = stdin().read(&mut buf) {
                if n == 0 {
                    break;
                }
                for req in p.append(&buf[..n]).expect("Failed") {
                    request_tx.send(req).ok();
                }
            }
        });
        Self {
            reader,
            requests,
            connected: false,
            events: Vec::new()
        }
    }

    pub fn is_connected(&self) -> bool {
        self.connected
    }

    pub fn handle<C: FnMut(&mut DapRequest)>(&mut self, mut callback: C) {
        for inner in self.events.drain(0..) {
            send_response(&DapEventMessage {
                typ: "event".to_string(),
                inner

            }).expect("Failed to send response");
        }
        while let Ok(mut r) = self.requests.try_recv() {
            callback(&mut r);
            if let DapCommand::Launch { .. } = r.command {
                self.connected = true;
            }
            if let Some(res) = r.response.take() {
                send_response(&res).expect("Failed to send response");
            }
            for inner in r.events {
                send_response(&DapEventMessage {
                    typ: "event".to_string(),
                    inner

                }).expect("Failed to send response");
            }
        }
    }

    pub fn event(&mut self, event: DapEvent) {
        self.events.push(event);
    }
}

fn send_response<S: Serialize>(msg: &S) -> Result<(), Box<dyn Error>> {
    let o = stdout();
    let mut o = o.lock();
    let json = serde_json::to_value(msg)?.to_string();
    write!(o, "Content-Length: {}\r\n\r\n{}", json.as_bytes().len(), json)?;
    o.flush()?;
    Ok(())
}


// Parser ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct DapParser {
    content_length: Option<usize>,
    buffer: Vec<u8>,
    index: usize
}

impl DapParser {
    const CRLF: &'static [u8; 4] = &[b'\r', b'\n', b'\r', b'\n'];

    fn new() -> Self {
        Self {
            content_length: None,
            buffer: Vec::new(),
            index: 0
        }
    }

    fn append(&mut self, bytes: &[u8]) -> Result<Vec<DapRequest>, Box<dyn Error>> {
        let mut messages = Vec::new();
        let mut d = bytes.iter();
        'outer:  loop {
            match self.content_length {
                Some(l) => {
                    for b in &mut d {
                        self.buffer.push(*b);
                        if self.buffer.len() == l {
                            let body = String::from_utf8(std::mem::take(&mut self.buffer))?;
                            let seq: DaqSequence = serde_json::from_str(&body)?;
                            match serde_json::from_str::<DapCommand>(&body) {
                                Ok(command) => {
                                    messages.push(DapRequest {
                                        seq: seq.seq,
                                        command_name: seq.command,
                                        command,
                                        response: None,
                                        events: Vec::new()
                                    });
                                },
                                Err(_) => {
                                    eprintln!("Failed to parse Command: {:?}", body);
                                }
                            }
                            self.content_length = None;
                            continue 'outer;
                        }
                    }
                    break;
                },
                None => {
                    for b in &mut d {
                        if *b == DapParser::CRLF[self.index] {
                            self.index += 1;
                            if self.index == 4 {
                                self.index = 0;
                                let header = String::from_utf8(std::mem::take(&mut self.buffer))?;
                                for line in header.split("\r\n") {
                                    let mut pair = line.split(':');
                                    if let (Some(key), Some(value)) = (pair.next(), pair.next()) {
                                        if key.to_lowercase() == "content-length" {
                                            self.content_length = Some(value.trim().parse()?);
                                        }
                                    }
                                }
                                if self.content_length.is_none() {
                                    return Err("Content-Length header is missing".into());
                                }
                                continue 'outer;
                            }

                        } else {
                            self.buffer.push(*b);
                        }
                    }
                    break;
                }
            }
        }
        Ok(messages)
    }
}

