// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::fmt;
use std::ops::Range;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use sameboy_rs::{BankedAddress, ReadableCore, RunnableCore, BankedAddressRange};


// Tile Abstractions ----------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct SpriteID(usize);
impl SpriteID {
    pub fn from_address(addr: BankedAddress) -> Self {
        let id = addr.address().saturating_sub(0xFE00) / 4;
        Self(id as usize)
    }

    pub fn index(&self) -> usize {
        self.0
    }
}

impl fmt::Display for SpriteID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Sprite #{}", self.0)
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct TileID(usize, u16);
impl TileID {
    pub fn x(&self) -> usize {
        self.0 % 16
    }

    pub fn y(&self) -> usize {
        self.0 / 16
    }

    pub fn index(&self) -> usize {
        self.0
    }

    pub fn bank(&self) -> u16 {
        self.1
    }

    pub fn from_address(addr: BankedAddress) -> Self {
        let tile_id = addr.address().saturating_sub(0x8000) as usize / Tile::BYTES_PER_TILE;
        Self(tile_id + addr.bank() as usize * Tile::COUNT, addr.bank())
    }

    pub fn from_xy(x: usize, y: usize, bank: u16) -> Self {
        Self(x + y * Tile::TILES_PER_ROW + bank as usize * Tile::COUNT, bank)
    }

    pub fn from_bg_cell(tile_id: u8, lcdc: u8, attr: u8) -> Self {
        let mut tile_id = tile_id as usize;
        if tile_id < 128 && (lcdc & 16) == 0 {
           tile_id += 256;
        }
        if (attr & 0b0000_1000) != 0 {
            Self(tile_id + Tile::COUNT, 0)

        } else {
            Self(tile_id, 0)
        }
    }

    pub fn from_sprite(tile_id: u8, attr: u8, is_color: bool) -> Self {
        if is_color && (attr & 0b0000_1000) != 0 {
            Self(tile_id as usize + Tile::COUNT, 1)

        } else {
            Self(tile_id as usize, 0)
        }
    }

    pub fn address(&self) -> BankedAddress {
        BankedAddress::new(0x8000 + ((self.0 % Tile::COUNT) * Tile::BYTES_PER_TILE) as u16, self.1)
    }

    pub fn range(&self) -> BankedAddressRange {
        self.address().with_size(Tile::BYTES_PER_TILE as u16)
    }

}

impl fmt::Display for TileID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "VRAM Tile #{:0>2X} ({})", self.0, self.0)
    }
}

pub struct Sprite {
    index: usize,
    x: u8,
    y: u8,
    attr: u8,
    tile_id: TileID,
    usage: String
}

impl Sprite {
    pub fn map_from_oam_index(oam: &[u8], index: usize, is_color: bool) -> Self {
        let data = &oam[index * 4..index * 4 + 4];
        let x = data[1];
        let y = data[0];
        let tile_id = data[2];
        let attr = data[3];
        let pal = if is_color { attr & 0b111 } else { (attr >> 4) & 0x1 };
        Self {
            index,
            x,
            y,
            attr,
            tile_id: TileID::from_sprite(tile_id, attr, is_color),
            usage: TileUsage::Sprite(pal).to_palette(is_color)
        }
    }

    pub fn id(&self) -> SpriteID {
        SpriteID(self.index)
    }

    pub fn index(&self) -> usize {
        self.index
    }

    pub fn x(&self) -> u8 {
        self.x
    }

    pub fn y(&self) -> u8 {
        self.y
    }

    pub fn tile_id(&self) -> TileID {
        self.tile_id
    }

    pub fn active(&self) -> bool {
        self.y > 0
    }

    pub fn bank(&self) -> u16 {
        self.tile_id.bank()
    }

    pub fn map_bank(attr: u8, is_color: bool) -> usize {
        if is_color && (attr & 0b0000_1000) != 0 {
            1

        } else {
            0
        }
    }
}

impl fmt::Display for Sprite {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Sprite #{} ({})\n  X: ${:0>2X} ({: >3})\n  Y: ${:0>2X} ({: >3})\n  Palette: {}\n  Priority: {}\n  X Flip: {}\n  Y Flip: {}",
            self.index,
            self.tile_id,
            self.x,
            self.x,
            self.y,
            self.y,
            self.usage,
            if self.attr & 0b0100_0000 != 0 { "Yes" } else { "No" },
            if self.attr & 0b0010_0000 != 0 { "Yes" } else { "No" },
            if self.attr & 0b0001_0000 != 0 { "Yes" } else { "No" },
        )
    }
}

pub struct MapID(usize, usize);

impl MapID {
    pub fn from_xy(x: usize, y: usize, lcdc: u8) -> Self {
        let index_offset = if (lcdc & 8) != 0 { 0x1C00 } else { 0x1800};
        let attr_offset = if (lcdc & 8) != 0 { 0x3C00 } else { 0x3800 };
        let offset = x + y * 32;
        Self(index_offset + offset, attr_offset + offset)
    }

    pub fn address(&self) -> BankedAddress {
        BankedAddress::new(0x8000 + self.0 as u16, 0)
    }

    pub fn tile_index(&self) -> usize {
        self.0
    }

    pub fn attr_index(&self) -> usize {
        self.1

    }
}

pub struct Tile;
impl Tile {
    pub const COUNT: usize = 384;
    pub const BYTES_PER_TILE: usize = 16;
    pub const TILES_PER_ROW: usize = 16;

    pub fn bank_vram_range(bank: usize) -> (usize, Range<usize>) {
        let tile_bytes = Self::COUNT * Self::BYTES_PER_TILE;
        if bank == 0 {
            (0, (0..tile_bytes))

        } else {
            (Self::COUNT, (0x2000..0x2000 + tile_bytes))
        }
    }

    pub fn vram_range(tile_id: usize) -> Range<usize> {
        let byte_offset = (tile_id % Self::COUNT) * Self::BYTES_PER_TILE;
        if tile_id < Self::COUNT {
            byte_offset..byte_offset + Self::BYTES_PER_TILE

        } else {
            0x2000 + byte_offset..0x2000 + byte_offset + Self::BYTES_PER_TILE
        }
    }

    #[allow(clippy::too_many_arguments)]
    pub fn draw(
        usage: TileUsage,
        palettes: &PaletteColors,
        is_color: bool,
        tile_data: &[u8],
        buffer_width: u32,
        buffer_data: &mut [u8],
        (fx, fy): (bool, bool),
        (gx, gy): (usize, usize)
    ) {
        let buffer_width = buffer_width as usize;
        for (line_index, line) in tile_data.chunks(2).enumerate() {
            let line_index = if fy { 7 - line_index } else { line_index };
            for x in 0..8 {
                // Combine palette index from two bytes per line
                let dx = if fx { 7 - x } else { x };
                let p1 = (line[0] >> (7 - dx)) & 0x1;
                let p2 = (line[1] >> (7 - dx)) & 0x1;
                let color_index = (p1 | (p2 << 1)) as usize;
                let color = usage.to_color(palettes, is_color, color_index);

                // Draw doubled pixels
                let tx = (gx * 8 + x) * 2 + gx;
                let ty = (gy * 8 + line_index) * 2 + gy;
                let ti = (tx + ty * buffer_width) * 4;
                let _ = &buffer_data[ti..ti + 4].copy_from_slice(&color);
                let _ = &buffer_data[ti + 4..ti + 8].copy_from_slice(&color);
                let ti = ti + buffer_width * 4;
                let _ = &buffer_data[ti..ti + 4].copy_from_slice(&color);
                let _ = &buffer_data[ti + 4..ti + 8].copy_from_slice(&color);
            }
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum TileUsage {
    Unused,
    Background(u8),
    Sprite(u8)
}

impl TileUsage {
    pub fn to_palette(self, is_color: bool) -> String {
        match (is_color, self) {
            (true, Self::Background(pal)) => {
                format!("BG ({})", pal)
            },
            (true, Self::Sprite(pal)) => {
                format!("OBP ({})", pal)
            },
            (false, Self::Background(_)) => {
                "BG".to_string()
            },
            (false, Self::Sprite(0)) => {
                "OBP0".to_string()
            },
            (false, Self::Sprite(1)) => {
                "OBP1".to_string()
            },
            (false, Self::Sprite(n)) => {
                format!("OBPX {}", n)
            },
            (_, _) => {
                "Unused".to_string()
            }
        }
    }

    fn to_color(self, palettes: &PaletteColors, is_color: bool, color_index: usize) -> [u8; 4] {
        match (is_color, self) {
            (true, Self::Background(pal)) => {
                palettes.1.0[pal as usize][color_index]
            },
            (true, Self::Sprite(pal)) => {
                palettes.1.1[pal as usize][color_index]
            },
            (false, Self::Background(_)) => {
                // DMG BG palette
                palettes.0[0][color_index]
            },
            (false, Self::Sprite(0)) => {
                palettes.0[1][color_index]
            },
            (false, Self::Sprite(1)) => {
                palettes.0[2][color_index]
            },
            (_, _) => {
                [
                    [160, 160, 160, 255],
                    [128, 128, 128, 255],
                    [ 96,  96,  96, 255],
                    [ 64,  64,  64, 255]
                ][color_index]
            }
        }
    }
}

impl Default for TileUsage {
    fn default() -> Self {
        Self::Unused
    }
}


// Palette Abstractions -------------------------------------------------------
// ----------------------------------------------------------------------------
pub type PaletteColors = ( [[[u8; 4]; 4]; 3], ([[[u8; 4]; 4]; 8], [[[u8; 4]; 4]; 8]));

pub struct TilePalette;
impl TilePalette {
    pub fn colors(palette_dmg: &[(u32, [f32; 4], [u8; 4]); 5], core: &RunnableCore, use_palette: bool) -> PaletteColors {
        let palette_dmg = if use_palette {
            [
                dmg_color_lookup(palette_dmg, map_dmg_palette_indicies(core.read_io_register(0x47), false)),
                dmg_color_lookup(palette_dmg, map_dmg_palette_indicies(core.read_io_register(0x48), true)),
                dmg_color_lookup(palette_dmg, map_dmg_palette_indicies(core.read_io_register(0x40), true))
            ]
        } else {
            [
                dmg_color_lookup(palette_dmg, [0, 1, 2, 3]),
                dmg_color_lookup(palette_dmg, [0, 1, 2, 3]),
                dmg_color_lookup(palette_dmg, [0, 1, 2, 3])
            ]
        };

        let bg_palette = core.cgb_bg_palettes();
        let obj_palette = core.cgb_obj_palettes();
        let palette_cgb = (
            [
                cgb_color_lookup(0, bg_palette, false),
                cgb_color_lookup(1, bg_palette, false),
                cgb_color_lookup(2, bg_palette, false),
                cgb_color_lookup(3, bg_palette, false),
                cgb_color_lookup(4, bg_palette, false),
                cgb_color_lookup(5, bg_palette, false),
                cgb_color_lookup(6, bg_palette, false),
                cgb_color_lookup(7, bg_palette, false),
            ],
            [
                cgb_color_lookup(0, obj_palette, true),
                cgb_color_lookup(1, obj_palette, true),
                cgb_color_lookup(2, obj_palette, true),
                cgb_color_lookup(3, obj_palette, true),
                cgb_color_lookup(4, obj_palette, true),
                cgb_color_lookup(5, obj_palette, true),
                cgb_color_lookup(6, obj_palette, true),
                cgb_color_lookup(7, obj_palette, true),
            ]
        );
        (palette_dmg, palette_cgb)
    }

}

fn map_dmg_palette_indicies(pal: u8, sprite: bool) -> [usize; 4] {
    if sprite {
        [
            4, // Always transparent
            ((pal >> 2) & 0b11) as usize,
            ((pal >> 4) & 0b11) as usize,
            ((pal >> 6) & 0b11) as usize,
        ]

    } else {
        [
            (pal & 0b11) as usize,
            ((pal >> 2) & 0b11) as usize,
            ((pal >> 4) & 0b11) as usize,
            ((pal >> 6) & 0b11) as usize,
        ]
    }
}

fn dmg_color_lookup(palette_dmg: &[(u32, [f32; 4], [u8; 4]); 5], map: [usize; 4]) -> [[u8; 4]; 4] {
    [
        palette_dmg[map[0]].2,
        palette_dmg[map[1]].2,
        palette_dmg[map[2]].2,
        palette_dmg[map[3]].2,
    ]
}

fn cgb_color_lookup(index: usize, data: &[u8], sprite: bool) -> [[u8; 4]; 4] {
    let o = index * 8;
    if sprite {
        [
            [0, 0, 0, 0],//gcb_to_rgb(((data[o + 1] as u16) << 8) | data[o] as u16),
            gcb_to_rgb(((data[o + 3] as u16) << 8) | data[o + 2] as u16),
            gcb_to_rgb(((data[o + 5] as u16) << 8) | data[o + 4] as u16),
            gcb_to_rgb(((data[o + 7] as u16) << 8) | data[o + 6] as u16)
        ]

    } else {
        [
            gcb_to_rgb(((data[o + 1] as u16) << 8) | data[o] as u16),
            gcb_to_rgb(((data[o + 3] as u16) << 8) | data[o + 2] as u16),
            gcb_to_rgb(((data[o + 5] as u16) << 8) | data[o + 4] as u16),
            gcb_to_rgb(((data[o + 7] as u16) << 8) | data[o + 6] as u16)
        ]
    }
}

fn gcb_to_rgb(c: u16) -> [u8; 4] {
    let b = (((c >> 10) & 0x1F) as f32 * 8.25).min(255.0) as u8;
    let g = (((c >> 5) & 0x1F) as f32 * 8.25).min(255.0) as u8;
    let r = ((c & 0x1F) as f32 * 8.25).min(255.0) as u8;
    [b, g, r, 255]
}

