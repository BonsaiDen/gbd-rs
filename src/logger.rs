// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use yansi::Paint;
use log::{Record, Level, Metadata, SetLoggerError, LevelFilter};


// Statics --------------------------------------------------------------------
// ----------------------------------------------------------------------------
static LOGGER: Logger = Logger;


// Logger Implementation ------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Logger;
impl Logger {
    pub fn init() -> Result<(), SetLoggerError> {
        log::set_logger(&LOGGER)
            .map(|_| log::set_max_level(LevelFilter::Info))
    }
}

impl log::Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Info
    }

    fn log(&self, record: &Record) {
        let accept = record.module_path().map(|p| {
            p.starts_with("mwx") || p.starts_with("analyzer") || p.starts_with("debugger_") || p.starts_with("emulator")

        }).unwrap_or(false);
        if self.enabled(record.metadata()) && accept {
            let prefix = if let Some(p) = record.module_path() {
                format!("[{}] [{}] [{}]", chrono::Local::now().format("%F %T"), p, record.level())

            } else {
                format!("[{}] [???] [{}]", chrono::Local::now(), record.level())
            };
            let color = match record.level() {
                Level::Error => Paint::red,
                Level::Warn => Paint::yellow,
                Level::Info => Paint::blue,
                Level::Debug => Paint::magenta,
                Level::Trace => Paint::green
            };
            println!("{} {}", color(prefix).bold(), record.args());
        }
    }

    fn flush(&self) {}
}

