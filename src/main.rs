// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::path::PathBuf;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use argh::FromArgs;
use emulator::{Emulator, Model};


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod logger;


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(FromArgs)]
#[argh(description="A GameBoy Debugger")]
struct Options {
    #[argh(switch, short='d', description="start with debugger open")]
    debug: bool,

    #[argh(switch, description="start and wait for a DAP connection (implies -d)")]
    dap: bool,

    #[argh(switch, short='h', description="start with emulation halted (always after --skip-bootrom)")]
    halted: bool,

    #[argh(switch, short='s', description="skip boot rom emulation")]
    skip_bootrom: bool,

    #[argh(option, short='m', description="a gameboy model to emulate (DMG, MGB, CGB, AGB, SGB, SGB2)")]
    model: Option<Model>,

    #[argh(option, description="base directory for source map path resolution")]
    source_dir: Option<PathBuf>,

    #[argh(switch, short='f', description="forward the ROM imageto an existing gbd process (if present)")]
    forward: bool,

    #[argh(positional, description="ROM image to emulate")]
    rom_file: Option<PathBuf>
}


// Standlone Application ------------------------------------------------------
// ----------------------------------------------------------------------------
fn main() {
    logger::Logger::init().ok();

    let options: Options = argh::from_env();
    let mut emulator = Emulator::new(
        options.model,
        options.rom_file,
        options.source_dir.unwrap_or_else(|| {
            std::env::current_dir().expect("missing current workding directory")
        })
    );
    emulator.set_skip_boot_rom(options.skip_bootrom);
    emulator.set_start_halted(options.halted);
    emulator.set_start_with_debugger_opened(options.debug);
    emulator.set_launch_stdio_dap(options.dap);
    emulator.launch(options.forward);
}

